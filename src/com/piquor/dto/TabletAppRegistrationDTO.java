package com.piquor.dto;

public class TabletAppRegistrationDTO {

	
	private String device_id;
	private String device_brand_name;
	private String device_manufeturer;
	private String device_board;
	private String device_display;
	private String device_modal;
	private String device_productname;
	private String device_serialno;
	private String is_campaign_asigned;
	private String default_campaign;
	private String campaignid;
	private String registration_entry_uploaded;
	private String registration_entry_upload_status;
	private String device_version;
	
	
	public TabletAppRegistrationDTO() {
		super();
		
		this.device_id = "NA";
		this.device_brand_name = "NA";
		this.device_manufeturer = "NA";
		this.device_board = "NA";
		this.device_display = "NA";
		this.device_modal = "NA";
		this.device_productname = "NA";
		this.device_serialno = "NA";
		this.is_campaign_asigned = "false";
		this.default_campaign = "NA";
		this.campaignid = "NA";
		this.registration_entry_upload_status = "0";
		this.registration_entry_uploaded = "0";
		this.device_version = "NA";
	
	}


	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getDevice_brand_name() {
		return device_brand_name;
	}
	public void setDevice_brand_name(String device_brand_name) {
		this.device_brand_name = device_brand_name;
	}
	public String getDevice_manufeturer() {
		return device_manufeturer;
	}
	public void setDevice_manufeturer(String device_manufeturer) {
		this.device_manufeturer = device_manufeturer;
	}
	public String getDevice_board() {
		return device_board;
	}
	public void setDevice_board(String device_board) {
		this.device_board = device_board;
	}
	public String getDevice_display() {
		return device_display;
	}
	public void setDevice_display(String device_display) {
		this.device_display = device_display;
	}
	public String getDevice_modal() {
		return device_modal;
	}
	public void setDevice_modal(String device_modal) {
		this.device_modal = device_modal;
	}
	public String getDevice_productname() {
		return device_productname;
	}
	public void setDevice_productname(String device_productname) {
		this.device_productname = device_productname;
	}
	public String getDevice_serialno() {
		return device_serialno;
	}
	public void setDevice_serialno(String device_serialno) {
		this.device_serialno = device_serialno;
	}
	public String getIs_campaign_asigned() {
		return is_campaign_asigned;
	}
	public void setIs_campaign_asigned(String is_campaign_asigned) {
		this.is_campaign_asigned = is_campaign_asigned;
	}
	public String getDefault_campaign() {
		return default_campaign;
	}
	public void setDefault_campaign(String default_campaign) {
		this.default_campaign = default_campaign;
	}
	public String getCampaignid() {
		return campaignid;
	}
	public void setCampaignid(String campaignid) {
		this.campaignid = campaignid;
	}

	public String getRegistration_entry_uploaded() {
		return registration_entry_uploaded;
	}


	public void setRegistration_entry_uploaded(String registration_entry_uploaded) {
		this.registration_entry_uploaded = registration_entry_uploaded;
	}


	public String getRegistration_entry_upload_status() {
		return registration_entry_upload_status;
	}


	public void setRegistration_entry_upload_status(
			String registration_entry_upload_status) {
		this.registration_entry_upload_status = registration_entry_upload_status;
	}


	public String getDevice_version() {
		return device_version;
	}


	public void setDevice_version(String device_version) {
		this.device_version = device_version;
	}


	@Override
	public String toString() {
		return "TabletAppRegistrationDTO [device_id=" + device_id
				+ ", device_brand_name=" + device_brand_name
				+ ", device_manufeturer=" + device_manufeturer
				+ ", device_board=" + device_board + ", device_display="
				+ device_display + ", device_modal=" + device_modal
				+ ", device_productname=" + device_productname
				+ ", device_serialno=" + device_serialno
				+ ", is_campaign_asigned=" + is_campaign_asigned
				+ ", default_campaign=" + default_campaign + ", campaignid="
				+ campaignid + ", registration_entry_uploaded="
				+ registration_entry_uploaded
				+ ", registration_entry_upload_status="
				+ registration_entry_upload_status + ", device_version="
				+ device_version + "]";
	}
	
	
	
	
}
