package com.piquor.dao.table.querybuilder;

public class MailerDAO {

	public static String getUnsendedMailListQuery(String flag)
	{
		
		String queryPrefix="select *  FROM user WHERE ";
		
		String queryCondition="";
		
		if(flag.equals("mailremaintosent"))
			queryCondition="mailsent=0 and fileUpload=1 and Status='0'"; //Mails Remaining to Sent
		
		else if(flag.equals("emailerror"))
			queryCondition= "status='2'"; //EmailError
		
		else if(flag.equals("pictureerror"))
			queryCondition="status='1'"; //photographs upload Error
		
		else if(flag.equals("remainingEntries"))
			queryCondition="mailsent=0 and fileupload=0 and status='0'";// Remaining Entries
		
		String Query=queryPrefix+queryCondition;
		
		return Query;
	}
}
