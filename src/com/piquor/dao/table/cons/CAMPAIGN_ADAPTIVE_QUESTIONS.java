package com.piquor.dao.table.cons;

public interface CAMPAIGN_ADAPTIVE_QUESTIONS {

	public final String TABLE_NAME_CAMPAIGN_ADAPTIVE_QUESTIONS = "campaign_adaptive_question";
	public final String COLUMN_NAME_QUESTION_ID = "question_id";
	public final String COLUMN_NAME_QUESTION = "question";
	public final String COLUMN_NAME_CAMPAIGN_ID = "campaign_id";
	public final String COLUMN_NAME_ID = "id";
	
	public final String STATE_ADD = "add";
	public final String STATE_MODIFY = "modify";
	public final String STATE_DELETE = "delete";
}
