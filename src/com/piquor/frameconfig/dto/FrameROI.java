package com.piquor.frameconfig.dto;

import javax.crypto.spec.PSource;

public class FrameROI {
	
	public FrameROI(int width,int height,int posX,int posY)
	{
		this.Width = width;
		this.Height = height;
		this.posX = posX;
		this.PosY = posY;
	}
	
	public int getWidth() {
		return Width;
	}
	public void setWidth(int width) {
		Width = width;
	}
	public int getHeight() {
		return Height;
	}
	public void setHeight(int height) {
		Height = height;
	}
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return PosY;
	}
	public void setPosY(int posY) {
		PosY = posY;
	}
	
	private int posX;
	private int PosY;
	private int Width;
	private int Height;
	
	public String toString()
	{
		String toString = "Frame ROI: width:"+Width+" Height:"+Height+" PosX:"+posX+" PosY:"+PosY;
		return toString;
	}
}
