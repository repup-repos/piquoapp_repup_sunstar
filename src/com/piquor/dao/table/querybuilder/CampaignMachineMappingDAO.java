package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.CAMPAIGN_MACHINE_MAPPING;

public class CampaignMachineMappingDAO implements CAMPAIGN_MACHINE_MAPPING {

	public static String getMachineMappingTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_CAMPAIGN_MACHINE_MAPPING;
		return query;
	}
	public static String createCampaignMahchineMappingTable()
	{
		String CREATE_CAMPAIGN_MACHINE_MAPPING_TABLE = "CREATE TABLE ["+TABLE_NAME_CAMPAIGN_MACHINE_MAPPING+"] "
				+ "("
				+ "["+COLUMN_NAME_MACHINE_MAPPING_RECORD_ID +"] varchar(100)  NOT NULL PRIMARY KEY ,"
				+ "["+COLUMN_NAME_MACHINE_MAPPING_CAMPAIGNID+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_MAPPING_CAMPAIGNNAME+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_MAPPING_MACHINEID+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_MAPPING_STARTINGDATE+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_MAPPING_ENDDATE+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_MAPPING_TIMESTAMP+"] varchar(100) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_CAMPAIGN_MACHINE_MAPPING_TABLE;
	}
	
	
}
