package com.services.deliverymode.utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import android.util.Log;

import com.piquor.constants.APP_CREDENTIALS;
import com.piquor.dto.UserTableDTO;
import com.piquor.errorhandler.ErrorUpdater;
import com.piquor.services.deliverymode.execute.HttpUrlExecute;
import com.piquor.services.deliverymode.execute.HttpUrlResponse;
import com.piquor.utility.PiquorTimestamp;
import com.services.deliverymode.userdto.UserParamsDTO;

public abstract class UrlBuilderUtility {

	public UserTableDTO userDTO;
	public UserParamsDTO userParamsDTO;
	private String buildedQueryUrl;
	private String LOG_TAG = "UrlBuilderUtility";
	private ErrorUpdater errorUpdater;
	
	public UrlBuilderUtility(UserTableDTO userDTO)
	{
		this.userDTO=userDTO;
		userParamsDTO=new UserParamsDTO();
		errorUpdater=new ErrorUpdater();
		buildUserParameters();
	
	}

	private void buildUserParameters() {
		
		String userEmail=userDTO.getEmail().trim();
		
		String photoPicId = userDTO.getPicId().trim();
		
		String userDOB = userDTO.getDateofbirth().trim();
				
		String dateOfCapture=userDTO.getDate();
		
		String userName=userDTO.getName().trim();
		
		userName = userName.replaceAll("\\s", "+");
		
		String fbUserName = userDTO.getFbUserName().trim();
		
		fbUserName = fbUserName.replaceAll("\\s", "+");
		
		String fbName = userDTO.getFbName().trim();
		
		fbName = fbName.replaceAll("\\s", "+");
		
		String fbUserId = userDTO.getFbUserId().trim();
		
		fbUserId = fbUserId.replaceAll("\\s", "+");
		
		String twitterUserId = userDTO.getTwitterId().trim();
		
		twitterUserId = twitterUserId.replaceAll("\\s", "+");
		
		String twitterUserName = userDTO.getTwitterName().trim();
		
		twitterUserName = twitterUserName.replaceAll("\\s", "+");
		
		String twitterUserFollowers = userDTO.getTwitterFollowers().trim();
		
		String twitterUserFriendsCount = userDTO.getTwitterFriendsCount().trim();
		
		String userDelieveryMode="";
		try {
			userDelieveryMode = URLEncoder.encode(userDTO.getDelieveryMode().trim(),"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {

		}
		
		String userCampaignId=userDTO.getCampaignId().trim();
		
		String userPhotoPermissionSocialMedia=userDTO.getPhotoPermissionSocialMedia().trim();
		
		String userGender=userDTO.getGender().trim();
		
		String userPhoneNo = "";
		
		
		try {
			userPhoneNo = URLEncoder.encode(""+ userDTO.getPhone().trim(), "ISO-8859-1");
		} catch (UnsupportedEncodingException e2) {

		}
		
		String userTwitterPostedText = APP_CREDENTIALS.TWITTER_HASTAG+" "+APP_CREDENTIALS.TWITTER_DESC;
		
		try {
			userTwitterPostedText = URLEncoder.encode(userTwitterPostedText, "ISO-8859-1");
		} catch (UnsupportedEncodingException e2) {

		}
		
		
		userParamsDTO.setUserCampaignId(userCampaignId);
		userParamsDTO.setUserEmail(userEmail);
		userParamsDTO.setPhotoCaptureDate(dateOfCapture);
		userParamsDTO.setUserDOB(userDOB);
		userParamsDTO.setUserName(userName);
		userParamsDTO.setUserDelieveryMode(userDelieveryMode);
		userParamsDTO.setUserPhone(userPhoneNo);
		userParamsDTO.setUserPhotoPermissionSocialMedia(userPhotoPermissionSocialMedia);
		userParamsDTO.setPhotoPicId(photoPicId);
		userParamsDTO.setUserGender(userGender);
		userParamsDTO.setUserFbName(fbName);
		userParamsDTO.setUserFbUserId(fbUserId);
		userParamsDTO.setUserFbUserName(fbUserName);
		userParamsDTO.setUserTwitterFollowers(twitterUserFollowers);
		userParamsDTO.setUserTwitterFriendsCount(twitterUserFriendsCount);
		userParamsDTO.setUserTwitterName(twitterUserName);
		userParamsDTO.setUserTwitterId(twitterUserId);
		userParamsDTO.setUserTwitterPostText(userTwitterPostedText);
		
	}
	
	
	
	public abstract boolean executeQueryString(String machineId);
	
	public boolean executeQuery(String serverServiceUrl)
	{
		buildedQueryUrl=serverServiceUrl;
		logInfo("ExecutingUrl Url:"+buildedQueryUrl+" For PicId:"+userDTO.getPicId()+"@executeQuery:UrlBuilderUtility "+PiquorTimestamp.getTimeStamp());
		HttpUrlResponse response=HttpUrlExecute.executeQuery(buildedQueryUrl);
		logInfo("UrlResponse Recived Log for PicId:"+userDTO.getPicId()+" Executed URL:"+buildedQueryUrl+":Response Recieved:"+response.getQueryUrlLog()+" Response from API"+response.getQueryUrlResponse()+"@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		
		if(isResponseValid(response.getQueryUrlResponse()))
		{
			return completeUserEntryOperation();
		}
		else
		{
			logInfo("Mail Delieverd Unsucussfully Setting in Url Execution Error Category Setting Flags To Done For PicId:"+userDTO.getPicId()+":@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
			errorUpdater.setUrlExectionError(userParamsDTO.getPhotoPicId());
			return false;
		}
		
		
	}
	
	public boolean completeUserEntryOperation()
	{
		logInfo(" Setting User Flags To Done For User:"+userDTO.getPicId()+":@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		return errorUpdater.setSucessFlagsForUserEntry(userParamsDTO.getPhotoPicId());
	}
	
	
	private boolean isResponseValid(String urlResponse)
	{
		if(urlResponse.equals("true"))
		{
			return true;
		}
		else if(urlResponse.toLowerCase(Locale.ENGLISH).contains("success"))
		{
			return true;
		}else if(urlResponse.toLowerCase(Locale.ENGLISH).contains("failed"))
		{
			logInfo("Failed Error At URL Entry In Universal Controller@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return true;
		}else if(urlResponse.trim().equals(""))
		{
			//ErrorMailerService.SendMail("No Response Found From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("No Response Found From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return true;
		}else if(urlResponse.equals("0"))
		{
			//ErrorMailerService.SendMail("Completely failed process Application And Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("Completely failed process Response Recived :0 From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return true;
		}else if(urlResponse.equals("1"))
		{
			logInfo("Completely processed entry response Recived :1 From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return true;
		}else if(urlResponse.equals("2"))
		{
			logInfo("Photo already uploaded response Recived :2 From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return true;
		}else if(urlResponse.equals("3"))
		{
			//ErrorMailerService.SendMail("Server Side exception @ DAO Layer At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("Server Side exception @ DAO Layer Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return false;
		}else if(urlResponse.equals("4"))
		{
			//ErrorMailerService.SendMail("Server Side exception @ DAO Layer At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("FaceBook permission blocked by user .faceBook upload failed . [Ignore exception ] Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return true;
		}else if(urlResponse.equals("5"))
		{	
			//ErrorMailerService.SendMail("Sever side exception while updating shared status of photograph in image_fb_twitter_table .rest operation success. Response Recieved:5 At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("Sever side exception while updating shared status of photograph in image_fb_twitter_table .rest operation success. Response Recieved:5 Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return false;
		}
		else 
		{
			//ErrorMailerService.SendMail("False Response Recieved  Services will be blocked at this machine till response successfull recieved  Server Reponse:"+urlResponse+" Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("No Response Found From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId());
			return false;
		}
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
