package com.piquor.datautility;

import java.util.ArrayList;

import android.util.Log;

import com.piquor.dao.table.querybuilder.ReviewEntryDAO;
import com.piquor.datastaticfields.ErrorFlags;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.errorhandler.ReviewDataErrorHandler;
import com.piquor.piquortabapp.MainActivity;
import com.services.launcher.ServicesLauncher;

public class ReviewPendingUrlEntryData implements ErrorFlags{
	
	ReviewDataErrorHandler reviewDataErrorHandler;
	private String LOG_TAG = "ReviewPendingUrlEntries";
	boolean pendingData=false;
	
	public ReviewPendingUrlEntryData()
	{
		reviewDataErrorHandler = new ReviewDataErrorHandler();
	}
	
	public boolean isDataPending()
	{
		
		int pictureUploadFailedEntries = MainActivity.getDBEntryCount(ReviewEntryDAO.getDbCountForRemainingMailSentQuery());
		ServicesLauncher.showMessage("Pending url entriea:"+pictureUploadFailedEntries);
		if(pictureUploadFailedEntries>0)
		{
			
			pendingData=true;
			return true;
			
		}else
		{
			pendingData=false;
			return false;
		}
	}
	
	public boolean processPendingUserData()
	{
		if(pendingData)
		{
			
			logInfo("Fetching Pending Url Entry Data from database @proccessPendingUserData:UserPendingUrlEntryData");
			ArrayList<ReviewEntryDTO> urlRemainingEntries = MainActivity
				.getUnsendedReviewMailList(URL_PENDING_DATA_FLAG);
			
			//		System.out.println(list.size());
			logInfo("Pending Url Entry Data Fetched  from database Started Processing totoal entries:"+urlRemainingEntries.size()+" @processPendingUserData:UserPendingUrlEntyData" );
			reviewDataErrorHandler.handleRemainUrlEntry(urlRemainingEntries);
			logInfo("Pending Url Entry Data Processing Completed @processPendingUserData:UserPendingUrlEntryData");
			
			return true;
		}else
		{
			return false;
		}
		
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG,info);
	}
}
