package com.piquor.dto;

public class AppConfigurationDTO {
	
	/*
	 * DTO to hold all application configuration 
	 * Configuration will be loaded and checked daily basis for updates
	 * All configuration will automatically populated and updated
	 */
	
	private String isVideoEnabled;
	private String isPhotoEnabled;
	private String isReviewEnabled;
	
	public AppConfigurationDTO()
	{
		isVideoEnabled  = "true";
		isPhotoEnabled  = "true";
		isReviewEnabled = "true";
	}
	
	public String getIsVideoEnabled() {
		return isVideoEnabled;
	}
	public void setIsVideoEnabled(String isVideoEnabled) {
		this.isVideoEnabled = isVideoEnabled;
	}
	public String getIsPhotoEnabled() {
		return isPhotoEnabled;
	}
	public void setIsPhotoEnabled(String isPhotoEnabled) {
		this.isPhotoEnabled = isPhotoEnabled;
	}
	public String getIsReviewEnabled() {
		return isReviewEnabled;
	}
	public void setIsReviewEnabled(String isReviewEnabled) {
		this.isReviewEnabled = isReviewEnabled;
	}
	
	@Override
	public String toString() {
		return "AppCofingurationDTO [isVideoEnabled=" + isVideoEnabled
				+ ", isPhotoEnabled=" + isPhotoEnabled + ", isReviewEnabled="
				+ isReviewEnabled + "]";
	}
	
}
