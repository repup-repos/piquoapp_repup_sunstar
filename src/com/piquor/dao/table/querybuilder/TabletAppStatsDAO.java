package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.TABLET_APP_STATS_COLUMNS;
import com.piquor.dto.TabletAppStatusDTO;

public class TabletAppStatsDAO implements TABLET_APP_STATS_COLUMNS{

	
	public static String getTableAppStatsDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_TABLET_APP_STATS;
		return query;
	}
	
	public static String  getCreateTabletAppStats()
	{
		String CREATE_TABLE_APP_STATS = "CREATE TABLE ["+TABLE_NAME_TABLET_APP_STATS+"] "
				+ "("
				+ "["+COLUMN_NAME_ID+"] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,"
				+ "["+COLUMN_NAME_DEVICE_ID+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_LAST_APP_OPENED_TIME+"] varchar(150) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_CAMPAIGNID+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_STATS_ENTRY_UPLOADED+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_TIMESTAMP+"] varchar(200) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_TABLE_APP_STATS;
	}
	
	public static String getInsertTabletAppStats(TabletAppStatusDTO configDTO)
	{
		String INSERT_QUERY_TRIPADVISOR = "insert into "
					+TABLE_NAME_TABLET_APP_STATS+""
					+ " ( "+COLUMN_NAME_CAMPAIGNID
					+","+COLUMN_NAME_DEVICE_ID
					+","+COLUMN_NAME_LAST_APP_OPENED_TIME
					+","+ COLUMN_NAME_TIMESTAMP 
					+","+ COLUMN_NAME_STATS_ENTRY_UPLOADED 
					+","+ COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS 
					+ ") "
					+ "values ('"+configDTO.getCampaignid()+
					"','"+configDTO.getDevice_id()
					+"','"+configDTO.getLast_app_opened_time()
					+"','"+configDTO.getTimestamp()
					+"','"+configDTO.getStatsentry_uploaded()
					+"','"+configDTO.getStatsentry_upload_status()+"'"
							+ ")";
		
		return INSERT_QUERY_TRIPADVISOR;
		   
	}
	
	public static String getUpdateEmailErrorQuery(TabletAppStatusDTO statsDTO)
	{
		String query = "update "+TABLE_NAME_TABLET_APP_STATS+" set "+COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"='"+2+"' where "+COLUMN_NAME_ID+"='"+statsDTO.getId()+"'";
		
		return query;
	}
	
	public static String getUpadateStatsSuccessEntryQuery(TabletAppStatusDTO statusDTO)
	{
		
		String query = "update "+TABLE_NAME_TABLET_APP_STATS+" set "+COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"='"+0+"', "+COLUMN_NAME_STATS_ENTRY_UPLOADED+"='1' where "+COLUMN_NAME_ID+"='"+statusDTO.getId()+"'";
		return query;
	}
	
	public static String getDbCountForRemainingMailSentQuery(TabletAppStatusDTO statsDTO)
	{
		String query = "SELECT count(*) FROM "+TABLE_NAME_TABLET_APP_STATS+" WHERE "+COLUMN_NAME_STATS_ENTRY_UPLOADED+"=0  and "+COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"='0'";
		return query;
	}
	
	public static String getDbCountForUrlEntryQuery()
	{
		String dbquery = "SELECT count(*) FROM "+TABLE_NAME_TABLET_APP_STATS+" WHERE "+COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"='2'";
		
		return dbquery;
	}
	

	public static String getUnsendedStatusMailListQuery(String flag) {
		
		
		String queryPrefix="select *  FROM "+TABLE_NAME_TABLET_APP_STATS+" WHERE ";
		
		String queryCondition="";
		
		if(flag.equals("mailremaintosent"))
		{
			queryCondition=COLUMN_NAME_STATS_ENTRY_UPLOADED+"='0'  and "+COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"='0'"; //Mails Remaining to Sent
		}
		else if(flag.equals("emailerror"))
		{
			queryCondition= COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"='2'"; //EmailError
		}
		else if(flag.equals("remainingEntries"))
		{
			queryCondition=COLUMN_NAME_STATS_ENTRY_UPLOADED+"='0'  and "+COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS+"='0'";// Remaining Entries
		}
		String Query=queryPrefix+queryCondition;
		
		return Query;
		
	}
	
	
}
