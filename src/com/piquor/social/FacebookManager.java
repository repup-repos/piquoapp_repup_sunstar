package com.piquor.social;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;

import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.piquor.constants.APP_CONS;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.AlertDialogManager;
import com.piquor.utility.InternetConnectionDetector;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class FacebookManager {
	private String LOG_TAG = "FacebookManager";
	
	private Fragment parentFragment;
	private UiLifecycleHelper uiHelper;

	private String ACCESS_TOKEN = "";
	private ProgressDialog progress;
	
	boolean isImageUploaded = false;
	
	public static FacebookManager instance = null;
	
	/*
	 * Creating a Singleton Access class
	 */
	public static FacebookManager GetInstance() {
		if (instance == null) {
			instance = new FacebookManager();
		}
		return instance;
	}
	
	public void setParentFragment(Fragment fragment) {
		parentFragment = fragment;
	}
	
	public void startNewSession() {
		List<String> permissions = new ArrayList<String>();
		permissions.add("public_profile");
		permissions.add("user_friends");
		permissions.add("email");
		permissions.add("publish_actions");
		//permissions.add("publish_stream");
		
//		String appId = parentFragment.getResources().getString(R.string.app_id);
		
		InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(parentFragment.getActivity());
		if(!internetConnectionDetector.isConnectingToInternet()) {
			new AlertDialogManager().showAlertDialog(parentFragment.getActivity(), "Intenet Connection Failed", "Please Ensure Internet Connectivity", false);
		}
		else {
			Session currentSession = Session.getActiveSession();
			
			// If a session already exists we need to close it and clear the information
			if (currentSession != null) {
				currentSession.closeAndClearTokenInformation();
				Session.setActiveSession(null);
				Log.d(LOG_TAG, "Already exists trying to log out");
				currentSession = null;
			}
			
			if (currentSession == null) {
				Session.setActiveSession(null);
				currentSession = new Session(parentFragment.getActivity());
				Session.setActiveSession(currentSession);
				
				Log.d(LOG_TAG, "Was null created a new session");
			}
			
			if (!currentSession.isOpened()) {
				Session.OpenRequest openRequest = null;    
	    		openRequest = new Session.OpenRequest(parentFragment).setCallback(sessionCallback);

	            if (openRequest != null) {
	                openRequest.setPermissions(permissions);	                
	                currentSession.openForPublish(openRequest);
	            }
	           
			}
			
			if (currentSession.isOpened()) {
				Log.d(LOG_TAG, "Logout this active session");
			}
		}
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	            ACCESS_TOKEN = session.getAccessToken();
	            Log.d(LOG_TAG, "Acess Token:"+ACCESS_TOKEN);
			if (!isImageUploaded) {
				Callback callback = new Callback() {

					@Override
					public void onCompleted(Response response) {

						progress.dismiss();
						progress.cancel();
						
						imageUploadCompleteMessageAndUserLogout();

					}
					
					// What if the image is not uploaded successfully ?
					
				};
				GraphUserCallback userIfno = new GraphUserCallback() {
					
					@Override
					public void onCompleted(GraphUser user, Response response) {
						
						String name       = user.getName();
						String fbUserName = user.getUsername();
						String fbUserId   = user.getId();
						if(name!=null)
						{
							if(!name.trim().equals(""))
							{
								
								MainActivity.setFbName(name);
							}
						} 
						if(fbUserName!=null)
						{
							if(!fbUserName.equals(""))
							{
								MainActivity.setFbUserName(fbUserName);
							}
						}
						if(fbUserId!=null)
						{
							if(!fbUserId.trim().equals(""))
							{
								MainActivity.setFbUserId(fbUserId);
							}
						}
						if(fbUserId!=null)
						{
							MainActivity.setDeliveryMode(DeliveryModeFlags.FACEBOOK_DELIVERYMODE_FLAG);
						}
						Log.d(LOG_TAG,"Graph User Data:Name"+name+":FbId:"+fbUserId+":FbUserName:"+fbUserName);
					}
				};
				progress = new ProgressDialog(parentFragment.getActivity());
				progress.setMessage("Uploading Image ...");
				progress.setCancelable(false);
				progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progress.setIndeterminate(true);
				progress.show();
				
				APP_CONS.set_tempImageFile(BitmapFactory.decodeFile(APP_CONS
						.get_framedImage().getAbsolutePath()));
				Request userInfoRequest = Request.newMeRequest(session,userIfno );
			    userInfoRequest.executeAsync();
				Request photoUploadRequest = Request.newUploadPhotoRequest(
						session, APP_CONS.get_tempImageFile(), callback);
				Bundle params = photoUploadRequest.getParameters();
				//params.putString("message", "#piquor");
				photoUploadRequest.setParameters(params);
				photoUploadRequest.executeAsync();
				
				isImageUploaded = true;
			}
			
	    }
	}
	
	protected void imageUploadCompleteMessageAndUserLogout() {
		MainActivity.setDeliveryMode(DeliveryModeFlags.FACEBOOK_DELIVERYMODE_FLAG);
		callFacebookLogout(parentFragment.getActivity());
		
		new AlertDialogManager().showAlertDialog(parentFragment.getActivity(), "Facebook Status", "Image Uploaded To Facebook.", true);
	}
	
	private Session.StatusCallback sessionCallback = new Session.StatusCallback() {
		@Override
	    public void call(Session session, SessionState state, Exception exception) {
	        onSessionStateChange(session, state, exception);
	    }
	};
	
	public void onCreate(Bundle savedInstance) {
		 uiHelper = new UiLifecycleHelper(parentFragment.getActivity(), null);
		 uiHelper.onCreate(savedInstance); 
	}
	
	public void onResume() {
		uiHelper.onResume();
	}
	
	public void onPause() {
	    uiHelper.onPause();
	}

	public void onDestroy() {
	    uiHelper.onDestroy();
	}

	public void onSaveInstanceState(Bundle outState) {
	    uiHelper.onSaveInstanceState(outState);
	}
	
	public  void callFacebookLogout(Context context) {
	    Session currentSession = Session.getActiveSession();
	    
		if (currentSession != null) {
        	currentSession.closeAndClearTokenInformation();
	    } 
		
	    new AlertDialogManager().showAlertDialog(parentFragment.getActivity(), "Facebook Status", "User Logged out from Facebook", true);
	}
	
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {
		 uiHelper.onActivityResult(requestCode, resultCode, data);
	 }
	 
	 /*
	  * the key hash is used in the Facebook Android app dashboard
	  */
	 public void getKeyHash() {
		 try {
		        PackageInfo info = parentFragment.getActivity().getPackageManager().getPackageInfo(
		                "com.piquor.piquortabapp", 
		                PackageManager.GET_SIGNATURES);
		        for (Signature signature : info.signatures) {
		            MessageDigest md = MessageDigest.getInstance("SHA");
		            md.update(signature.toByteArray());
		            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		            }
		    } catch (NameNotFoundException e) {

		    } catch (NoSuchAlgorithmException e) {
		    	
		    }
	 }
}
