package com.services.deliverymode.urlbuilder;

import android.util.Log;

import com.piquor.dto.UserTableDTO;
import com.piquor.utility.PiquorTimestamp;
import com.services.deliverymode.staticfields.DeliveryModeUrls;
import com.services.deliverymode.utility.UrlBuilderUtility;

public class EmailUrlBuilder extends UrlBuilderUtility implements DeliveryModeUrls{
	
		public String LOG_TAG = "EmailUrlBuilder";
		public EmailUrlBuilder(UserTableDTO userDTO) {
			super(userDTO);
		}

		String emailEntryUrl;
		
		@Override
		public boolean executeQueryString(String machineId) {
			
			if(!userParamsDTO.getUserEmail().equals(""))
			{
				
				emailEntryUrl=EMAIL_URL_PRETFIX+buildEmailQuery(machineId);
				
				
				logInfo("Executing Email Entry URL:"+emailEntryUrl+":For PicId:"+userDTO.getPicId()+"Time:"+PiquorTimestamp.getTimeStamp());
				
				
				return executeQuery(emailEntryUrl);
				
			}
			else
			{
				System.out.println(">> User Email Not Found EmailURLBuilder");
				logInfo(" User Email Id Not Found:@executeQuery:EmailUrlBuilde Time:"+PiquorTimestamp.getTimeStamp());
				return completeUserEntryOperation();
			}
		}

		private String buildEmailQuery(String machineId) {
			
			String queryParameter = "name=" + userParamsDTO.getUserName() + "&emailId=" + userParamsDTO.getUserEmail()
					+ "&dob=" + userParamsDTO.getUserDOB() + "&gender="
					+ userParamsDTO.getUserGender() + "&phone=" + userParamsDTO.getUserPhone()
					+ "&captureDate=" + userParamsDTO.getPhotoCaptureDate() + "&campaignId="
					+ userParamsDTO.getUserCampaignId() + "&picId="
					+ userParamsDTO.getPhotoPicId() + "&machineId="
					+ machineId;
			return queryParameter;
		}
		
		public void logInfo(String info)
		{
			Log.d(LOG_TAG, info);
		}
		

	}

