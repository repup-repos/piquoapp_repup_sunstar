package com.services.launcher;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.InternetConnectionDetector;
import com.services.servicecontroller.ServicesController;

public class ServicesLauncher {
	
	private ServicesController servicesController;
	
	private String LOG_TAG = "ServicesLauncher";
	private static MainActivity parentActivity;
	
	public ServicesLauncher()
	{
		Log.d(LOG_TAG, "Starting Services Launcher");
		int timeOutInSeconds = Integer.parseInt(MainActivity.getCamapignInformation().getMailerResponseTime().trim());
		servicesController = new ServicesController(timeOutInSeconds);
	}
	
	public void setMainActivity(MainActivity mainActivity)
	{
		ServicesLauncher.parentActivity = mainActivity;
	}
	
	public void stopSevices()
	{
		servicesController.stopTimer();
	}
	
	public void startServices()
	{
		Log.d(LOG_TAG, "Starting Services::");
		servicesController.startTimer();
	}
	
	private final static int UPDATE_TOAST = 0;
	private static String message = "";
	
	public static void showMessage(String info)
	{
		/*myHandler.sendEmptyMessage(UPDATE_TOAST);
		message = info;*/
		Log.d("ServicesLauncher", "showMessage");
	}
	
	private final static Handler myHandler = new Handler() {
	    public void handleMessage(Message msg) {
	        final int what = msg.what;
	        switch(what) {
	        case UPDATE_TOAST: showToast(); break;
	       
	        }
	    }
	};

	protected static void showToast() {
		
		Toast.makeText(parentActivity, message,  Toast.LENGTH_SHORT).show();
		
	}
	
	public static boolean checkInternetConnectivity()
	 {
		 InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(parentActivity);
			if(!internetConnectionDetector.isConnectingToInternet())
			{
				return false;
			}
			return true;
	 }
}
