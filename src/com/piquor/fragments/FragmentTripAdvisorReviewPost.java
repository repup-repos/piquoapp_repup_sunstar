package com.piquor.fragments;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.dto.ReviewsConfigDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.modal.ReviewTripPostModal;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;

public class FragmentTripAdvisorReviewPost extends Fragment implements
		FRAGMENT_SCREEN_NAMES, ACTIVITY_FLAGS {

	private String LOG_TAG = "FragmentTripAdvisorFinalScreen";
	private ProgressDialog progress;
	private boolean isJavaScriptLoaded = false;
	private static final String target_url = "https://www.tripadvisor.in/UserReviewEdit-g304551-d6915868-a_placetype.10022-a_referredFromLocationSearch.true-a_ReviewName.-e-wpage1-Fire_and_Ice_Rooftop_Restaurant-New_Delhi_National_Capital_Territory_of_Delhi.html";
	private static final String target_url_prefix = "www.tripadvisor.in";
	private Context mContext;
	private WebView mWebview;
	private WebView mWebviewPop;
	private FrameLayout mContainer;
	private long mLastBackPressTime = 0;
	private Toast mToast;
	private int status = 0;
	private ReviewEntryDTO reviewEntryDTO;
	private FragementChangeListener fragmentChangeListener;
	private ReviewTripPostModal reviewTripPostModal;
	private ArrayList<ReviewsConfigDTO> tripAdvisorList;
	private boolean isErrror = false;
	
	private static int retryConnectionNumber = 0;
	private final static int CONNECTION_RETRY_MAX = 1;
	private final static int REQUEST_TIMEOUT = 2000;
	private FragmentManager fragmentManager;
	private FragmentActivity fragmentActivity;
	
	private String INTERNET_ERROR_TEXT = "Slow internet connectivity,\n we are unable to proceed further.\n Your review has been saved.";
	private String BATTERY_ERROR_TEXT = "Warning: Low battery ! \n Your review has been saved.";
	private String FLAG_INTERNET_ERROR = "INTERNET_ERROR";
	private String FLAG_BATTERY_ERROR = "BATTERY_ERROR";
	
	
	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
	}

	@Override
	public View onCreateView(LayoutInflater inflator, ViewGroup parent,
			Bundle savedInstance) {

		View view = inflator.inflate(R.layout.fragmenttareviewsubmission,
				parent, false);

		isJavaScriptLoaded = false;

		ImageButton nextButton = (ImageButton) view
				.findViewById(R.id.fragmentrestaurant_reviewsubmissiondone);
		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
			
				goToNextScreen();
			}
		});
		
		ImageButton backButton = (ImageButton) view.findViewById(R.id.fragmentrestaurant_reviewsubmissionback);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				goToBackScreen();
			}

			
		});

		progress = new ProgressDialog(getActivity());
		progress.setMessage("Loading Application ...");
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.setCanceledOnTouchOutside(false);
		progress.show();
		Log.d(LOG_TAG, "Initialized tripadvisor review entries here");

		mContext = getActivity().getApplicationContext();
		mContext.deleteDatabase("webview.db");
		mContext.deleteDatabase("webviewCache.db");
		CookieSyncManager.createInstance(getActivity());
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.removeAllCookie();
		clearCache(mContext, 10);
		mWebview = (WebView) view.findViewById(R.id.webview_tareviewsubmisson);
		// mWebviewPop = (WebView) findViewById(R.id.webviewPop);
		mContainer = (FrameLayout) view
				.findViewById(R.id.frame_tareviewsubmission);
		mWebview.clearCache(true);
		mWebview.clearHistory();
		mWebview.clearFormData();
		WebSettings webSettings = mWebview.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setAppCacheEnabled(false);
		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setSupportMultipleWindows(true);
		webSettings.setDomStorageEnabled(true);
		webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
		webSettings.setUserAgentString("Mozilla/5.0 (Linux; Android 5.0.2; SM-T531 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.93 Safari/537.36");
		
		mWebview.setWebViewClient(new UriWebViewClient());
		mWebview.setWebChromeClient(new UriChromeClient());
		// new TripAdvisorWebViewProcess().execute();

		// String url =
		// TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURENT_URL().trim();
		mWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		tripAdvisorList = MainActivity.getCurrentTripAdvisorInfo();
		reviewTripPostModal = new ReviewTripPostModal(reviewEntryDTO, tripAdvisorList.get(0).getTripAdvisorRestaurantURL());
		
		ReviewWebWorker reviewWebWorker = new ReviewWebWorker();
		reviewWebWorker.execute();
		
		
		Message msg = listenForNetworkAvailability.obtainMessage();
		msg.what = 2;
		listenForNetworkAvailability.sendMessage(msg);
		
		//Log.d(LOG_TAG, "Got battery level :" + getBatteryLevel());
		
		return view;
	}
	
	
	private void postErrorDialog(String error)
	{
		progress.dismiss();
		
		if(error.equals(""))
		{
		   Log.d(LOG_TAG," No error found: ");
		}else if(fragmentManager==null)
		{
//			System.out.println("Fragment Manager null");
			Log.d(LOG_TAG, "Fragment Manager null");
			fragmentManager = getActivity()
					.getSupportFragmentManager();
			if(fragmentManager==null)
			{
				Log.d(LOG_TAG, "Fragment Manager Can't be null");
			}
			FragmentReviewPostErrorDialog fragmentReviewAdaptiveScreen = new FragmentReviewPostErrorDialog();
			fragmentReviewAdaptiveScreen.setErrorDescription(error,reviewEntryDTO);
			
			fragmentReviewAdaptiveScreen.show(fragmentManager, "ReviewErrorDialog");
		}else
		{
			FragmentReviewPostErrorDialog fragmentReviewAdaptiveScreen = new FragmentReviewPostErrorDialog();
			fragmentReviewAdaptiveScreen.setErrorDescription(error,reviewEntryDTO);
			
			fragmentReviewAdaptiveScreen.show(fragmentManager, "ReviewErrorDialog");

		}
		
	}
	
	
	private void setError(String flag)
	{
		if (!isErrror) {
			if (flag.equals(FLAG_INTERNET_ERROR)) {
				
				postErrorDialog(INTERNET_ERROR_TEXT);
				
			} else if (flag.equals(FLAG_BATTERY_ERROR)) {
				
				postErrorDialog(BATTERY_ERROR_TEXT);
			}
			isErrror = true;
		}
	}
	
	
	private  void isNetworkAvailable(final Handler handler,
            final int timeout) {
        new Thread() {
           
        	private boolean responded = false;

            @Override
            public void run() {
                URL url = null;
                try {
                    url = new URL(tripAdvisorList.get(0).getTripAdvisorRestaurantURL());
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }
                String host = "";
                if (null != url) {
                    host = url.getHost();
                }

                Log.i("NetworkCheck", "[PING] host: " + host);
                Process process = null;
                try {
                    process = new ProcessBuilder()
                            .command("/system/bin/ping", "-c 1",
                                    "-w " + (timeout / 1000), "-n", host)
                            .redirectErrorStream(true).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                InputStream in = process.getInputStream();
                StringBuilder s = new StringBuilder();
                int i;

                try {
                    while ((i = in.read()) != -1) {
                        s.append((char) i);

                        if ((char) i == '\n') {
                            Log.i("NetworkCheck",
                                    "[PING] log: " + s.toString());
                            if (s.toString().contains("64 bytes from")) {
                                // If there were a response from the server at
                                // all, we have Internet access
                                responded = true;
                            }
                            s.delete(0, s.length());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    // Destroy the PING process
                    process.destroy();

                    try {
                        // Close the input stream - avoid memory leak
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Send the response to the handler, 1 for success, 0
                    // otherwise
                    handler.sendEmptyMessage(!responded ? 0 : 1);
                }
            }
        }.start();
    }
	
	private  Handler listenForNetworkAvailability = new Handler() {
	    @Override
	    public void handleMessage(Message msg) {
	        if (msg.what == 2) { // code if not connected
	            
	            if (retryConnectionNumber <= CONNECTION_RETRY_MAX) {
	                Log.d("NetworkCheck", "checking for connectivity");
	              ///  Here you could disable & re-enable your WIFI/3G connection before retry

	                // Start the ping process again with delay
	                Handler handler = new Handler();
	                handler.postDelayed(new Runnable() {
	                    @Override
	                    public void run() {
	                        isNetworkAvailable(listenForNetworkAvailability, REQUEST_TIMEOUT);
	                    }
	                }, 5000);
	                retryConnectionNumber++;
	            } else {
	                
	            	Log.d("NetworkCheck", "failed to establish an connection");
	                // code if not connected
	            	Log.d("NetworkCheck","Failed to create network connectivity");
	                
	            }
	        }else if(msg.what==0)
	        {
	        	Log.d(LOG_TAG,": Internet is disconnected: ");
	        	setError(FLAG_INTERNET_ERROR);
	        }
	        else {            
	        
	            Log.i("NetworkCheck", "connected");
	            retryConnectionNumber = 0;
	            // code if connected
	        }
	    }
	};
	
	
	class ReviewWebWorker extends AsyncTask<Void, Void, Void>
	{
		
		String reviewPage = "";
		
		@Override
		protected Void doInBackground(Void... params) {
			
			int SDK_INT = android.os.Build.VERSION.SDK_INT;
		    if (SDK_INT > 8) 
		    {
		        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
		                .permitAll().build();
		        StrictMode.setThreadPolicy(policy);
		    }
			reviewPage = reviewTripPostModal.getReviewPage();
			
			return null;
		}
		
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if (reviewPage.equals("false")) {
				
				showNoInternetConnectionPopup();
				
			} else {
				String mime = "text/html";
				String encoding = "utf-8";
				Log.d(LOG_TAG, "Posting Data.......");
				mWebview.invalidate();
				// mWebview.loadData(reviewPage, mime, encoding);
				mWebview.loadDataWithBaseURL(
						"http://media-cdn.tripadvisor.com/", reviewPage, mime,
						encoding, "https://www.google.co.in");
			}
		}
		
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener) getActivity();
		Log.d(LOG_TAG, "Checking battery level: "+getBatteryLevel());
		if(Math.round(getBatteryLevel())<=3)
		{
			setError(BATTERY_ERROR_TEXT);
		}
		fragmentManager = getActivity()
				.getSupportFragmentManager();
		fragmentActivity = getActivity();
	}
	
	private float getBatteryLevel() {
	 		
		return fragmentChangeListener.getBatteryLevel();
	}
	
	public void showNoInternetConnectionPopup() {
		
		setError(FLAG_INTERNET_ERROR);
	
	}

	protected void goToNextScreen() {

		saveEntryOnDatabase();
		try {
			setMobileDataEnabled(getActivity(), false);
			Log.d(LOG_TAG, "Set mobile data enabled is false");
		} catch (Exception e) {
			Log.d(LOG_TAG,
					"Exception at mobile data enabled: ", e);
		}
		fragmentChangeListener.navigateToReviewMediumSelection();
		
		

	}
	
	private void goToBackScreen() {
		
		Log.d(LOG_TAG, "Going to Previous Screen");
		fragmentChangeListener.navigateToBackScreen();
	}

	private class UriWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			String host = Uri.parse(url).getHost();
			// Log.d("shouldOverrideUrlLoading", url);
			Log.d("OverrideUrl", "Host Url:" + host + ":Url Recieved:" + url);
			if (host.equals(target_url_prefix)) {
				// This is my web site, so do not override; let my WebView load
				// the page
				if (mWebviewPop != null) {
					mWebviewPop.setVisibility(View.GONE);
					mContainer.removeView(mWebviewPop);
					mWebviewPop = null;
				}
				return false;
			}

			if (host.equals("m.facebook.com")) {
				return false;
			}else if(host.equals("accounts.google.com"))
			{
				return false;
			}
			
			// Otherwise, the link is not for a page on my site, so launch
			// another Activity that handles URLs
			/*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);*/
			return false;
		}

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			Log.d("onReceivedSslError", "onReceivedSslError");
				
			
			// super.onReceivedSslError(view, handler, error);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			
			
			if (progress != null) {
				if (progress.isShowing()) {
					progress.dismiss();

					loadJavaScriptHere();
				}
			}
			
			/*String host = Uri.parse(url).getHost();
			if (host.equals(target_url_prefix)) {
				// This is my web site, so do not override; let my WebView load
				// the page
				Log.d(LOG_TAG, "Apply JavaScript Here");

				loadJavaScriptHere();
			}

			if (host.equals("m.facebook.com")) {

			}*/

		}

	}

	class UriChromeClient extends WebChromeClient {

		@Override
		public boolean onCreateWindow(WebView view, boolean isDialog,
				boolean isUserGesture, Message resultMsg) {
			mWebviewPop = new WebView(mContext);
			mWebviewPop.setVerticalScrollBarEnabled(false);
			mWebviewPop.setWebChromeClient(this);
			mWebviewPop.setHorizontalScrollBarEnabled(false);
			mWebviewPop.setWebViewClient(new FaceBookClient());
			mWebviewPop.getSettings().setJavaScriptEnabled(true);
			mWebviewPop.getSettings().setDomStorageEnabled(true);
			mWebviewPop.getSettings().setSavePassword(false);
			mWebviewPop.setLayoutParams(new FrameLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT));
			mContainer.addView(mWebviewPop);
			
			WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
			transport.setWebView(mWebviewPop);
			resultMsg.sendToTarget();
			status = 1;
			return true;
		}

		@Override
		public void onCloseWindow(WebView window) {
			Log.d("onCloseWindow", "called");
			mContainer.removeView(mWebviewPop);
			mWebviewPop =null;
		        mWebview.setVisibility(View.VISIBLE);
		        mWebview.requestFocus();
		}
		
		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			
			super.onProgressChanged(view, newProgress);
			Log.d(LOG_TAG, "progress: "+newProgress);
		}
		
		private class FaceBookClient extends WebViewClient{
		     @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
		        Log.i("REQUEST URL",url);
		        return false;
		    }  
		}
	}

	private void loadJavaScriptHere() {

		mWebview.post(new Runnable() {

			@Override
			public void run() {

				if (!isJavaScriptLoaded) {

					Log.d(LOG_TAG, "Url Loaded");
					mWebview.post(new Runnable() {

						@Override
						public void run() {
							String javascript = "javascript: (function() {"
									+ "document.forms[\"USER_REVIEW_FORM\"].submit();"
									+ "}) ();";
							
							mWebview.loadUrl(javascript);
						}
					});
					isJavaScriptLoaded = true;
				} else {
					String javascript = "javascript: (function() {document.getElementById('SUBMIT').getElementsByTagName('span')[0].click();}) ();";

					//mWebview.loadUrl(javascript);
				}

			}

		});
	}

	int clearCacheFolder(final File dir, final int numDays) {

		int deletedFiles = 0;
		if (dir != null && dir.isDirectory()) {
			try {
				for (File child : dir.listFiles()) {

					// first delete subdirectories recursively
					if (child.isDirectory()) {
						deletedFiles += clearCacheFolder(child, numDays);
					}

					// then delete the files and subdirectories in this dir
					// only empty directories can be deleted, so subdirs have
					// been done first
					if (child.lastModified() < new Date().getTime() - numDays
							* DateUtils.DAY_IN_MILLIS) {
						if (child.delete()) {
							deletedFiles++;
						}
					}
				}
			} catch (Exception e) {
				Log.e(LOG_TAG,
						String.format("Failed to clean the cache, error %s",
								e.getMessage()));
			}
		}
		return deletedFiles;
	}

	/*
	 * Delete the files older than numDays days from the application cache 0
	 * means all files.
	 */
	public void clearCache(final Context context, final int numDays) {
		Log.i(LOG_TAG, String.format(
				"Starting cache prune, deleting files older than %d days",
				numDays));
		int numDeletedFiles = clearCacheFolder(context.getCacheDir(), numDays);
		Log.i(LOG_TAG, String.format(
				"Cache pruning completed, %d files deleted", numDeletedFiles));
	}

	public void saveEntryOnDatabase() {
		
	
		
		Calendar cal = Calendar.getInstance();
		String DATE_FORMAT_NOW = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String date = sdf.format(cal.getTime());
		
		
		String optionalParameter = "Hotel Optional: "
				+ " CheckIn Rating:"
				+ reviewEntryDTO.getReviewentry_ratingCheckIn()
				+ " Rooms Rating:"
				+ reviewEntryDTO.getReviewentry_ratingRooms()
				+ " Cleanliness:"
				+ reviewEntryDTO.getReviewentry_ratingClieanliness()
				+ " Amneties:"
				+ reviewEntryDTO.getReviewentry_ratingAmneties()
				+ " Food and Beverages:"
				+ reviewEntryDTO.getReviewentry_ratingFoodandBeverages()
				+ " Wifi & Internet:"
				+ reviewEntryDTO.getReviewentry_ratingWifiAndInternet()
				+ " Checkout:"
				+ reviewEntryDTO.getReviewentry_ratingCheckout()
				+ " Checkout:"
				+ reviewEntryDTO.getReviewentry_ratingconceirge();
		reviewEntryDTO.setReviewentry_campaignId(MainActivity
				.getCamapignInformation().getCampaignId().trim());
		reviewEntryDTO.setReviewentry_optionaldesc(optionalParameter);
		reviewEntryDTO.setReviewentry_date(date);
		reviewEntryDTO.setReviewentry_type(PLACE_REVIEW_TYPE_TRIPADVISOR);
		reviewEntryDTO.setReviewentry_status(String.valueOf(status));
		reviewEntryDTO.setReviewentry_timestamp(getTimeStamp());
		//reviewEntryDTO.setReviewentry_emailid(TripAdisorEssentialsDTO.getRevientry_emailid());
		reviewEntryDTO.setReviewentry_platformid("1");
		reviewEntryDTO.setReviewentry_reviewUploaded("0");
		reviewEntryDTO.setReviewentry_reviewuploadStatus("0");
		
		MainActivity.addReviewEntriesInDatabase(reviewEntryDTO);

	}

	private String getTimeStamp() {
		Calendar calendar = Calendar.getInstance();
		Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		return currentTimestamp.toString();
	}

	private void setMobileDataEnabled(Context context, boolean enabled)
			throws Exception {

		
		Log.d(LOG_TAG, "SDK Version: " + Build.VERSION.SDK_INT);
		if (Build.VERSION.SDK_INT > 19) {
			
			setMobileDataEnabledFromLollipop(getActivity(),enabled);
			
		} else {

			final ConnectivityManager conman = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class
					.forName(conman.getClass().getName());
			final Field iConnectivityManagerField = conmanClass
					.getDeclaredField("mService");
			iConnectivityManagerField.setAccessible(true);
			final Object iConnectivityManager = iConnectivityManagerField
					.get(conman);
			final Class iConnectivityManagerClass = Class
					.forName(iConnectivityManager.getClass().getName());
			final Method setMobileDataEnabledMethod = iConnectivityManagerClass
					.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);

			setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
		}
	}

	public void setReviewUserDTO(ReviewEntryDTO reviewEntryDTO) {

		Log.d(LOG_TAG, "Reivew Entry DTO Recieved:"+reviewEntryDTO.toString());
		this.reviewEntryDTO = reviewEntryDTO;
	}
	
	
	private final static String COMMAND_L_ON = "svc data enable\n ";
	private final static String COMMAND_L_OFF = "svc data disable\n ";
	private final static String COMMAND_SU = "su";
	
	private void setMobileDataEnabledFromLollipop(Context context,boolean enable)
	{
		 String command;
		    if(enable)
		        command = COMMAND_L_ON;
		    else
		        command = COMMAND_L_OFF;        

		    try{
		        Process su = Runtime.getRuntime().exec(COMMAND_SU);
		        DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

		        outputStream.writeBytes(command);
		        outputStream.flush();

		        outputStream.writeBytes("exit\n");
		        outputStream.flush();
		        try {
		            su.waitFor();
		        } catch (InterruptedException e) {
		            e.printStackTrace();
		        }

		        outputStream.close();
		    }catch(IOException e){
		        e.printStackTrace();
		    }
	}


}
