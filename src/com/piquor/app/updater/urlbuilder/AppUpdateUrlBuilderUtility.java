package com.piquor.app.updater.urlbuilder;

import android.util.Log;

import com.piquor.app.updater.dbupdates.AdaptiveQuestionUpdater;
import com.piquor.dto.CampaignInfoDTO;
import com.piquor.services.deliverymode.execute.HttpUrlExecute;
import com.piquor.services.deliverymode.execute.HttpUrlResponse;
import com.piquor.utility.PiquorTimestamp;

public abstract class AppUpdateUrlBuilderUtility {

//	public AdaptiveQuestion adaptiveQuesiton;
	public CampaignInfoDTO campaignInfoDTO;
	private String buildedQueryUrl;
	private String LOG_TAG = "AppUpdateUrlBuilderUtility";
	private AdaptiveQuestionUpdater adaptiveQuestionUpdater;
	
	
	public AppUpdateUrlBuilderUtility(CampaignInfoDTO campaignInfoDTO)
	{
		this.campaignInfoDTO = campaignInfoDTO;
		adaptiveQuestionUpdater = new AdaptiveQuestionUpdater();
	}
	
    public abstract boolean executeQueryString();
	
	public boolean executeQuery(String serverServiceUrl)
	{
		buildedQueryUrl=serverServiceUrl;
		logInfo("ExecutingUrl Url:"+buildedQueryUrl+" For CampaignId:"+campaignInfoDTO.getCampaignId()+"@executeQuery:UrlBuilderUtility "+PiquorTimestamp.getTimeStamp());
		HttpUrlResponse response=HttpUrlExecute.executeQuery(buildedQueryUrl);
		logInfo("UrlResponse Recived Log for CampaignId :"+campaignInfoDTO.getCampaignId()+" Executed URL:"+buildedQueryUrl+":Response Recieved:"+response.getQueryUrlLog()+" Response from API"+response.getQueryUrlResponse()+"@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		
		if(isResponseValid(response.getQueryUrlResponse()))
		{
			if(completeAdaptiveQueryOperation(response.getQueryUrlResponse()))
					{
						adaptiveQuestionUpdater.saveToDatabase();
						return true;
					}else
					{
						logInfo("Problem in reading url response serious error");
						return false;
					}
						
		}
		else
		{
			logInfo("Review Execution Query Failed  Url  CampaignId:"+campaignInfoDTO.getCampaignId()+":@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		
			return false;
		}
		
		
	}

	private boolean completeAdaptiveQueryOperation(String responseJson) {
		
		
		return adaptiveQuestionUpdater.createAdaptiveQuestionList(responseJson);
		
	}
	
	private boolean isResponseValid(String urlResponse)
	{
		if(urlResponse.trim().equals("false"))
		{
			return false;
		}else
		{
			return true;
		}
		
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
