package com.services.deliverymode.utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import android.util.Log;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.errorhandler.ReviewErrorUpdater;
import com.piquor.services.deliverymode.execute.HttpUrlExecute;
import com.piquor.services.deliverymode.execute.HttpUrlResponse;
import com.piquor.utility.PiquorTimestamp;

public abstract class ReviewUrlBuilderUtility {

	public ReviewEntryDTO reviewEntryDTO;
	public ReviewEntryDTO reviewParamsDTO;
	private String buildedQueryUrl;
	private String LOG_TAG = "UrlBuilderUtility";
	private ReviewErrorUpdater errorUpdater;
	
	public ReviewUrlBuilderUtility(ReviewEntryDTO reviewEntryDTO)
	{
		this.reviewEntryDTO=reviewEntryDTO;
		reviewParamsDTO=new ReviewEntryDTO();
		errorUpdater=new ReviewErrorUpdater();
		buildUserParameters();
	
	}

	private void buildUserParameters() {
		
		String reviewentry_userEmail=reviewEntryDTO.getReviewentry_emailid().trim();
		String reviewentry_campaignId = reviewEntryDTO.getReviewentry_campaignId().trim();
		String reviewentry_date = reviewEntryDTO.getReviewentry_date().trim();
		String reviewentry_overallrating = reviewEntryDTO.getReviewentry_overallrating().trim();
		String reviewentry_wereyouherefor = reviewEntryDTO.getReviewentry_wereyouherefor().trim();
		
		try {
			reviewentry_wereyouherefor = URLEncoder.encode(reviewentry_wereyouherefor,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {     }
		
		String reviewentry_sortofvisit = reviewEntryDTO.getReviewentry_sortofvisit().trim();
		try {
			reviewentry_sortofvisit = URLEncoder.encode(reviewentry_sortofvisit,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {     }
		
		String reviewentry_title = reviewEntryDTO.getReviewentry_title().trim();
		try {
			reviewentry_title = URLEncoder.encode(reviewentry_title,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {		}
		
		String reviewentry_desc = reviewEntryDTO.getReviewentry_desc();
		try {
			reviewentry_desc = URLEncoder.encode(reviewentry_desc,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {		}
		
		String reviewentry_optionaldesc = reviewEntryDTO.getReviewentry_optionaldesc();
		try {
			reviewentry_optionaldesc = URLEncoder.encode(reviewentry_optionaldesc,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {		}
		
		String reviewentry_platformid = reviewEntryDTO.getReviewentry_platformid().trim();
		String reviewentry_status = reviewEntryDTO.getReviewentry_status().trim();
		String reviewentry_timestamp = reviewEntryDTO.getReviewentry_timestamp().trim();
		
		reviewParamsDTO.setReviewentry_emailid(reviewentry_userEmail);
		reviewParamsDTO.setReviewentry_campaignId(reviewentry_campaignId);
		try {
			reviewentry_date = URLEncoder.encode(reviewentry_date,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {		}
		
		reviewParamsDTO.setReviewentry_date(reviewentry_date);
		reviewParamsDTO.setReviewentry_overallrating(reviewentry_overallrating);
		reviewParamsDTO.setReviewentry_wereyouherefor(reviewentry_wereyouherefor);
		reviewParamsDTO.setReviewentry_sortofvisit(reviewentry_sortofvisit);
		reviewParamsDTO.setReviewentry_title(reviewentry_title);
		reviewParamsDTO.setReviewentry_desc(reviewentry_desc);
		reviewParamsDTO.setReviewentry_optionaldesc(reviewentry_optionaldesc);
		reviewParamsDTO.setReviewentry_platformid(reviewentry_platformid);
		reviewParamsDTO.setReviewentry_status(reviewentry_status);
		
		try {
			reviewentry_timestamp = URLEncoder.encode(reviewentry_timestamp,"ISO-8859-1");
		} catch (UnsupportedEncodingException e) {		}
		
		
		reviewParamsDTO.setReviewentry_timestamp(reviewentry_timestamp);
		
	}
	
	
	
	public abstract boolean executeQueryString();
	
	public boolean executeQuery(String serverServiceUrl)
	{
		buildedQueryUrl=serverServiceUrl;
		logInfo("ExecutingUrl Url:"+buildedQueryUrl+" For CampaignId:"+reviewEntryDTO.getReviewentry_campaignId()+" and Emailid:"+reviewEntryDTO.getReviewentry_emailid()+"@executeQuery:UrlBuilderUtility "+PiquorTimestamp.getTimeStamp());
		HttpUrlResponse response=HttpUrlExecute.executeQuery(buildedQueryUrl);
		logInfo("UrlResponse Recived Log for PicId:"+reviewEntryDTO.getReviewentry_campaignId()+"and emailid:"+reviewEntryDTO.getReviewentry_emailid()+" Executed URL:"+buildedQueryUrl+":Response Recieved:"+response.getQueryUrlLog()+" Response from API"+response.getQueryUrlResponse()+"@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		
		if(isResponseValid(response.getQueryUrlResponse()))
		{
			return completeUserEntryOperation();
		}
		else
		{
			logInfo("Mail Delieverd Unsucussfully Setting in Url Execution Error Category Setting Flags To Done For CampaignId:"+reviewEntryDTO.getReviewentry_campaignId()+":@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
			errorUpdater.setUrlExectionError(reviewEntryDTO);
			return false;
		}
		
		
	}
	
	public boolean completeUserEntryOperation()
	{
		logInfo(" Setting User Flags To Done For EmailId:"+reviewEntryDTO.getReviewentry_emailid()+":@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		return errorUpdater.setSucessFlagsForUserEntry(reviewEntryDTO);
	}
	
	
	private boolean isResponseValid(String urlResponse)
	{
		if(urlResponse.equals("true"))
		{
			return true;
		}
		else if(urlResponse.toLowerCase(Locale.ENGLISH).contains("success"))
		{
			return true;
		}else if(urlResponse.toLowerCase(Locale.ENGLISH).contains("failed"))
		{
			logInfo("Failed Error At URL Entry In Universal Controller@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return true;
		}else if(urlResponse.trim().equals(""))
		{
			//ErrorMailerService.SendMail("No Response Found From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("No Response Found From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return true;
		}else if(urlResponse.equals("0"))
		{
			//ErrorMailerService.SendMail("Completely failed process Application And Server Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("Completely failed process Response Recived :0 From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return true;
		}else if(urlResponse.equals("1"))
		{
			logInfo("Completely processed entry response Recived :1 From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return true;
		}else if(urlResponse.equals("2"))
		{
			logInfo("Photo already uploaded response Recived :2 From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return true;
		}else if(urlResponse.equals("3"))
		{
			//ErrorMailerService.SendMail("Server Side exception @ DAO Layer At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("Server Side exception @ DAO Layer Error At URL Entry@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return false;
		}else if(urlResponse.equals("4"))
		{
			//ErrorMailerService.SendMail("Server Side exception @ DAO Layer At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("FaceBook permission blocked by user .faceBook upload failed . [Ignore exception ] Error At URL Entry@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return true;
		}else if(urlResponse.equals("5"))
		{	
			//ErrorMailerService.SendMail("Sever side exception while updating shared status of photograph in image_fb_twitter_table .rest operation success. Response Recieved:5 At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("Sever side exception while updating shared status of photograph in image_fb_twitter_table .rest operation success. Response Recieved:5 Error At URL Entry@isResponseValid:UrlBuilderUtility for EmailId:"+reviewEntryDTO.getReviewentry_emailid());
			return false;
		}
		else 
		{
			//ErrorMailerService.SendMail("False Response Recieved  Services will be blocked at this machine till response successfull recieved  Server Reponse:"+urlResponse+" Error At URL Entry@isResponseValid:UrlBuilderUtility for picId:"+userDTO.getPicId()+" Having CampaignId:"+userDTO.getCampaignId()+" Url:"+buildedQueryUrl, "Application & Server",userDTO.getCampaignId());
			logInfo("No Response Found From Server Error At URL Entry@isResponseValid:UrlBuilderUtility for emailid:"+reviewEntryDTO.getReviewentry_emailid());
			return false;
		}
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
