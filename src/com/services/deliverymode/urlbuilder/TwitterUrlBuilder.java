package com.services.deliverymode.urlbuilder;

import android.util.Log;

import com.piquor.dto.UserTableDTO;
import com.services.deliverymode.staticfields.DeliveryModeUrls;
import com.services.deliverymode.utility.UrlBuilderUtility;

public class TwitterUrlBuilder extends UrlBuilderUtility implements DeliveryModeUrls {

	public TwitterUrlBuilder(UserTableDTO userDTO) {
		super(userDTO);
	}
	
	private String LOG_TAG = "TwitterUrlBuilder";
	private String twitterEntryUrl = "";
	@Override
	public boolean executeQueryString(String machineId) {
		
		twitterEntryUrl=TWITTER_URL_PREFIX+buildTwitterUrl(machineId);		
		logInfo("Executing TWitter Entry Url:"+twitterEntryUrl+":For PicId:"+userDTO.getPicId());
		return executeQuery(twitterEntryUrl);	
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG,info);
	}
	
	public String buildTwitterUrl(String machineId)
	{
		/*String queryParameter ="toc=" + userParamsDTO.getPhotoCaptureDate()
				+ "&campaignId=" + userParamsDTO.getUserCampaignId()
				+ "&picId=" + userParamsDTO.getPhotoPicId() + "&machineId="
				+ machineId+"&flag=1";*/
		
		
		String queryParameter ="campaignId="+userParamsDTO.getUserCampaignId()+
				"&machineId="+machineId+"&picId="+userParamsDTO.getPhotoPicId()+
				"&textPost="+userParamsDTO.getUserTwitterPostText()+
				"&twitterUserId="+userParamsDTO.getUserTwitterId()+
				"&twitterUserName="+userParamsDTO.getUserTwitterName()+
				"&location=na&friends="+userParamsDTO.getUserTwitterFriendsCount()+
				"&followers="+userParamsDTO.getUserTwitterFollowers()+
				"&captureDate="+userParamsDTO.getPhotoCaptureDate()+
				"&policyAgreement=0";
;
		
		return queryParameter;
	}
	
}
