package com.piquor.frameconfig.core;

	
import java.util.ArrayList;

import android.util.Log;

import com.piquor.frameconfig.dto.FrameConfig;
import com.piquor.frameconfig.dto.FrameROI;
import com.piquor.piquortabapp.R;


public class FrameConfigLoader {
   
	private static FrameROI bgFrameROI;
	private static FrameROI fgFrameROI;
	private static ArrayList<FrameConfig> framesConfigration = null;
	private static String LOG_TAG = "FrameConfigLoader";
	
	private static void loadFrameConfiguration() {
		
		framesConfigration = new ArrayList<FrameConfig>();
		bgFrameROI = new FrameROI(640, 480, 0, 0);
		fgFrameROI = new FrameROI(640, 480, 0, 0);
		

		/*addFrameConfig(fgFrameROI, bgFrameROI, R.drawable.frame,
				R.drawable.frame_back, R.drawable.thumb);*/

		//bgFrameROI = new FrameROI(677, 659, 421, 69);
		addFrameConfig(fgFrameROI, bgFrameROI, R.drawable.frame1,
				R.drawable.frame_back, R.drawable.thumb1);
	}
	
	private static void addFrameConfig(FrameROI fgROI,FrameROI bgROI,int fgFrameId,int bgFrameId,int thumbnailId)
	{
		FrameConfig frameConfig = 
				new FrameConfig(framesConfigration.size(),fgFrameId,bgFrameId,thumbnailId,bgFrameROI,fgFrameROI);
		addFrame(framesConfigration.size(),frameConfig);
		
		Log.d(LOG_TAG,"FrameConfig Added:"+frameConfig.toString());
	}

	private static void addFrame(int index, FrameConfig frameConfig) {
		
		framesConfigration.add(index,frameConfig);
	}
	
	public static ArrayList<FrameConfig> getFramesConfiguration()
	{
		if(framesConfigration == null)
		{
			loadFrameConfiguration();
		}
		return framesConfigration;
	}
	
	
}
