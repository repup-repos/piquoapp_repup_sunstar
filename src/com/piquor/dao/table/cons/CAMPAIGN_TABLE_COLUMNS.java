package com.piquor.dao.table.cons;

public interface CAMPAIGN_TABLE_COLUMNS {

	public final String CAMPAIGN_TABLE_NAME = "campaign_machine_configuration";
	public final String COLUMN_NAME_ID = "id";
	public final String COLUMN_NAME_CAMPAIGNID = "campaignid";
	public final String COLUMN_NAME_MACHINEID = "machineid";
	public final String COLUMN_NAME_MAILERPAUSETIME = "mailerpausetime";
}
