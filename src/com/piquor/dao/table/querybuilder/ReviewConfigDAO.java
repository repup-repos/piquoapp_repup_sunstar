package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.PLACE_REVIEW_CONIFG_COLUMNS;
import com.piquor.dto.ReviewsConfigDTO;

public class ReviewConfigDAO implements PLACE_REVIEW_CONIFG_COLUMNS {
	
	
	public static String getDeletePlaceQuery(String selectedPlaceName)
	{
		String query =  "delete from "+TABLE_NAME_REVIEW_CONFIGURATION+" where "+COLUMN_NAME_REVIEW_PLACE_NAME+"='"+selectedPlaceName+"'";
		return query;
	}
	
	public static String getInsertReviewConfigurationQuery1(ReviewsConfigDTO configDTO)
	{
		String INSERT_QUERY_TRIPADVISOR = "insert into "
					+TABLE_NAME_REVIEW_CONFIGURATION+""
					+ " ( "+COLUMN_NAME_REVIEW_PLACE_NAME
					+","+COLUMN_NAME_REVIEW_PLACE_TRIPADVISOR_URL
					+","+COLUMN_NAME_REVIEW_PLACE_TYPE
					+","+ COLUMN_NAME_REVIEW_PLACE_ZOMATO_URL 
					+ ") "
					+ "values ('"+configDTO.getTripAdvisorRestaurentName()+
					"','"+configDTO.getTripAdvisorRestaurantURL()
					+"','"+configDTO.getTripAdvisorRestaurantType()
					+"','"+configDTO.getTripAdvisorRestaurantZomatoUrl()+"')";
		
		return INSERT_QUERY_TRIPADVISOR;
		   
	}
	
	public static String getInsertReviewConfigurationQuery(ReviewsConfigDTO configDTO)
	{
		String query = "insert into "+TABLE_NAME_REVIEW_CONFIGURATION+" ( "+COLUMN_NAME_REVIEW_PLACE_NAME+","+COLUMN_NAME_REVIEW_PLACE_TRIPADVISOR_URL+","+COLUMN_NAME_REVIEW_PLACE_TYPE+" ) values ('"+configDTO.getTripAdvisorRestaurentName()+"','"+configDTO.getTripAdvisorRestaurantURL()+"','"+configDTO.getTripAdvisorRestaurantType()+"')";
		return query;
	}
	
	public static String getUpdateTripAdvisorDetialQuery(ReviewsConfigDTO dto)
	{
		String CAMPAIGN_UPDATE_QUERY = "UPDATE " + TABLE_NAME_REVIEW_CONFIGURATION + " SET ";
		CAMPAIGN_UPDATE_QUERY += COLUMN_NAME_REVIEW_PLACE_NAME + "='"+dto.getTripAdvisorRestaurentName()+"',"+COLUMN_NAME_REVIEW_PLACE_TRIPADVISOR_URL+"='"+dto.getTripAdvisorRestaurantURL()+"' where "+COLUMN_NAME_REVIEW_ID+"="+dto.getId();
		
		return CAMPAIGN_UPDATE_QUERY;
		
	}
	
	public static String getTripAdvisorInfoQuery()
	{
		String query = "Select "+COLUMN_NAME_REVIEW_ID+","+COLUMN_NAME_REVIEW_PLACE_NAME+","+COLUMN_NAME_REVIEW_PLACE_TRIPADVISOR_URL+","+COLUMN_NAME_REVIEW_PLACE_TYPE+" FROM " + TABLE_NAME_REVIEW_CONFIGURATION ;
		
		return query;
		
	}
	
	public static String getReviewConfigTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_REVIEW_CONFIGURATION;
		return query;
	}
	public static String  getCreateReviewConfigTable()
	{
		String CREATE_TRIPADVISOR_CONFIG_TABLE = "CREATE TABLE ["+TABLE_NAME_REVIEW_CONFIGURATION+"] "
				+ "("
				+ "["+COLUMN_NAME_REVIEW_ID+"] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,"
				+ "["+COLUMN_NAME_REVIEW_PLACE_NAME+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_REVIEW_PLACE_TRIPADVISOR_URL+"] varchar(4000) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_REVIEW_PLACE_TYPE+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_REVIEW_PLACE_ZOMATO_URL+"] varchar(200) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_TRIPADVISOR_CONFIG_TABLE;
	}
	
	
}
