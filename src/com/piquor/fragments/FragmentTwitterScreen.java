package com.piquor.fragments;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.constants.APP_CONS;
import com.piquor.constants.APP_CREDENTIALS;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;
import com.piquor.twitter.utility.TwitterConstants;
import com.piquor.utility.AlertDialogManager;
import com.piquor.utility.InternetConnectionDetector;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class FragmentTwitterScreen extends DialogFragment implements ACTIVITY_FLAGS,
		FRAGMENT_SCREEN_NAMES {
	// Preference Constants
 
    private final String TWITTER_CALLBACK_URL = "http://api.twitter.com/oauth/authorize?force_login=true";
    private	ImageView login;
    private Twitter twitter;
    private RequestToken requestToken = null;
    private AccessToken accessToken;
    private String oauth_url,oauth_verifier,profile_url;
    private Dialog auth_dialog;
    private WebView web;
    private SharedPreferences pref;
    private ProgressDialog progress;
    
    private String LOG_TAG = "FramgmentTwitterScreen";
    // Twitter
   
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();
    
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)	{
		
		View view = inflator.inflate(R.layout.fragmenttwitterscreeen, null);
		
		login = (ImageView)view.findViewById(R.id.login);
	    pref = getActivity().getPreferences(0);
	    twitter = new TwitterFactory().getInstance();
	    twitter.setOAuthConsumer(pref.getString(TwitterConstants.TWITTER_PREF_CONSUMER_KEY_FLAG, ""), pref.getString(TwitterConstants.TWITTER_PREF_CONSUMER_SECRET_FLAG, ""));
	    InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(getActivity());
	    if(internetConnectionDetector.isConnectingToInternet())
	    {
	    	login.setOnClickListener(new LoginProcess());
	    }else
	    {
	    	new AlertDialogManager().showAlertDialog(getActivity(), "Intenet Connection Failed", "Please Ensure Internet Connectivity", false);
	    	goToBackTwitter();
	    }
	  /*  Button btnNextTwitter = (Button) view.findViewById(R.id.btnNextTwitter);
	    btnNextTwitter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				goToPreviousTwitter();
			}
		});*/
	    
	   /* Button btnBackTwitter = (Button) view.findViewById(R.id.btnBackTwitter);
	    btnBackTwitter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				goToBackTwitter();
			}
		});*/
        /** This if conditions is tested once is
         * redirected from twitter page. Parse the uri to get oAuth
         * Verifier
         * */
       /* if (!isTwitterLoggedInAlready()) {
        	
          Log.d(LOG_TAG, "Some Work Here");
        }*/
        
		return view;
	}
	protected void goToBackTwitter() {
		
		dismiss();
	}

	
	class LoginProcess implements OnClickListener {
	    @Override
	    public void onClick(View v) {
	      // TODO Auto-generated method stub
	      new TokenGet().execute();
	    }}
	
	
	private class TokenGet extends AsyncTask<String, String, String> {
       
		 @Override
		    protected void onPreExecute() {

		 }

		@Override
        protected String doInBackground(String... args) 
		{
			
			try {
					requestToken = twitter.getOAuthRequestToken(TWITTER_CALLBACK_URL);
					oauth_url = requestToken.getAuthorizationURL();
				} catch (TwitterException e) {
						// TODO Auto-generated catch block
					e.printStackTrace();
				}
                      return oauth_url;
        }	
         
		@Override
        protected void onPostExecute(String oauth_url) {
           
			if(oauth_url != null){
        	  
			   oauth_url += "&force_login=true";
        	   Log.e("URL", oauth_url);
        	   auth_dialog = new Dialog(getActivity());
        	   auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        	   auth_dialog.setContentView(R.layout.auth_dialog);
			   web = (WebView)auth_dialog.findViewById(R.id.webv);
        	   web.getSettings().setJavaScriptEnabled(true);
        	   
        	   web.loadUrl(oauth_url);
        	   web.setWebViewClient(new WebViewClient() {
        	   boolean authComplete = false;
                  
        	   @Override
               public void onPageStarted(WebView view, String url, Bitmap favicon){
                    super.onPageStarted(view, url, favicon);
               }
                
        	   
        	   @Override
        	   public void onPageFinished(WebView view, String url) {
				
        		super.onPageFinished(view, url);
				if (url.contains("oauth_verifier")&& authComplete == false) 
				{
					authComplete = true;
				    Log.e("Url", url);
				    Uri uri = Uri.parse(url);
					oauth_verifier = uri.getQueryParameter("oauth_verifier");
					auth_dialog.dismiss();
					new AccessTokenGet().execute();
						
				} else if (url.contains("denied")) {
							
					auth_dialog.dismiss();
				    Toast.makeText(getActivity(),
									"Sorry !, Permission Denied",
									Toast.LENGTH_SHORT).show();
					  }
					}
				});
        	   
				auth_dialog.show();
				auth_dialog.setCancelable(true);
			
			}
			else
			{
                 Toast.makeText(getActivity(), "Sorry !, Network Error or Invalid Credentials", Toast.LENGTH_SHORT).show();
			}
         }
    }
    private class AccessTokenGet extends AsyncTask<String, String, Boolean> {
      @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setMessage("Fetching Data ...");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();
    }
         @Override
         protected Boolean doInBackground(String... args) {
           try {
           accessToken = twitter.getOAuthAccessToken(requestToken, oauth_verifier);
           SharedPreferences.Editor edit = pref.edit();
           edit.putString(TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_FLAG, accessToken.getToken());
           edit.putString(TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_SECRET_FLAG, accessToken.getTokenSecret());
           User user = twitter.showUser(accessToken.getUserId());
   
           long twitterId = user.getId();
           String twitterName = user.getName();
           int twitterFriends = user.getFriendsCount();
           int twitterFollowers = user.getFollowersCount();
           if(twitterId != 0)
           {
        	   MainActivity.setTwitterId(twitterId);
           }
           if(twitterName!=null)
           {
        	   if(!twitterName.equals(""))
        	   {
        		   MainActivity.setTwitterName(twitterName);
        	   }
           }
           if(twitterFollowers != 0)
           {
        	   MainActivity.setTwitterFollowers(twitterFollowers);
           }
           if(twitterFriends != 0)
           {
        	   MainActivity.setTwitterFriends(twitterFriends);
           }
           if(twitterId!=0)
           {
        	   MainActivity.setDeliveryMode(DeliveryModeFlags.TWITTER_DELIVERYMODE_FLAG);
           }
           
           Log.d(LOG_TAG,"Twitter Info To be Inserted: "
           		+ "TwitterId:"+twitterId+":TwitterName:"+twitterName+
           		":TwitterFollowers:"+twitterFollowers+
           		":TwitterFriends:"+twitterFriends);
        	
           profile_url = user.getOriginalProfileImageURL();
           edit.putString(TwitterConstants.TWITTER_PREF_USER_NAME_FLAG, user.getName());
           edit.putString(TwitterConstants.TWITTER_PREF_IMAGE_URL_FLAG, user.getOriginalProfileImageURL());
           edit.commit();
           postImageToTwitter();
      } catch (TwitterException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
                       return true;
         }
          @Override
          protected void onPostExecute(Boolean response) {
            if(response){
                    progress.hide();
                    Log.d(LOG_TAG,"Final Execution");
                    disconnectFromTwitter();
                    new AlertDialogManager().showAlertDialog(getActivity(), "Twitter Status", "Image Uploaded To Twitter", true);
                    MainActivity.setDeliveryMode(DeliveryModeFlags.TWITTER_DELIVERYMODE_FLAG);
                    goToBackTwitter();
                    //.makeText(getActivity(), "Image Uploaded To Twitter", Toast.LENGTH_SHORT).show();
            }
            }
          }
     
	
 
    public void postImageToTwitter() {
    	 try{
    	        StatusUpdate status = new StatusUpdate(APP_CREDENTIALS.TWITTER_HASTAG+" "+APP_CREDENTIALS.TWITTER_DESC);
    	        status.setMedia(APP_CONS.get_framedImage());
    	        twitter.updateStatus(status);}
    	    catch(TwitterException e){
    	        Log.d("TAG", "Pic Upload error" + e.getErrorMessage());
    	        
    	    }
    	 
    	
	}

	private void disconnectFromTwitter() {
		
		SharedPreferences.Editor editor = pref.edit();
		editor.remove(TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_FLAG);
		editor.remove(TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_SECRET_FLAG);
		editor.remove(TwitterConstants.TWITTER_PREF_IMAGE_URL_FLAG);
		editor.remove(TwitterConstants.TWITTER_PREF_USER_NAME_FLAG);
		editor.commit();
	}



	
}
