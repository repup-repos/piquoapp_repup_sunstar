package com.services.deliverymode.staticfields; 

public interface DeliveryModeFlags {

	public final String EMAIL_DELIVERYMODE_FLAG="email";
	public final String SMS_DELIVERYMODE_FLAG="sms";
	public final String FACEBOOK_DELIVERYMODE_FLAG="facebook";
	public final String MMS_DELIVERYMODE_FLAG="mms";
	public final String TWITTER_DELIVERYMODE_FLAG="twitter";
	public final String WHATSAPP_DELIVERYMODE_FLAG="whatsapp";
}
