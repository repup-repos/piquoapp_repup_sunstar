package com.piquor.services;

import android.util.Log;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.dto.UserTableDTO;
import com.piquor.errorhandler.ErrorUpdater;
import com.piquor.errorhandler.ReviewErrorUpdater;
import com.piquor.services.delieverymode.emailentryhandler.EmailEntryHandler;
import com.piquor.utility.PiquorTimestamp;
import com.services.deliverymode.staticfields.DeliveryModeFlags;
import com.services.deliverymode.universalentryhandler.UniversalEntryHandler;
import com.services.deliverymode.urlbuilder.FacebookUrlBuilder;
import com.services.deliverymode.urlbuilder.ReviewUrlBuilder;
import com.services.deliverymode.urlbuilder.TwitterUrlBuilder;
import com.services.launcher.ServicesLauncher;

public class UrlExecutorService implements DeliveryModeFlags {

	private ErrorUpdater errorUpdater;
	private ReviewErrorUpdater reviewErrorUpdater;
	private String LOG_TAG = "UrlExecutorService";

	public UrlExecutorService() {
		
		errorUpdater = new ErrorUpdater();
		reviewErrorUpdater = new ReviewErrorUpdater();
	}
	
	public void processReviewRemoteEntry(final ReviewEntryDTO reviewEntryDTO)
	{
		logInfo("Review Entry recieved for campaignid:"+reviewEntryDTO.getReviewentry_campaignId()+":EmailId:"+reviewEntryDTO.getReviewentry_emailid()+" @processRemoteEntry:UrlExecutorService");
		
		Thread thread = new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	
	        	logInfo("Review Url Entry Started for :CampaignId:"+reviewEntryDTO.getReviewentry_campaignId()+" and Emailid:"+reviewEntryDTO.getReviewentry_emailid()+"@processEmailEntry:UrlExecutorService");
	        	boolean response=new ReviewUrlBuilder(reviewEntryDTO).executeQueryString();
	        	logInfo("REview URL Entry Successfully Stored:"+reviewEntryDTO.getReviewentry_emailid()+ ":Twitter Url Entry Response:"+response+"@processEmailEntry:UrlExecutorService");
	        	System.out.println(">> Twitter Url Entry Response"+response);
	        	ServicesLauncher.showMessage(">> Twitter Url Entry Response"+response);
	        	
	        }
	    });
	    thread.start();
	    long endTimeMillis = System.currentTimeMillis() + 20000;
	    
	    
	    while (thread.isAlive()) {
	    	//System.out.println("you are alive");
	        if (System.currentTimeMillis() > endTimeMillis) {
	        	
	        	System.out.print("Url connection Timed Out");
	        	logInfo(PiquorTimestamp.getTimeStamp()
						+ "Url Connection Timed Out Twtter [UrlExecutorService.processEmailEntry] ");
	        	
	        	ServicesLauncher.showMessage("Url Timed Out Check Internet Connection");
	        	boolean response=reviewErrorUpdater.updateEmailEror(reviewEntryDTO);
	        	
	        	logInfo("Review Entry Error Set for EmailId:"+reviewEntryDTO.getReviewentry_emailid()+": Response Recived:"+response+"@processEmailEntry:UrlExecutorService");
	        	
	        	break;
	        }
	        try {
	            //System.out.println("Going Going");
	            Thread.sleep(500);
	        }
	        catch (InterruptedException t) {}
	    }
	    thread.interrupt();
	    
		
	}
	
	public void processRemoteEntry(final UserTableDTO userDTO,final String machineId) {

		
		String deliveryMode = userDTO.getDelieveryMode().trim();
		logInfo("DeliveryMode recieved:"+deliveryMode+":PicId:"+userDTO.getPicId()+"@processRemoteEntry:UrlExecutorService");
		String picId=userDTO.getPicId().trim();
		
		String[] CompletedeliveryModesArray={TWITTER_DELIVERYMODE_FLAG,FACEBOOK_DELIVERYMODE_FLAG,EMAIL_DELIVERYMODE_FLAG,SMS_DELIVERYMODE_FLAG,WHATSAPP_DELIVERYMODE_FLAG,MMS_DELIVERYMODE_FLAG};
		
		if (deliveryMode.equals(TWITTER_DELIVERYMODE_FLAG)) {

			Thread thread = new Thread(new Runnable() {
		        @Override
		        public void run() {
		        	
		        	logInfo("Twitter URL Entry Started:PicId:"+userDTO.getPicId()+"@processEmailEntry:UrlExecutorService");
		        	boolean response=new TwitterUrlBuilder(userDTO).executeQueryString(machineId);
		        	logInfo("Twitter URL Entry Successfully Stored:"+userDTO.getPicId()+ ":Twitter Url Entry Response:"+response+"@processEmailEntry:UrlExecutorService");
		        	System.out.println(">> Twitter Url Entry Response"+response);
		        	ServicesLauncher.showMessage(">> Twitter Url Entry Response"+response);
		        	
		        }
		    });
		    thread.start();
		    long endTimeMillis = System.currentTimeMillis() + 20000;
		    
		    
		    while (thread.isAlive()) {
		    	//System.out.println("you are alive");
		        if (System.currentTimeMillis() > endTimeMillis) {
		        	
		        	System.out.print("Url connection Timed Out");
		        	logInfo(PiquorTimestamp.getTimeStamp()
							+ "Url Connection Timed Out Twtter [UrlExecutorService.processEmailEntry] ");
		        	
		        	ServicesLauncher.showMessage("Url Timed Out Check Internet Connection");
		        	boolean response=errorUpdater.emailErorr(picId);
		        	
		        	logInfo("Twitter Entry Error Set for PicId:"+userDTO.getPicId()+": Response Recived:"+response+"@processEmailEntry:UrlExecutorService");
		        	
		        	break;
		        }
		        try {
		            //System.out.println("Going Going");
		            Thread.sleep(500);
		        }
		        catch (InterruptedException t) {}
		    }
		    thread.interrupt();
		    
		    
			
		}
		if (deliveryMode.equals(FACEBOOK_DELIVERYMODE_FLAG)) {
			
			Thread thread = new Thread(new Runnable() {
		        @Override
		        public void run() {
		        	logInfo("Facebook URL Entry Started:PicId:"+userDTO.getPicId()+"@processEmailEntry:UrlExecutorService");
		        	boolean response=new FacebookUrlBuilder(userDTO).executeQueryString(machineId);
		        	String responseLog="Facebook Entry Response:"+response;
		        	ServicesLauncher.showMessage("Facebook Entry Response:"+response);
		        	logInfo("Facebook URL Entry Successfully Stored:"+userDTO.getPicId()+ ":Facebook Url Entry Response:"+response+"@processEmailEntry:UrlExecutorService");
		        	
		        	System.out.println(responseLog);
		        }
		    });
		    thread.start();
		    long endTimeMillis = System.currentTimeMillis() + 20000;
		    while (thread.isAlive()) {
		    	//System.out.println("you are alive");
		        if (System.currentTimeMillis() > endTimeMillis) {
		        	logInfo(PiquorTimestamp.getTimeStamp()
							+ "Url Connection Timed Out facebook [HttpMail.remoteEntry]");
		        	System.out.print("Url connection Timed Out");
		        	ServicesLauncher.showMessage(">> URL Connection Timed Out");
		        	boolean response=errorUpdater.emailErorr(picId);
		        	
		        	logInfo("Facebook URL Entry Error Set for PicId:"+userDTO.getPicId()+": Response Recived:"+response+"@processEmailEntry:UrlExecutorService");
		        	
		        	break;
		        }
		        try {
		            //System.out.println("Going Going");
		            Thread.sleep(500);
		        }
		        catch (InterruptedException t) {}
		    }
		    thread.interrupt();
			
		}
		
		if (deliveryMode.equals(EMAIL_DELIVERYMODE_FLAG)) {
			
			Thread thread = new Thread(new Runnable() {
		        @Override
		        public void run() {
		        	logInfo("Email URL Entry Started:PicId:"+userDTO.getPicId()+"@processEmailEntry:UrlExecutorService time:"+PiquorTimestamp.getTimeStamp());
		        	boolean response=new EmailEntryHandler(userDTO).executeEmailEntryServices(machineId);	
		        	String responseLog="Email Entry Response:"+response;
		        	ServicesLauncher.showMessage((responseLog));
		        	logInfo("Email URL Entry Processed:PicId:"+userDTO.getPicId()+" :Recieved Response:"+responseLog+" @processEmailEntry:urlExecutionService time:"+PiquorTimestamp.getTimeStamp());
		        	
		        }
		    });
		    thread.start();
		    
		    
		    long endTimeMillis = System.currentTimeMillis() + 20000;
		    while (thread.isAlive()) {
		    	//System.out.println("you are alive");
		        if (System.currentTimeMillis() > endTimeMillis) {
		        	 
		        	System.out.print("Url connection Timed Out");
		        	ServicesLauncher.showMessage(" URL Connection Timed Out");
		        	boolean response=errorUpdater.emailErorr(picId);
		        	
		        	logInfo("Email URL Entry Error Set for PicId:"+userDTO.getPicId()+": Response Recived:"+response+"@processEmailEntry:UrlExecutorService time:"+PiquorTimestamp.getTimeStamp());
				       
		        	break;
		        }
		        try {
		            //System.out.println("Going Going");
		            Thread.sleep(500);
		        }
		        catch (InterruptedException t) {}
		    }
		    thread.interrupt();
			
		}
		
		if (deliveryMode.contains("#")) {	
			
			Thread thread = new Thread(new Runnable() {
		        @Override
		        public void run() {
		        	
		        	logInfo(" Processing Universal Entry Services for PicId:"+userDTO.getPicId()+" @processEmailEntry:urlExecutorServoce");
		        	boolean response=new UniversalEntryHandler(userDTO).executeUniversalEntryServices(machineId);
		        	String responseLog="Universal Entry Response:"+response;
		        	ServicesLauncher.showMessage(responseLog);
		            logInfo(responseLog);
		        }
		    });
		    thread.start();
		    long endTimeMillis = System.currentTimeMillis() + 40000;
		    while (thread.isAlive()) {
		    	//System.out.println("you are alive");
		        if (System.currentTimeMillis() > endTimeMillis) {
		        	logInfo(PiquorTimestamp.getTimeStamp()
							+ "Url Connection Timed Out Universal Controller [HttpMail.remoteEntry]");
		        	System.out.print("Url connection Timed Out");
		        	ServicesLauncher.showMessage("URL Connection Timed Out");
		        	boolean response=errorUpdater.emailErorr(picId);
		        	logInfo("Universal URL Entry Error Set for PicId:"+userDTO.getPicId()+": Response Recived:"+response+"@processEmailEntry:UrlExecutorService time:"+PiquorTimestamp.getTimeStamp());
		        	
		        	break;
		        }
		        try {
		            //System.out.println("Going Going");
		            Thread.sleep(500);
		        }
		        catch (InterruptedException t) {}
		    }
		    
		    thread.interrupt();
			
		}else if(!matchDeileveryModes(deliveryMode, CompletedeliveryModesArray))
		{
			System.out.println(">>Invalid Delievery Mode:"+deliveryMode);
			ServicesLauncher.showMessage(">>Invalid Delievery Mode:"+deliveryMode);
			errorUpdater.removeAnyError(picId);
			
		}

	}
	public  boolean matchDeileveryModes(String inputString, String[] items)
	{
	    for(int i =0; i < items.length; i++)
	    {
	        if(inputString.contains(items[i]))
	        {
	            return true;
	        }
	    }
	    return false;
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}

}

