package com.services.deliverymode.urlbuilder;

import java.util.ArrayList;
import java.util.StringTokenizer;

import android.util.Log;

import com.piquor.dto.UserTableDTO;

public class UniversalMultiEmailHandler {

	UserTableDTO userDTO;
	private String LOG_TAG = "UniversalMultiEmailHandler";
	public UniversalMultiEmailHandler(UserTableDTO userDTO)
	{
		this.userDTO=userDTO;
	}
	
	public boolean executeMailerServices(String machineId)
	{
		
		String emailIds=userDTO.getEmail().trim();
		StringTokenizer emailtoken = new StringTokenizer(emailIds, ";");
	    String middleWhiteSpaceRemovedEmailIds = "";
	    boolean responseEmailEntry=false;
	    boolean responseUniversalEntry=false;
		while (emailtoken.hasMoreElements()) {

			if (!middleWhiteSpaceRemovedEmailIds.equals("")) {
				middleWhiteSpaceRemovedEmailIds += ";";
			}
			String mainEmail = emailtoken.nextToken().trim();
			middleWhiteSpaceRemovedEmailIds += mainEmail;
		
		}
		emailIds= middleWhiteSpaceRemovedEmailIds;
		
		
		StringTokenizer emailListoken = new StringTokenizer(emailIds, ";");
		
		logInfo("ArrayList prepared  for PicId:"+userDTO.getPicId()+" for EmailIds "+emailIds+"@executeMailerServices:UniversalMultiEmailHandler");
		ArrayList<String> emailIdList=new ArrayList<String>();
		while (emailListoken.hasMoreElements()) {
			
			
			emailIdList.add(emailListoken.nextElement().toString().trim());
		}
		for(int emailIdIndex=0;emailIdIndex<emailIdList.size();emailIdIndex++)
		{
			
			String emailId=emailIdList.get(emailIdIndex);
			if(emailIdIndex==0)
			{
			    userDTO.setEmail(emailId);
			    logInfo("Registering first EmailId:"+emailId+" as universal entry holder for PicId"+userDTO.getPicId());
			    responseUniversalEntry= new UniversalUrlBuilder(userDTO).executeQueryString(machineId);

			}else
			{
				if(!emailId.equals(""))
				{
					userDTO.setEmail(emailId);
					userDTO.setDelieveryMode("email");
					logInfo("Registering "+emailIdIndex+" EmailId:"+emailId+" as Email Entry Holder for PicId"+userDTO.getPicId());
					responseEmailEntry= new EmailUrlBuilder(userDTO).executeQueryString(machineId);
				}
			}
		}
		if(responseEmailEntry&&responseUniversalEntry)
		{
			return true;
		}else
		{
			return false;
		}
		
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG,info);
	}
	
}
