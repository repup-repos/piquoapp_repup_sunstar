package com.services.deliverymode.userdto;

public class UserParamsDTO {
	private String photoCaptureDate;
	private String userGender;
	private String photoPicId;
	private String userName;
	private String userEmail;
	private String userPhone;
	private String userDelieveryMode;
	private String userCampaignId;
	private String userDOB;
	private String userPhotoPermissionSocialMedia;
	private String userFbUserName;
	private String userFbUserId;
	private String userFbName;
	private String userTwitterId;
	private String userTwitterName;
	private String userTwitterFollowers;
	private String userTwitterFriendsCount;
	private String userTwitterPostText;
	
	
	public UserParamsDTO() {
	}

	public String getPhotoCaptureDate() {
		return photoCaptureDate;
	}

	public void setPhotoCaptureDate(String photoCaptureDate) {
		this.photoCaptureDate = photoCaptureDate;
	}

	public String getUserGender() {
		return userGender;
	}

	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	
	public String getUserDOB() {
		return userDOB;
	}

	public void setUserDOB(String userDOB) {
		this.userDOB = userDOB;
	}


	public String getUserDelieveryMode() {
		return userDelieveryMode;
	}

	public void setUserDelieveryMode(String userDelieveryMode) {
		this.userDelieveryMode = userDelieveryMode;
	}

	public String getUserCampaignId() {
		return userCampaignId;
	}

	public void setUserCampaignId(String userCampaignId) {
		this.userCampaignId = userCampaignId;
	}

	public String getUserPhotoPermissionSocialMedia() {
		return userPhotoPermissionSocialMedia;
	}

	public void setUserPhotoPermissionSocialMedia(
			String userPhotoPermissionSocialMedia) {
		this.userPhotoPermissionSocialMedia = userPhotoPermissionSocialMedia;
	}

	public String getPhotoPicId() {
		return photoPicId;
	}

	public void setPhotoPicId(String photoPicId) {
		this.photoPicId = photoPicId;
	}

	public String getUserFbUserName() {
		return userFbUserName;
	}

	public void setUserFbUserName(String userFbUserName) {
		this.userFbUserName = userFbUserName;
	}

	public String getUserFbUserId() {
		return userFbUserId;
	}

	public void setUserFbUserId(String userFbUserId) {
		this.userFbUserId = userFbUserId;
	}

	public String getUserFbName() {
		return userFbName;
	}

	public void setUserFbName(String userFbName) {
		this.userFbName = userFbName;
	}

	public String getUserTwitterId() {
		return userTwitterId;
	}

	public void setUserTwitterId(String userTwitterId) {
		this.userTwitterId = userTwitterId;
	}

	public String getUserTwitterName() {
		return userTwitterName;
	}

	public void setUserTwitterName(String userTwitterName) {
		this.userTwitterName = userTwitterName;
	}

	public String getUserTwitterFollowers() {
		return userTwitterFollowers;
	}

	public void setUserTwitterFollowers(String userTwitterFollowers) {
		this.userTwitterFollowers = userTwitterFollowers;
	}

	public String getUserTwitterFriendsCount() {
		return userTwitterFriendsCount;
	}

	public void setUserTwitterFriendsCount(String userTwitterFriendsCount) {
		this.userTwitterFriendsCount = userTwitterFriendsCount;
	}

	public String getUserTwitterPostText() {
		return userTwitterPostText;
	}

	public void setUserTwitterPostText(String userTwitterPostText) {
		this.userTwitterPostText = userTwitterPostText;
	}
}
