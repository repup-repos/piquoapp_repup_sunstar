package com.piquor.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.modal.ReviewEntryModal;
import com.piquor.piquortabapp.R;
import com.piquor.utility.AlertDialogManager;
import com.piquor.utility.InternetConnectionDetector;

public class FragmentTripAdvisorReviewEntry extends Fragment implements OnClickListener{

	
//	private EditText editTextName;
	private EditText editTextEmail;
//	private EditText editTextRoomNo;
	private EditText editTextReviewTitle;
	private RadioGroup rgSortOfVisit;
	private RatingBar rtnCheckIn;
	private RatingBar rtnRooms;
	private RatingBar rtnCleanliness;
	private RatingBar rtnAmneties;
	private RatingBar rtnFoodandBeverages;
	private RatingBar rtnWifiandInternt;
	private RatingBar rtnCheckout;
	private RatingBar rtnConceirge;
	private ImageButton btnBack;
	private ImageButton btnSubmit;
	private ReviewEntryModal reviewEntryModal;
	private ReviewEntryDTO reviewEntryDTO;
	private FragementChangeListener fragmentChangeListener;
	private String LOG_TAG = "FragmentTripAdvisorReviewEntry";
	private FragmentActivity fragmentActivity;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		
		fragmentChangeListener = (FragementChangeListener) getActivity();
		fragmentActivity = getActivity();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_reviewentry, container,false);
		
		/*editTextName = (EditText) view.findViewById(R.id.editTextName);
		editTextName.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if(!hasFocus)
				{
					hideKeyboard(v);
				}
			}
		});*/
		editTextEmail = (EditText) view.findViewById(R.id.editTextEmailId);
		/*editTextEmail.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if(!hasFocus)
				{
					hideSoftKeyboard();
				}
			}
		});*/
	/*	editTextRoomNo = (EditText) view.findViewById(R.id.editTextRoomNo);
		editTextRoomNo.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if(!hasFocus)
				{
					hideKeyboard(v);
				}
			}
		});*/
		editTextReviewTitle = (EditText) view.findViewById(R.id.editTextReviewTitle);
		/*editTextReviewTitle.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if(!hasFocus)
				{
					hideKeyboard(v);
				}
			}
		});*/
		
		
		rgSortOfVisit = (RadioGroup) view.findViewById(R.id.groupSortOfVisit);
		
		rtnCheckIn = (RatingBar) view.findViewById(R.id.ratingCheckIn);
		rtnRooms = (RatingBar) view.findViewById(R.id.ratingRoomRating);
		rtnCleanliness = (RatingBar) view.findViewById(R.id.ratingCleanliness);
		rtnAmneties = (RatingBar) view.findViewById(R.id.ratingAmneties);
		rtnFoodandBeverages = (RatingBar) view.findViewById(R.id.ratingFoodAndBeverages);
		rtnWifiandInternt = (RatingBar) view.findViewById(R.id.ratingWifiandInternet);
		rtnCheckout = (RatingBar) view.findViewById(R.id.ratingCheckOut);
		rtnConceirge = (RatingBar) view.findViewById(R.id.ratingConcierge);
		
		btnBack = (ImageButton) view.findViewById(R.id.imBtnBackReviewEntry);
		btnSubmit = (ImageButton) view.findViewById(R.id.imBtnSubmtReviewEntry);
		
		
		btnBack.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
		reviewEntryModal = new ReviewEntryModal();
		setUpUIForEditText(view);
		return view;
	}

	
	public void setUpUIForEditText(View view)
	{
		 if(!(view instanceof EditText)) {

		        view.setOnTouchListener(new OnTouchListener() {

		            public boolean onTouch(View v, MotionEvent event) {
		                hideSoftKeyboard();
		                return false;
		            }

		        });
		    }

		    //If a layout container, iterate over children and seed recursion.
		    if (view instanceof ViewGroup) {

		        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

		            View innerView = ((ViewGroup) view).getChildAt(i);

		            setUpUIForEditText(innerView);
		        }
		    }
	}
	
	@Override
	public void onClick(View v) {
		
		if(v.getId()==R.id.imBtnBackReviewEntry)
		{
			goBackFromReviewEntry();
		}else if(v.getId()==R.id.imBtnSubmtReviewEntry)
		{
			if(checkInternetConnectivity())
			{
				goReviewEntrySubmission();
			}
		}
	}

	private void goBackFromReviewEntry() {
		
		fragmentChangeListener.navigateToReviewMediumSelection();
	}

	private void goReviewEntrySubmission() {
		
		
		String response = reviewEntryModal.setReviewEntryTextData(editTextReviewTitle.getText().toString().trim(), editTextEmail.getText().toString().trim());
		if(response.trim().equals(""))
		{
			Log.d(LOG_TAG, "Fetching review ratings here");
			int checkInRating = (int) rtnCheckIn.getRating();
			int roomRating = (int) rtnRooms.getRating();
			int cleanlinessRating = (int)rtnCleanliness.getRating();
			int amnetiesRating = (int) rtnAmneties.getRating();
			int foodandbeverageRating = (int) rtnFoodandBeverages.getRating();
			int wifiandInternetRating = (int) rtnWifiandInternt.getRating();
			int checkOutRating = (int) rtnCheckout.getRating();
			int conceirgeRating = (int) rtnConceirge.getRating();
			
			Log.d(LOG_TAG, "Check in rating: "+checkInRating+" Room Rating: "+roomRating+
					" CleanlinessRating: "+cleanlinessRating+" Amneties Ratings:"+amnetiesRating
					+" Food and Beverage: "+foodandbeverageRating+" Wifi and Internet "+wifiandInternetRating
					+" Checkout Rating "+checkOutRating+" ConceirgeRating "+conceirgeRating
					);
			
			
			response = reviewEntryModal.setRatings((int)rtnCheckIn.getRating(),(int) rtnRooms.getRating(),(int) rtnCleanliness.getRating(), 
					(int)rtnAmneties.getRating(),(int) rtnFoodandBeverages.getRating(),(int) rtnWifiandInternt.getRating(), 
					(int)rtnCheckout.getRating(), (int)rtnConceirge.getRating());
			
			Log.d(LOG_TAG, "Response recieve is:"+response);
			if(response.trim().equals(""))
			{
				
				response = reviewEntryModal.setSortOfVisit(rgSortOfVisit);
				Log.d(LOG_TAG, "Response Recieved:"+response);
				if(response.trim().equals(""))
				{
					reviewEntryDTO = reviewEntryModal.getReviewEntryDTO();
					Log.d(LOG_TAG, "Overall Rating:"+reviewEntryDTO.getReviewentry_overallrating());
					fragmentChangeListener.navigateToAdaptiveQuesitonScreen(reviewEntryDTO);
				}else
				{
					showMessage(response);
				}
				
			}else
			{
				showMessage(response);
			}
		}else
		{
			showMessage(response);
		}
	}
	
	private void showMessage(String message)
	{
		new AlertDialogManager().showAlertDialog(getActivity(), "Info", message, false);

	}
	
	/*public void hideKeyboard(View view) {
		
        InputMethodManager inputMethodManager =(InputMethodManager)fragmentActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }*/
	
	public  void hideSoftKeyboard() {
	    InputMethodManager inputMethodManager = (InputMethodManager)  fragmentActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(fragmentActivity.getCurrentFocus().getWindowToken(), 0);
	}
	
	private boolean checkInternetConnectivity() {
		InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(
				getActivity());
		if (!internetConnectionDetector.isConnectingToInternet()) {
			new AlertDialogManager().showAlertDialog(getActivity(),
					"Intenet Connection Failed",
					"Please Ensure Internet Connectivity", false);
			return false;
		}
		return true;
	}
	
}
