package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.CAMPAIGN_TABLE_COLUMNS;
import com.piquor.dto.CampaignInfoDTO;

public class CampaignInfoDAO implements CAMPAIGN_TABLE_COLUMNS {

	
	public static String getInsertCampaignInformationQuery(CampaignInfoDTO campaignInfoDTO)
	{
		String CAMPAIGN_INSERT_QUERY = "INSERT INTO " + CAMPAIGN_TABLE_NAME + "(";
		
		CAMPAIGN_INSERT_QUERY += COLUMN_NAME_CAMPAIGNID + ","+ COLUMN_NAME_MACHINEID+","+COLUMN_NAME_MAILERPAUSETIME+")";
	
		CAMPAIGN_INSERT_QUERY += " VALUES ('"+campaignInfoDTO.getCampaignId().trim()+"','"+campaignInfoDTO.getMachineId().trim()+"','"+campaignInfoDTO.getMailerResponseTime().trim()+"');";
		
		return CAMPAIGN_INSERT_QUERY;
	}
	
	public static String getUpdateCampaignInformationQuery(CampaignInfoDTO campaignDTO)
	{
		String CAMPAIGN_UPDATE_QUERY = "UPDATE " + CAMPAIGN_TABLE_NAME + " SET ";
		CAMPAIGN_UPDATE_QUERY += COLUMN_NAME_CAMPAIGNID + "='"+campaignDTO.getCampaignId()+"',"
				+ COLUMN_NAME_MACHINEID+"='"+campaignDTO.getMachineId()+"',"+COLUMN_NAME_MAILERPAUSETIME+"='"+campaignDTO.getMailerResponseTime()+"'";
		return CAMPAIGN_UPDATE_QUERY;
	}
	
	public static String getCampaignInfomrationQuery()
	{
		String selectQuery = "SELECT  "+COLUMN_NAME_CAMPAIGNID+","+COLUMN_NAME_MACHINEID+","+COLUMN_NAME_MAILERPAUSETIME+" FROM " + CAMPAIGN_TABLE_NAME;
		return selectQuery;
		
	}
	
	public static String getCampaignTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + CAMPAIGN_TABLE_NAME;
		return query;
	}
	
	public static String getCampaignTableCreateQuery()
	{
		String CREATE_CAMPAIGN_TABLE = "CREATE TABLE ["+CAMPAIGN_TABLE_NAME+"] "
				+ "(["+COLUMN_NAME_CAMPAIGNID+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINEID+"] varchar(50) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MAILERPAUSETIME+"] varchar(30) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_ID+"] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT"
				+ ")";
		
		return CREATE_CAMPAIGN_TABLE;
	}
	
	
	
	
}
