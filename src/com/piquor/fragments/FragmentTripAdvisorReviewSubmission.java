package com.piquor.fragments;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.dto.TripAdisorEssentialsDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;

public class FragmentTripAdvisorReviewSubmission extends Fragment implements
		FRAGMENT_SCREEN_NAMES, ACTIVITY_FLAGS {

	private String LOG_TAG = "FragmentTripAdvisorFinalScreen";
	private ProgressDialog progress;
	private boolean isJavaScriptLoaded = false;
	private static final String target_url="https://www.tripadvisor.in/UserReviewEdit-g304551-d6915868-a_placetype.10022-a_referredFromLocationSearch.true-a_ReviewName.-e-wpage1-Fire_and_Ice_Rooftop_Restaurant-New_Delhi_National_Capital_Territory_of_Delhi.html";
	private static final String target_url_prefix="www.tripadvisor.in";
	private Context mContext;
	private WebView mWebview;
	private WebView mWebviewPop;
	private FrameLayout mContainer;
	private long mLastBackPressTime = 0;
	private Toast mToast;
	private int status = 0;
	private final String FLAG_VISIT_REASON_COUPLES = "Couples";
	private final String FLAG_VISIT_REASON_FAMILTY = "Family";
	private final String FLAG_VISIT_REASON_FRIENDS = "Friends";
	private final String FLAG_VISIT_REASON_SOLO = "Solo";
	private final String FLAG_VISIT_REASON_BUSINESS = "Business";
	
	private FragementChangeListener fragmentChangeListener;

	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
	}

	@Override
	public View onCreateView(LayoutInflater inflator, ViewGroup parent,
			Bundle savedInstance) {

		View view = inflator.inflate(R.layout.fragmenttareviewsubmission,
				parent, false);

		isJavaScriptLoaded = false;
		
	/*	Button nextButton = (Button) view
				.findViewById(R.id.btnMenuButtonFinalScreen);
		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				goToNextScreen();
			}

		});*/
		
		ImageButton nextButton = (ImageButton) view.findViewById(R.id.fragmentrestaurant_reviewsubmissiondone);
		nextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				goToNextScreen();
			}
		});
		
		progress = new ProgressDialog(getActivity());
		progress.setMessage("Loading Application ...");
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.setCanceledOnTouchOutside(false);
		progress.show();
		Log.d(LOG_TAG, "Initialized tripadvisor review entries here");
		
		mContext=getActivity().getApplicationContext();
		mContext.deleteDatabase("webview.db");
		mContext.deleteDatabase("webviewCache.db");
		CookieSyncManager.createInstance(getActivity());         
		CookieManager cookieManager = CookieManager.getInstance();        
		cookieManager.removeAllCookie();
		clearCache(mContext, 10);
		mWebview = (WebView) view.findViewById(R.id.webview_tareviewsubmisson);
		    //mWebviewPop = (WebView) findViewById(R.id.webviewPop);
		mContainer = (FrameLayout) view.findViewById(R.id.frame_tareviewsubmission);
		mWebview.clearCache(true);
		mWebview.clearHistory();
		mWebview.clearFormData();
		WebSettings webSettings = mWebview.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setAppCacheEnabled(false);
		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setSupportMultipleWindows(true);
		webSettings.setUserAgentString("Mozilla/5.0 (Linux; Android 5.0.2; SM-T531 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.93 Safari/537.36");
		mWebview.setWebViewClient(new UriWebViewClient());
	    mWebview.setWebChromeClient(new UriChromeClient());
	 
		//new TripAdvisorWebViewProcess().execute();
		
		
	//	String url = TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURENT_URL().trim();
		String url = "http://whatsmyuseragent.com/";
		mWebview.loadUrl(url);
	
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener) getActivity();

	}
	
	protected void goToNextScreen() {
		
		saveEntryOnDatabase();
		try {
			setMobileDataEnabledFromLollipop(getActivity(),false);
			Log.d(LOG_TAG, "Set mobile data enabled is false");
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception at mobile data enabled: "+e.getMessage());
		}
		fragmentChangeListener.navigateToReviewMediumSelection();
	
		
	}

	private class UriWebViewClient extends WebViewClient {
	    @Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        String host = Uri.parse(url).getHost();
	        //Log.d("shouldOverrideUrlLoading", url);
	        Log.d("OverrideUrl", "Host Url:"+host+":Url Recieved:"+url);
	        if (host.equals(target_url_prefix)) 
	        {
	            // This is my web site, so do not override; let my WebView load
	            // the page
	            if(mWebviewPop!=null)
	            {
	                mWebviewPop.setVisibility(View.GONE);
	                mContainer.removeView(mWebviewPop);
	                mWebviewPop=null;
	            }
	            return false;
	        }

	        if(host.equals("m.facebook.com"))
	        {
	            return false;
	        }
	        // Otherwise, the link is not for a page on my site, so launch
	        // another Activity that handles URLs
	        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	        startActivity(intent);
	        return true;
	    }

	    @Override
	    public void onReceivedSslError(WebView view, SslErrorHandler handler,
	            SslError error) {
	        Log.d("onReceivedSslError", "onReceivedSslError");
	        
	        
	        //super.onReceivedSslError(view, handler, error);
	    }
	    
	    @Override
	    public void onPageFinished(WebView view, String url) {
	    	super.onPageFinished(view, url);
	    	
	    	if(progress.isShowing())
	    	{
	    		progress.dismiss();
	    	}
	    	String host = Uri.parse(url).getHost();
	    	 if (host.equals(target_url_prefix)) 
		        {
		            // This is my web site, so do not override; let my WebView load
		            // the page
	    		 	Log.d(LOG_TAG,"Apply JavaScript Here");
	    		 	
	    		 	loadJavaScriptHere();
		        }

		        if(host.equals("m.facebook.com"))
		        {
		        	
		        }
	    	
	    }

		
	}

	class UriChromeClient extends WebChromeClient {

	    @Override
	    public boolean onCreateWindow(WebView view, boolean isDialog,
	            boolean isUserGesture, Message resultMsg) {
	        mWebviewPop = new WebView(mContext);
	        mWebviewPop.setVerticalScrollBarEnabled(false);
	        mWebviewPop.setHorizontalScrollBarEnabled(false);
	        mWebviewPop.setWebViewClient(new UriWebViewClient());
	        mWebviewPop.getSettings().setJavaScriptEnabled(true);
	        mWebviewPop.getSettings().setSavePassword(false);
	        mWebviewPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
	                ViewGroup.LayoutParams.MATCH_PARENT));
	        mContainer.addView(mWebviewPop);
	        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
	        transport.setWebView(mWebviewPop);
	        resultMsg.sendToTarget();
	        status = 1;
	        return true;
	    }

	    @Override
	    public void onCloseWindow(WebView window) {
	        Log.d("onCloseWindow", "called");
	    }

	}
	
		
	private void loadJavaScriptHere() {
		
		
		mWebview.post(new Runnable() {

			@Override
			public void run() {

				if (!isJavaScriptLoaded) {

					if (TripAdisorEssentialsDTO
							.getTRIP_ADVISOR_PLACE_TYPE()
							.equals(PLACE_HOTEL_NAME)) {

						Calendar now = Calendar.getInstance();
						int year = now.get(Calendar.YEAR);
						int month = now.get(Calendar.MONTH);
						month = month + 1;
						
						int tripAdvisorRating = (int)TripAdisorEssentialsDTO.getTRIP_ADVISOR_OVERALL_RATING();
						Log.d(LOG_TAG,"Tripadvisor overall ratings: "+tripAdvisorRating);
						
						
						String javascript = "javascript: (function() {document.getElementsByClassName(\"bigRating\")[0].getElementsByTagName(\"label\")[0].removeClass(\"visuallyHidden\");}) ();";
						
						mWebview.loadUrl(javascript);
						
						
						javascript = "javascript: (function() {document.getElementById('qid10').value = "
								+ tripAdvisorRating
								+ ";}) ();";
						mWebview.loadUrl(javascript);
						
						//document.getElementById('qid10').value = 2
						javascript = "javascript: (function() {document.getElementById(\"bubble_rating\")"
								+ ".addClass(\"b"
						+tripAdvisorRating*10+"\");}) ();";
						
						mWebview.loadUrl(javascript);
						//document.getElementById("bubble_rating").addClass("b20")
						javascript = "javascript: (function() {"
								+ "document.getElementById(\"bubble_rating\")"
								+ ".getElementsByTagName(\"img\")[0].click();}) ();";
						
					    mWebview.loadUrl(javascript);
								
						
						Log.d(LOG_TAG, "JavaScript::"+javascript);
						
						javascript = "javascript: (function() {document.getElementsByName('qid373').checked = true;}) ();";
						
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('ReviewTitle').value = '"
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_TITILE()
								+ "';}) ();";
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('ReviewText').value = '"
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_DESC()
								+ "';}) ();";
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('qid200').value = '"
								+ TripAdisorEssentialsDTO
										.getTRIP_SHOR_OF_VISIT()
								+ "';}) ();";
						
						mWebview.loadUrl(javascript);
						///here value matters
						String sortOfVisit = TripAdisorEssentialsDTO.getTRIP_SHOR_OF_VISIT().trim();
						if(sortOfVisit.equals(FLAG_VISIT_REASON_BUSINESS))
						{
							javascript = "javascript: (function() {"
									+ "document.getElementsByClassName('jfy_cloud')[0].click()"
									+ ";}) ();";
							//javascript = document.getElementsByClassName("jfy_cloud")[0]
						}else if(sortOfVisit.equals(FLAG_VISIT_REASON_COUPLES))
						{
							javascript = "javascript: (function() {"
									+ "document.getElementsByClassName('jfy_cloud')[1].click()"
									+ ";}) ();";
						}else if(sortOfVisit.equals(FLAG_VISIT_REASON_FAMILTY))
						{
							javascript = "javascript: (function() {"
									+ "document.getElementsByClassName('jfy_cloud')[2].click()"
									+ ";}) ();";
						}else if(sortOfVisit.equals(FLAG_VISIT_REASON_FRIENDS))
						{
							javascript = "javascript: (function() {"
									+ "document.getElementsByClassName('jfy_cloud')[3].click()"
									+ ";}) ();";
						}else if(sortOfVisit.equals(FLAG_VISIT_REASON_SOLO))
						{
							javascript = "javascript: (function() {"
									+ "document.getElementsByClassName('jfy_cloud')[4].click()"
									+ ";}) ();";
						}
						
						
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('trip_date_month_year').value = '"
								+ month
								+ ","
								+ year
								+ "';}) ();";
						mWebview.loadUrl(javascript);

						if (TripAdisorEssentialsDTO
								.getIsThisHotelBoutique()
								.equals("yes")) {
							javascript = "javascript:  (function() {document.getElementsByName('war_boutique')[0].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						} else if (TripAdisorEssentialsDTO
								.getIsThisHotelBoutique()
								.equals("no")) {
							javascript = "javascript: (function() {document.getElementsByName('war_boutique')[1].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						} else if (TripAdisorEssentialsDTO
								.getIsThisHotelBoutique()
								.equals("notsure")) {
							javascript = "javascript: (function() {document.getElementsByName('war_boutique')[2].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						}

						if (TripAdisorEssentialsDTO
								.getIsThisHotelTryndy().equals(
										"yes")) {
							javascript = "javascript: (function() {document.getElementsByName('war_trendy')[0].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						} else if (TripAdisorEssentialsDTO
								.getIsThisHotelTryndy().equals(
										"no")) {
							javascript = "javascript: (function() {document.getElementsByName('war_trendy')[1].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						} else if (TripAdisorEssentialsDTO
								.getIsThisHotelTryndy().equals(
										"notsure")) {
							javascript = "javascript: (function() {document.getElementsByName('war_trendy')[2].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						}

						if (TripAdisorEssentialsDTO
								.getIsThisHotelCityCenter()
								.equals("yes")) {
							javascript = "javascript: (function() {document.getElementsByName('war_citycenter')[0].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						} else if (TripAdisorEssentialsDTO
								.getIsThisHotelCityCenter()
								.equals("no")) {
							javascript = "javascript: (function() {document.getElementsByName('war_citycenter')[1].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						} else if (TripAdisorEssentialsDTO
								.getIsThisHotelCityCenter()
								.equals("notsure")) {
							javascript = "javascript: (function() {document.getElementsByName('war_citycenter')[2].checked = 'true';}) ();";
							mWebview.loadUrl(javascript);
						}

						javascript = "javascript: (function() {document.getElementById('qid12').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_SERVICES_RATING()
								+ ";}) ();";

						mWebview.loadUrl(javascript);

						javascript = "javascript: (function() {document.getElementById('qid14').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_CLEANLINESS_RATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);

						javascript = "javascript: (function() {document.getElementById('qid190').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_SLEEP_QWALITY_RATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);

						String hotelAreaQuestion = TripAdisorEssentialsDTO
								.getTRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR();
						if (!hotelAreaQuestion.equals("")) {
							StringTokenizer st2 = new StringTokenizer(
									hotelAreaQuestion, "#");

							while (st2.hasMoreElements()) {
								String value = st2
										.nextElement()
										.toString().trim();
								if (!value.trim().equals("")) {
									if (value
											.equals("Shopping")) {
										javascript = "javascript: (function() {document.getElementsByClassName('jfy_cloud')[0].click();}) ();";
										mWebview.loadUrl(javascript);
									} else if (value
											.equals("Outdoor activities")) {
										javascript = "javascript: (function() {document.getElementsByClassName('jfy_cloud')[1].click();}) ();";
										mWebview.loadUrl(javascript);
									} else if (value
											.equals("Nightlife")) {
										javascript = "javascript: (function() {document.getElementsByClassName('jfy_cloud')[2].click();}) ();";
										mWebview.loadUrl(javascript);
									} else if (value
											.equals("Restaurants")) {
										javascript = "javascript: (function() {document.getElementsByClassName('jfy_cloud')[3].click();}) ();";
										mWebview.loadUrl(javascript);
									} else if (value
											.equals("Museums")) {
										javascript = "javascript: (function() {document.getElementsByClassName('jfy_cloud')[4].click();}) ();";
										mWebview.loadUrl(javascript);
									}
								}
							}
						}
						String noise = TripAdisorEssentialsDTO
								.getTRIP_ADVISOR_HOTEL_AREA_NOISE()
								.trim();
						if (noise.equals("Quiet")) {

							javascript = "javascript: (function() {document.getElementsByClassName('tag')[0].click();}) ();";
							mWebview.loadUrl(javascript);
						} else if (noise.equals("Average")) {
							javascript = "javascript: (function() {document.getElementsByClassName('tag')[1].click();}) ();";
							mWebview.loadUrl(javascript);
						} else if (noise.equals("Loud")) {
							javascript = "javascript: (function() {document.getElementsByClassName('tag')[2].click();}) ();";
							mWebview.loadUrl(javascript);
						}

						javascript = "javascript: (function() {document.getElementById('noFraud').checked = true;}) ();";
						mWebview.loadUrl(javascript);
						
						javascript = "javascript: (function() {document.getElementById('SUBMIT').getElementsByTagName('span')[0].click();}) ();";
						
						javascript = "javascript: (function() {document.getElementById(\"USER_REVIEW_FORM\").submit();}) ();";
						
						Log.d(LOG_TAG, "Submitting a form here");
						
						mWebview.loadUrl(javascript);
					} else if (TripAdisorEssentialsDTO
							.getTRIP_ADVISOR_PLACE_TYPE()
							.equals(PLACE_RESTAURANT_NAME)) {

						Calendar now = Calendar.getInstance();
						int year = now.get(Calendar.YEAR);
						int month = now.get(Calendar.MONTH);
						month = month + 1;
						String javascript = "javascript: (function() {document.getElementById('qid10').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_OVERALL_RATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementsByName('qid373').checked = true;}) ();";
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('ReviewTitle').value = '"
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_TITILE()
								+ "';}) ();";
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('ReviewText').value = '"
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_DESC()
								+ "';}) ();";
						
						
						Log.d(LOG_TAG, "Restaurant Information:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_DESC());
						
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('qid371').value = '"
								+ TripAdisorEssentialsDTO
										.getTRIP_SHOR_OF_VISIT()
								+ "';}) ();";
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('qid391').value = '"
								+ TripAdisorEssentialsDTO
										.getTRIP_WERE_YOU_HERE_FOR()
								+ "';}) ();";
						mWebview.loadUrl(javascript);
						javascript = "javascript: (function() {document.getElementById('trip_date_month_year').value = '"
								+ month
								+ ","
								+ year
								+ "';}) ();";
						mWebview.loadUrl(javascript);

						javascript = "javascript: (function() {document.getElementById('qid12').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_SERVICES_RATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);

						javascript = "javascript: (function() {document.getElementById('qid300').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_FOOD_RATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);

						javascript = "javascript: (function() {document.getElementById('qid13').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_VALUE_RATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);

						javascript = "javascript: (function() {document.getElementById('qid301').value = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);

						int costCounter = -1;
						// loading cost per person data
						if (TripAdisorEssentialsDTO
								.getTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE()
								.equals("1")) {
							costCounter = 0;
						} else if (TripAdisorEssentialsDTO
								.getTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE()
								.equals("10")) {
							costCounter = 1;
						} else if (TripAdisorEssentialsDTO
								.getTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE()
								.equals("100")) {
							costCounter = 2;
						} else if (TripAdisorEssentialsDTO
								.getTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE()
								.equals("1000")) {
							costCounter = 3;
						}
						if (costCounter != -1) {
							javascript = "javascript: (function() {document.getElementsByName('qid372')["
									+ costCounter
									+ "].checked = true;}) ();";
							mWebview.loadUrl(javascript);
						}

						// /Reservation Accepted
						javascript = "javascript: (function() {document.getElementById('qid353').checked = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED()
								+ ";}) ();";
						mWebview.loadUrl(javascript);
						// Outdoor Seating
						javascript = "javascript: (function() {document.getElementById('qid337').checked = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING()
								+ ";}) ();";
						mWebview.loadUrl(javascript);
						// Scenic View
						javascript = "javascript: (function() {document.getElementById('qid339').checked = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE()
								+ ";}) ();";
						mWebview.loadUrl(javascript);
						// Delivery qid318
						javascript = "javascript: (function() {document.getElementById('qid318').checked = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY()
								+ ";}) ();";
						mWebview.loadUrl(javascript);
						// Late Night
						javascript = "javascript: (function() {document.getElementById('qid314').checked = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT()
								+ ";}) ();";
						mWebview.loadUrl(javascript);
						// Brunch
						javascript = "javascript: (function() {document.getElementById('qid311').checked = "
								+ TripAdisorEssentialsDTO
										.getTRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH()
								+ ";}) ();";
						mWebview.loadUrl(javascript);

						javascript = "javascript: (function() {document.getElementById('noFraud').checked = true;}) ();";
					
						mWebview.loadUrl(javascript);
						
						
						javascript = "javascript: (function() {document.getElementById('SUBMIT').getElementsByTagName('span')[0].onclick();}) ();";
						
						
						//javascript = "javascript: (function() {document.getElementById(\"USER_REVIEW_FORM\").submit();}) ();";
						
						Log.d(LOG_TAG, javascript);
						
						mWebview.loadUrl(javascript);
					}

					Log.d(LOG_TAG, "Url Loaded");
					isJavaScriptLoaded = true;
				}else
				{
					String javascript = "javascript: (function() {document.getElementById('SUBMIT').getElementsByTagName('span')[0].click();}) ();";
					
					mWebview.loadUrl(javascript);
				}

			}

		});
	}
	
	 int clearCacheFolder(final File dir, final int numDays) {

	    int deletedFiles = 0;
	    if (dir!= null && dir.isDirectory()) {
	        try {
	            for (File child:dir.listFiles()) {

	                //first delete subdirectories recursively
	                if (child.isDirectory()) {
	                    deletedFiles += clearCacheFolder(child, numDays);
	                }

	                //then delete the files and subdirectories in this dir
	                //only empty directories can be deleted, so subdirs have been done first
	                if (child.lastModified() < new Date().getTime() - numDays * DateUtils.DAY_IN_MILLIS) {
	                    if (child.delete()) {
	                        deletedFiles++;
	                    }
	                }
	            }
	        }
	        catch(Exception e) {
	            Log.e(LOG_TAG, String.format("Failed to clean the cache, error %s", e.getMessage()));
	        }
	    }
	    return deletedFiles;
	}

	/*
	 * Delete the files older than numDays days from the application cache
	 * 0 means all files.
	 */
	public  void clearCache(final Context context, final int numDays) {
	    Log.i(LOG_TAG, String.format("Starting cache prune, deleting files older than %d days", numDays));
	    int numDeletedFiles = clearCacheFolder(context.getCacheDir(), numDays);
	    Log.i(LOG_TAG, String.format("Cache pruning completed, %d files deleted", numDeletedFiles));
	}
	public void saveEntryOnDatabase()
	{
		ReviewEntryDTO reviewEntryDTO = new ReviewEntryDTO();
//		reviewEntryDTO.setReivewentry_configurationid(TripAdisorEssentialsDTO.getReview_campaignId());
		reviewEntryDTO.setReviewentry_campaignId(MainActivity.getCamapignInformation().getCampaignId().trim());
		Calendar cal = Calendar.getInstance();
		String DATE_FORMAT_NOW = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String date=sdf.format(cal.getTime());
		reviewEntryDTO.setReviewentry_date(date);
		reviewEntryDTO.setReviewentry_desc(TripAdisorEssentialsDTO.getTRIP_ADVISOR_DESC().trim());
		reviewEntryDTO.setReviewentry_sortofvisit(TripAdisorEssentialsDTO.getTRIP_SHOR_OF_VISIT());
		reviewEntryDTO.setReviewentry_overallrating(String.valueOf(TripAdisorEssentialsDTO.getTRIP_ADVISOR_OVERALL_RATING()));
		reviewEntryDTO.setReviewentry_title(TripAdisorEssentialsDTO.getTRIP_ADVISOR_TITILE());
		reviewEntryDTO.setReviewentry_type(PLACE_REVIEW_TYPE_TRIPADVISOR);
		reviewEntryDTO.setReviewentry_wereyouherefor(TripAdisorEssentialsDTO.getTRIP_WERE_YOU_HERE_FOR());
		
		String optionalParameter = "Hotel Optional: "
									+ "Is This is a Boutique Hotel :"+TripAdisorEssentialsDTO.getIsThisHotelBoutique()
									+" Is This is a City Center :"+TripAdisorEssentialsDTO.getIsThisHotelCityCenter()
									+" Is This hotel tryndy :"+TripAdisorEssentialsDTO.getIsThisHotelTryndy()
									+" Hotel Area Known For :"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR()
									+" Hotel Area Noise:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_HOTEL_AREA_NOISE()
									+" Services Rating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_SERVICES_RATING()
									+" Cleanliness Rating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_CLEANLINESS_RATING()
									+" Sleep Qwality Rating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_SLEEP_QWALITY_RATING()
									+" Restaurant Optional:"
									+" Atmoshphare Rating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING()
									+" Services Rating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_SERVICES_RATING()
									+" Value Rating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_VALUE_RATING()
									+" Food Rating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_FOOD_RATING()
									+" Cost Estimate:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE()
									+" Restaurant Have Brunch:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH()
									+" Restaurant Have Delivery:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY()
									+" Restaurant Have Latenight:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT()
									+" Restaurant Have Outdoorscene:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE()
									+" Restaurant Have Outdoorseating:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING()
									+" Restaurant Have Reservation accepted:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED()
									;
		reviewEntryDTO.setReviewentry_optionaldesc(optionalParameter);
		reviewEntryDTO.setReviewentry_status(String.valueOf(status));
		reviewEntryDTO.setReviewentry_timestamp(getTimeStamp());
		reviewEntryDTO.setReviewentry_emailid(TripAdisorEssentialsDTO.getRevientry_emailid());
		reviewEntryDTO.setReviewentry_platformid("1");
		reviewEntryDTO.setReviewentry_reviewUploaded("0");
		reviewEntryDTO.setReviewentry_reviewuploadStatus("0");
		MainActivity.addReviewEntriesInDatabase(reviewEntryDTO);
		
	}
	
	private String getTimeStamp()
	{
		Calendar calendar = Calendar.getInstance();
		Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime());
		return currentTimestamp.toString();
	}
	
	private void setMobileDataEnabled(Context context, boolean enabled) throws Exception {
	    
		
		final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    final Class conmanClass = Class.forName(conman.getClass().getName());
	    final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
	    iConnectivityManagerField.setAccessible(true);
	    final Object iConnectivityManager = iConnectivityManagerField.get(conman);
	    final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
	    final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
	    setMobileDataEnabledMethod.setAccessible(true);

	    setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
	}
	
	private final static String COMMAND_L_ON = "svc data enable\n ";
	private final static String COMMAND_L_OFF = "svc data disable\n ";
	private final static String COMMAND_SU = "su";
	
	private void setMobileDataEnabledFromLollipop(Context context,boolean enable)
	{
		 String command;
		    if(enable)
		        command = COMMAND_L_ON;
		    else
		        command = COMMAND_L_OFF;        

		    try{
		        Process su = Runtime.getRuntime().exec(COMMAND_SU);
		        DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

		        outputStream.writeBytes(command);
		        outputStream.flush();

		        outputStream.writeBytes("exit\n");
		        outputStream.flush();
		        try {
		            su.waitFor();
		        } catch (InterruptedException e) {
		            e.printStackTrace();
		        }

		        outputStream.close();
		    }catch(IOException e){
		        e.printStackTrace();
		    }
	}


	
}
