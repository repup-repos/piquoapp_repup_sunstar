package com.piquor.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;
import com.piquor.utility.AlertDialogManager;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class FragmentEmailScreen extends DialogFragment implements DeliveryModeFlags,FRAGMENT_SCREEN_NAMES,ACTIVITY_FLAGS {

	private String LOG_TAG = "FragmentEmailScreen";
	private int EMAIL_SENT_START  = 0;
	private int EMAIL_SENT_COMPLATE = 1;
	private EditText emailTextView;
	private EditText phoneNoTextView;
	private EditText nameTextView;
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
	}
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflator = getActivity().getLayoutInflater();
		View view = inflator.inflate(R.layout.fragmentemailscreen, null);
		alertBuilder.setView(view);
		alertBuilder.setMessage("Fill below entries");
		alertBuilder.setNegativeButton(R.string.txtemaildialogcancel, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				
				closeCurrentDialog();
			}
		});
		alertBuilder.setCancelable(false);
		alertBuilder.setPositiveButton(R.string.txtemaildilogok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
//				processEmailEntry();
			}
		});
		
		emailTextView = (EditText) view.findViewById(R.id.txtEmail);
		phoneNoTextView = (EditText) view.findViewById(R.id.txtPhone);
		nameTextView = (EditText) view.findViewById(R.id.txtName);
		
		Dialog dialog =  alertBuilder.create();
		
		return dialog;
		
	}
	
	
	@Override
	public void onStart() {
		super.onStart();
	
		AlertDialog d = (AlertDialog)getDialog();
	    if(d != null)
	    {
	        Button positiveButton = (Button) d.getButton(Dialog.BUTTON_POSITIVE);
	        positiveButton.setOnClickListener(new View.OnClickListener()
	                {
	                    @Override
	                    public void onClick(View v)
	                    {
	                    	processEmailEntry();
	                    }
	                });
	    }
	}
	
	protected void goToNextScreenWithConfigurationInfo() {
		
	//	progressDialog = ProgressDialog.show(getActivity(), "", "", true);
		
		mHandler.sendEmptyMessage(EMAIL_SENT_START);
		
	}
	
	private Handler mHandler = new Handler(){
		
		private String name;
		private String email;
		private String phone;

		public void handleMessage(Message msg)
		{
			final int what = msg.what;
			if(what==EMAIL_SENT_COMPLATE)
			{
				//progressDialog.dismiss();
				Log.d(LOG_TAG,":Setting Email Deliverymode:");
				new AlertDialogManager().showAlertDialog(getActivity(), "Info", "Email Sent Please check you Inbox/Spam", false);
				dismiss();

			
			}else if(what==EMAIL_SENT_START)
			{
				name = nameTextView.getText().toString().trim();
				email = emailTextView.getText().toString().trim();
				phone = phoneNoTextView.getText().toString().trim();
				
				if(name.equals(""))
				{
					name = "NA";
				}
				if(phone.equals(""))
				{
					phone = "NA";
				}
				MainActivity.setName(name);
				MainActivity.setEmail(email);
				MainActivity.setPhoneNo(phone);
				
				String validationText = getValidationText(email, phone);
				
				if (validationText.equals("")) {
					
					
					startEmailSendingProcedure();
				}else
				{
					new AlertDialogManager().showAlertDialog(getActivity(), "Validation Failed", validationText, false);
				}
				
			}
		}
	};
	
	private void startEmailSendingProcedure() {
		
		storeUserEntryIntoDabase();
		mHandler.sendEmptyMessage(EMAIL_SENT_COMPLATE);
	}


	private void storeUserEntryIntoDabase() {
		
		Log.d(LOG_TAG, "Navigating To Effect Screen");
		MainActivity.setDeliveryMode(DeliveryModeFlags.EMAIL_DELIVERYMODE_FLAG);
		
	}

	
	public String getValidationText(String email,String phoneNO)
	{
		if(email.equals(""))
		{
			return "Please Enter EmailId";
		}else if(android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()==false)
		{	return "Invalid EmailId";
		}
		if(!phoneNO.equals("NA")&&!Patterns.PHONE.matcher(phoneNO).matches())
		{
			return "Invalid Phone No";
		}
		return "";
	}


	private void processEmailEntry() {
		
		Log.d(LOG_TAG,"Processing Email Entry");
		mHandler.sendEmptyMessage(EMAIL_SENT_START);
	}

	private void closeCurrentDialog() {
		
		dismiss();
	}
	
	

}
