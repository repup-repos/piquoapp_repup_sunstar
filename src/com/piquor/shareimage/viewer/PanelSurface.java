package com.piquor.shareimage.viewer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.piquor.constants.APP_CONS;

public class PanelSurface extends SurfaceView implements SurfaceHolder.Callback
{
	
	Bitmap kangaroo = null;
	CanvasThread canvasThread;
	int fullScreenWidth;
	int fullScreenHeight;
	
	public PanelSurface(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		fullScreenHeight = size.y;
		fullScreenWidth = size.x;
		
		
		
		getHolder().addCallback(this);
		canvasThread = new CanvasThread(getHolder(), this);
		setFocusable(true);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
		canvasThread.setRunning(true);
		if(!canvasThread.isAlive())
		{
			
			if (APP_CONS.get_framedImage() != null) {
				kangaroo = BitmapFactory.decodeFile(APP_CONS.get_framedImage()
						.getAbsolutePath());
				canvasThread.start();
			}else
			{
				Log.d("PanelSurface", "Surface is Changed");
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
	
		canvasThread.setRunning(false);
	}
	
	
	@Override
	public void draw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.draw(canvas);
		if (APP_CONS.get_framedImage() != null) {
			/*kangaroo = BitmapFactory.decodeFile(APP_CONS.get_framedImage()
					.getAbsolutePath());*/
			int width = kangaroo.getWidth();
			int height = kangaroo.getHeight();

			float scaleWidth = ((float) fullScreenWidth) / width;
			float scaleHeight = ((float) fullScreenHeight) / height;

			Matrix matrix = new Matrix();
			matrix.postScale(scaleWidth, scaleHeight);

			Paint paint = new Paint();
			canvas.drawColor(Color.BLACK);
			// canvas.drawBitmap(kangaroo, 0,0,null);
			if(kangaroo!=null)
			{
				canvas.drawBitmap(kangaroo, matrix, null);
			}
		}
	}
	
}

