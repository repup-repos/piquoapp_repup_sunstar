package com.piquor.constants;

import java.io.File;

import android.graphics.Bitmap;
import android.util.Log;

public class APP_CONS {
	
	public static File get_file() {
		return _file;
	}
	public static void set_file(File _file) {
		APP_CONS._file = _file;
	}
	public static File get_dir() {
		return _dir;
	}
	public static void set_dir(File _dir) {
		APP_CONS._dir = _dir;
	}
	public static Bitmap getBitmapCaptured() {
		return bitmapCaptured;
	}
	public static void setBitmapCaptured(Bitmap bitmapCaptured) {
		
		APP_CONS.bitmapCaptured = bitmapCaptured;
		setBitmapProcessed(bitmapCaptured);
	}
	public static Bitmap getBitmapProcessed() {
		return bitmapProcessed;
	}
	public static void setBitmapProcessed(Bitmap bitmapProcessed) {
		APP_CONS.bitmapProcessed = bitmapProcessed;
	}
	
	public static File get_framedImage() {
		return _framedImage;
	}
	public static void set_framedImage(File _framedImage) {
		APP_CONS._framedImage = _framedImage;
	}

	public static String getPicId() {
		return picId;
	}
	public static void setPicId(String picId) {
		APP_CONS.picId = picId;
	}

	private static File _file 			  = null;
	private static File _dir              = null;
	private static Bitmap bitmapCaptured  = null;
	private static Bitmap bitmapProcessed = null;
	private static File _framedImage	  = null;
	private static String picId           = null;
	private static Bitmap _tempImageFile  = null;
	private static Bitmap _tempImageScaledFile = null;
	private static File _tempCaptureFile = null;
	
	public static void releaseAllImageData() {
		
		Log.d("APP_CONS", "Recycle all app constants on APP_CONS");
		if(bitmapCaptured != null)
		{
			Log.d("APP_CONS", "Recycle Captured Image");
			bitmapCaptured.recycle();
		}
		if(bitmapProcessed != null)
		{
			Log.d("APP_CONS", "Recycle Processed Image");
			bitmapProcessed.recycle();
		}
		if(_tempImageFile!=null)
		{
			Log.d("APP_CONS", "Recycle Temp Image File");
			_tempImageFile.recycle();
		}
		if(_tempImageScaledFile!=null)
		{
			Log.d("APP_CONS", "Recycle Image Scale Image");
			_tempImageScaledFile.recycle();
		}
		
		_file = null;
		_dir = null;
		_framedImage = null;
		bitmapCaptured = null;
		_tempImageFile = null;
		_tempImageScaledFile = null;
		_tempCaptureFile = null;
	}
	public static void recycleProcessedBitmap() {
	
		
		Log.d(LOG_TAG,"Releasing all processed bitmap");
		if(bitmapProcessed != null)
		{
			bitmapProcessed.recycle();
			bitmapProcessed = null;
		}
		if(_tempImageFile != null)
		{
			_tempImageFile.recycle();
			_tempImageFile = null;
		}
	}
	private static String LOG_TAG = "APP_CONS";
	
	public static Bitmap get_tempImageFile() {
		return _tempImageFile;
	}
	public static void set_tempImageFile(Bitmap _tempImageFile) {
		
		Log.d(LOG_TAG, "Creating temp image file");
		APP_CONS._tempImageFile = _tempImageFile;
	}
	public static Bitmap get_tempImageScaledFile() {
		return _tempImageScaledFile;
	}
	public static void set_tempImageScaledFile(Bitmap _tempImageScaledFile) {
		APP_CONS._tempImageScaledFile = _tempImageScaledFile;
	}
	public static File get_tempCaptureFile() {
		return _tempCaptureFile;
	}
	public static void set_tempCaptureFile(File _tempCaptureFile) {
		APP_CONS._tempCaptureFile = _tempCaptureFile;
	}
	
}
