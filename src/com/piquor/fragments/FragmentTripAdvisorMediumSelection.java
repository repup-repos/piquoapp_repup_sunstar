package com.piquor.fragments;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.R;
import com.piquor.utility.AlertDialogManager;
import com.piquor.utility.InternetConnectionDetector;

public class FragmentTripAdvisorMediumSelection extends Fragment implements FRAGMENT_SCREEN_NAMES,ACTIVITY_FLAGS,OnClickListener {
	
	
	private String LOG_TAG           = "FragmentTripADvisorMediumSelection";
	private String FLAG_TRIPADVISOR  = "FragmentTripAdvisorReview";
	private FragementChangeListener fragmentChangeListener;
	private final static String COMMAND_L_ON = "svc data enable\n ";
	private final static String COMMAND_L_OFF = "svc data disable\n ";
	private final static String COMMAND_SU = "su";
	
	@Override 
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)
	{
		
		View view = inflator.inflate(R.layout.fragmentreviewmediumselection, parent,false);
		
		
		ImageButton btnTripAdvisorReview = (ImageButton) view.findViewById(R.id.btnTripAdvisorReview);
		btnTripAdvisorReview.setOnClickListener(this);
		btnTripAdvisorReview.setTag(FLAG_TRIPADVISOR);
		
		
		ImageButton btnHome = (ImageButton) view.findViewById(R.id.fragmentreviewmedium_btnhome);
		btnHome.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				goToHomeScreen();
			}
		});
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener) getActivity();
	
	}


	@Override
	public void onClick(View v) {
		
		String tag = v.getTag().toString().trim();
		if(tag.equals((FLAG_TRIPADVISOR)))
				{
					try {
							setMobileDataEnabled(getActivity(), true);
						} catch (Exception e) {
				
						Log.d(LOG_TAG, "Exception x:"+e.getStackTrace().toString());
				
						}
					moveToTripAdvisorMediumSelection();
				}
	}


	private void moveToTripAdvisorMediumSelection() {
		
		if(checkInternetConnectivity())
		{
			fragmentChangeListener.navigateToReiewEntryScreen();
		}
	}
	
	protected void goToHomeScreen() {
		
		fragmentChangeListener.navigateToHomeScreen();
	}
	
	
	private boolean checkInternetConnectivity() {
		InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(
				getActivity());
		if (!internetConnectionDetector.isConnectingToInternet()) {
			new AlertDialogManager().showAlertDialog(getActivity(),
					"Intenet Connection Failed",
					"Please Ensure Internet Connectivity", false);
			return false;
		}
		return true;
	}
	
	private void setMobileDataEnabled(Context context, boolean enabled)
			throws Exception {

		Log.d(LOG_TAG, "SDK Version: " + Build.VERSION.SDK_INT);
		if (Build.VERSION.SDK_INT > 19) {
			
			setMobileDataEnabledFromLollipop(getActivity(), enabled);
			Log.v(LOG_TAG, "Lollipop enabling data on mobile");
		} else {
			
			Log.v(LOG_TAG, "Kitkat enabling data on mobile");
			final ConnectivityManager conman = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class
					.forName(conman.getClass().getName());
			final Field iConnectivityManagerField = conmanClass
					.getDeclaredField("mService");
			iConnectivityManagerField.setAccessible(true);
			final Object iConnectivityManager = iConnectivityManagerField
					.get(conman);
			final Class iConnectivityManagerClass = Class
					.forName(iConnectivityManager.getClass().getName());
			final Method setMobileDataEnabledMethod = iConnectivityManagerClass
					.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);

			setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
		}
	}
	
	private void setMobileDataEnabledFromLollipop(Context context,boolean enable)
	{
		 String command;
		    if(enable)
		        command = COMMAND_L_ON;
		    else
		        command = COMMAND_L_OFF;        

		    try{
		        Process su = Runtime.getRuntime().exec(COMMAND_SU);
		        DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

		        outputStream.writeBytes(command);
		        outputStream.flush();

		        outputStream.writeBytes("exit\n");
		        outputStream.flush();
		        try {
		            su.waitFor();
		        } catch (InterruptedException e) {
		            e.printStackTrace();
		        }

		        outputStream.close();
		    }catch(IOException e){
		        e.printStackTrace();
		    }
	}
}
