package com.piquor.dao.table.cons;

public interface CAMPAIGN_MACHINE_MAPPING {
	
	public final String TABLE_NAME_CAMPAIGN_MACHINE_MAPPING = "campaign_machine_mapping";
	
	public final String COLUMN_NAME_MACHINE_MAPPING_RECORD_ID = "RecordId";

	public final String COLUMN_NAME_MACHINE_MAPPING_CAMPAIGNID = "CampaignId";

	public final String COLUMN_NAME_MACHINE_MAPPING_CAMPAIGNNAME = "CampaignName";

	public final String COLUMN_NAME_MACHINE_MAPPING_MACHINEID = "MachineId";

	public final String COLUMN_NAME_MACHINE_MAPPING_STARTINGDATE = "StartingDate";

	public final String COLUMN_NAME_MACHINE_MAPPING_ENDDATE = "EndDate";

	public final String COLUMN_NAME_MACHINE_MAPPING_TIMESTAMP = "TIMESTAMP";
	
}
