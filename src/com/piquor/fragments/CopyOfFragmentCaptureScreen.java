/*package com.piquor.fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImage.OnPictureSavedListener;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.piquor.camera.utilities.CameraHelper;
import com.piquor.camera.utilities.CameraHelper.CameraInfo2;
import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.constants.APP_CONS;
import com.piquor.fileUtils.ImageFileUtility;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.frameconfig.core.FrameConfigLoader;
import com.piquor.frameconfig.dto.FrameConfig;
import com.piquor.gpucamera.GPUImageFilterTools;
import com.piquor.gpucamera.GPUImageFilterTools.FilterAdjuster;
import com.piquor.imagecomposer.core.FrameBuilder;
import com.piquor.imagecomposer.core.FrameBuilderBitmapCons;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;
import com.piquor.utility.SaveFinalImageUtility;

public class CopyOfFragmentCaptureScreen extends Fragment implements FRAGMENT_SCREEN_NAMES,
		ACTIVITY_FLAGS,OnClickListener {

	private String LOG_TAG = "FragmentCaptureScreen";
	private int REQUEST_IMAGE_CAPTURE=  1;
	private GPUImage  mGPUImage;
	private CameraHelper mCameraHelper;
	private CameraLoader mCamera;
	private GPUImageFilter mFilter;
	private FilterAdjuster mFilterAdjuster;
	private GestureDetector myGestureDetector;
	View.OnTouchListener gestureListener;
	private int swipeCounter = 0;
	private int totalFilters = 0;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	private View toastLayout;
	private TextView txtToast;
	private FragementChangeListener fragmentChangeListener;
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
		
		Log.d(LOG_TAG, "Checking for Camera Availabilty");
		if(isCameraAvailable())
		{
			Log.d(LOG_TAG, "Camera Is Available Creating Image Directory");
			createImageDirectory();
			Log.d(LOG_TAG,"Image Directory Creation Successfull");
			if(APP_CONS.getBitmapCaptured()==null)
			{
				Log.d(LOG_TAG, "Directory Created Starting Capture Intent");
				startCaptureIntent();
			}
		}else
		{
			Log.d(LOG_TAG, "Camera Is Not Connected");
			Toast.makeText(getActivity(), "Camera Is Not Connected", Toast.LENGTH_LONG).show();

		}
		
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener)getActivity();
	}


	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)
	{
		
		View view = inflator.inflate(R.layout.fragmentcapturescreen, parent,false);
		
		GPUImageFilterTools.initializeGPUImageFilterTools(getActivity().getApplicationContext());
		totalFilters = GPUImageFilterTools.totalFilters()-1;
		Log.d(LOG_TAG, "Total Filters:"+totalFilters);
				
		//((SeekBar) view.findViewById(R.id.seekBar)).setOnSeekBarChangeListener(this);
        view.findViewById(R.id.fragmentcapturescreen_capture).setOnClickListener(this);

        mGPUImage = new GPUImage(getActivity());
        mGPUImage.setGLSurfaceView((GLSurfaceView) view.findViewById(R.id.surfaceView));

        mCameraHelper = new CameraHelper(getActivity());
        mCamera = new CameraLoader();
	    
        View cameraSwitchView = view.findViewById(R.id.img_switch_camera);
        cameraSwitchView.setOnClickListener(this);
        if (!mCameraHelper.hasFrontCamera() || !mCameraHelper.hasBackCamera()) {
            cameraSwitchView.setVisibility(View.GONE);
        }
        
       GLSurfaceView glSurfaceView = (GLSurfaceView) view.findViewById(R.id.surfaceView);
       
       myGestureDetector = new GestureDetector(getActivity(),
				new MyGestureDetector());
       
       gestureListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return myGestureDetector.onTouchEvent(event);
			}
		};
       
	   glSurfaceView.setOnTouchListener(gestureListener);
	 
	   swipeCounter = 0;
		
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mCamera.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		mCamera.onPause();
	}
	
	private void navigetToImagePreviewScreen() {
		
		Log.d(LOG_TAG, "Navigating To Effect Screen");
		
		 * FragementChangeListener fragmentChangeListener = (FragementChangeListener) getActivity();
			fragmentChangeListener.replaceFragment(SET_PIC_ID, FRAGMENT_CAPTURE_SCREEN, FRAGMENT_EFFECT_SCREEN);
		 	
		fragmentChangeListener.navigateToImageSharingScreen();
	}

	private void createImageDirectory() {
		
		APP_CONS.set_dir(ImageFileUtility.getAppDirectory());
	}

	private boolean isCameraAvailable() {
		
		PackageManager pm = getActivity().getApplicationContext().getPackageManager();
		if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA))
		{
			return true;
		}else if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT))
		{
			return true;
		}else if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	//CAMERA LOADER
		 private class CameraLoader {
			 
		        private int mCurrentCameraId = 0;
		        private Camera mCameraInstance;

		        public void onResume() {
		            setUpCamera(mCurrentCameraId);
		        }

		        public void onPause() {
		            releaseCamera();
		        }

		        public void switchCamera() {
		            releaseCamera();
		            mCurrentCameraId = (mCurrentCameraId + 1) % mCameraHelper.getNumberOfCameras();
		            setUpCamera(mCurrentCameraId);
		        }

		        private void setUpCamera(final int id) {
		            mCameraInstance = getCameraInstance(id);
		            Parameters parameters = mCameraInstance.getParameters();
		            // TODO adjust by getting supportedPreviewSizes and then choosing
		            // the best one for screen size (best fill screen)
		            for(Camera.Size size : parameters.getSupportedPreviewSizes())
		            {
		            	   Log.d("CaptureFragment:","Capture Fragmement: Width: "+size.width+": height :"+size.height);
		      	         
		            }
		            parameters.setPreviewSize(640, 480);
		            if (parameters.getSupportedFocusModes().contains(
		                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
		                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
		            }
		            mCameraInstance.setParameters(parameters);

		            int orientation = mCameraHelper.getCameraDisplayOrientation(
		                    getActivity(), mCurrentCameraId);
		            CameraInfo2 cameraInfo = new CameraInfo2();
		            mCameraHelper.getCameraInfo(mCurrentCameraId, cameraInfo);
//		            boolean flipHorizontal = cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT
//		                    ? false : false;
		            mGPUImage.setUpCamera(mCameraInstance, orientation, false, false);
		            
		            
		            boolean flipHorizontal = cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT
	                ? true : false;
		            mGPUImage.setUpCamera(mCameraInstance, orientation, flipHorizontal, false);
		        
		        
		        }

		        *//** A safe way to get an instance of the Camera object. *//*
		        private Camera getCameraInstance(final int id) {
		            Camera c = null;
		            try {
		                c = mCameraHelper.openCamera(id);
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		            return c;
		        }

		        private void releaseCamera() {
		            mCameraInstance.setPreviewCallback(null);
		            mCameraInstance.release();
		            mCameraInstance = null;
		        }
		    }

		 @Override
			public void onClick(View v) {

				switch (v.getId()) {

				case R.id.fragmentcapturescreen_capture:
					if (mCamera.mCameraInstance.getParameters().getFocusMode()
							.equals(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
						takePicture();
					} else {
						mCamera.mCameraInstance
								.autoFocus(new Camera.AutoFocusCallback() {

									@Override
									public void onAutoFocus(final boolean success,
											final Camera camera) {
										takePicture();
									}
								});
					}
					break;

				case R.id.img_switch_camera:
					mCamera.switchCamera();
					break;
				}
			}
		 
		 private void switchFilterTo(final GPUImageFilter filter) {
		        if (mFilter == null
		                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
		            mFilter = filter;
		            mGPUImage.setFilter(mFilter);
		            mFilterAdjuster = new FilterAdjuster(mFilter);
		            
		        }
		    }
		 
		 private void takePicture() {
		        // TODO get a size that is about the size of the screen
		        Camera.Parameters params = mCamera.mCameraInstance.getParameters();
		       // params.setPictureSize(1280, 960);
//		        params.setRotation(90);
		        mCamera.mCameraInstance.setParameters(params);
		        for (Camera.Size size2 : mCamera.mCameraInstance.getParameters()
		                .getSupportedPictureSizes()) {
		            Log.i("ASDF", "Supported: " + size2.width + "x" + size2.height);
		        }
		        mCamera.mCameraInstance.takePicture(null, null,
		                new Camera.PictureCallback() {

		                    @Override
		                    public void onPictureTaken(byte[] data, final Camera camera) {
		                    	
		                    	 Log.d(LOG_TAG, "Creating File to Whold Captured Image");
		         			 	try {
		         		             APP_CONS.set_tempCaptureFile(File.createTempFile(ImageFileUtility.getImageFileName(), ".jpg",APP_CONS.get_dir()));
		         		        } catch (Exception ex) {
		         		        }
		                       // final File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
		                        if (APP_CONS.get_tempCaptureFile() == null) {
		                            Log.d("ASDF",
		                                    "Error creating media file, check storage permissions");
		                            return;
		                        }
		                    	
		                    	 APP_CONS.set_tempCaptureFile(getOutputMediaFile(MEDIA_TYPE_IMAGE));
		                         if (APP_CONS.get_tempCaptureFile() == null) {
		                             Log.d("ASDF",
		                                     "Error creating media file, check storage permissions");
		                             return;
		                         }


		                        try {
		                            FileOutputStream fos = new FileOutputStream(APP_CONS.get_tempCaptureFile());
		                            fos.write(data);
		                            fos.close();
		                        } catch (FileNotFoundException e) {
		                            Log.d("ASDF", "File not found: " + e.getMessage());
		                        } catch (IOException e) {
		                            Log.d("ASDF", "Error accessing file: " + e.getMessage());
		                        }

		                        data = null;
		                        
		                        APP_CONS.set_tempImageFile(BitmapFactory.decodeFile(APP_CONS.get_tempCaptureFile()
		                                .getAbsolutePath()));
		                        // mGPUImage.setImage(bitmap);
		                        final GLSurfaceView view = (GLSurfaceView) getActivity().findViewById(R.id.surfaceView);
		                        view.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		                        
		                        String imageFileName = ImageFileUtility.getImageFileName();
		                        
		                        mGPUImage.saveToPictures(APP_CONS.get_tempImageFile(), ImageFileUtility.ALBUM_NAME_PHOTO,
		                        		imageFileName + ".jpg",
		                                new OnPictureSavedListener() {

		                                    @Override
		                                    public void onPictureSaved(final Uri
		                                            uri) {
		                                    	//APP_CONS.get_tempCaptureFile().delete();
		                                       // camera.startPreview();
//		                                    	 APP_CONS.set_file(new File()));
		           		                        APP_CONS.set_file(new File(getRealPathFromURI(uri)));
		           		                        APP_CONS.setBitmapCaptured(ImageFileUtility.loadImageBitmap(APP_CONS.get_file().getAbsolutePath()));
		         		            	    
		                                        view.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
		                            	    	new ImageFrameComposer().execute();
		                    		            
		                                    }
		                        });
		                        
		                        
		                        
		                      	
		            	    	Log.d("CaptureFragment","Image Saved Moving To Capture Fragment");
		            		    
		            	    	Log.d(LOG_TAG,"Creating User DTO");
		            	    	MainActivity.createUserDTO();
		            	    	
		                    }
		                });
		    }
		 
		   private static File getOutputMediaFile(final int type) {
		        // To be safe, you should check that the SDCard is mounted
		        // using Environment.getExternalStorageState() before doing this.

		        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
		                Environment.DIRECTORY_PICTURES), "PiquorApp Photos");
		        // This location works best if you want the created images to be shared
		        // between applications and persist after your app has been uninstalled.

		        // Create the storage directory if it does not exist
		        if (!mediaStorageDir.exists()) {
		            if (!mediaStorageDir.mkdirs()) {
		                Log.d("MyCameraApp", "failed to create directory");
		                return null;
		            }
		        }

		        // Create a media file name
		        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		        File mediaFile;
		        if (type == MEDIA_TYPE_IMAGE) {
		            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
		                    "IMG_" + timeStamp + ".jpg");
		        } else if (type == MEDIA_TYPE_VIDEO) {
		            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
		                    "VID_" + timeStamp + ".mp4");
		        } else {
		            return null;
		        }

		        return mediaFile;
		    }
		 private String getRealPathFromURI(Uri contentUri) {
			   
			 	String[] proj = { MediaStore.Images.Media.DATA };
			    CursorLoader loader = new CursorLoader(getActivity().getApplicationContext(), contentUri, proj, null, null, null);
			    Cursor cursor = loader.loadInBackground();
			    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			    cursor.moveToFirst();
			    return cursor.getString(column_index);
			}
		
		class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {

			final int SWIPE_MIN_DISTANCE = 120;
	        final int SWIPE_MAX_OFF_PATH = 250;
	        final int SWIPE_THRESHOLD_VELOCITY = 200;
	        private  final int SWIPE_THRESHOLD = 100;
	        private  final int SWIPE_VELOCITY_THRESHOLD = 100;
			private Toast toast;
	        
			@Override
			public boolean onDown(MotionEvent e) {
				return true;
			}

			@Override
			public boolean onDoubleTap(MotionEvent e) {

				Log.d(LOG_TAG, "onDoubleTap");
				return super.onDoubleTap(e);
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
					float velocityY) {
				String velocity = "onFling: \n" + e1.toString() + "\n"
						+ e2.toString() + "\n" + "velocityX= "
						+ String.valueOf(velocityX) + "\n" + "velocityY= "
						+ String.valueOf(velocityY) + "\n";
				Log.d(LOG_TAG, "onFling velocity=" + velocity);
				 float diffY = e2.getY() - e1.getY();
	              float diffX = e2.getX() - e1.getX();
	              if (Math.abs(diffX) > Math.abs(diffY)) {
	                  if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
	                      if (diffX > 0) {
	                        
	                    	  if (swipeCounter > 0) {
									swipeCounter--;
								}
									switchFilterTo(GPUImageFilterTools
											.chooseFilter(swipeCounter));
									
	                          
	                      } else {
	                    	
	                    	  if (swipeCounter < totalFilters) {
									swipeCounter++;
								}
				
							switchFilterTo(GPUImageFilterTools
									.chooseFilter(swipeCounter));
							
						}
	                      }
	                 
	              } 
				
				
				return false;
			}

			@Override
			public void onLongPress(MotionEvent e) {
				Log.d(LOG_TAG, "onLongPress: \n" + e.toString());
				super.onLongPress(e);
			}

			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {
				Log.d(LOG_TAG, "onSingleTapConfirmed: \n" + e.toString());
				return super.onSingleTapConfirmed(e);
			}

		};
		
		
		class ImageFrameComposer extends AsyncTask<String, String, String>
		{
			
			private ArrayList<FrameConfig> framesConfiguration;
			ProgressDialog progressDialog;
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				framesConfiguration = FrameConfigLoader.getFramesConfiguration();
				progressDialog = ProgressDialog.show(getActivity(), "", "", true);
				progressDialog.setIndeterminate(true);
				progressDialog.setCancelable(false);
				progressDialog.show();
			}
			
			

			@Override
			protected String doInBackground(String... params) {
			
				int totalFrames = framesConfiguration.size();
				for(int thumbnailCounter = 0;thumbnailCounter<totalFrames;thumbnailCounter++ )
				{
					FrameConfig frameConfig = framesConfiguration.get(thumbnailCounter);
					
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inScaled = false;
					if(thumbnailCounter == 0)
					{
						if(FrameBuilderBitmapCons.getBgImage()==null)
						{
							FrameBuilderBitmapCons.setBgImage(BitmapFactory.decodeResource(getResources(), frameConfig.getBgFrameID(),options));
							Log.d(LOG_TAG,"Dim:widthxheight"+FrameBuilderBitmapCons.getBgImage().getWidth()+"x"+FrameBuilderBitmapCons.getBgImage().getHeight());
						}
						if(FrameBuilderBitmapCons.getFgImage()==null)
						{
							FrameBuilderBitmapCons.setFgImage(BitmapFactory.decodeResource(getResources(), frameConfig.getFgFrameID(),options));
							Log.d(LOG_TAG,"Dim:widthxheight"+FrameBuilderBitmapCons.getFgImage().getWidth()+"x"+FrameBuilderBitmapCons.getFgImage().getHeight());
						}
						
						if(FrameBuilderBitmapCons.getFinalFrameImage()==null)
						{
							FrameBuilderBitmapCons.setFinalFrameImage(new FrameBuilder().placeFrameOverImage(frameConfig));
						}
					}
				}
				
				saveImageToSDCard();
				
				return null;
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				progressDialog.dismiss();
				navigetToImagePreviewScreen();
				
			}
			
			private void saveImageToSDCard() {
				
				SaveFinalImageUtility save = new SaveFinalImageUtility();
				
				boolean value = save.saveImage();
				
				String picId = APP_CONS.get_framedImage().getName();
				StringTokenizer tokenizer = new StringTokenizer(picId, ".");
				String pId = "";
				while (tokenizer.hasMoreElements()) {

					pId = (String) tokenizer.nextElement().toString()
							.trim();
					Log.d(LOG_TAG, "PicID: Recieved :" + pId);
					break;
				}
				MainActivity.setPicId(pId);
				MainActivity.setCamapignId(MainActivity.getCamapignInformation().getCampaignId().trim());
				
				//Set current pic date
				java.util.Date now = new java.util.Date();
				//converting java.util.Date to java.sql.Date in Java
		        java.sql.Date sqlDate = new java.sql.Date(now.getTime());
		        Log.d(LOG_TAG,"Converted value of java.sql.Date : " + sqlDate.toString());
				MainActivity.setDate(sqlDate.toString());
		        
				Log.d(LOG_TAG, "Saving image response "+value);
				Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				Uri contentUri = Uri.fromFile(APP_CONS.get_framedImage());
				mediaScanIntent.setData(contentUri);
				getActivity().getApplicationContext().sendBroadcast(mediaScanIntent);
			}

		}
}
*/