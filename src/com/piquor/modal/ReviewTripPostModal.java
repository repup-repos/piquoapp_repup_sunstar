package com.piquor.modal;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.dto.ReviewPageTempValuesDTO;
import com.piquor.utility.ReviewPageBuilder;
import com.piquor.utility.ReviewPageConstantsCreator;

public class ReviewTripPostModal {

	private String reviewUrl;
	private ReviewEntryDTO reviewEntryDTO;
	private ReviewPageBuilder reviewPageBuilder;
	private ReviewPageConstantsCreator reviewPageConstantsCretor;
	private ReviewPageTempValuesDTO reviewPageValues;
	
	public ReviewTripPostModal(ReviewEntryDTO reviewEntryDTO,String reviewUrl)
	{
		this.reviewUrl = reviewUrl;
		this.reviewEntryDTO = reviewEntryDTO;
	    reviewPageValues = new ReviewPageTempValuesDTO();
		
		reviewPageConstantsCretor = new ReviewPageConstantsCreator(reviewUrl);
	}
	
	
	public String getReviewPage()
	{
		reviewPageValues = reviewPageConstantsCretor.getReviewPageData(reviewUrl);
		if(reviewPageValues==null)
		{
			return "false";
		}else
		{
			reviewPageBuilder = new ReviewPageBuilder(reviewEntryDTO, reviewPageValues);
			String reviewPage = reviewPageBuilder.getReviewPage();
			return reviewPage;
		}
	}
}
