package com.piquor.utility;

import java.sql.Timestamp;
import java.util.Date;

public class PiquorTimestamp {
	
static Timestamp timestamp = null;
	
	public static Timestamp getTimeStamp(){
	
		Date date= new java.util.Date();
		timestamp = new Timestamp(date.getTime());
		return timestamp;
	}
}
