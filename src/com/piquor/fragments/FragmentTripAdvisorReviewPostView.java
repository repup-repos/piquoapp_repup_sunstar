package com.piquor.fragments;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;

public class FragmentTripAdvisorReviewPostView extends Fragment implements
		FRAGMENT_SCREEN_NAMES, ACTIVITY_FLAGS {

	private ReviewEntryDTO reviewEntryDTO;
	private String LOG_TAG = "FragmentTripAdvisorReviewPostView";
	private FragementChangeListener fragmentChangeListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragmenttareviewsumbissionview,
				container, false);

		ImageButton imButton = (ImageButton) view
				.findViewById(R.id.fragmentrestaurant_reviewsubmissiondoneview);
		imButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				completeReviewProcess();
			}
		});

		return view;

	}

	protected void completeReviewProcess() {
			goToNextScreen();
	}

	protected void goToNextScreen() {

		saveEntryOnDatabase();

		fragmentChangeListener.navigateToReviewMediumSelection();

	}

	public void saveEntryOnDatabase() {

		Calendar cal = Calendar.getInstance();
		String DATE_FORMAT_NOW = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String date = sdf.format(cal.getTime());

		int status = 0;

		String optionalParameter = "" + " CheckIn Rating:"
				+ reviewEntryDTO.getReviewentry_ratingCheckIn()
				+ ",Rooms Rating:"
				+ reviewEntryDTO.getReviewentry_ratingRooms() + ",Cleanliness:"
				+ reviewEntryDTO.getReviewentry_ratingClieanliness()
				+ ",Amneties:" + reviewEntryDTO.getReviewentry_ratingAmneties()
				+ ",Food and Beverages:"
				+ reviewEntryDTO.getReviewentry_ratingFoodandBeverages()
				+ ",Wifi & Internet:"
				+ reviewEntryDTO.getReviewentry_ratingWifiAndInternet()
				+ ",Checkout:" + reviewEntryDTO.getReviewentry_ratingCheckout()
				+ ",Checkout:"
				+ reviewEntryDTO.getReviewentry_ratingconceirge();
		
		reviewEntryDTO.setReviewentry_campaignId(MainActivity
				.getCamapignInformation().getCampaignId().trim());
		reviewEntryDTO.setReviewentry_optionaldesc(optionalParameter);
		reviewEntryDTO.setReviewentry_date(date);
		reviewEntryDTO.setReviewentry_type(PLACE_REVIEW_TYPE_TRIPADVISOR);
		reviewEntryDTO.setReviewentry_status(String.valueOf(status));
		reviewEntryDTO.setReviewentry_timestamp(getTimeStamp());
		// reviewEntryDTO.setReviewentry_emailid(TripAdisorEssentialsDTO.getRevientry_emailid());
		reviewEntryDTO.setReviewentry_platformid("1");
		reviewEntryDTO.setReviewentry_reviewUploaded("0");
		reviewEntryDTO.setReviewentry_reviewuploadStatus("0");

		MainActivity.addReviewEntriesInDatabase(reviewEntryDTO);

	}

	private String getTimeStamp() {
		Calendar calendar = Calendar.getInstance();
		Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		return currentTimestamp.toString();
	}

	public void setReviewUserDTO(ReviewEntryDTO reviewEntryDTO) {

		Log.d(LOG_TAG, "Reivew Entry DTO Recieved:" + reviewEntryDTO.toString());
		this.reviewEntryDTO = reviewEntryDTO;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener) getActivity();

	}

}
