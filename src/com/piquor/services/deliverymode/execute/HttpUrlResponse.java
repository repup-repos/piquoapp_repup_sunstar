package com.piquor.services.deliverymode.execute;

public class HttpUrlResponse {

	private String queryUrlResponse;
	private String queryUrlLog;
	
	public String getQueryUrlResponse() {
		return queryUrlResponse;
	}
	public void setQueryUrlResponse(String queryUrlResponse) {
		this.queryUrlResponse = queryUrlResponse;
	}
	public String getQueryUrlLog() {
		return queryUrlLog;
	}
	public void setQueryUrlLog(String queryUrlLog) {
		this.queryUrlLog = queryUrlLog;
	}
}
