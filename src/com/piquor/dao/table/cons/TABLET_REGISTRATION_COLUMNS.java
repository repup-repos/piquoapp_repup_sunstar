package com.piquor.dao.table.cons;

public interface TABLET_REGISTRATION_COLUMNS {

	
	public final String TABLE_NAME_REGISTRATION= "tablet_registration";
	public final String COLUMN_NAME_DEVICE_ID = "device_id";
	public final String COLUMN_NAME_BRAND_NAME = "brand_name";
	public final String COLUMN_NAME_MANUFETURER = "device_manufecturer";
	public final String COLUMN_NAME_BOARD = "device_board";
	public final String COLUMN_NAME_DISPLAY = "device_display";
	public final String COLUMN_NAME_MODAL = "device_modal_name";
	public final String COLUMN_NAME_PRODUCTNAME = "device_product_name";
	public final String COLUMN_NAME_SERIAL_NO = "device_serial_no";
	public final String COLUMN_NAME_VERSION = "device_version";
	public final String COLUMN_NAME_IS_CAMPAIGN_ASSIGNED = "iscampaignassigned";
	public final String COLUMN_NAME_CAMPAIGNiD = "campaignid";
	public final String COLUMN_NAME_DEFAULT_CAMAPGIN_ID = "default_campaign_id";
	public final String COLUMN_NAME_REGISTRATION_ENTRY_UPLOADED = "registration_uploaded";
	public final String COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS = "registration_uploadstatus";

}
