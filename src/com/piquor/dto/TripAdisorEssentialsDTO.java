package com.piquor.dto;


public class TripAdisorEssentialsDTO {
	
	private static String TRIP_ADVISOR_RESTAURENT_NAME = "";
	private static String TRIP_ADVISOR_RESTAURENT_URL = "";
	private static float TRIP_ADVISOR_OVERALL_RATING = 1;
	private static String TRIP_WERE_YOU_HERE_FOR = "";
	private static String TRIP_SHOR_OF_VISIT = "";
	private static String TRIP_ADVISOR_TITILE = "";
	private static String TRIP_ADVISOR_DESC = "";
	private static float TRIP_ADVISOR_CLEANLINESS_RATING = 1;
	private static float TRIP_ADVISOR_SERVICES_RATING = 1;
	private static float TRIP_ADVISOR_SLEEP_QWALITY_RATING = 1;
	private static String TRIP_ADVISOR_PLACE_TYPE = "";
	private static String TRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR = "";
	private static String TRIP_ADVISOR_HOTEL_AREA_NOISE = "";
	private static float TRIP_ADVISOR_RESTAURANT_FOOD_RATING  =  1;
	private static float TRIP_ADVISOR_RESTAURANT_VALUE_RATING = 1;
	private static float TRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING = 1;
	private static float TRIP_ADVISOR_RESTAURANT_SERVICES_RATING = 1;
	private static String TRIP_ADVISOR_RESTAURANT_COST_ESTIMATE = "";
	private static String TRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH  = "false";
	private static String TRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY = "false" ;
	private static String TRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT = "false" ;
	private static String TRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE = "false";
	private static String TRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING ="false";
	private static String TRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED = "false";
	private static String TRIP_ADVISOR_HOTEL_IS_THIS_A_BOUTIQUEHOTEL = "false";
	private static String TRIP_ADVISOR_HOTEL_IS_TIHS_A_TRYNDYHOEL = "false";
	private static String TRIP_ADVISOR_HOTEL_IS_THIS_A_CITYCENTER = "false";
	private static String ZOMATO_PLACE_REVIEW_URL = "NA";
	private static String PLACE_REVIEW_ID = "NA";
	private static String revientry_emailid = "na";
	private static String review_entryplatform_id = "na";
	private static String review_entrystatus = "0";
	private static String review_timestamp = "na";
	private static String review_campaignId = "na";
	
	
	public static String getTRIP_ADVISOR_RESTAURENT_NAME() {
		return TRIP_ADVISOR_RESTAURENT_NAME;
	}
	public static void setTRIP_ADVISOR_RESTAURENT_NAME(
			String tRIP_ADVISOR_RESTAURENT_NAME) {
		TRIP_ADVISOR_RESTAURENT_NAME = tRIP_ADVISOR_RESTAURENT_NAME;
	}
	public static String getTRIP_ADVISOR_RESTAURENT_URL() {
		return TRIP_ADVISOR_RESTAURENT_URL;
	}
	public static void setTRIP_ADVISOR_RESTAURENT_URL(
			String tRIP_ADVISOR_RESTAURENT_URL) {
		TRIP_ADVISOR_RESTAURENT_URL = tRIP_ADVISOR_RESTAURENT_URL;
	}
	public static float getTRIP_ADVISOR_OVERALL_RATING() {
		return TRIP_ADVISOR_OVERALL_RATING;
	}
	public static void setTRIP_ADVISOR_OVERALL_RATING(
			float tRIP_ADVISOR_OVERALL_RATING) {
		TRIP_ADVISOR_OVERALL_RATING = tRIP_ADVISOR_OVERALL_RATING;
	}

	public static String getTRIP_ADVISOR_TITILE() {
		return TRIP_ADVISOR_TITILE;
	}
	public static void setTRIP_ADVISOR_TITILE(String tRIP_ADVISOR_TITILE) {
		TRIP_ADVISOR_TITILE = tRIP_ADVISOR_TITILE;
	}
	public static String getTRIP_ADVISOR_DESC() {
		return TRIP_ADVISOR_DESC;
	}
	public static void setTRIP_ADVISOR_DESC(String tRIP_ADVISOR_DESC) {
		TRIP_ADVISOR_DESC = tRIP_ADVISOR_DESC;
	}
	
	public static String getTRIP_SHOR_OF_VISIT() {
		return TRIP_SHOR_OF_VISIT;
	}
	public static void setTRIP_SHOR_OF_VISIT(String tRIP_SHOR_OF_VISIT) {
		TRIP_SHOR_OF_VISIT = tRIP_SHOR_OF_VISIT;
	}
	public static String getTRIP_WERE_YOU_HERE_FOR() {
		return TRIP_WERE_YOU_HERE_FOR;
	}
	public static void setTRIP_WERE_YOU_HERE_FOR(String tRIP_WERE_YOU_HERE_FOR) {
		TRIP_WERE_YOU_HERE_FOR = tRIP_WERE_YOU_HERE_FOR;
	}
	public static void setIsThisaBoutiqueHotel(String string) {
		
		TRIP_ADVISOR_HOTEL_IS_THIS_A_BOUTIQUEHOTEL = string;
	}
	public static void setIsThisaCityCenter(String string) {
		
		TRIP_ADVISOR_HOTEL_IS_THIS_A_CITYCENTER = string;
	}
	public static void setIsThisHotelaTryndy(String string) {
		
		TRIP_ADVISOR_HOTEL_IS_TIHS_A_TRYNDYHOEL = string;
	}
	
	public static String getIsThisHotelBoutique()
	{
		return TRIP_ADVISOR_HOTEL_IS_THIS_A_BOUTIQUEHOTEL;
	}
	public static String getIsThisHotelCityCenter()
	{
		return TRIP_ADVISOR_HOTEL_IS_THIS_A_CITYCENTER;
	}
	public static String getIsThisHotelTryndy()
	{
		return TRIP_ADVISOR_HOTEL_IS_TIHS_A_TRYNDYHOEL;
	}
	
	public static float getTRIP_ADVISOR_CLEANLINESS_RATING() {
		return TRIP_ADVISOR_CLEANLINESS_RATING;
	}
	public static void setTRIP_ADVISOR_CLEANLINESS_RATING(
			float tRIP_ADVISOR_CLEANLINESS_RATING) {
		TRIP_ADVISOR_CLEANLINESS_RATING = tRIP_ADVISOR_CLEANLINESS_RATING;
	}
	public static float getTRIP_ADVISOR_SERVICES_RATING() {
		return TRIP_ADVISOR_SERVICES_RATING;
	}
	public static void setTRIP_ADVISOR_SERVICES_RATING(
			float tRIP_ADVISOR_SERVICES_RATING) {
		TRIP_ADVISOR_SERVICES_RATING = tRIP_ADVISOR_SERVICES_RATING;
	}
	public static float getTRIP_ADVISOR_SLEEP_QWALITY_RATING() {
		return TRIP_ADVISOR_SLEEP_QWALITY_RATING;
	}
	public static void setTRIP_ADVISOR_SLEEP_QWALITY_RATING(
			float tRIP_ADVISOR_SLEEP_QWALITY_RATING) {
		TRIP_ADVISOR_SLEEP_QWALITY_RATING = tRIP_ADVISOR_SLEEP_QWALITY_RATING;
	}
	public static String getTRIP_ADVISOR_PLACE_TYPE() {
		return TRIP_ADVISOR_PLACE_TYPE;
	}
	public static void setTRIP_ADVISOR_PLACE_TYPE(String tRIP_ADVISOR_PLACE_TYPE) {
		TRIP_ADVISOR_PLACE_TYPE = tRIP_ADVISOR_PLACE_TYPE;
	}
	public static String getTRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR() {
		return TRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR;
	}
	public static void setTRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR(
			String tRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR) {
		TRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR = tRIP_ADVISOR_HOTEL_AREA_KNOWN_FOR;
	}
	public static String getTRIP_ADVISOR_HOTEL_AREA_NOISE() {
		return TRIP_ADVISOR_HOTEL_AREA_NOISE;
	}
	public static void setTRIP_ADVISOR_HOTEL_AREA_NOISE(
			String tRIP_ADVISOR_HOTEL_AREA_NOISE) {
		TRIP_ADVISOR_HOTEL_AREA_NOISE = tRIP_ADVISOR_HOTEL_AREA_NOISE;
	}
	public static float  getTRIP_ADVISOR_RESTAURANT_FOOD_RATING() {
		return TRIP_ADVISOR_RESTAURANT_FOOD_RATING;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_FOOD_RATING(
			float tRIP_ADVISOR_RESTAURANT_FOOD_RATING) {
		TRIP_ADVISOR_RESTAURANT_FOOD_RATING = tRIP_ADVISOR_RESTAURANT_FOOD_RATING;
	}
	public static float getTRIP_ADVISOR_RESTAURANT_VALUE_RATING() {
		return TRIP_ADVISOR_RESTAURANT_VALUE_RATING;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_VALUE_RATING(
			float tRIP_ADVISOR_RESTAURANT_VALUE_RATING) {
		TRIP_ADVISOR_RESTAURANT_VALUE_RATING = tRIP_ADVISOR_RESTAURANT_VALUE_RATING;
	}
	public static float getTRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING() {
		return TRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING(
			float tRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING) {
		TRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING = tRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING;
	}
	public static float getTRIP_ADVISOR_RESTAURANT_SERVICES_RATING() {
		return TRIP_ADVISOR_RESTAURANT_SERVICES_RATING;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_SERVICES_RATING(
			float tRIP_ADVISOR_RESTAURANT_SERVICES_RATING) {
		TRIP_ADVISOR_RESTAURANT_SERVICES_RATING = tRIP_ADVISOR_RESTAURANT_SERVICES_RATING;
	}
	public static String getTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE() {
		return TRIP_ADVISOR_RESTAURANT_COST_ESTIMATE;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE(
			String tRIP_ADVISOR_RESTAURANT_COST_ESTIMATE) {
		TRIP_ADVISOR_RESTAURANT_COST_ESTIMATE = tRIP_ADVISOR_RESTAURANT_COST_ESTIMATE;
	}
	public static String getTRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH() {
		return TRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH(
			String tRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH) {
		TRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH = tRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH;
	}
	public static String getTRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY() {
		return TRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY(
			String tRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY) {
		TRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY = tRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY;
	}
	public static String getTRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT() {
		return TRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT(
			String tRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT) {
		TRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT = tRIP_ADVISOR_RESTAURANT_HAVE_LATENIGHT;
	}
	public static String getTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE() {
		return TRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE(
			String tRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE) {
		TRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE = tRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE;
	}
	public static String getTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING() {
		return TRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING(
			String tRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING) {
		TRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING = tRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING;
	}
	public static String getTRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED() {
		return TRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED;
	}
	public static void setTRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED(
			String tRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED) {
		TRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED = tRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED;
	}
	public static String getZOMATO_PLACE_REVIEW_URL() {
		return ZOMATO_PLACE_REVIEW_URL;
	}
	public static void setZOMATO_PLACE_REVIEW_URL(
			String zOMATO_PLACE_REVIEW_URL) {
		ZOMATO_PLACE_REVIEW_URL = zOMATO_PLACE_REVIEW_URL;
	}
	public static String getPLACE_REVIEW_ID() {
		return PLACE_REVIEW_ID;
	}
	public static void setPLACE_REVIEW_ID(String pLACE_REVIEW_ID) {
		PLACE_REVIEW_ID = pLACE_REVIEW_ID;
	}
	public static String getRevientry_emailid() {
		return revientry_emailid;
	}
	public static void setRevientry_emailid(String revientry_emailid) {
		TripAdisorEssentialsDTO.revientry_emailid = revientry_emailid;
	}
	public static String getReview_entryplatform_id() {
		return review_entryplatform_id;
	}
	public static void setReview_entryplatform_id(String review_entryplatform_id) {
		TripAdisorEssentialsDTO.review_entryplatform_id = review_entryplatform_id;
	}
	public static String getReview_entrystatus() {
		return review_entrystatus;
	}
	public static void setReview_entrystatus(String review_entrystatus) {
		TripAdisorEssentialsDTO.review_entrystatus = review_entrystatus;
	}
	public static String getReview_timestamp() {
		return review_timestamp;
	}
	public static void setReview_timestamp(String review_timestamp) {
		TripAdisorEssentialsDTO.review_timestamp = review_timestamp;
	}
	public static String getReview_campaignId() {
		return review_campaignId;
	}
	public static void setReview_campaignId(String review_campaignId) {
		TripAdisorEssentialsDTO.review_campaignId = review_campaignId;
	}
	
}	
