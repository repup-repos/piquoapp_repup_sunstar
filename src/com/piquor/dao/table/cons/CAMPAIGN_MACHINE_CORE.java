package com.piquor.dao.table.cons;

public interface CAMPAIGN_MACHINE_CORE {

	public final String TABLE_NAME_CAMPAIGN_MACHINE_CORE = "machine_core"; 
			
	public final String COLUMN_NAME_MACHINE_CORE_RECORDID = "RecordId";

	public final String COLUMN_NAME_MACHINE_CORE_MACHINEID = "MachineId";

	public final String COLUMN_NAME_MACHINE_CORE_DEFAULT_CAMPAIGNID = "DefaultCampaignId";

	public final String COLUMN_NAME_MACHINE_CORE_CONFIGURATIONMANGER_RESTART_TIMEPATTERN = "ConfigurationMangerTimePattern";

	public final String COLUMN_NAME_MACHINE_CORE_STATUSREPORTER_RESTART_TIMEPATTERN = "StatusReporterTimePattern";

	public final String COLUMN_NAME_MACHINE_CORE_SERVICEHANDLER_RESTART_TIMEPATTERN = "ServiceHandlerTimePattern";
	
	public final String COLUMN_NAME_MACHINE_CORE_TIMESTAMP = "TIMESTAMP";
	
	public final String COLUMN_NAME_MACHINE_CORE_UPDATESTATUS = "UpdateStatus";

}
