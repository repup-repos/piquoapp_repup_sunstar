package com.piquor.dao.table.cons;

public interface PLACE_REVIEW_ENTRIES_COLUMNS {
	
	public final String TABLE_NAME_PLACE_REVIEW_ENTRY = "reviewentries";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_ID = "id";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_CAMPAIGN_ID = "campaignid";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_TYPE = "place_review_type";	
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_DATE = "place_review_date";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_OVERALL_RATING = "pleace_essntial_entry_overall_rating";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_WEREYOUHEREFOR = "place_essential_entry_wereyouherefor";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_SORTOFVISIT = "place_essential_entry_sortofvisit";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWTITLE = "place_essential_entry_review_titile";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWDESC = "place_essential_entry_review_desc";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_OPTIONAL_ENTRIES = "place_optional_entries";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_PLATFORM_ID = "platform_id";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_EMAILID = "emailid";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_STATUS = "status";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_TIMESTAMP = "timestamp";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED = "review_uploaded";
	public final String COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS = "review_uploadstatus";
	
}
