package com.piquor.imageupload;

import java.io.File;

import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.piquor.errorhandler.ErrorUpdater;
import com.piquor.services.helper.ServicesHelper;

public class S3Upload {
	
	private String 	secretKey="OCyf1XxDaEoOSiCsQXMngLrjjmY0UTSAcN4qvGzg";
	private String  accessKey="AKIAIS2NBCWD7NN725WQ";
	private String LOG_TAG = "S3Upload";
	private ErrorUpdater errorUpdater;
	
	public S3Upload()
	{
		errorUpdater = new ErrorUpdater();
	}
		
	
	public boolean uploadImageToS3(String picId)
	{
	
		try{
			Log.d(LOG_TAG, "Creating S3 Image Upload for Mailer");
			
			AmazonS3Client s3Client = new AmazonS3Client( new BasicAWSCredentials( accessKey, secretKey ) );  
			File file = ServicesHelper.getFilePath(picId);
			
			Log.d(LOG_TAG, "Uploaded Image File Name "+file.getName()+": path :::"+file.getAbsolutePath());
			
			
			PutObjectRequest por = new PutObjectRequest( S3ImageConstants.BUCKET_NAME, file.getName(),file );  
			s3Client.putObject( por );
			
			Log.d(LOG_TAG, "Image Upload to S3");
			}catch(Exception e)
			{
				Log.d(LOG_TAG, "Exception in Picutre Upload setting entry in database"+e.getMessage());
				e.printStackTrace();
				errorUpdater.setPictureUploadError(picId);
				return false;
			}
		    errorUpdater.removeUpdateError(picId);
			return true;
	}
}
