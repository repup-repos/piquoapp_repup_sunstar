package com.piquor.dto;

public class TabletAppStatusDTO {

	private String id;
	private String device_id;
	private String last_app_opened_time;
	private String campaignid;
	private String timestamp;
	private String statsentry_uploaded;
	private String statsentry_upload_status;

	
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getLast_app_opened_time() {
		return last_app_opened_time;
	}
	public void setLast_app_opened_time(String last_app_opened_time) {
		this.last_app_opened_time = last_app_opened_time;
	}
	public String getCampaignid() {
		return campaignid;
	}
	public void setCampaignid(String campaignid) {
		this.campaignid = campaignid;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getStatsentry_uploaded() {
		return statsentry_uploaded;
	}

	public void setStatsentry_uploaded(String statsentry_uploaded) {
		this.statsentry_uploaded = statsentry_uploaded;
	}

	public String getStatsentry_upload_status() {
		return statsentry_upload_status;
	}

	public void setStatsentry_upload_status(String statsentry_upload_status) {
		this.statsentry_upload_status = statsentry_upload_status;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "TabletAppStatusDTO [id=" + id + ", device_id=" + device_id
				+ ", last_app_opened_time=" + last_app_opened_time
				+ ", campaignid=" + campaignid + ", timestamp=" + timestamp
				+ ", statsentry_uploaded=" + statsentry_uploaded
				+ ", statsentry_upload_status=" + statsentry_upload_status
				+ "]";
	}
	
	
	
	
	
}
