package com.piquor.fragments.listener;

public interface FragmentTextSetView {
	
	public void updateTextView();
}
