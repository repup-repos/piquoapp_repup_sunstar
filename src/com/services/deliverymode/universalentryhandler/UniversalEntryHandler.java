package com.services.deliverymode.universalentryhandler;

import java.util.StringTokenizer;

import android.util.Log;

import com.piquor.dto.UserTableDTO;
import com.piquor.services.UrlExecutorService;
import com.services.deliverymode.staticfields.DeliveryModeFlags;
import com.services.deliverymode.urlbuilder.FacebookUrlBuilder;
import com.services.deliverymode.urlbuilder.TwitterUrlBuilder;
import com.services.deliverymode.urlbuilder.UniversalMultiEmailHandler;
import com.services.deliverymode.urlbuilder.UniversalUrlBuilder;

public class UniversalEntryHandler implements DeliveryModeFlags {

	UserTableDTO userDTO;
	private String LOG_TAG = "UniversalEntryHandler";
	public UniversalEntryHandler(UserTableDTO userDTO)
	{
		this.userDTO=userDTO;
	}
	
	public boolean executeUniversalEntryServices(String machineId)
	{
		
		String checkEmail=userDTO.getEmail().trim();
		String deliveryModes=userDTO.getDelieveryMode().trim();
		StringTokenizer deliveryModeToken = new StringTokenizer(deliveryModes, "#");
		String deliverModeArray[] = { "", "", "", "" };
		String shortedDeliveryModes="";
		String deliveryModeatCurrentIndex;
		boolean isFacebookEntry=false;
		boolean isTwitterEntry=false;
		
		while (deliveryModeToken.hasMoreElements()) {

			deliveryModeatCurrentIndex = deliveryModeToken.nextElement().toString().trim();
		
			if (deliveryModeatCurrentIndex.equals(EMAIL_DELIVERYMODE_FLAG)) {
				deliverModeArray[0] = deliveryModeatCurrentIndex;
			} else if (deliveryModeatCurrentIndex.equals(SMS_DELIVERYMODE_FLAG)) {
				deliverModeArray[1] = deliveryModeatCurrentIndex;
			} else if (deliveryModeatCurrentIndex.equals(MMS_DELIVERYMODE_FLAG)) {
				deliverModeArray[2] = deliveryModeatCurrentIndex;
			} else if (deliveryModeatCurrentIndex.equals(FACEBOOK_DELIVERYMODE_FLAG)) {
				isFacebookEntry=true;
			} else if (deliveryModeatCurrentIndex.equals(TWITTER_DELIVERYMODE_FLAG)) {
				isTwitterEntry=true;
			} else if (deliveryModeatCurrentIndex.equals(WHATSAPP_DELIVERYMODE_FLAG)) {				
				deliverModeArray[3] = deliveryModeatCurrentIndex;
			}
		}
		
		for (String deliveryMode : deliverModeArray) {
			if (!shortedDeliveryModes.equals("") && !deliveryMode.isEmpty()) {
				shortedDeliveryModes += "#" + deliveryMode;
			} else if (!deliveryMode.isEmpty()) {
				shortedDeliveryModes = deliveryMode;
			}
		}
		logInfo("Universal DelieryModes are "+deliveryModes+" for picId:"+userDTO.getPicId()+"@executeUniversalEntryServices:UniversalEntryHandler");
		logInfo("Shorted DelieryModes are "+shortedDeliveryModes+" for picId:"+userDTO.getPicId()+"@executeUniversalEntryServices:UniversalEntryHandler");
			
		userDTO.setDelieveryMode(shortedDeliveryModes);
		
		if (isUniversalDelieryMode(shortedDeliveryModes)) {
			
			logInfo("Shorted Delievery Mode Found Universal Started Processing  @executeUniversalEntryServices:UniversalEntryHandler");
	
			if (checkEmail.contains(";")) {

				boolean resposneStatus=false;
				
				logInfo("Multiple EmailId's detected for Universal Controller Processing MultipleEmailIds @executeUniversalEntryServices:UniversalEntryHandler");
				UniversalMultiEmailHandler multiEmailHandler = new UniversalMultiEmailHandler(
						userDTO);
				resposneStatus= multiEmailHandler.executeMailerServices(machineId);
				
				if(isFacebookEntry)
				{
					logInfo("Facebook Entry detected for Universal Controller Processing MultipleEmailIds @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
					FacebookUrlBuilder facebookUrlBuilder=new FacebookUrlBuilder(userDTO);
					resposneStatus=facebookUrlBuilder.executeQueryString(machineId);
					logInfo("Facebook Entry Processed for Universal Controller Processing MultipleEmailIds Response Recieved:"+resposneStatus+" @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
					
				}
				if(isTwitterEntry)
				{
					logInfo("Twitter Entry detected for Universal Controller Processing MultipleEmailIds @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
				
					resposneStatus=new TwitterUrlBuilder(userDTO).executeQueryString(machineId);
					logInfo("Twitter Entry Processed for Universal Controller Processing MultipleEmailIds Response Recieved:"+resposneStatus+" @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
					
				}
				
				
				
				return resposneStatus;
				
			} else {

				boolean responseStatus=true;
				UniversalUrlBuilder universalUrlBuidler=new UniversalUrlBuilder(userDTO);
				responseStatus=universalUrlBuidler.executeQueryString(machineId);
				if(isFacebookEntry)
				{
					logInfo("Facebook Entry detected for Universal Controller Processing UniversalMode @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
					FacebookUrlBuilder facebookUrlBuilder=new FacebookUrlBuilder(userDTO);
					responseStatus=facebookUrlBuilder.executeQueryString(machineId);
					logInfo("Facebook Entry Processed for Universal Controller Processing UniversalMode Response Recieved:"+responseStatus+" @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
					
				}
				if(isTwitterEntry)
				{
					logInfo("Twitter Entry detected for Universal Controller Processing MultipleEmailIds @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
				
					responseStatus=new TwitterUrlBuilder(userDTO).executeQueryString(machineId);
					logInfo("Twitter Entry Processed for Universal Controller Processing MultipleEmailIds Response Recieved:"+responseStatus+" @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
					
				}
				return responseStatus;
			}
		}
		else
		{
			logInfo("Shorted Delievery Mode Not Found Universal Started Processing As Normal DelieveryMode  @executeUniversalEntryServices:UniversalEntryHandler");
			
//			Launcher.log("Shorted DeliveryMode is not Universal DeliveryMode Executing All Entries with new DeliveryMode");
			
			boolean responseStatus=true;
			
			new UrlExecutorService().processRemoteEntry(userDTO, machineId);
			
			
			if(isFacebookEntry)
			{
				logInfo("Facebook Entry detected for Shorted Delivery Mode Processing UniversalMode @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
				FacebookUrlBuilder facebookUrlBuilder=new FacebookUrlBuilder(userDTO);
				responseStatus=facebookUrlBuilder.executeQueryString(machineId);
				logInfo("Facebook Entry Processed for Shorted Delivery Mode Processing UniversalMode Response Recieved:"+responseStatus+" @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
				
			}
			if(isTwitterEntry)
			{
				logInfo("Twitter Entry detected for Shorted Delivery Mode Processing MultipleEmailIds @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
			
				responseStatus=new TwitterUrlBuilder(userDTO).executeQueryString(machineId);
				logInfo("Twitter Entry Processed for Shorted Delivery Mode Processing MultipleEmailIds Response Recieved:"+responseStatus+" @executeUniversalEntryServices:UniversalEntryHandler for PicId:"+userDTO.getPicId());
				
			}
			return responseStatus;
			
		}
	}

	private boolean isUniversalDelieryMode(String shortedDeliveryModes) {
		
		logInfo("Checking if shorted delievery mode requires UniversalEntryServices: @executeUniversalEntryServices:UniversalEntryHandler");

		if (shortedDeliveryModes.contains("#")) {
			return true;
		} else {
			
			return false;
		}
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
