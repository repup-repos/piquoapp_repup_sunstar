package com.piquor.imagecomposer.core;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;

import com.piquor.constants.APP_CONS;
import com.piquor.frameconfig.dto.FrameConfig;
import com.piquor.frameconfig.dto.FrameROI;

public class FrameBuilder {
	
	private String LOG_TAG = "FrameBuilder";
	
	
	public Bitmap placeFrameOverImage(FrameConfig frameConfig)
	{
		 FrameROI bgFrameROI = frameConfig.getBgFrameROI();
		 Log.d(LOG_TAG,"Starting Frame Compositions Over Image");
		 
		 //Bitmap mutableBitmap = capturedImage.copy(Bitmap.Config.ARGB_8888, true);
		  
         //WIDTH AND HEIGHT OF DESIRED FRAMEID IMAGE
         int bgIwidth  = FrameBuilderBitmapCons.getBgImage().getWidth();
		 int bgIheight = FrameBuilderBitmapCons.getBgImage().getHeight();
		 int fgwidth = FrameBuilderBitmapCons.getBgImage().getWidth();
		 int fgheight = FrameBuilderBitmapCons.getBgImage().getHeight();
		 
		 //CALCULATING FINAL IMAGE HEIGHT AND WIDTH
		
		 int finalImageHeight = Math.max(bgIheight, fgheight);
		 int finalImageWidth  = Math.max(bgIwidth, fgwidth);
		
		 Log.d(LOG_TAG,"Comparision Heigth and Width FG and FG:"+bgIwidth+"x"+bgIheight+"::cmp::"+fgwidth+"x"+fgheight);
	
		 Log.d(LOG_TAG,"Recived Final Image HeightxWidth:"+finalImageHeight+"x"+finalImageWidth);

		 if(finalImageHeight!=bgIheight||finalImageWidth!=bgIwidth)
		 {	
			 FrameBuilderBitmapCons.setBgImage(Bitmap.createScaledBitmap(FrameBuilderBitmapCons.getBgImage(), finalImageWidth, finalImageHeight, false));
		 }
		 if(finalImageHeight!=fgheight||finalImageWidth!=fgwidth){
			 
			 FrameBuilderBitmapCons.setFgImage(Bitmap.createScaledBitmap(FrameBuilderBitmapCons.getFgImage(), finalImageWidth, finalImageHeight, false));
		 }
		 Bitmap bgImage = FrameBuilderBitmapCons.getBgImage().copy(Bitmap.Config.ARGB_8888, true);
		 
		 Canvas canvas = new Canvas(bgImage);
		 
		 if(APP_CONS.getBitmapCaptured().getHeight()!=bgFrameROI.getHeight()||APP_CONS.getBitmapCaptured().getWidth()!=bgFrameROI.getWidth()){
			 
			 Log.d(LOG_TAG, "Creating BGFrame ROI:"+bgFrameROI.getWidth()+"x"+bgFrameROI.getHeight());
			 APP_CONS.setBitmapProcessed(Bitmap.createScaledBitmap(APP_CONS.getBitmapCaptured(), bgFrameROI.getWidth(), bgFrameROI.getHeight(), false));
		 }
			
		 canvas.drawBitmap(APP_CONS.getBitmapProcessed(), frameConfig.getBgFrameROI().getPosX(), frameConfig.getBgFrameROI().getPosY(), null);
		 
		 
		 
		 canvas.drawBitmap(FrameBuilderBitmapCons.getFgImage(), frameConfig.getFgFrameROI().getPosX(), frameConfig.getFgFrameROI().getPosY(),null);
		 
		 Log.d(LOG_TAG,"Image Composition Successfully Created");
		 
		 return bgImage;
	}
}
