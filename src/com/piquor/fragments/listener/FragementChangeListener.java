package com.piquor.fragments.listener;

import com.piquor.dto.ReviewEntryDTO;

public interface FragementChangeListener {
	
	/*
	 * Interface used for communication between MainActivity/Fragments
	 * needs to be implemented in all fragments needs navigation including MainActivity
	 */
	
//	public void replaceFragment(String info,String currentFramgmentName,String nextFragmentName);

	
	
	public void navigateToReviewPublishScreen();
	
	public void navigateToRestaurantReviewOptionalScreen();
	
	public void navigateToHotelReviewOptionalScreen();
	
	public void navigateToBackScreen();
	
	public void navigateToCaptureScreen();
	
	public void navigateToReviewMediumSelection();
	
	public void navigateToReviewPlaceSelection();
	
	public void navigateToHomeScreen();
	
	public void navigateToImageSharingScreen();
	
	public void navigateToReiewEntryScreen();
	
	public void navigateToAdaptiveQuesitonScreen(ReviewEntryDTO reviewEntryDTO);
	
//	public void navigateToTripAdvisorPostScreen(ReviewEntryDTO reviewEntryDTO);
	public void navigateTotripAdvisorPostViewScreen(ReviewEntryDTO reviewEntryDTO);
	
	public void setTemporaryOnDescDialogText(String reviewDesc);
	
	public String getTemporaryOnDescDialogText();
	
	public float getBatteryLevel();
}
