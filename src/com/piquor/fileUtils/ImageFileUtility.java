package com.piquor.fileUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.piquor.piquortabapp.MainActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

public class ImageFileUtility {
	
	public static String LOG_TAG = "ImageFileUtility";
	public static String ALBUM_NAME_PHOTO = "PiquorApp Photos";
	
	public static File getAppDirectory()
	{
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"PiquorApp Photos");
		if(!mediaStorageDir.exists())
		{
			Log.d(LOG_TAG, "Media Storage Directory Names With PiquorApp dosen't Exsists Creating Directory");
			
			if(! mediaStorageDir.mkdirs())
			{
				Log.d(LOG_TAG,"Creating Media Storage Director");
			}
			Log.d(LOG_TAG, "Media Storage Created");
		}else
		{
			Log.d(LOG_TAG, "Media Storage Already Created");
		}
		return mediaStorageDir;
	}
	
	public static File getAppVideoDirectory()
	{
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"PiquorApp Videos");
		if(!mediaStorageDir.exists())
		{
			Log.d(LOG_TAG, "Media Storage Directory Names With PiquorApp dosen't Exsists Creating Directory");
			
			if(! mediaStorageDir.mkdirs())
			{
				Log.d(LOG_TAG,"Creating Media Storage Director");
			}
			Log.d(LOG_TAG, "Media Storage Created");
		}else
		{
			Log.d(LOG_TAG, "Media Storage Already Created");
		}
		return mediaStorageDir;
	}
	
	public static String getImageFileName()
	{
		  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		  String imageFileName = "JPEG_" + timeStamp + "_";
		  return imageFileName;
	}
	
	public static String getVideoFileName()
	{
		Log.d(LOG_TAG, "Creating Final Frame File To UPload Image");
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
		String videoId = MainActivity.getCamapignInformation().getMachineId() + timeStamp;
		return videoId;
	}
	

	

	
	public static Bitmap loadImageBitmap(String fileName)
	{
		return BitmapFactory.decodeFile(fileName);
	}
}
