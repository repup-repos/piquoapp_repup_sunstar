package com.piquor.datautility;

import java.util.ArrayList;

import android.util.Log;

import com.piquor.datastaticfields.ErrorFlags;
import com.piquor.dto.UserTableDTO;
import com.piquor.errorhandler.DataErrorHandler;
import com.piquor.piquortabapp.MainActivity;
import com.services.launcher.ServicesLauncher;

public class UserPendingUrlEntryData  implements ErrorFlags{

	DataErrorHandler errorHandler;
	private String LOG_TAG;
	
	public UserPendingUrlEntryData()
	{
		errorHandler=new DataErrorHandler();
	}
	
	boolean pendingData=false;
	
	public boolean isDataPending()
	{
		String query = "SELECT count(*) FROM user WHERE mailsent=0 and fileUpload=1 and Status='0'";
		
		int pictureUploadFailedEntries = MainActivity.getDBEntryCount(query);
		ServicesLauncher.showMessage("Pending photos:"+pictureUploadFailedEntries);
		if(pictureUploadFailedEntries>0)
		{
			
			pendingData=true;
			return true;
			
		}else
		{
			pendingData=false;
			return false;
		}
	}
	
	public boolean processPendingUserData(String machineId)
	{
		if(pendingData)
		{
			
			logInfo("Fetching Pending Url Entry Data from database @proccessPendingUserData:UserPendingUrlEntryData");
			ArrayList<UserTableDTO> urlRemainingEntries = MainActivity
				.getUnsendedMailList(URL_PENDING_DATA_FLAG);
			
			//		System.out.println(list.size());
			logInfo("Pending Url Entry Data Fetched  from database Started Processing totoal entries:"+urlRemainingEntries.size()+" @processPendingUserData:UserPendingUrlEntyData" );
			errorHandler.handleRemainUrlEntry(urlRemainingEntries, machineId);
			logInfo("Pending Url Entry Data Processing Completed @processPendingUserData:UserPendingUrlEntryData");
			
			return true;
		}else
		{
			return false;
		}
		
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG,info);
	}
	
}
