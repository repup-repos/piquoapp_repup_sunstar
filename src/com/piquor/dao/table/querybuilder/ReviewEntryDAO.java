package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.PLACE_REVIEW_ENTRIES_COLUMNS;
import com.piquor.dto.ReviewEntryDTO;

public class ReviewEntryDAO implements PLACE_REVIEW_ENTRIES_COLUMNS {
	
	public static String getReviewEntryTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_PLACE_REVIEW_ENTRY;
		return query;
	}
	
	public static String getReviewEntryCreateTableQuery()
	{
		String CREATE_REVIEWENTRY_TABLE = "CREATE TABLE ["+TABLE_NAME_PLACE_REVIEW_ENTRY+"] "
				+ "("
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_ID+"] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_CAMPAIGN_ID+"] varchar(300) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_DATE+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_OVERALL_RATING+"] varchar(10) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_SORTOFVISIT+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_WEREYOUHEREFOR+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWTITLE+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWDESC+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_TYPE+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_OPTIONAL_ENTRIES+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_STATUS+"] varchar(10) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_EMAILID+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_TIMESTAMP+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED+"] varchar(2) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"] varchar(2) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PLACE_REVIEW_ENTRY_PLATFORM_ID+"] varchar(200) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_REVIEWENTRY_TABLE;
		
	}
	
	public static String getAddReviewEntryDAOQuery(ReviewEntryDTO reviewEntryDTO)
	{
	/*	String query =  "insert into "
				+ ""+TABLE_NAME_PLACE_REVIEW_ENTRY
				+" ( "+COLUMN_NAME_PLACE_REVIEW_ENTRY_CAMPAIGN_ID+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_DATE+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_WEREYOUHEREFOR+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_SORTOFVISIT+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_OVERALL_RATING+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWTITLE+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWDESC+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_OPTIONAL_ENTRIES+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_STATUS+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_EMAILID+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_TIMESTAMP+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_PLATFORM_ID+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_TYPE+" ) "
				+ "values ('"
				+reviewEntryDTO.getReviewentry_campaignId()+"','"
				+reviewEntryDTO.getReviewentry_date()+"','"
				+reviewEntryDTO.getReviewentry_wereyouherefor()+"','"
				+reviewEntryDTO.getReviewentry_sortofvisit()+"','"
				+reviewEntryDTO.getReviewentry_overallrating()+"','"
				+reviewEntryDTO.getReviewentry_title()+"','"
				+reviewEntryDTO.getReviewentry_desc()+"','"
				+reviewEntryDTO.getReviewentry_optionaldesc()+"','"
				+reviewEntryDTO.getReviewentry_status()+"','"
				+reviewEntryDTO.getReviewentry_emailid()+"','"
				+reviewEntryDTO.getReviewentry_timestamp()+"','"
				+reviewEntryDTO.getReviewentry_platformid()+"','"
				+reviewEntryDTO.getReviewentry_reviewUploaded()+"','"
				+reviewEntryDTO.getReviewentry_reviewuploadStatus()+"','"
				+reviewEntryDTO.getReviewentry_type()+"')";*/
		
		String query =  "insert into "
				+ ""+TABLE_NAME_PLACE_REVIEW_ENTRY
				+" ( "+COLUMN_NAME_PLACE_REVIEW_ENTRY_CAMPAIGN_ID+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_DATE+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_WEREYOUHEREFOR+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_SORTOFVISIT+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_OVERALL_RATING+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWTITLE+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWDESC+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_OPTIONAL_ENTRIES+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_STATUS+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_EMAILID+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_TIMESTAMP+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_PLATFORM_ID+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+","+
				COLUMN_NAME_PLACE_REVIEW_ENTRY_TYPE+" ) "
				+ "values (?,"
				+"?"+","
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?,"
				+"?)";
		
		return query;
	}
	
	public static String getUpdateEmailErrorQuery(ReviewEntryDTO reviewEntryDTO)
	{
		String query = "update "+TABLE_NAME_PLACE_REVIEW_ENTRY+" set "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"='"+2+"' where "+COLUMN_NAME_PLACE_REVIEW_ENTRY_ID+"='"+reviewEntryDTO.getReviewentry_id()+"'";
		
		return query;
	}
	
	public static String getUpadateReviewSuccessEntryQuery(ReviewEntryDTO reviewEntryDTO)
	{
		
		String query = "update "+TABLE_NAME_PLACE_REVIEW_ENTRY+" set "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"='"+0+"', "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED+"='1' where "+COLUMN_NAME_PLACE_REVIEW_ENTRY_ID+"='"+reviewEntryDTO.getReviewentry_id()+"'";
		return query;
	}
	
	public static String getDbCountForRemainingMailSentQuery()
	{
		String query = "SELECT count(*) FROM "+TABLE_NAME_PLACE_REVIEW_ENTRY+" WHERE "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED+"=0  and "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"='0'";
		return query;
	}
	
	public static String getDbCountForUrlEntryQuery()
	{
		String dbquery = "SELECT count(*) FROM "+TABLE_NAME_PLACE_REVIEW_ENTRY+" WHERE "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"='2'";
		
		return dbquery;
	}
	

	public static String getUnsendedReviewMailListQuery(String flag) {
		
		
		String queryPrefix="select *  FROM "+TABLE_NAME_PLACE_REVIEW_ENTRY+" WHERE ";
		
		String queryCondition="";
		
		if(flag.equals("mailremaintosent"))
		{
			queryCondition=COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED+"='0'  and "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"='0'"; //Mails Remaining to Sent
		}
		else if(flag.equals("emailerror"))
		{
			queryCondition= COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"='2'"; //EmailError
		}
		else if(flag.equals("remainingEntries"))
		{
			queryCondition=COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADED+"='0'  and "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEW_UPLOADEDSTATUS+"='0'";// Remaining Entries
		}
		String Query=queryPrefix+queryCondition;
		
		return Query;
		
	}
	
	public static String getDublicateEntryCheckQuery(ReviewEntryDTO reviewEntryDTO)
	{
		String query = "SELECT count(*) FROM "+TABLE_NAME_PLACE_REVIEW_ENTRY+" WHERE "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWDESC+"='"+reviewEntryDTO.getReviewentry_desc().trim()+"' and "+COLUMN_NAME_PLACE_REVIEW_ENTRY_REVIEWTITLE+"='"+reviewEntryDTO.getReviewentry_title().trim()+"'";
		return query;
	}
	
}
