package com.piquor.fragments;

import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.TripAdisorEssentialsDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.R;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class FragmentTripAdvisorHotelSupportiveQuestion1 extends Fragment implements DeliveryModeFlags,FRAGMENT_SCREEN_NAMES,ACTIVITY_FLAGS,OnClickListener {
	
	private String LOG_TAG =FRAGMENT_REVIEW_TRIPADVISOR_HOTEL_OPTIONAL_QST1;
	
	private RadioGroup rgroupCityCenter;
	private RadioGroup rgroupButique;
	private RadioGroup rgroupTryndy;
	
	private RadioButton rbCityCenterYes;
	private RadioButton rbCityCenterNo;
	private RadioButton rbCityCenterNotSure;
	
	private RadioButton rbButiqueYes;
	private RadioButton rbButiqueNo;
	private RadioButton rbButiqueNotSure;
	
	private RadioButton rbTryndyYes;
	private RadioButton rbTryndyNo;
	private RadioButton rbTryndyNotSure;

	private RatingBar ratingService;

	private RatingBar ratingCleanliness;

	private RatingBar ratingSleepQwality;
	
	private Button btnQst1Restaurant;
	private Button btnQst1NightLife;
	private Button btnQst1Museums;
	private Button btnQst1Shopping;
	private Button btnQst1OutdoorActivities;
	private Button btnQst2Quiet;
	private Button btnQst2Average;
	private Button btnQst2Loud;
	private String quest2Answer = "";
	private String quest1Answer = "";
	private int previousClick = 0;
	
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		fragmentChangeListener = (FragementChangeListener) getActivity();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)	{
	
		View view = inflator.inflate(R.layout.fragmenttahoteloptionalquestion1, parent,false);
		
		rgroupButique = (RadioGroup) view.findViewById(R.id.radioButiqueGroup);
		rgroupCityCenter = (RadioGroup) view.findViewById(R.id.radioCityCenterGroup);
		rgroupTryndy = (RadioGroup) view.findViewById(R.id.radioTryndyGroup);
		
		rbCityCenterNo = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionCityCenterNo);
		rbCityCenterNotSure = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionCityCenterNotSure);
		rbCityCenterYes = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionTryndyYes);
		
		rbButiqueNo = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionButiqueNo);
		rbButiqueNotSure = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionButiqueNotSure);
		rbButiqueYes = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionButiqueYes);
		
		rbTryndyYes = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionTryndyYes);
		rbTryndyNo = (RadioButton) view.findViewById(R.id.rbtxtHotelOptionTryndyNo);
		rbTryndyNotSure = (RadioButton) view.findViewById(R.id.rbtxtHotelOptiontTryndyNotSure);
		
		rgroupButique.check(R.id.rbtxtHotelOptionButiqueNotSure);
		rgroupCityCenter.check(R.id.rbtxtHotelOptionCityCenterNotSure);
		rgroupTryndy.check(R.id.rbtxtHotelOptiontTryndyNotSure);
		
		ImageButton btnSubmitToTripAdvisor = (ImageButton) view.findViewById(R.id.fragmentHotelSupportedOption_btnSubmitToTripAdvisor);
		btnSubmitToTripAdvisor.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				replaceTripAdvisorSubmitScreen();
			}
		});
		
		ratingService = (RatingBar) view.findViewById(R.id.ratingBarRestaurantServices);
		ratingCleanliness = (RatingBar) view.findViewById(R.id.ratingBarAtmoshphare);
		ratingSleepQwality = (RatingBar) view.findViewById(R.id.ratingBarRestaurantFood);
		
		 LayerDrawable stars2 = (LayerDrawable) ratingService.getProgressDrawable();
	     stars2.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
//	             stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
	     stars2.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
	        
	     LayerDrawable stars3 = (LayerDrawable) ratingCleanliness.getProgressDrawable();
	     stars3.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
//	             stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
	     stars3.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
	     
	     LayerDrawable stars4 = (LayerDrawable) ratingSleepQwality.getProgressDrawable();
	     stars4.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
//	             stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
	     stars4.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
		
	     
	     btnQst1Restaurant = (Button) view.findViewById(R.id.btnRestaurant);
		 btnQst1Restaurant.setOnClickListener(this);	
		 btnQst1NightLife = (Button) view.findViewById(R.id.btnNightLife);
		 btnQst1NightLife.setOnClickListener(this);
		 btnQst1Museums = (Button) view.findViewById(R.id.btnMuseums);
		 btnQst1Museums.setOnClickListener(this);
		 btnQst1Shopping = (Button) view.findViewById(R.id.btnShopping);
		 btnQst1Shopping.setOnClickListener(this);
		 btnQst1OutdoorActivities = (Button) view.findViewById(R.id.btnOutDoorActivity);
		 btnQst1OutdoorActivities.setOnClickListener(this);
		 btnQst2Quiet = (Button) view.findViewById(R.id.btnQuite);
		 btnQst2Quiet.setOnClickListener(this);
		 btnQst2Average = (Button) view.findViewById(R.id.btnAverage);
		 btnQst2Average.setOnClickListener(this);
		 btnQst2Loud = (Button) view.findViewById(R.id.btnLoud);
		 btnQst2Loud.setOnClickListener(this);
	     
	     
		return view;
	}

	protected void replaceTripAdvisorSubmitScreen() {

		int cityCenterId = rgroupCityCenter.getCheckedRadioButtonId();
		int boutiqueId = rgroupCityCenter.getCheckedRadioButtonId();
		int tryndyId = rgroupTryndy.getCheckedRadioButtonId();

		if (rbButiqueNo.getId() == boutiqueId) {
			
			TripAdisorEssentialsDTO.setIsThisaBoutiqueHotel("no");
		} else if (rbButiqueNotSure.getId() == boutiqueId) {
			
			TripAdisorEssentialsDTO.setIsThisaBoutiqueHotel("notsure");
		} else if (rbButiqueYes.getId() == boutiqueId) {
			
			TripAdisorEssentialsDTO.setIsThisaBoutiqueHotel("yes");
		}else
		{
			TripAdisorEssentialsDTO.setIsThisaBoutiqueHotel("notsure");
		}
		if(rbCityCenterNo.getId()==cityCenterId)
		{
			TripAdisorEssentialsDTO.setIsThisaCityCenter("no");
		}else if(rbCityCenterNotSure.getId()==cityCenterId){
			
			TripAdisorEssentialsDTO.setIsThisaCityCenter("notsure");
		}else if(rbCityCenterYes.getId()==cityCenterId)
		{
			TripAdisorEssentialsDTO.setIsThisaCityCenter("yes");
		}else
		{
			TripAdisorEssentialsDTO.setIsThisaCityCenter("notsure");
		}
		
		if(rbTryndyNo.getId()==tryndyId)
		{
			TripAdisorEssentialsDTO.setIsThisHotelaTryndy("no");
		}else if(rbTryndyNotSure.getId()==tryndyId)
		{
			TripAdisorEssentialsDTO.setIsThisHotelaTryndy("notsure");
		}else if(rbTryndyYes.getId()==tryndyId)
		{
			TripAdisorEssentialsDTO.setIsThisHotelaTryndy("yes");
		}else
		{
			TripAdisorEssentialsDTO.setIsThisHotelaTryndy("notsure");
		}
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_CLEANLINESS_RATING(ratingCleanliness.getRating());
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_SLEEP_QWALITY_RATING(ratingService.getRating());
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_SERVICES_RATING(ratingSleepQwality.getRating());
	
		fragmentChangeListener.navigateToReviewPublishScreen();
		
	}


	private int concurrentClick = 0;
	private String selectedText = "";
	
	@Override
	public void onClick(View v) {
		
		Button clickedButton = (Button)v;
		int transparent_color = android.R.color.transparent;
		
		String text = clickedButton.getTag().toString().trim();
		if(concurrentClick==1&&previousClick==v.getId()||quest1Answer.contains(text))
		{
			concurrentClick = 0;
			String flag = "";
			
			if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt1)))
			{		
				flag = "qus1";
				clickedButton.setBackgroundResource(transparent_color);
			}
			else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt2)))
			{		
				flag = "qus1";
				clickedButton.setBackgroundResource(transparent_color);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt3)))
			{		
				flag = "qus1";
				clickedButton.setBackgroundResource(transparent_color);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt4)))
			{		
				flag = "qus1";
				clickedButton.setBackgroundResource(transparent_color);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt5)))
			{		
				flag = "qus1";
				clickedButton.setBackgroundResource(transparent_color);
			}
			if(flag.equals("qus1"))
			{
				removeQuestion1Text(text);
			}
			
		}else
		{
			Log.d(LOG_TAG, "Checking Single Touch for changing touch");
			if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt1)))
			{		
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}
			else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt2)))
			{		
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt3)))
			{		
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt4)))
			{		
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion1Opt5)))
			{		
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt1)))
			{
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt2)))
			{
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt3)))
			{
				clickedButton.setBackgroundResource(R.color.restaurantselectionListWereYouHereForSelectedColor);
			}
			
			if(v.getId()==btnQst1Museums.getId())
			{
				setQuestion1Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}else if(v.getId()==btnQst1NightLife.getId())
			{
				
				setQuestion1Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}else if(v.getId()==btnQst1OutdoorActivities.getId())
			{
				setQuestion1Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}else if(v.getId()==btnQst1Restaurant.getId())
			{
				setQuestion1Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}else if(v.getId()==btnQst1Shopping.getId())
			{
				setQuestion1Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}else if(v.getId()==btnQst2Average.getId())
			{
				setQuestion2Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}else if(v.getId()==btnQst2Loud.getId())
			{
				setQuestion2Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}else if(v.getId()==btnQst2Quiet.getId())
			{
				setQuestion2Answer(clickedButton.getTag().toString().trim());
				concurrentClick++;
			}
		}
		String flag = "";
		String flag2 = "";
		if(selectedText.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt1)))
		{
			flag2 = "qus2";
			
		}else if(selectedText.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt2)))
		{
			flag2 = "qus2";
		}else if(selectedText.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt3)))
		{
			flag2 = "qus2";
		}
		if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt1))&&flag2.equals("qus2"))
		{
			flag = "qus2";
			btnprevClicked.setBackgroundResource(transparent_color);
		}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt2))&&flag2.equals("qus2"))
		{
			flag = "qus2";
			btnprevClicked.setBackgroundResource(transparent_color);
		}else if(text.equals(getResources().getString(R.string.btntxtHotelOptionSupporteQuestion2Opt3))&&flag2.equals("qus2"))
		{
			flag = "qus2";
			btnprevClicked.setBackgroundResource(transparent_color);
		}
		
		previousClick = v.getId();
		if(concurrentClick==1)
		{
			concurrentClick = 0;
		}
		selectedText = v.getTag().toString().trim();
		btnprevClicked = (Button) v;
	}
	
	private Button btnprevClicked;

	private FragementChangeListener fragmentChangeListener;

	private void removeQuestion2Text(String text) {
	
		quest2Answer = quest2Answer.replaceAll(text, "");
	}

	private void removeQuestion1Text(String text) {
		quest1Answer = quest1Answer.replaceAll(text, "");
	}

	private void setQuestion2Answer(String qst2) {
		
		addQuestion2Answer(qst2);
	}

	private void addQuestion1Anser(String qst1) {
		
		if (!quest1Answer.contains(qst1)) {

			if (quest1Answer.equals("")) {
				quest1Answer = quest2Answer + qst1;
			} else {
				quest1Answer = quest1Answer + "#" + qst1;
			}
		}
	}

	private void setQuestion1Answer(String qst1) {
	
		
		addQuestion1Anser(qst1);
	}
	
	private void addQuestion2Answer(String qst1)
	{
		if (quest2Answer.contains(qst1)) {
			if (quest2Answer.equals("")) {
				quest2Answer =  qst1;
				}
		}
	}

}
