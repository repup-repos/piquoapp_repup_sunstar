package com.piquor.serviceshandler;

import android.util.Log;

import com.piquor.datautility.ReviewPendingUrlEntryData;
import com.piquor.datautility.ReviewRemainUrlEntryData;
import com.piquor.datautility.ReviewUrlEntryErrorData;
import com.piquor.utility.PiquorTimestamp;
import com.services.launcher.ServicesLauncher;

public class RepupServicesHandler {

	ReviewPendingUrlEntryData userPendingUrlEntryData;
	ReviewRemainUrlEntryData reviewRmainngEntryData;
	ReviewUrlEntryErrorData reviewUrlEntryErrorData;
	private String LOG_TAG = "PiquorServicesHandler";
	
	public RepupServicesHandler()
	{
		userPendingUrlEntryData=new ReviewPendingUrlEntryData();
		reviewRmainngEntryData=new ReviewRemainUrlEntryData();
		reviewUrlEntryErrorData=new ReviewUrlEntryErrorData();
	
	}
	
	public void executeServices()
	{
		
		if(isInternetConnected())
		{
			logInfo("Internet Connected Starting Processing @PiquorServiceaHandler.executeServices() Time:"+PiquorTimestamp.getTimeStamp());
			if(userPendingUrlEntryData.isDataPending())
			{
				logInfo("Pending Email Entries Found @PiquorServiceHandler.executeServices() Time:"+PiquorTimestamp.getTimeStamp());
				userPendingUrlEntryData.processPendingUserData();
				logInfo("Pending Email Entries Processed @PiquorServiceHandler.executeServices Time:"+PiquorTimestamp.getTimeStamp());
			}else if(reviewUrlEntryErrorData.isDataPending())
			{
				logInfo("Url Error Entries Found @PiquorServiceHandler.executeServices Time:"+PiquorTimestamp.getTimeStamp());
				reviewUrlEntryErrorData.processPendingUserData();
				logInfo("Url Error Entries Processed @PiquorServiceHandler.executeServices Time:"+PiquorTimestamp.getTimeStamp());
			}else if(reviewRmainngEntryData.isDataPending())
			{
				logInfo("Remaing User Entries Found @PiquorServiceHandler.executeServices");
				reviewRmainngEntryData.processPendingUserData();
				logInfo("Remaing User Entries Processed @PiquorServiceHandler.executeServices");
			}else
			{
				logInfo(" No Reviews Pending @PiquorServiceHandler.execteservices()");
			}
			
		}
		else
		{
			logInfo("Internet Disconnected Executing Scripts:@execteServieces:PiquorServiceHandler");
		 
		}
	}

	

	private boolean isInternetConnected() {
		
		logInfo("Checking Internet Connection isInternetConnected@PiquorSericeHandler");
		return ServicesLauncher.checkInternetConnectivity();
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
}
