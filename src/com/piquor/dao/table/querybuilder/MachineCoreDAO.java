package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.CAMPAIGN_MACHINE_CORE;
import com.piquor.dto.MachineCoreDTO;
import com.piquor.dto.ReviewsConfigDTO;

public class MachineCoreDAO implements CAMPAIGN_MACHINE_CORE {

	public static String getMahchineCoreTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_CAMPAIGN_MACHINE_CORE;
		return query;
	}
	public static String getCreateMachineCoreTableQuery()
	{
		String CREATE_MACHINE_CORE_TABLE = "CREATE TABLE ["+TABLE_NAME_CAMPAIGN_MACHINE_CORE+"] "
				+ "("
				+ "["+COLUMN_NAME_MACHINE_CORE_RECORDID+"] varchar(100)  NOT NULL PRIMARY KEY ,"
				+ "["+COLUMN_NAME_MACHINE_CORE_MACHINEID+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_CORE_DEFAULT_CAMPAIGNID+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_CORE_CONFIGURATIONMANGER_RESTART_TIMEPATTERN+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_CORE_STATUSREPORTER_RESTART_TIMEPATTERN+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_CORE_SERVICEHANDLER_RESTART_TIMEPATTERN+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_CORE_UPDATESTATUS+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MACHINE_CORE_TIMESTAMP+"] varchar(100) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_MACHINE_CORE_TABLE;
	}
	
	public static String getMachineCoreTotalRowQuery()
	{
		String query = "SELECT COUNT(*) FROM "+TABLE_NAME_CAMPAIGN_MACHINE_CORE;
		
		return query;
		
	}
	
	public static String getMachineCoreTotalEntryQuery(MachineCoreDTO machineCoreDTO)
	{
		String query =  "insert into "
				+ ""+TABLE_NAME_CAMPAIGN_MACHINE_CORE
				+" ( "+COLUMN_NAME_MACHINE_CORE_RECORDID+","+
				COLUMN_NAME_MACHINE_CORE_DEFAULT_CAMPAIGNID+","+
				COLUMN_NAME_MACHINE_CORE_MACHINEID+","+
				COLUMN_NAME_MACHINE_CORE_SERVICEHANDLER_RESTART_TIMEPATTERN+","+
				COLUMN_NAME_MACHINE_CORE_STATUSREPORTER_RESTART_TIMEPATTERN+","+
				COLUMN_NAME_MACHINE_CORE_CONFIGURATIONMANGER_RESTART_TIMEPATTERN+","+
				COLUMN_NAME_MACHINE_CORE_TIMESTAMP+","+
				COLUMN_NAME_MACHINE_CORE_UPDATESTATUS+") "
				+ "values ('"
				+machineCoreDTO.getRecordid()+"','"
				+machineCoreDTO.getDefaultcampaignid()+"','"
				+machineCoreDTO.getMachineid()+"','"
				+machineCoreDTO.getConfigurationmanager_servicehandlertimepattern()+"','"
				+machineCoreDTO.getConfigurationmanager_statusreportertimepattern()+"','"
				+machineCoreDTO.getMachinecore_configurationmanagerrestarttimepattern()+"','"
				+machineCoreDTO.getTimestamp()+"','"
				+machineCoreDTO.getUpdatestatus()+"')";
		
		return query;
	}
	public static String getUpdateErrorStatusOnMachineCoreQuery() {

		String MACHINE_CORE_ERROR_UPDATE = "UPDATE " + TABLE_NAME_CAMPAIGN_MACHINE_CORE + " SET ";
		MACHINE_CORE_ERROR_UPDATE += COLUMN_NAME_MACHINE_CORE_UPDATESTATUS + "='0'";
			
		return MACHINE_CORE_ERROR_UPDATE;
	
	}
	
	public static String getUpdateCompleteStatusOnMachineCoreQuery()
	{
		String MACHINE_CORE_UPDATE_SUCCEEDED_STATUS = "UPDATE " + TABLE_NAME_CAMPAIGN_MACHINE_CORE + " SET ";
		MACHINE_CORE_UPDATE_SUCCEEDED_STATUS += COLUMN_NAME_MACHINE_CORE_UPDATESTATUS + "='1'";
		
		return MACHINE_CORE_UPDATE_SUCCEEDED_STATUS;
		
	}
	
	public static String getUpdateStatusOnMachineCoreQuery()
	{
		String query = "select "+COLUMN_NAME_MACHINE_CORE_UPDATESTATUS+" from "+TABLE_NAME_CAMPAIGN_MACHINE_CORE;
		
		return query;
	}
	
}
