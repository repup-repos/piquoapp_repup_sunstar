package com.services.servicecontroller;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.piquor.serviceshandler.AppUpdateServiceHandler;
import com.piquor.serviceshandler.PiquorServicesHandler;
import com.piquor.serviceshandler.RepupServicesHandler;
import com.piquor.utility.PiquorTimestamp;

public class ServicesController {
	
	Handler timerHandler;
	PiquorServicesHandler serviceHandler;
	RepupServicesHandler repupServicesHandler;
	AppUpdateServiceHandler appUpdateServiceHandler;
	
	private long milliseconds;
	private String LOG_TAG = "ServicesController";
	
	public ServicesController(int timeOutInSeconds)
	{
		milliseconds = 1000 * timeOutInSeconds;
		timerHandler = new Handler();
		serviceHandler = new PiquorServicesHandler();
		repupServicesHandler = new RepupServicesHandler();
		appUpdateServiceHandler = new AppUpdateServiceHandler();
	}
	
	Runnable runningTask = new Runnable() {
		
		@Override
		public void run() {
			
			
			logInfo("MailerInstanceStarted Executing service handler for operations @UrlExectorServiceController Time:"+PiquorTimestamp.getTimeStamp());
			
					logInfo("Executing Services:");
					new ExecuteServiceHandler().execute();
		}
	};
	
	public void startTimer()
	{
		logInfo("Services Started Here");
		timerHandler.postDelayed(runningTask, milliseconds);
	}
	
	public void stopTimer()
	{
		logInfo("|||||||||||||||||||||||||||| Stopping Timer");
		timerHandler.removeCallbacks(runningTask);
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
	class ExecuteServiceHandler extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			
			serviceHandler.executeServices();
			repupServicesHandler.executeServices();
			appUpdateServiceHandler.executeServices();
			return null;
		}
		
		@Override
		 protected void onPostExecute(Void result) {
			 
			delayAgainTimer();
		 }

	    
	}

	public void delayAgainTimer() {

		timerHandler.postDelayed(runningTask, milliseconds);
	}
	
}
