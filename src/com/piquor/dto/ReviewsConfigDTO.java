package com.piquor.dto;

public class ReviewsConfigDTO {

	private String id;
	private String tripAdvisorRestaurentName;
	private String tripAdvisorRestaurantURL;
	private String tripAdvisorRestaurantType;
	private String tripAdvisorRestaurantZomatoUrl;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTripAdvisorRestaurentName() {
		return tripAdvisorRestaurentName;
	}
	public void setTripAdvisorRestaurentName(String tripAdvisorRestaurentName) {
		this.tripAdvisorRestaurentName = tripAdvisorRestaurentName;
	}
	public String getTripAdvisorRestaurantURL() {
		return tripAdvisorRestaurantURL;
	}
	public void setTripAdvisorRestaurantURL(String tripAdvisorRestaurantURL) {
		this.tripAdvisorRestaurantURL = tripAdvisorRestaurantURL;
	}
	
	public String getTripAdvisorRestaurantType() {
		return tripAdvisorRestaurantType;
	}
	public void setTripAdvisorRestaurantType(String tripAdvisorRestaurantType) {
		this.tripAdvisorRestaurantType = tripAdvisorRestaurantType;
	}
	@Override
	public String toString() {
		return "TripAdvisorConfigDTO [id=" + id
				+ ", tripAdvisorRestaurentName=" + tripAdvisorRestaurentName
				+ ", tripAdvisorRestaurantURL=" + tripAdvisorRestaurantURL
				+ ", tripAdvisorRestaurantType=" + tripAdvisorRestaurantType
				+ "]";
	}
	public String getTripAdvisorRestaurantZomatoUrl() {
		return tripAdvisorRestaurantZomatoUrl;
	}
	public void setTripAdvisorRestaurantZomatoUrl(
			String tripAdvisorRestaurantZomatoUrl) {
		this.tripAdvisorRestaurantZomatoUrl = tripAdvisorRestaurantZomatoUrl;
	}
}	
