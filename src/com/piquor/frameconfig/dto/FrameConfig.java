package com.piquor.frameconfig.dto;


public class FrameConfig {
	
	private int frameId;
	private int fgFrameID;
	private int bgFrameID;
	private int thumbnailId;
	private FrameROI bgFrameROI;
	private FrameROI fgFrameROI;
	
	public FrameConfig(int frameId,int fgFrameId,int bgFrameId,int thumbnailId,FrameROI bgFrameROI,FrameROI fgFrameROI)
	{
		this.frameId = frameId;
		this.thumbnailId = thumbnailId;
		this.fgFrameID = fgFrameId;
		this.bgFrameID = bgFrameId;
		this.bgFrameROI = bgFrameROI;
		this.fgFrameROI = fgFrameROI;
	}
	
	public int getFrameId() {
		return frameId;
	}


	public void setFrameId(int frameId) {
		this.frameId = frameId;
	}


	public int getFgFrameID() {
		return fgFrameID;
	}


	public void setFgFrameID(int fgFrameID) {
		this.fgFrameID = fgFrameID;
	}


	public int getBgFrameID() {
		return bgFrameID;
	}


	public void setBgFrameID(int bgFrameID) {
		this.bgFrameID = bgFrameID;
	}
	
	public FrameROI getBgFrameROI() {
		return bgFrameROI;
	}
	public void setBgFrameROI(FrameROI bgFrameROI) {
		this.bgFrameROI = bgFrameROI;
	}
	public FrameROI getFgFrameROI() {
		return fgFrameROI;
	}
	public void setFgFrameROI(FrameROI fgFrameROI) {
		this.fgFrameROI = fgFrameROI;
	}

	public String toString()
	{
		String toString = "FrameConfiguration: FgFrameId:"+fgFrameID+" ROI:"+fgFrameROI.toString()+" BgFrameId:"+bgFrameID+" ROI:"+bgFrameROI.toString()+" FrameId:"+frameId;
		return toString;
	}

	public int getThumbnailId() {
		return thumbnailId;
	}

	public void setThumbnailId(int thumbnailId) {
		this.thumbnailId = thumbnailId;
	}
}
