package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.USER_VIDEOS_TABLE_COLUMNS;
import com.piquor.dto.VideoUserDTO;

public class VideoUserDAO implements USER_VIDEOS_TABLE_COLUMNS {
	
	public static String getVideoUserTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_VIDEO_USER;
		return query;
	}
	
	public static String getVideoUserCreateTable()
	{
		String CREATE_VIDEOUSER_TABLE = "CREATE TABLE ["+TABLE_NAME_VIDEO_USER+"] "
				+ "("
				+ "["+COLUMN_NAME_VIDEO_USER_ID+"] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,"
				+ "["+COLUMN_NAME_VIDEO_USER_VIDEOID+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_VIDEO_USER_CAPTURE_DATE+"] varchar(100) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_VIDEOUSER_TABLE;
	}
	
	public static String getVideoInsertQuery(VideoUserDTO userDTO)
	{
		String query =  "insert into "
				+ ""+TABLE_NAME_VIDEO_USER
				+" ( "+COLUMN_NAME_VIDEO_USER_VIDEOID+","+
				COLUMN_NAME_VIDEO_USER_CAPTURE_DATE+" ) "
				+ "values ('"
				+userDTO.getUservideoId()+"','"
				+userDTO.getVideoCaptureDate()+"')";
		return query;
	}
}
