package com.services.deliverymode.urlbuilder;

import android.util.Log;

import com.piquor.dto.UserTableDTO;
import com.piquor.utility.PiquorTimestamp;
import com.services.deliverymode.staticfields.DeliveryModeUrls;
import com.services.deliverymode.utility.UrlBuilderUtility;

public class UniversalUrlBuilder extends UrlBuilderUtility implements DeliveryModeUrls {

	public UniversalUrlBuilder(UserTableDTO userDTO) {
		super(userDTO);
	}

	String universalEntryUrl;
	
	@Override
	public boolean executeQueryString(String machineId) {
		
	    
		universalEntryUrl=UNIVERSAL_URL_PREFIX+buildUniverEntryUrl(machineId);
		Log.d("UniveralUrlBuilder","Executing Universal Entry URL:"+universalEntryUrl+":For PicId:"+userDTO.getPicId()+"executeQueryString:UniversalUrlBuilder"+"Time:"+PiquorTimestamp.getTimeStamp());
		return executeQuery(universalEntryUrl);
	
	}

	private String buildUniverEntryUrl(String machineId) {
		
		String queryParameter = "name=" + userParamsDTO.getUserName() + "&emailId=" + userParamsDTO.getUserEmail()
				+ "&dob=" + userParamsDTO.getUserDOB() + "&phone=" + userParamsDTO.getUserPhone()
				+ "&captureDate=" + userParamsDTO.getPhotoCaptureDate() + "&campaignId="
				+ userParamsDTO.getUserCampaignId() + "&picId="
				+ userParamsDTO.getPhotoPicId() + "&machineId="
				+ machineId
				+ "&deliveryMode=" + userParamsDTO.getUserDelieveryMode()
				+ "&socialMediaPermission="
				+ userParamsDTO.getUserPhotoPermissionSocialMedia()
				+ "&gender=" + userParamsDTO.getUserGender();
		return queryParameter;
	}

	
}
