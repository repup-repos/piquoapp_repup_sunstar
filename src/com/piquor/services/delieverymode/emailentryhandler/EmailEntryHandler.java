package com.piquor.services.delieverymode.emailentryhandler;

import java.util.ArrayList;
import java.util.StringTokenizer;

import android.util.Log;

import com.piquor.dto.UserTableDTO;
import com.piquor.utility.PiquorTimestamp;
import com.services.deliverymode.staticfields.DeliveryModeFlags;
import com.services.deliverymode.urlbuilder.EmailUrlBuilder;

public class EmailEntryHandler implements DeliveryModeFlags {

	private UserTableDTO userDTO;
	private static String LOG_TAG = "EmailEntryHandler";
	public EmailEntryHandler(UserTableDTO userDTO)
	{
		this.userDTO=userDTO;
	}
	
	public boolean executeEmailEntryServices(String machineId)
	{
		String chkEmailId=userDTO.getEmail().trim();
		
		boolean responseEmailEntry=false;
		logInfo("Checking For Multiple EmailIds for Picid:"+userDTO.getPicId());
		if(chkEmailId.contains(";"))
		{
			logInfo("Multiple Email Ids Detected For PicId:"+userDTO.getPicId()+" Starting Processing For Each EmailIds emailIds:"+chkEmailId +"EmailEntryHandler:executeEmailEntryServices() time:"+PiquorTimestamp.getTimeStamp());
			ArrayList<String> emailIdList=new ArrayList<String>();
			StringTokenizer emailToken=new StringTokenizer(chkEmailId,";");
			
			while (emailToken.hasMoreElements()) {

				emailIdList.add(emailToken.nextElement().toString().trim());
			}
			logInfo("ArrayList prepared  for PicId:"+userDTO.getPicId()+" for EmailIds Lenght:"+emailIdList.size()+" @EmailEntryHandler:executeEmailEntryServices");
			
			for(int emailIdIndex=0;emailIdIndex<emailIdList.size();emailIdIndex++)
			{
				
				String emailId=emailIdList.get(emailIdIndex);
				
					if(!emailId.equals(""))
					{
						userDTO.setEmail(emailId);
						userDTO.setDelieveryMode(EMAIL_DELIVERYMODE_FLAG);
						logInfo("Registering "+emailIdIndex+" EmailId:"+emailId+" as Email Entry Holder for PicId:"+userDTO.getPicId()+"EmailEntryHandler:executeEmailEntryServices() time:"+PiquorTimestamp.getTimeStamp());
						responseEmailEntry= new EmailUrlBuilder(userDTO).executeQueryString(machineId);
				}	
			}
		
		}else
		{
			logInfo("Single Email Id Detected for PicId:"+userDTO.getPicId()+" Starting Processing: EmailEntryHandler:executeEmailEntryServices() time:"+PiquorTimestamp.getTimeStamp());
			responseEmailEntry=new EmailUrlBuilder(userDTO).executeQueryString(machineId);
		}
		
		return responseEmailEntry;
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}



