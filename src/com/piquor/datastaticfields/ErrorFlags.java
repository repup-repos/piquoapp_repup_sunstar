package com.piquor.datastaticfields;

public interface ErrorFlags {
	
	public final String PICTURE_UPLOAD_EFFOR_FLAG="pictureerror";
	public final String URL_ENTRY_ERROR_FLAG="emailerror";
	public final String URL_PENDING_DATA_FLAG="mailremaintosent";
	public final String USER_REMAIING_DATA_FLAG="remainingEntries";
	
}
