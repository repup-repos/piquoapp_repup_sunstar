package com.piquor.imagecomposer.core;

import android.graphics.Bitmap;
import android.util.Log;

public class FrameBuilderBitmapCons {

	private static Bitmap fgImage = null;
	private static Bitmap bgImage = null;
	private static Bitmap finalFrameImage = null;
	
	private static Bitmap fgImage1 = null;
	public static Bitmap getFgImage1() {
		return fgImage1;
	}
	public static void setFgImage1(Bitmap fgImage1) {
		FrameBuilderBitmapCons.fgImage1 = fgImage1;
	}
	public static Bitmap getBgImage1() {
		return bgImage1;
	}
	public static void setBgImage1(Bitmap bgImage1) {
		FrameBuilderBitmapCons.bgImage1 = bgImage1;
	}
	public static Bitmap getFinalFrameImage1() {
		return finalFrameImage1;
	}
	public static void setFinalFrameImage1(Bitmap finalFrameImage1) {
		FrameBuilderBitmapCons.finalFrameImage1 = finalFrameImage1;
	}
	public static Bitmap getFgImage2() {
		return fgImage2;
	}
	public static void setFgImage2(Bitmap fgImage2) {
		FrameBuilderBitmapCons.fgImage2 = fgImage2;
	}
	public static Bitmap getBgImage2() {
		return bgImage2;
	}
	public static void setBgImage2(Bitmap bgImage2) {
		FrameBuilderBitmapCons.bgImage2 = bgImage2;
	}
	public static Bitmap getFinalFrameImage2() {
		return finalFrameImage2;
	}
	public static void setFinalFrameImage2(Bitmap finalFrameImage2) {
		FrameBuilderBitmapCons.finalFrameImage2 = finalFrameImage2;
	}
	private static Bitmap bgImage1 = null;
	private static Bitmap finalFrameImage1 = null;
	
	private static Bitmap fgImage2 = null;
	private static Bitmap bgImage2 = null;
	private static Bitmap finalFrameImage2 = null;
	
	private static Bitmap resizedFrameImage = null;
	
	public static Bitmap getFgImage() {
		return fgImage;
	}
	public static void setFgImage(Bitmap fgImage) {
		FrameBuilderBitmapCons.fgImage = fgImage;
	}
	public static Bitmap getBgImage() {
		return bgImage;
	}
	public static void setBgImage(Bitmap bgImage) {
		FrameBuilderBitmapCons.bgImage = bgImage;
	}
	public static Bitmap getFinalFrameImage() {
		return finalFrameImage;
	}
	public static void setFinalFrameImage(Bitmap finalFrameImage) {
		FrameBuilderBitmapCons.finalFrameImage = finalFrameImage;
	}
	private static String LOG_TAG = "FrameBuilderBitmapCons";
	
	public static void recycleTempImages()
	{
		if(finalFrameImage!=null)
		{
			Log.d(LOG_TAG, "Relasing Final Frame Image");
			finalFrameImage.recycle();
			finalFrameImage = null;
			
		}
		if(finalFrameImage1!=null)
		{
			finalFrameImage1.recycle();
			finalFrameImage1 = null;
		}
		if(finalFrameImage2!=null)
		{
			finalFrameImage2.recycle();
			finalFrameImage2 = null;
		}
		if(resizedFrameImage!= null)
		{
			resizedFrameImage.recycle();
			resizedFrameImage = null;
		}
	}
	
	public static void recycleAllImages()
	{
		
		Log.d("FrameBuilderBitmapCons", "Recycle All Bitmap Images on FrameBuilderBitmapCons");
		if(finalFrameImage!=null)
		{
			finalFrameImage.recycle();
			finalFrameImage = null;
		}
		if(fgImage!= null)
		{
			Log.d(LOG_TAG," >> Relasing foreground image");
			fgImage.recycle();
			fgImage = null;
		}
		if(bgImage!= null)
		{
			Log.d(LOG_TAG," >> Relasing background image");
			bgImage.recycle();
			bgImage = null;
		}
		if(fgImage1!=null)
		{
			fgImage1.recycle();
			fgImage1 = null;
		}
		if(bgImage1!=null)
		{
			bgImage1.recycle();
			bgImage1 = null;
		}
		if(fgImage2!=null)
		{
			fgImage2.recycle();
			fgImage2 = null;
		}
		if(bgImage2!=null)
		{
			bgImage2.recycle();
			bgImage2 = null;
		}
	}
	public static Bitmap getResizedFrameImage() {
		return resizedFrameImage;
	}
	public static void setResizedFrameImage(Bitmap resizedFrameImage) {
		FrameBuilderBitmapCons.resizedFrameImage = resizedFrameImage;
	}
}
