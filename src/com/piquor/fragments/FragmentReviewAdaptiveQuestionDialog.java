package com.piquor.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.piquor.app.adaptivequestion.utility.AdaptiveQuestions;
import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.TripAdisorEssentialsDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragmentTextSetView;
import com.piquor.piquortabapp.R;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class FragmentReviewAdaptiveQuestionDialog extends DialogFragment
		implements  DeliveryModeFlags,
		FRAGMENT_SCREEN_NAMES, ACTIVITY_FLAGS {

	private String LOG_TAG = "FragmentAdaptiveQestionScreen";
	private int REVIEW_SUBMIT_START = 0;
	private int REVIEW_SUBMIT_COMPLETE = 1;
	private EditText etReviewAnswer;
	private TextView tvReviewQuestion;
	private int questionCounter = 0;
	private AdaptiveQuestions adaptiveQuestion;
	// private TextView tvReviewCharacterCount;
//	private int minimumCharacterLimit = 0;
	private String DESCRIPTION_ERROR = "Empty";
	private String[] QUESTION_KEYS;

	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
	}
	
	
	
	@Override
	public void onActivityCreated(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onActivityCreated(arg0);
		
		fragmentTextSetView = (FragmentTextSetView) getActivity();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
				getActivity());
		LayoutInflater inflator = getActivity().getLayoutInflater();
		View view = inflator.inflate(R.layout.fragmentreviewadaptivequestion,
				null);
		alertBuilder.setView(view);
		setCancelable(false);
//		alertBuilder.setMessage("Please provide your feedback");
		alertBuilder.setCancelable(false);
	/*	alertBuilder.setNeutralButton(R.string.txtreviewadaptivedilog_next,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
		alertBuilder.setPositiveButton(R.string.txtreviewadaptivedilog_skip,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						// processEmailEntry();
					}
				});*/
		
		ImageButton neutralButton = (ImageButton) view.findViewById(R.id.btnfragmentreviewadaptivequestion_skip);
		neutralButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				processReviewEntry();
			}
		});
		
		ImageButton postiveButton = (ImageButton) view.findViewById(R.id.btnfragmentreviewadaptivequestion_next);
		postiveButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				processReviewEntry();
			}
		});
		
		

		etReviewAnswer = (EditText) view
				.findViewById(R.id.txtReviewAdaptiveAnswer);
		tvReviewQuestion = (TextView) view
				.findViewById(R.id.txtReviewAdaptiveQuestion);
		// tvReviewCharacterCount = (TextView)
		// view.findViewById(R.id.txtAdaptiveReviewCharacterCount);
		adaptiveQuestion = new AdaptiveQuestions();

		QUESTION_KEYS = adaptiveQuestion.getQuestionKeys(); 
		
		if(adaptiveQuestion.isQuestionsEmpty())
		{
			dismiss();
		}else
		{

			String questionText = adaptiveQuestion
					.getNextQuestion(QUESTION_KEYS[questionCounter]);
			// minimumCharacterLimit = QUESTION_WEIGHTS[questionCounter];
			tvReviewQuestion.setText(questionText);
			// tvReviewCharacterCount.setText("Minimum Character left "+minimumCharacterLimit);
			questionCounter++;

			/*
			 * etReviewAnswer.setOnFocusChangeListener(new
			 * OnFocusChangeListener() {
			 * 
			 * @Override public void onFocusChange(View v, boolean hasFocus) {
			 * 
			 * if(etReviewAnswer.getText().toString().length() > 0){
			 * if(etReviewAnswer.getText().toString().length() <
			 * minimumCharacterLimit) {
			 * etReviewAnswer.setError(DESCRIPTION_ERROR); } } } });
			 */
			// etReviewAnswer.addTextChangedListener(mTextEditorWatcher);
		}
		Dialog dialog = alertBuilder.create();

		return dialog;

	}

	/*
	 * private final TextWatcher mTextEditorWatcher = new TextWatcher() { public
	 * void beforeTextChanged(CharSequence s, int start, int count, int after) {
	 * }
	 * 
	 * public void onTextChanged(CharSequence s, int start, int before, int
	 * count) { //This sets a textview to the current length
	 * 
	 * int leftCharacters = minimumCharacterLimit-s.length();
	 * 
	 * if(leftCharacters<0) { leftCharacters = 0; }
	 * tvReviewCharacterCount.setText("Minmum Character Left:"+leftCharacters);
	 * }
	 * 
	 * public void afterTextChanged(Editable s) { } };
	 */

	@Override
	public void onStart() {
		super.onStart();

		AlertDialog d = (AlertDialog) getDialog();
		/*if (d != null) {
			Button positiveButton = (Button) d
					.getButton(Dialog.BUTTON_POSITIVE);
			positiveButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					processReviewEntry();
				}
			});
			Button negativeButton = (Button) d
					.getButton(Dialog.BUTTON_NEUTRAL);
			negativeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					processReviewEntry();
				}
			});
		}*/
	}

	protected void goToNextScreenWithConfigurationInfo() {

		// progressDialog = ProgressDialog.show(getActivity(), "", "", true);

		mHandler.sendEmptyMessage(REVIEW_SUBMIT_START);

	}

	private Handler mHandler = new Handler() {

		private String reviewanswer_text;

		public void handleMessage(Message msg) {
			final int what = msg.what;
			if (what == REVIEW_SUBMIT_COMPLETE) {
				// progressDialog.dismiss();
				Log.d(LOG_TAG, ":Setting Email Deliverymode:");
				// new AlertDialogManager().showAlertDialog(getActivity(),
				// "Info", "Email Sent Please check you Inbox/Spam", false);
				dismiss();

			} else if (what == REVIEW_SUBMIT_START) {
				reviewanswer_text = etReviewAnswer.getText().toString().trim();

				adaptiveQuestion.addAnswer(reviewanswer_text);
				etReviewAnswer.setText("");
				if (questionCounter < QUESTION_KEYS.length) {
					String question = adaptiveQuestion
							.getNextQuestion(QUESTION_KEYS[questionCounter]);
					// tvReviewCharacterCount.setText("");
					tvReviewQuestion.setText(question);
					// tvReviewCharacterCount.setText("");

				//	minimumCharacterLimit = QUESTION_WEIGHTS[questionCounter];
					// tvReviewCharacterCount.setText("Minimum Character left "+minimumCharacterLimit);
					questionCounter++;
				} else {
					submitReviewDescription();
				}

				Log.d(LOG_TAG, adaptiveQuestion.getReviewOfVisitor());
			}
		}
	};
	private FragmentTextSetView fragmentTextSetView;

	private void processReviewEntry() {

		Log.d(LOG_TAG, "Processing Email Entry");
		mHandler.sendEmptyMessage(REVIEW_SUBMIT_START);
	}

	protected void submitReviewDescription() {

		String reviewDesc = adaptiveQuestion.getReviewOfVisitor();
		Log.d(LOG_TAG, "Review Description" + reviewDesc);
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_DESC(reviewDesc);
		
		fragmentTextSetView.updateTextView();

		mHandler.sendEmptyMessage(REVIEW_SUBMIT_COMPLETE);
	}

}
