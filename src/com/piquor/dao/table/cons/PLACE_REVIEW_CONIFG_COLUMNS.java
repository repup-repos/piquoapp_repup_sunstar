package com.piquor.dao.table.cons;

public interface PLACE_REVIEW_CONIFG_COLUMNS {

	public final String TABLE_NAME_REVIEW_CONFIGURATION = "ReviewConfiguration";
	public final String COLUMN_NAME_REVIEW_ID = "id";
	public final String COLUMN_NAME_REVIEW_PLACE_NAME = "placename";
	public final String COLUMN_NAME_REVIEW_PLACE_TRIPADVISOR_URL = "placetripadvisorurl";
	public final String COLUMN_NAME_REVIEW_PLACE_TYPE = "placetype";
	public final String COLUMN_NAME_REVIEW_PLACE_ZOMATO_URL = "placezomatourl";
	
}	
