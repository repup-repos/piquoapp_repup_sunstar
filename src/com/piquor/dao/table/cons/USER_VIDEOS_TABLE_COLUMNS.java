package com.piquor.dao.table.cons;

public interface USER_VIDEOS_TABLE_COLUMNS {

	public String TABLE_NAME_VIDEO_USER = "video_user";
	public String COLUMN_NAME_VIDEO_USER_ID = "id";
	public String COLUMN_NAME_VIDEO_USER_VIDEOID = "video_id";
	public String COLUMN_NAME_VIDEO_USER_CAPTURE_DATE = "capture_date";
}
