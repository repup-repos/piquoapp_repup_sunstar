package com.piquor.services.deliverymode.execute;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpClientWrapper {

	public static HttpClient wrapClient(HttpClient base)
	{
		try
		{
			
			return new DefaultHttpClient(base.getParams());
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
}
