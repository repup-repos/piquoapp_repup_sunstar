package com.piquor.fragments;

import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.TripAdisorEssentialsDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.R;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class FragmentTripAdvisorRestaurantOptionalRating extends Fragment implements DeliveryModeFlags,FRAGMENT_SCREEN_NAMES,ACTIVITY_FLAGS {
	
	
	private RatingBar ratingAtmosphere;
	private RatingBar ratingValue;
	private RatingBar ratingServices;
	private RatingBar ratingFood;
	private RadioGroup qst1Group;
	private RadioButton radioDoubleRuppe;
	private RadioButton radioSingleRupee;
	private RadioButton radioTripleRupee;
	private RadioButton fourthRupee;
	private CheckBox chkBrunch;
	private CheckBox chkDelivery;
	private CheckBox chkLateNight;
	private CheckBox chkOutdoorScene;
	private CheckBox chkOutdoorSeating;
	private CheckBox chkReservationAccepted;
	private FragementChangeListener fragmentChangeListener;
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	
		fragmentChangeListener = (FragementChangeListener) getActivity();
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)	{
	
		View view = inflator.inflate(R.layout.fragmenttarestaurentoptionalrating, parent,false);
		
		ratingAtmosphere = (RatingBar) view.findViewById(R.id.ratingBarAtmoshphare);
		ratingValue = (RatingBar) view.findViewById(R.id.ratingBarRestaurantValue);
		ratingServices = (RatingBar) view.findViewById(R.id.ratingBarRestaurantServices);
		ratingFood = (RatingBar) view.findViewById(R.id.ratingBarRestaurantFood);
		
		LayerDrawable stars = (LayerDrawable) ratingAtmosphere.getProgressDrawable();
		stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
//        stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);

        LayerDrawable stars1 = (LayerDrawable) ratingValue.getProgressDrawable();
		stars1.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
//        stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
        stars1.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
        
        LayerDrawable stars2 = (LayerDrawable) ratingServices.getProgressDrawable();
        stars2.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
//             stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
        stars2.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
        
        LayerDrawable stars3 = (LayerDrawable) ratingFood.getProgressDrawable();
        stars3.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
//             stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
        stars3.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);

		
		float ratingStep = 1.0f;
		ratingAtmosphere.setStepSize(ratingStep);
		ratingValue.setStepSize(ratingStep);
		ratingServices.setStepSize(ratingStep);
		ratingFood.setStepSize(ratingStep);
		
		ImageButton btnPostToTripAdvisor = (ImageButton) view.findViewById(R.id.fragmentrestaurant_optional_hotelbtnsumbit);
		btnPostToTripAdvisor.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				replaceScreenWithTripAdvisor();
			}
		});
		
		qst1Group = (RadioGroup) view.findViewById(R.id.radioGroupRestaurantCost);
		
		radioDoubleRuppe = (RadioButton) view.findViewById(R.id.rbDoubleRuppe);
		radioSingleRupee = (RadioButton) view.findViewById(R.id.rbSingleRuppe);
		radioTripleRupee = (RadioButton) view.findViewById(R.id.rbTripleRuppe);
		fourthRupee = (RadioButton) view.findViewById(R.id.rbFourthRuppe);
		
		chkBrunch = (CheckBox) view.findViewById(R.id.chkBrunch);
		chkDelivery = (CheckBox) view.findViewById(R.id.chkDelivery);
		chkLateNight = (CheckBox) view.findViewById(R.id.chkLateNight);
		chkOutdoorScene = (CheckBox) view.findViewById(R.id.chkoutdoorscenicview);
		chkOutdoorSeating = (CheckBox) view.findViewById(R.id.chkoutdoorseating);
		chkReservationAccepted = (CheckBox) view.findViewById(R.id.chkreservationaccepted);
		
		return view;
	}

	protected void replaceScreenWithTripAdvisor() {
		
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_ATMOHSPHARE_RATING(ratingAtmosphere.getRating());
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_FOOD_RATING(ratingFood.getRating());
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_SERVICES_RATING(ratingServices.getRating());
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_VALUE_RATING(ratingValue.getRating());
		int id = qst1Group.getCheckedRadioButtonId();
		if(qst1Group.getCheckedRadioButtonId()==-1)
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE("0");
		}else
		{
			if(id == radioDoubleRuppe.getId())
			{
				TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE("10");
			}else if(id == radioSingleRupee.getId())
			{
				TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE("1");
			}else if(id == radioTripleRupee.getId())
			{
				TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE("100");
			}else if(id == fourthRupee.getId())
			{
				TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_COST_ESTIMATE("1000");
			}
		}
		if(chkBrunch.isChecked())
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH("true");
		}else
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_BRUNCH("false");
		}
		if(chkDelivery.isChecked())
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY("true");
		}else
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_DELIVERY("false");
		}
		if(chkLateNight.isChecked())
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE("true");
		}else
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE("false");
		}
		if(chkOutdoorScene.isChecked())
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE("true");
		}else
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SCENE("false");
		}
		if(chkOutdoorSeating.isChecked())
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING("true");
		}else
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_OUTDOOR_SEATING("false");
		}
		if(chkReservationAccepted.isChecked())
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED("true");
		}else
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURANT_HAVE_RESERVATION_ACCEPTED("false");
		}
		fragmentChangeListener.navigateToReviewPublishScreen();
	
	}
}
