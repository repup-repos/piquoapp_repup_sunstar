package com.piquor.services.helper;

import java.io.File;

import android.util.Log;

import com.piquor.fileUtils.ImageFileUtility;

public class ServicesHelper {
	
	private static String LOG_TAG = "ServicesHelper";
	public ServicesHelper()
	{
		
	}
	
	public static File getFilePath(String picId)
	{
		File fileDirectory = ImageFileUtility.getAppDirectory();
		String directory = fileDirectory.getAbsolutePath().toString().trim();
		Log.d(LOG_TAG, "Directory Path:"+directory);
		String filePath = directory+"/"+picId+".jpeg";
		
		Log.d(LOG_TAG, "Final Constructed File Path::: even i got printed here ::   "+filePath);
		
       /* final File[] files = fileDirectory.listFiles();
        Log.d(LOG_TAG,"Total Number Of Files:"+files.length);
        Log.d(LOG_TAG, "Print me too");
        for (File file : files ) {
            if (file.canRead() && (file.listFiles().length > 0) ) {  // it is a real directory (not a USB drive)...
                Log.d(LOG_TAG, "External Storage: " + file.getAbsolutePath());
            }
            Log.d(LOG_TAG, "External Storage: " + file.getAbsolutePath());
        }*/
		File file = new File(filePath);
		if(file.exists())
		{
			Log.d(LOG_TAG, "File Exsists:");
		}else
		{
			Log.d(LOG_TAG, "File Not Exsists");
		}
		return file;
		
	}
}

