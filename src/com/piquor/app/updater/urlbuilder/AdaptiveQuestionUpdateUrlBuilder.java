package com.piquor.app.updater.urlbuilder;

import android.util.Log;

import com.piquor.dto.CampaignInfoDTO;

public class AdaptiveQuestionUpdateUrlBuilder extends AppUpdateUrlBuilderUtility implements UrlPathCons {

	private String LOG_TAG = "AdaptiveQuestionUrlBuilder";
	
	public AdaptiveQuestionUpdateUrlBuilder(CampaignInfoDTO campaignDTO)
	{
		super(campaignDTO);
		
	}
	
	
	@Override
	public boolean executeQueryString() {
		
		String query = URL_FETCH_ADAPTIVE_QUESTIONS+campaignInfoDTO.getCampaignId();
		logInfo("Adaptive Question Fetch Query: " + query);
		
		return executeQuery(query);
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
	
	
	
}
