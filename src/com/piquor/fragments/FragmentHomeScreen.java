package com.piquor.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.piquor.constants.ACTIVITY_FLAGS;

/*
 * FragmentHomeScreen : Fragment class for fragmentHomeScreen
 * necessary interface to implement (FRAGMENT_CONSTANTS)  
 */

public class FragmentHomeScreen extends Fragment implements ACTIVITY_FLAGS {
	
	private String LOG_TAG = "FragmentHomeScreen";
	//Array to hold button text for menu buttons
	private ArrayList<String> txtBtnHomeMenu;
	private final String VIDEO_BUTTON_TEXT = "Video";
	private final String PHOTO_BUTTON_TEXT = "Photo";
	private final String REVIEW_BUTTON_TEXT = "Review";
	private LinearLayout llMenuHolder;
	
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)
	{
		View view = inflator.inflate(com.piquor.piquortabapp.R.layout.fragmenthomescreen, parent,false);
		
		/*ImageView imgScreenSaver = (ImageView) view.findViewById(R.id.imgHomeScreenSaver);
		imgScreenSaver.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				navigateToCaptureFragment();
			}
		});*/

		
		return view;
	}
	
	public void navigateToCaptureFragment()
	{
	/*	Log.d(LOG_TAG, "Navigating To Capture Fragment");
		FragementChangeListener fragmentListener = (FragementChangeListener)getActivity();
		fragmentListener.replaceFragment(CREATE_USER_DTO, FRAGMENT_HOME_SCREEN, FRAGMENT_SCREEN_SAVER);
*/	}
	//// ADD buttons to Leaner layout after initializing ArrayList to hold button text
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}

	
	
}
