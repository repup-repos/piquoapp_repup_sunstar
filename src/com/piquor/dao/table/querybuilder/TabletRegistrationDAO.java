package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.TABLET_REGISTRATION_COLUMNS;
import com.piquor.dto.TabletAppRegistrationDTO;

public class TabletRegistrationDAO  implements TABLET_REGISTRATION_COLUMNS{

	public static String getTabletRegistrationDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_REGISTRATION;
		return query;
	}
	
	public static String getCreateTabletRegistrationQuery()
	{
		String CREATE_TABLE_APP_STATS = "CREATE TABLE ["+TABLE_NAME_REGISTRATION+"] "
				+ "("
				+ "["+COLUMN_NAME_DEVICE_ID+"] varchar(200)  NOT NULL PRIMARY KEY,"
				+ "["+COLUMN_NAME_BOARD+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_BRAND_NAME+"] varchar(150) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MANUFETURER+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_MODAL+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_PRODUCTNAME+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_SERIAL_NO+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_IS_CAMPAIGN_ASSIGNED+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_DEFAULT_CAMAPGIN_ID+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_CAMPAIGNiD+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_VERSION+"] varchar(100) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADED+"] varchar(10) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"] varchar(10) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_DISPLAY+"] varchar(200) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_TABLE_APP_STATS;
		
	}
	
	public static String getInsertRegistrationQuery(TabletAppRegistrationDTO registrationDTO)
	{
		String INSERT_QUERY_TRIPADVISOR = "insert into "
				+TABLE_NAME_REGISTRATION+""
				+ " ( "+COLUMN_NAME_DEVICE_ID
				+","+COLUMN_NAME_BOARD
				+","+COLUMN_NAME_BRAND_NAME
				+","+COLUMN_NAME_DEFAULT_CAMAPGIN_ID
				+","+COLUMN_NAME_DISPLAY
				+","+COLUMN_NAME_MANUFETURER
				+","+COLUMN_NAME_MODAL
				+","+COLUMN_NAME_PRODUCTNAME
				+","+COLUMN_NAME_SERIAL_NO
				+","+COLUMN_NAME_IS_CAMPAIGN_ASSIGNED
				+","+ COLUMN_NAME_CAMPAIGNiD
				+","+ COLUMN_NAME_REGISTRATION_ENTRY_UPLOADED
				+","+ COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS
				+","+ COLUMN_NAME_VERSION
				+ ") "
				+ "values ('"+registrationDTO.getDevice_id()+
				"','"+registrationDTO.getDevice_board()
				+"','"+registrationDTO.getDevice_brand_name()
				+"','"+registrationDTO.getDefault_campaign()
				+"','"+registrationDTO.getDevice_display()
				+"','"+registrationDTO.getDevice_manufeturer()
				+"','"+registrationDTO.getDevice_modal()
				+"','"+registrationDTO.getDevice_productname()
				+"','"+registrationDTO.getDevice_serialno()
				+"','"+registrationDTO.getIs_campaign_asigned()
				+"','"+registrationDTO.getCampaignid()
				+"','"+registrationDTO.getRegistration_entry_uploaded()	
				+"','"+registrationDTO.getRegistration_entry_upload_status()
				+"','"+registrationDTO.getDevice_version()+"'"	
				+ ")";
		
		return INSERT_QUERY_TRIPADVISOR;
	}
	
	
	public static String getUpdateEmailErrorQuery(String deviceId)
	{
		String query = "update "+TABLE_NAME_REGISTRATION+" set "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"='"+2+"' where "+COLUMN_NAME_DEVICE_ID+"='"+deviceId+"'";
		
		return query;
	}
	
	public static String getUpadateRegistrationSuccessEntryQuery(String deviceId)
	{
		
		String query = "update "+TABLE_NAME_REGISTRATION+" set "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"='"+0+"', "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADED+"='1' where "+COLUMN_NAME_DEVICE_ID+"='"+deviceId+"'";
		return query;
	}
	
	public static String getDbCountForRemainingMailSentQuery(String deviceId)
	{
		String query = "SELECT count(*) FROM "+TABLE_NAME_REGISTRATION+" WHERE "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADED+"=0  and "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"='0'";
		return query;
	}
	
	public static String getDbCountForUrlEntryQuery()
	{
		String dbquery = "SELECT count(*) FROM "+TABLE_NAME_REGISTRATION+" WHERE "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"='2'";
		
		return dbquery;
	}
	
	
	public static String getDbCountForHavingEntryQuery()
	{
		String dbquery = "SELECT count(*) FROM "+TABLE_NAME_REGISTRATION+" WHERE 1";
		
		return dbquery;
	}

	public static String getUnsendedRegistrationMailListQuery(String flag) {
		
		
		String queryPrefix="select *  FROM "+TABLE_NAME_REGISTRATION+" WHERE ";
		
		String queryCondition="";
		
		if(flag.equals("mailremaintosent"))
		{
			queryCondition=COLUMN_NAME_REGISTRATION_ENTRY_UPLOADED+"='0'  and "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"='0'"; //Mails Remaining to Sent
		}
		else if(flag.equals("emailerror"))
		{
			queryCondition= COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"='2'"; //EmailError
		}
		else if(flag.equals("remainingEntries"))
		{
			queryCondition=COLUMN_NAME_REGISTRATION_ENTRY_UPLOADED+"='0'  and "+COLUMN_NAME_REGISTRATION_ENTRY_UPLOADEDSTATUS+"='0'";// Remaining Entries
		}
		String Query=queryPrefix+queryCondition;
		
		return Query;
		
	}

	public static String getSelectQueryForDeviceId() {
		
		String dbquery = "SELECT "+COLUMN_NAME_DEVICE_ID+" FROM "+TABLE_NAME_REGISTRATION+" WHERE 1";
		
		return dbquery;
	}
	

	
	
}
