package com.piquor.dto;

public class ReviewPageTempValuesDTO {
	
	
	private String placeDetail;
	private String placeGeo;
	private String placeReviewName;
	private String placeReviewLocation;
	private String placeerrorCheck;
	private String placeFollowUp;
	private String placeLsoId; 
	private String placeAltsessid;
	private String placePval;
	private String placePC;
	private String placeToken;
	
	public ReviewPageTempValuesDTO()
	{
		placeAltsessid = "";
		placeDetail = "";
		placeReviewName = "";
		placeReviewLocation = "";
		placeerrorCheck = "";
		placeFollowUp = "";
		placeLsoId = "";
		placeAltsessid = "";
		placePval = "";
		placePC = "";
		placeToken = "";
		placeGeo = "";
	}
	
	public String getPlaceDetail() {
		return placeDetail;
	}
	public void setPlaceDetail(String placeDetail) {
		this.placeDetail = placeDetail;
	}
	public String getPlaceGeo() {
		return placeGeo;
	}
	public void setPlaceGeo(String placeGeo) {
		this.placeGeo = placeGeo;
	}
	public String getPlaceReviewName() {
		return placeReviewName;
	}
	public void setPlaceReviewName(String placeReviewName) {
		this.placeReviewName = placeReviewName;
	}
	public String getPlaceReviewLocation() {
		return placeReviewLocation;
	}
	public void setPlaceReviewLocation(String placeReviewLocation) {
		this.placeReviewLocation = placeReviewLocation;
	}
	public String getPlaceerrorCheck() {
		return placeerrorCheck;
	}
	public void setPlaceerrorCheck(String placeerrorCheck) {
		this.placeerrorCheck = placeerrorCheck;
	}
	public String getPlaceFollowUp() {
		return placeFollowUp;
	}
	public void setPlaceFollowUp(String placeFollowUp) {
		this.placeFollowUp = placeFollowUp;
	}
	public String getPlaceLsoId() {
		return placeLsoId;
	}
	public void setPlaceLsoId(String placeLsoId) {
		this.placeLsoId = placeLsoId;
	}
	public String getPlaceAltsessid() {
		return placeAltsessid;
	}
	public void setPlaceAltsessid(String placeAltsessid) {
		this.placeAltsessid = placeAltsessid;
	}
	public String getPlacePval() {
		return placePval;
	}
	public void setPlacePval(String placePval) {
		this.placePval = placePval;
	}
	public String getPlacePC() {
		return placePC;
	}
	public void setPlacePC(String placePC) {
		this.placePC = placePC;
	}
	public String getPlaceToken() {
		return placeToken;
	}
	public void setPlaceToken(String placeToken) {
		this.placeToken = placeToken;
	}
	@Override
	public String toString() {
		return "ReviewPageTempValuesDTO [placeDetail=" + placeDetail
				+ ", placeGeo=" + placeGeo + ", placeReviewName="
				+ placeReviewName + ", placeReviewLocation="
				+ placeReviewLocation + ", placeerrorCheck=" + placeerrorCheck
				+ ", placeFollowUp=" + placeFollowUp + ", placeLsoId="
				+ placeLsoId + ", placeAltsessid=" + placeAltsessid
				+ ", placePval=" + placePval + ", placePC=" + placePC
				+ ", placeToken=" + placeToken + "]";
	}
	
	
	
	
}
