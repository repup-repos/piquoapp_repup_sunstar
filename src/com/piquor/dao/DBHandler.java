package com.piquor.dao;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.piquor.dao.table.querybuilder.CampaignAdaptiveQuestionDAO;
import com.piquor.dao.table.querybuilder.CampaignInfoDAO;
import com.piquor.dao.table.querybuilder.CampaignMachineMappingDAO;
import com.piquor.dao.table.querybuilder.FrameConfigDAO;
import com.piquor.dao.table.querybuilder.MachineConfigDAO;
import com.piquor.dao.table.querybuilder.MachineCoreDAO;
import com.piquor.dao.table.querybuilder.MailerDAO;
import com.piquor.dao.table.querybuilder.ReviewConfigDAO;
import com.piquor.dao.table.querybuilder.ReviewEntryDAO;
import com.piquor.dao.table.querybuilder.TabletAppStatsDAO;
import com.piquor.dao.table.querybuilder.TabletRegistrationDAO;
import com.piquor.dao.table.querybuilder.UserTableDAO;
import com.piquor.dao.table.querybuilder.VideoUserDAO;
import com.piquor.dto.AdaptiveQuestion;
import com.piquor.dto.CampaignInfoDTO;
import com.piquor.dto.DataInfoDTO;
import com.piquor.dto.MachineCoreDTO;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.dto.ReviewsConfigDTO;
import com.piquor.dto.TabletAppRegistrationDTO;
import com.piquor.dto.TabletAppStatusDTO;
import com.piquor.dto.UserTableDTO;
import com.piquor.dto.VideoUserDTO;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class DBHandler extends SQLiteOpenHelper {

	private  final String LOG_TAG = "DBHandler";
	private  AtomicInteger mOpenCounter = new AtomicInteger();
	private  SQLiteDatabase mDatabase;
		public DBHandler(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		CampaignInfoDTO campaignInfoDTO = new CampaignInfoDTO();
		campaignInfoDTO.setCampaignId("8a6182c84410594c014410620edfff003");
		campaignInfoDTO.setMachineId("INDL0034");
		campaignInfoDTO.setMailerResponseTime("5");
		
		
		/*ReviewsConfigDTO configDTO = new ReviewsConfigDTO();
		configDTO.setTripAdvisorRestaurantType("restaurant");
		configDTO.setTripAdvisorRestaurentName("Dakshin");
		configDTO.setTripAdvisorRestaurantZomatoUrl("https://www.zomato.com/ncr/dakshin-sheraton-saket-new-delhi#quick-review-form");
		configDTO.setTripAdvisorRestaurantURL("https://www.tripadvisor.in/UserReviewEdit-g304551-d787357-a_placetype.10022-a_referredFromLocationSearch.true-a_ReviewName.-e-wpage1-Dakshin-New_Delhi_National_Capital_Territory_of_Delhi.html");
	*/	

		ReviewsConfigDTO configDTO1 = new ReviewsConfigDTO();
		configDTO1.setTripAdvisorRestaurantType("hotel");
		configDTO1.setTripAdvisorRestaurentName("Hotel Sunstar Heights");
		configDTO1.setTripAdvisorRestaurantZomatoUrl("NA");
		//configDTO1.setTripAdvisorRestaurantURL("https://www.tripadvisor.in/UserReviewEdit-g304551-d583011-ehttp%3A__2F____2F__www__2E__tripadvisor__2E__in__2F__Hotel__5F__Review__2D__g304551__2D__d583011__2D__Reviews__2D__Radisson__5F__Blu__5F__Marina__5F__Connaught__5F__Place__2D__New__5F__Delhi__5F__National__5F__Capital__5F__Territory__5F__of__5F__Delhi__2E__html-Radisson_Blu_Marina_Connaught_Place-New_Delhi_National_Capital_Territory_of_Delhi.html");
		configDTO1.setTripAdvisorRestaurantURL("https://www.tripadvisor.in/UserReviewEdit-g304551-d1960259-ehttp%3A__2F____2F__www__2E__tripadvisor__2E__in__2F__Hotel__5F__Review__2D__g304551__2D__d1960259__2D__Reviews__2D__Hotel__5F__Sunstar__5F__Heights__2D__New__5F__Delhi__5F__National__5F__Capital__5F__Territory__5F__of__5F__Delhi__2E__html-Hotel_Sunstar_Heights-New_Delhi_National_Capital_Territory_of_Delhi.html");
		/*ReviewsConfigDTO configDTO2 = new ReviewsConfigDTO();
		configDTO2.setTripAdvisorRestaurantType("restaurant");
		configDTO2.setTripAdvisorRestaurentName("Baywatch");
		configDTO2.setTripAdvisorRestaurantZomatoUrl("https://www.zomato.com/ncr/baywatch-sheraton-saket-delhi#quick-review-form");
		configDTO2.setTripAdvisorRestaurantURL("https://www.tripadvisor.in/UserReviewEdit-g304551-d787357-a_placetype.10022-a_referredFromLocationSearch.true-a_ReviewName.-e-wpage1-Dakshin-New_Delhi_National_Capital_Territory_of_Delhi.html");
		
		ReviewsConfigDTO configDTO3 = new ReviewsConfigDTO();
		configDTO3.setTripAdvisorRestaurantType("restaurant");
		configDTO3.setTripAdvisorRestaurentName("Panasian");
		configDTO3.setTripAdvisorRestaurantZomatoUrl("https://www.zomato.com/ncr/pan-asian-sheraton-saket-new-delhi#quick-review-form");
		configDTO3.setTripAdvisorRestaurantURL("https://www.tripadvisor.in/UserReviewEdit-g304551-d787357-a_placetype.10022-a_referredFromLocationSearch.true-a_ReviewName.-e-wpage1-Dakshin-New_Delhi_National_Capital_Territory_of_Delhi.html");
	*/	
	    db.execSQL(UserTableDAO.getCreateUserTableQuery());
	    db.execSQL(CampaignInfoDAO.getCampaignTableCreateQuery());    
	    db.execSQL(ReviewConfigDAO.getCreateReviewConfigTable());
	    db.execSQL(ReviewEntryDAO.getReviewEntryCreateTableQuery());
	    db.execSQL(VideoUserDAO.getVideoUserCreateTable());
	    db.execSQL(MachineCoreDAO.getCreateMachineCoreTableQuery());
	    db.execSQL(FrameConfigDAO.getFrameConfigTableCreateQuery());
		db.execSQL(CampaignAdaptiveQuestionDAO.getCampaignAdaptiveQuestionsTableCreateQuery());;
	    db.execSQL(MachineConfigDAO.getMachineConfTableQuery());
		//db.execSQL(ReviewConfigDAO.getInsertReviewConfigurationQuery1(configDTO));
		db.execSQL(ReviewConfigDAO.getInsertReviewConfigurationQuery1(configDTO1));
		//db.execSQL(ReviewConfigDAO.getInsertReviewConfigurationQuery1(configDTO2));
		//db.execSQL(ReviewConfigDAO.getInsertReviewConfigurationQuery1(configDTO3));
		db.execSQL(CampaignInfoDAO.getInsertCampaignInformationQuery(campaignInfoDTO));
		db.execSQL(TabletRegistrationDAO.getCreateTabletRegistrationQuery());
		db.execSQL(TabletAppStatsDAO.getCreateTabletAppStats());
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	
		db.execSQL(UserTableDAO.getUserTableDropQuery());
		db.execSQL(CampaignInfoDAO.getCampaignTableDropQuery());
		db.execSQL(MachineConfigDAO.getMachineCoreTableDropQuery());
		db.execSQL(CampaignMachineMappingDAO.getMachineMappingTableDropQuery());
		db.execSQL(FrameConfigDAO.getFrameConfigTableDropQuery());
		db.execSQL(MachineConfigDAO.getMachineCoreTableDropQuery());
		db.execSQL(ReviewConfigDAO.getReviewConfigTableDropQuery());
		db.execSQL(VideoUserDAO.getVideoUserTableDropQuery());
		db.execSQL(CampaignAdaptiveQuestionDAO.getCampaignAdaptiveQuestionsTableDropQuery());
		db.execSQL(TabletAppStatsDAO.getTableAppStatsDropQuery());
		db.execSQL(TabletRegistrationDAO.getTabletRegistrationDropQuery());
		onCreate(db);
	}
	
	public  synchronized SQLiteDatabase openDatabase() {
        if(mOpenCounter.incrementAndGet() == 1) {
            // Opening new database
        	Log.d(LOG_TAG, "Returing new Database");
            mDatabase = this.getWritableDatabase();
        }else
        {
        	Log.d(LOG_TAG, "Returning already exsisted database");
        }
        
        return mDatabase;
    }

    public synchronized void closeDatabase() {
        if(mOpenCounter.decrementAndGet() == 0) {
            // Closing database
        	Log.d(LOG_TAG, "Closing current database");
            mDatabase.close();

        }else
        {
        	Log.d(LOG_TAG,"Current Database Is Used:");
        }
    }
	
    public void addUser(UserTableDTO userDTO) {

		SQLiteDatabase db = openDatabase();
		try {

			String query = UserTableDAO.getUserEntryQuery(userDTO);
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
	}
	
	public UserTableDTO findProduct(int id) {
		
		String query = UserTableDAO.findUser(id);
		
		SQLiteDatabase db = openDatabase();

		Cursor cursor = db.rawQuery(query, null);

		UserTableDTO userDTO = new UserTableDTO();

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			userDTO.setPhotoPermissionSocialMedia(cursor.getString(0));
			userDTO.setDateofbirth(cursor.getString(1));
			userDTO.setCampaignId(cursor.getString(2));
			userDTO.setNOOfPrints(cursor.getString(3));
			userDTO.setStatus(cursor.getString(4));
			userDTO.setFeedback(cursor.getString(5));
			userDTO.setFrameSelected(cursor.getString(6));
			userDTO.setDate(cursor.getString(7));
			userDTO.setGender(cursor.getString(8));
			userDTO.setId(cursor.getInt(9));
			userDTO.setPicId(cursor.getString(10));
			userDTO.setName(cursor.getString(11));
			userDTO.setEmail(cursor.getString(12));
			userDTO.setMailSent(cursor.getString(13));
			userDTO.setFileUpload(cursor.getString(14));
			userDTO.setPhone(cursor.getString(15));
			userDTO.setDelieveryMode(cursor.getString(16));
			userDTO.setFbUserId(cursor.getString(17));
			userDTO.setFbUserName(cursor.getString(18));
			userDTO.setFbName(cursor.getString(19));
			userDTO.setTwitterId(cursor.getString(20));
		    userDTO.setTwitterName(cursor.getString(21));
		    userDTO.setTwitterFollowers(cursor.getString(22));
		    userDTO.setTwitterFriendsCount(cursor.getString(23));
			cursor.close();
		} else {
			userDTO = null;
		}
		closeDatabase();
		return userDTO;
	}
	
	public UserTableDTO getLastInsertedUser()
	{
		long totalRowCount = getTotalRowCounts("TOTAL");
		Log.d(LOG_TAG,"Total Row Count is:"+totalRowCount);
		if(totalRowCount > 0)
		{
			return findProduct((int)totalRowCount);
		}
		return null;
	}

	private long getTotalRowCounts(String flag) {
		
		SQLiteDatabase db = openDatabase();
		String query = UserTableDAO.getTotalRowCountQuery(flag);
		Cursor cursor = db.rawQuery(query, null);
		cursor.moveToFirst();
		int value = 0;
		if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
			value = cursor.getInt(0);
		} else {
			cursor.close();
			closeDatabase();
		    return 0;
		}
		
		cursor.close();
		closeDatabase();
		
		
		return value;
	}
	
	
	
	public DataInfoDTO getDataEntryInfo()
	{
		Log.d(LOG_TAG, "Gathering total Data");
		DataInfoDTO dataDTO = new DataInfoDTO();
		dataDTO.setTotalEntries(getTotalRowCounts("TOTAL"));
		dataDTO.setTotalEmailEntries(getTotalRowCounts(DeliveryModeFlags.EMAIL_DELIVERYMODE_FLAG));
		dataDTO.setTotalTwitterEntries(getTotalRowCounts(DeliveryModeFlags.TWITTER_DELIVERYMODE_FLAG));
		dataDTO.setTotalFacebookEntries(getTotalRowCounts(DeliveryModeFlags.FACEBOOK_DELIVERYMODE_FLAG));
		dataDTO.setTotalUploadedEntries(getTotalRowCounts("UPLOADED"));
		dataDTO.setTotalPrintEntires(getTotalRowCounts("PRINT"));
		Log.d(LOG_TAG,"Total Data Info Gathered Values:"+dataDTO.toString());
		
		return dataDTO;
	}
	
	//Insert Campaign Query Information
	public void updateCampaignInformation(CampaignInfoDTO campaignDTO)
	{
		
		SQLiteDatabase db = openDatabase();
		try {

			String CAMPAIGN_UPDATE_QUERY = CampaignInfoDAO.getUpdateCampaignInformationQuery(campaignDTO);
			db.execSQL(CAMPAIGN_UPDATE_QUERY);
			Log.d(LOG_TAG, "Executed Query:" + CAMPAIGN_UPDATE_QUERY);
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
	}

	//METHOD TO GET CAMPAIGN INFORMATION
	public CampaignInfoDTO getCampaignInformation()
	{
		String selectQuery = CampaignInfoDAO.getCampaignInfomrationQuery();
		
		CampaignInfoDTO campaignDTO = null;
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			 db = openDatabase();
			 cursor = db.rawQuery(selectQuery, null);
			campaignDTO = new CampaignInfoDTO();
			 Log.d(LOG_TAG, "Setting All Data To Campaign Table");
		        if (cursor.moveToFirst()) {
		            do {
		            	campaignDTO.setCampaignId(cursor.getString(0));
		            	campaignDTO.setMachineId(cursor.getString(1));
		            	campaignDTO.setMailerResponseTime(cursor.getString(2));
		    		    Log.d(LOG_TAG, "User DTOS"+campaignDTO.toString());
		    			cursor.close();
		            } while (cursor.moveToNext());
		        }
		} catch (Exception e) {
				
			Log.d(LOG_TAG,"Exception message:"+e.getMessage());
		}finally
		{
			
			closeDatabase();
		}
		return campaignDTO;
	}
	
	public void deleteAllUserEntries() {
		
		
		SQLiteDatabase db = openDatabase();
		try
		{
			String deleteQuery = UserTableDAO.getDeleteUserTableEntriesQuery();
			Log.d(LOG_TAG,"Queries executed are:"+deleteQuery);
			db.execSQL(deleteQuery);
		}catch(Exception e)
		{
			Log.d(LOG_TAG,"Exception Occurred:"+e.getMessage());
		}finally
		{
			closeDatabase();
		}
	}

	
	//MAILER FUNCTIONS
	public boolean executeUpdateQuery(String query) {
		SQLiteDatabase db = openDatabase();
		try {
			
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
			return true;
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
			return false;
		} finally {
			closeDatabase();
		}
		
	}
	
	public int getDBEntryCount(String query)
	{
		SQLiteDatabase db = openDatabase();
		
		Cursor cursor = db.rawQuery(query, null);

		// ensure there is at least one row and one column
		cursor.moveToFirst();
		int value = 0;
		if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
			value = cursor.getInt(0);
		
			
		} else {
			cursor.close();
			closeDatabase();
		    return 0;
		}
		
		cursor.close();
		closeDatabase();
		
		return value;
		
		
	}

	public ArrayList<UserTableDTO> getUnsendedMailList(String flag) {
		
		String Query=MailerDAO.getUnsendedMailListQuery(flag);
		
		ArrayList<UserTableDTO> list = new ArrayList<UserTableDTO>();
		
		SQLiteDatabase db = null;
		Cursor cursor = null;
		
		try {
			db = openDatabase();
			cursor = db.rawQuery(Query, null);
			UserTableDTO userDTO = new UserTableDTO();
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					userDTO = new UserTableDTO();
					userDTO.setPhotoPermissionSocialMedia(cursor.getString(0));
					userDTO.setDateofbirth(cursor.getString(1));
					userDTO.setCampaignId(cursor.getString(2));
					userDTO.setNOOfPrints(cursor.getString(3));
					userDTO.setStatus(cursor.getString(4));
					userDTO.setFeedback(cursor.getString(5));
					userDTO.setFrameSelected(cursor.getString(6));
					userDTO.setDate(cursor.getString(7));
					userDTO.setGender(cursor.getString(8));
					userDTO.setId(cursor.getInt(9));
					userDTO.setPicId(cursor.getString(10));
					userDTO.setName(cursor.getString(11));
					userDTO.setEmail(cursor.getString(12));
					userDTO.setMailSent(cursor.getString(13));
					userDTO.setFileUpload(cursor.getString(14));
					userDTO.setPhone(cursor.getString(15));
					userDTO.setDelieveryMode(cursor.getString(16));
					userDTO.setFbUserId(cursor.getString(17));
					userDTO.setFbUserName(cursor.getString(18));
					userDTO.setFbName(cursor.getString(19));
					userDTO.setTwitterId(cursor.getString(20));
					userDTO.setTwitterName(cursor.getString(21));
					userDTO.setTwitterFollowers(cursor.getString(22));
					userDTO.setTwitterFriendsCount(cursor.getString(23));

					Log.d(LOG_TAG, "User DTOS" + userDTO.toString());
					
					list.add(userDTO);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			
			Log.d(LOG_TAG, "Exception message:" + e.getMessage());
		}finally{
			cursor.close();
			closeDatabase();
		}
		
		return list;
	}
	
	public ArrayList<ReviewEntryDTO> getUnsendedReviewMailList(String flag) {
		
		String Query=ReviewEntryDAO.getUnsendedReviewMailListQuery(flag);
		
		ArrayList<ReviewEntryDTO> list = new ArrayList<ReviewEntryDTO>();
		
		SQLiteDatabase db = null;
		Cursor cursor = null;
		
		try {
			db = openDatabase();
			cursor = db.rawQuery(Query, null);
			ReviewEntryDTO userDTO = new ReviewEntryDTO();
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					userDTO = new ReviewEntryDTO();
					userDTO.setReviewentry_id(String.valueOf(cursor.getInt(0)));
					userDTO.setReviewentry_campaignId(cursor.getString(1));
					userDTO.setReviewentry_date(cursor.getString(2));
					userDTO.setReviewentry_overallrating(cursor.getString(3));
					userDTO.setReviewentry_sortofvisit(cursor.getString(4));
					userDTO.setReviewentry_wereyouherefor(cursor.getString(5));
					userDTO.setReviewentry_title(cursor.getString(6));
					userDTO.setReviewentry_desc(cursor.getString(7));
					userDTO.setReviewentry_type(cursor.getString(8));
					userDTO.setReviewentry_optionaldesc(cursor.getString(9));
					userDTO.setReviewentry_status(cursor.getString(10));
					userDTO.setReviewentry_emailid(cursor.getString(11));
					userDTO.setReviewentry_timestamp(cursor.getString(12));
					userDTO.setReviewentry_reviewUploaded(cursor.getString(13));
					userDTO.setReviewentry_reviewuploadStatus(cursor.getString(14));
					userDTO.setReviewentry_platformid(cursor.getString(15));
				

					Log.d(LOG_TAG, "User DTOS" + userDTO.toString());
					
					list.add(userDTO);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			
			Log.d(LOG_TAG, "Exception message:" + e.getMessage());
		}finally{
			cursor.close();
			closeDatabase();
		}
		
		return list;
	}
	
	
	
	public ArrayList<ReviewsConfigDTO> getTripAdvisorInfo()
	{
		
		String query = ReviewConfigDAO.getTripAdvisorInfoQuery();	
		Cursor cursor = null;
		ArrayList<ReviewsConfigDTO> list = new ArrayList<ReviewsConfigDTO>();

		try {
			
			SQLiteDatabase db = openDatabase();

			cursor = db.rawQuery(query, null);

			ReviewsConfigDTO tripAdvisorDTO = new ReviewsConfigDTO();

			if (cursor.moveToFirst()) {
				do {
					tripAdvisorDTO = new ReviewsConfigDTO();
					tripAdvisorDTO.setId(cursor.getString(0));
					tripAdvisorDTO.setTripAdvisorRestaurentName(cursor
							.getString(1));
					tripAdvisorDTO.setTripAdvisorRestaurantURL(cursor
							.getString(2));
					tripAdvisorDTO.setTripAdvisorRestaurantType(cursor
							.getString(3));
					Log.d(LOG_TAG, "DTO Values:" + tripAdvisorDTO.toString());
					list.add(tripAdvisorDTO);
				} while (cursor.moveToNext());
			} else {
				list = null;
			}
		} catch (Exception ex) {
			list = null;
		} finally {
			cursor.close();
			closeDatabase();
		}
		
		return list;
	}
	
public void updateTripAdvisorDetials(ArrayList<ReviewsConfigDTO> list) {
		
		SQLiteDatabase db = openDatabase();
		try {
			
			 for(ReviewsConfigDTO dto:list)
			 {
				String CAMPAIGN_UPDATE_QUERY = ReviewConfigDAO.getUpdateTripAdvisorDetialQuery(dto);
				db.execSQL(CAMPAIGN_UPDATE_QUERY);
				Log.d(LOG_TAG, "Executed Query:" + CAMPAIGN_UPDATE_QUERY);
			 }
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
			
		} finally {
			closeDatabase();
		}
		
		
	}

	public void addPlaceInfo(ReviewsConfigDTO configDTO) {
		
		SQLiteDatabase db = openDatabase();
		try {

			String query = ReviewConfigDAO.getInsertReviewConfigurationQuery(configDTO);
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
	}

	public void deletePlace(String selectedPlaceName) {
		
		SQLiteDatabase db = openDatabase();
		try {

			String query =  ReviewConfigDAO.getDeletePlaceQuery(selectedPlaceName);
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
	}
	
	public void addRestaurantReviewEntries(ReviewEntryDTO reviewEntryDTO) {

		if (!checkForDuplicationEntry(reviewEntryDTO)) {
			SQLiteDatabase db = openDatabase();
			try {

				String query = ReviewEntryDAO
						.getAddReviewEntryDAOQuery(reviewEntryDTO);
				SQLiteStatement statement = db.compileStatement(query);
				statement.bindString(1,
						reviewEntryDTO.getReviewentry_campaignId());
				statement.bindString(2, reviewEntryDTO.getReviewentry_date());
				statement.bindString(3,
						reviewEntryDTO.getReviewentry_wereyouherefor());
				statement.bindString(4,
						reviewEntryDTO.getReviewentry_sortofvisit());
				statement.bindString(5,
						reviewEntryDTO.getReviewentry_overallrating());
				statement.bindString(6, reviewEntryDTO.getReviewentry_title());
				statement.bindString(7, reviewEntryDTO.getReviewentry_desc());
				statement.bindString(8,
						reviewEntryDTO.getReviewentry_optionaldesc());
				statement.bindString(9, reviewEntryDTO.getReviewentry_status());
				statement.bindString(10,
						reviewEntryDTO.getReviewentry_emailid());
				statement.bindString(11,
						reviewEntryDTO.getReviewentry_timestamp());
				statement.bindString(12,
						reviewEntryDTO.getReviewentry_platformid());
				statement.bindString(13,
						reviewEntryDTO.getReviewentry_reviewUploaded());
				statement.bindString(14,
						reviewEntryDTO.getReviewentry_reviewuploadStatus());
				statement.bindString(15, reviewEntryDTO.getReviewentry_type());
				statement.executeInsert();

				Log.d(LOG_TAG, "Executed Query:" + query);
			} catch (Exception e) {
				Log.d(LOG_TAG, "Exception:" + e.getMessage());
			} finally {
				closeDatabase();
			}
		} else {
			Log.d(LOG_TAG, " Value already entered :: ");
		}
	}
	
	private boolean checkForDuplicationEntry(ReviewEntryDTO reviewEntryDTO) {

		SQLiteDatabase db = openDatabase();
		Log.d(LOG_TAG,
				"Executing query: "
						+ ReviewEntryDAO
								.getDublicateEntryCheckQuery(reviewEntryDTO));
		Cursor cursor = db.rawQuery(
				ReviewEntryDAO.getDublicateEntryCheckQuery(reviewEntryDTO),
				null);
		// ensure there is at least one row and one column
		cursor.moveToFirst();
		int value = 0;
		if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
			Log.d(LOG_TAG, "Value obtained from database:  "+value);
			value = cursor.getInt(0);
		} else {
			cursor.close();
			closeDatabase();
			value = 0;
		}
		cursor.close();
		closeDatabase();
		if (value > 0) {
			return true;
		}
		return false;
	}
	
	public void addVideoUserEntriesInDatabase(VideoUserDTO videoUserDTO)
	{
		SQLiteDatabase db = openDatabase();
		try {

			String query =  VideoUserDAO.getVideoInsertQuery(videoUserDTO);
			
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
	}
	
	
	public int getTotalEntryInMachineCoreTable()
	{
		SQLiteDatabase db = openDatabase();
		try {

			Cursor cur = db.rawQuery(MachineCoreDAO.getMachineCoreTotalRowQuery(), null);
			if (cur != null) {
			    cur.moveToFirst();                       // Always one row returned.
			    if (cur.getInt (0) == 0) {               // Zero count means empty table.
			       return 0;
			    }
			    
			   
			}
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
		return 1;
	}
	
	public void saveMachineCoreTableData(MachineCoreDTO machineCoreDTO)
	{
		SQLiteDatabase db = openDatabase();
		try {

			String query =  MachineCoreDAO.getMachineCoreTotalEntryQuery(machineCoreDTO);
			
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
		
	}

	public void setUpdateErrorStatusOnMachineCore() {
		
		SQLiteDatabase db = openDatabase();
		try {

			String query =  MachineCoreDAO.getUpdateErrorStatusOnMachineCoreQuery();
			
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
		} finally {
			closeDatabase();
		}
	}

	public boolean setUpdateCompleteOnMachineCore() {
		
		SQLiteDatabase db = openDatabase();
		try {

			String query =  MachineCoreDAO.getUpdateCompleteStatusOnMachineCoreQuery();
			
			db.execSQL(query);
			Log.d(LOG_TAG, "Executed Query:" + query);
			return true;
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
			return false;
		} finally {
			closeDatabase();
			
		}
		
	}
	
	
	public boolean saveAdaptiveQuestions(ArrayList<AdaptiveQuestion> adaptiveQuestionList)
	{
		
		SQLiteDatabase db = openDatabase();
		try {
			
			 for(AdaptiveQuestion dto:adaptiveQuestionList)
			 {
				String CAMPAIGN_ADAPTIVE_QUESTION_INSERT_QUERY 
				= CampaignAdaptiveQuestionDAO.getCampaignAdaptiveQuestionCRUDQuery(dto);
				db.execSQL(CAMPAIGN_ADAPTIVE_QUESTION_INSERT_QUERY);
				Log.d(LOG_TAG, "Executed Query:" + CAMPAIGN_ADAPTIVE_QUESTION_INSERT_QUERY);
			 }
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
			return false;
			
		} finally {
			closeDatabase();
		}
		return true;
		
	}
	
	

	public boolean getMachineCoreUpdateStatus() {
		
		
		String Query=MachineCoreDAO.getUpdateStatusOnMachineCoreQuery();
		
		String updateStatus = "";
		SQLiteDatabase db = null;
		Cursor cursor = null;
		
		try {
			db = openDatabase();
			cursor = db.rawQuery(Query, null);
			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					updateStatus = cursor.getString(0);
					
				} while (cursor.moveToNext());
			}
			if(updateStatus.trim().equals("0"))
			{
				return false;
			}else
			{
				return true;
			}
		} catch (Exception e) {
			
			Log.d(LOG_TAG, "Exception message:" + e.getMessage());
			return false;
		}finally{
			cursor.close();
			closeDatabase();
		}
		
		
	}

	public ArrayList<AdaptiveQuestion> getAllAdaptiveQuestinList() {
		
		String query = CampaignAdaptiveQuestionDAO.getAdaptiveQuestionQuery();;
		
		ArrayList<AdaptiveQuestion> list = new ArrayList<AdaptiveQuestion>();
		
		Cursor cursor = null;
		
		try
		{
			SQLiteDatabase db = openDatabase();
			cursor = db.rawQuery(query, null);
			AdaptiveQuestion adaptiveQuestion = new AdaptiveQuestion();

			if (cursor.moveToFirst()) {
				do {
			
					adaptiveQuestion = new AdaptiveQuestion();
					adaptiveQuestion.setQuestionId(cursor.getString(0));
					adaptiveQuestion.setQuestion(cursor.getString(1));
					adaptiveQuestion.setCampignId(cursor.getString(2));
			
					Log.d(LOG_TAG,"DTO Values:"+adaptiveQuestion.toString());
					list.add(adaptiveQuestion);
				} while (cursor.moveToNext());
			} else {
			list = new ArrayList<AdaptiveQuestion>();
			}
		}catch(Exception ex)
		{
			Log.d(LOG_TAG, "Exception message:" + ex.getMessage());
			list = new ArrayList<AdaptiveQuestion>();
		}finally{
			cursor.close();
			closeDatabase();
		}
		
		return list;
	}

	public boolean saveTabletRegistrationData(
			TabletAppRegistrationDTO tabletAppRegistrationDTO) {
		
		SQLiteDatabase db = openDatabase();
		try {
			
			
				String TABLET_REGISTRATION_DATA  
				= TabletRegistrationDAO.getInsertRegistrationQuery(tabletAppRegistrationDTO);
				Log.d(LOG_TAG, "Executed Query:" + TABLET_REGISTRATION_DATA);
				db.execSQL(TABLET_REGISTRATION_DATA);
				Log.d(LOG_TAG, "Executed Query:" + TABLET_REGISTRATION_DATA);
			 
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
			return false;
			
		} finally {
			closeDatabase();
		}
		return true;
	}

	public String getDeviceIdFromRegistrationTable() {
		
		String query = TabletRegistrationDAO.getSelectQueryForDeviceId();
		
		String device_id = "";
		
		Cursor cursor = null;
		
		try
		{
			SQLiteDatabase db = openDatabase();
			cursor = db.rawQuery(query, null);

			if (cursor.moveToFirst()) {
				do {
			
					
					device_id = cursor.getString(0);
					Log.d(LOG_TAG,"DTO Values: "+device_id);
					
				} while (cursor.moveToNext());
			} else {
				device_id = "";
			}
		}catch(Exception ex)
		{
			Log.d(LOG_TAG, "Exception message:" + ex.getMessage());
			
		}finally{
			cursor.close();
			closeDatabase();
		}
		
		
		return device_id;
	}

	public boolean saveTabletAppStatus(TabletAppStatusDTO tabletAppStatusDTO) {
		
		SQLiteDatabase db = openDatabase();
		try {
			
			
				String TABLET_REGISTRATION_DATA  
				= TabletAppStatsDAO.getInsertTabletAppStats(tabletAppStatusDTO);
				db.execSQL(TABLET_REGISTRATION_DATA);
				Log.d(LOG_TAG, "Executed Query:" + TABLET_REGISTRATION_DATA);
			 
		} catch (Exception e) {
			Log.d(LOG_TAG, "Exception:" + e.getMessage());
			return false;
			
		} finally {
			closeDatabase();
		}
		return true;
	}
	
}
