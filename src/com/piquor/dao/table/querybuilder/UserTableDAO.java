package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.USER_TABLE_COLUMNS;
import com.piquor.dto.UserTableDTO;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class UserTableDAO implements USER_TABLE_COLUMNS {
	
	public static String getDeleteUserTableEntriesQuery()
	{
		String deleteQuery = "delete from user";
		
		return deleteQuery;
	}
	
    public static String getTotalRowCountQuery(String flag)
    {
    	
    	String query = "";
		if(flag.equals("TOTAL"))
		{
			query = "select count(*) from " + TABLE_NAME_USER;
		}else if(flag.equals(DeliveryModeFlags.EMAIL_DELIVERYMODE_FLAG))
		{
			query = "select count(*) from "+ TABLE_NAME_USER + " where "+COLUMN_DELIEVERYMODE+" like '%"+DeliveryModeFlags.EMAIL_DELIVERYMODE_FLAG+"%'"; 
		}else if(flag.equals(DeliveryModeFlags.TWITTER_DELIVERYMODE_FLAG))
		{
			query = "select count(*) from "+ TABLE_NAME_USER + " where "+COLUMN_DELIEVERYMODE+" like '%"+DeliveryModeFlags.TWITTER_DELIVERYMODE_FLAG+"%'"; 
		}else if(flag.equals(DeliveryModeFlags.FACEBOOK_DELIVERYMODE_FLAG))
		{
			query = "select count(*) from "+ TABLE_NAME_USER + " where "+COLUMN_DELIEVERYMODE+" like '%"+DeliveryModeFlags.FACEBOOK_DELIVERYMODE_FLAG+"%'"; 
		}else if(flag.equals("PRINT"))
		{
			query = "select count(*) from "+ TABLE_NAME_USER + " where "+COLUMN_ISPRINTED+"!='0'"; 
		}else if(flag.equals("UPLOADED"))
		{
			query = "select count(*) from "+ TABLE_NAME_USER + " where "+COLUMN_FILEUPLOAD+"='1' AND "+COLUMN_MAILSENT+"='1'"; 
		}
		
		return query;
    }
	
	public static String findUser(int id)
	{
		String query = "Select * FROM " + TABLE_NAME_USER + " WHERE " + COLUMN_ID + " =  " + id + "";
		return query;
	}
	
	
	public static String getUserTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + "user";
		return query;
	}
	
	public static String getCreateUserTableQuery()
	{
		String CREATE_USERS_TABLE = "CREATE TABLE [user] "
				+ "([socialMediaPermission] varchar(2) DEFAULT 'NULL' NULL,"
				+ "[dob] varchar(20) DEFAULT 'NULL' NULL,"
				+ "[campaignid] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[IsPrinted] varchar(2) DEFAULT '''0''' NULL,"
				+ "[status] varchar(2) DEFAULT 'NULL' NULL,"
				+ "[feedback] varchar(20) DEFAULT 'NULL' NULL,"
				+ "[frame_selected] varchar(2) DEFAULT 'NULL' NULL,"
				+ "[date] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[gender] varchar(6) DEFAULT 'NULL' NULL,"
				+ "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,"
				+ "[picID] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[name] varchar(80) DEFAULT 'NULL' NULL,"
				+ "[email] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[mailsent] varchar(2) DEFAULT 'NULL' NULL,"
				+ "[fileUpload] varchar(2) DEFAULT 'NULL' NULL,"
				+ "[phone] varchar(20) DEFAULT 'NULL' NULL,"
				+ "[DelieveryMode] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[FbUserId] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[FbUserName] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[FbName] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[TwitterId] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[TwitterName] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[TwitterFollowers] varchar(100) DEFAULT 'NULL' NULL,"
				+ "[TwitterFriends] varchar(100) DEFAULT 'NULL' NULL"
				+ ")";
		return CREATE_USERS_TABLE;
	}
	
	public static String getUserEntryQuery(UserTableDTO userDTO)
	{
		String query = "INSERT INTO " + TABLE_NAME_USER + "(";
		query += COLUMN_CAMPAIGN_ID + "," + COLUMN_DATE + ","
				+ COLUMN_DOB_COLUMN + "," + COLUMN_EMAIL + ","
				+ COLUMN_DELIEVERYMODE + "," + COLUMN_FEEDBACK + ","
				+ COLUMN_ISPRINTED + "," + COLUMN_FILEUPLOAD + ","
				+ COLUMN_MAILSENT + "," + COLUMN_FRAME_SELECTED + ","
				+ COLUMN_GENDER + "," + COLUMN_PICID + ","
				+ COLUMN_SOCIAL_MEDIA_PERMISSION + "," + COLUMN_PHONE + ","
				+ COLUMN_STATUS + "," + COLUMN_NAME + "," + COLUMN_FBUSERID
				+ "," + COLUMN_FBUSERNAME + "," + COLUMN_FBNAME + ","
				+ COLUMN_TWITTER_FOLLOWERS + "," + COLUMN_TWITTER_FRIENDS
				+ "," + COLUMN_TWITTER_ID + "," + COLUMn_TWITTERNAME + ")";
		query += " VALUES ('" + userDTO.getCampaignId() + "','" + userDTO.getDate()
				+ "','" + userDTO.getDateofbirth() + "','"+ userDTO.getEmail() 
				+ "','" + userDTO.getDelieveryMode()+ "','" + userDTO.getFeedback() 
				+ "','"+ userDTO.getNOOfPrints() + "','" + userDTO.getFileUpload()
				+ "','"+ userDTO.getMailSent() + "','"+ userDTO.getFrameSelected() 
				+ "','" + userDTO.getGender()+ "','" + userDTO.getPicId() 
				+ "','"+ userDTO.getPhotoPermissionSocialMedia() + "','"+ userDTO.getPhone() 
				+ "','" + userDTO.getStatus() + "','"+ userDTO.getName() + "','" + userDTO.getFbUserId() 
				+ "','"+ userDTO.getFbUserName() + "','" + userDTO.getFbName()
				+ "','" + userDTO.getTwitterFollowers() + "','"+ userDTO.getTwitterFriendsCount() 
				+ "','"+ userDTO.getTwitterId() + "','" + userDTO.getTwitterName()+"'"
				+ ");";
		return query;
	}
}
