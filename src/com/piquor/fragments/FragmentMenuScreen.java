package com.piquor.fragments;

import java.lang.ref.WeakReference;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.piquor.com.dao.model.SavedDBInfo;
import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.constants.APP_CONS;
import com.piquor.constants.APP_CREDENTIALS;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;
import com.piquor.print.activity.PrintDialogActivity;
import com.piquor.social.FacebookManager;
import com.piquor.twitter.utility.TwitterConstants;
import com.piquor.utility.AlertDialogManager;
import com.piquor.utility.InternetConnectionDetector;
import com.services.deliverymode.staticfields.DeliveryModeFlags;

public class FragmentMenuScreen extends Fragment implements ACTIVITY_FLAGS,
		FRAGMENT_SCREEN_NAMES {

	private String LOG_TAG = "FragmentMenuScreen";
	private FacebookManager fbManager;
	private final String TWITTER_CALLBACK_URL = "http://api.twitter.com/oauth/authorize?force_login=true";
	private ImageView login;
	private Twitter twitter;
	private RequestToken requestToken = null;
	private AccessToken accessToken;
	private String oauth_url, oauth_verifier, profile_url;
	private Dialog auth_dialog;
	private WebView web;
	private SharedPreferences pref;
	private ProgressDialog progress;
	private FragementChangeListener fragmentChangeListener;
	private RelativeLayout relativeLayout;
	private ImageView imgframedImageView;
	private LinearLayout linearLayout;

	@Override
	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		fbManager = FacebookManager.GetInstance();
		fbManager.setParentFragment((Fragment) this);
		fbManager.onCreate(savedInstance);
	}

	@Override
	public void onResume() {
		super.onResume();
		fbManager.onResume();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		fbManager.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();
		fbManager.onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		fbManager.onSaveInstanceState(outState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		fbManager.onActivityResult(requestCode, resultCode, data);
		Log.i(LOG_TAG, "OnActivity of fragment reached");
	}

	// private ImageView imgframedImageView;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener) getActivity();

	}

	@Override
	public View onCreateView(LayoutInflater inflator, ViewGroup parent,
			Bundle savedInstance) {
		
	
		
		View view = inflator
				.inflate(R.layout.fragmentmenuscreen, parent, false);
		
		relativeLayout = (RelativeLayout) view.findViewById(R.id.menuLayout);
		
		imgframedImageView = (ImageView) view.findViewById(R.id.imgMenuScreenImagePreview);
		
		linearLayout = (LinearLayout) view.findViewById(R.id.imgHolderFrames);

		pref = getActivity().getPreferences(0);
		twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(pref.getString(
				TwitterConstants.TWITTER_PREF_CONSUMER_KEY_FLAG, ""), pref
				.getString(TwitterConstants.TWITTER_PREF_CONSUMER_SECRET_FLAG,
						""));

		ImageButton btnFacebook = (ImageButton) view
				.findViewById(R.id.btnFacebook);
		btnFacebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// goToFacebookScreen();
				startFacebookSession();
			}
		});

		ImageButton btnTwitter = (ImageButton) view
				.findViewById(R.id.btnTwitter);
		btnTwitter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				goToTwitterScreen();
			}
		});
		ImageButton btnEmail = (ImageButton) view.findViewById(R.id.btnEmail);
		btnEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				goToEmailScreen();

			}
		});
		ImageButton btnRetake = (ImageButton) view.findViewById(R.id.btnHome);
		btnRetake.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				goToHomeScreen();
			}
		});

		ImageButton btnBackMenu = (ImageButton) view
				.findViewById(R.id.btnBack_menuscreen);
		btnBackMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				goToMenuBackScreen();
			}
		});
		
		
		ViewTreeObserver vto = imgframedImageView.getViewTreeObserver();
	    vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	        @Override
	        public void onGlobalLayout() {
	       
	        	//Toast.makeText(getActivity(), "On Global Layout", Toast.LENGTH_SHORT).show();
	        	loadFramedImageHere();
	    }
	    });
		

		return view;
	}
	
	private void loadFramedImageHere()
	{
		if (APP_CONS.get_framedImage() != null) {
			
			BitmapWorkerTask bitmapWorkerTask = new BitmapWorkerTask(imgframedImageView);
			bitmapWorkerTask.execute();
		}
	}
	
	/*private void loadFramedImageHere() {

		if (APP_CONS.get_framedImage() != null) {

			String pathName = APP_CONS.get_framedImage().getAbsolutePath();
			// Resources res = getResources();

			if (APP_CONS.get_tempImageFile() == null) {

				Log.d(LOG_TAG, "Loading Temp Image File");
				APP_CONS.set_tempImageFile(BitmapFactory.decodeFile(pathName));
			}
			int width = APP_CONS.get_tempImageFile().getWidth();
			int height = APP_CONS.get_tempImageFile().getHeight();
			if (APP_CONS.get_tempImageScaledFile() == null) {

				// Get current dimensions AND the desired bounding box
				Log.d(LOG_TAG, "Creating Temp Scaled File");
				int bounding = dpToPx(900);
				if (bounding != 1) {
					Log.d("Test", "original width = " + Integer.toString(width));
					Log.d("Test",
							"original height = " + Integer.toString(height));
					Log.d("Test", "bounding = " + Integer.toString(bounding));

					// Determine how much to scale: the dimension requiring less
					// scaling
					// is
					// closer to the its side. This way the image always stays
					// inside
					// your
					// bounding box AND either x/y axis touches it.
					float xScale = ((float) bounding) / width;
					float yScale = ((float) bounding) / height;
					float scale = (xScale <= yScale) ? xScale : yScale;
					Log.i("Test", "xScale = " + Float.toString(xScale));
					Log.i("Test", "yScale = " + Float.toString(yScale));
					Log.i("Test", "scale = " + Float.toString(scale));

					// Create a matrix for the scaling and add the scaling data
					Matrix matrix = new Matrix();
					matrix.postScale(scale, scale);

					// Create a new bitmap and convert it to a format understood
					// by
					// the
					// ImageView
					APP_CONS.set_tempImageScaledFile(Bitmap.createBitmap(
							APP_CONS.get_tempImageFile(), 0, 0, width, height,
							matrix, true));
					width = APP_CONS.get_tempImageScaledFile().getWidth(); // re-use
					height = APP_CONS.get_tempImageScaledFile().getHeight(); // re-use

					Log.i("Test", "scaled width = " + Integer.toString(width));
					Log.i("Test", "scaled height = " + Integer.toString(height));

					Log.d(LOG_TAG,
							"Created Scaled Image here with menu options");
					// Apply the scaled bitmap
					imgframedImageView.setImageDrawable(new BitmapDrawable(
							APP_CONS.get_tempImageScaledFile()));
				}
			} else {
				// Log.d(LOG_TAG,"Image Already Available:");
				imgframedImageView.setImageDrawable(new BitmapDrawable(APP_CONS
						.get_tempImageScaledFile()));

			}

			// Now change ImageView's dimensions to match the scaled image
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imgframedImageView
					.getLayoutParams();
			params.width = width;
			params.height = height;
			imgframedImageView.setLayoutParams(params);
		}
		//Log.i("Test", "done");
	}*/
	
	
	private int dpToPx(int dp)
	{
		if(getActivity()!=null)
		{
			float density = getActivity().getApplicationContext().getResources().getDisplayMetrics().density;
			return Math.round((float)dp * density);
		}else 
		{
			return 1;
		}
	}
	
	

	/*private void loadBackgroundImage() {
		
		if(APP_CONS.get_framedImage()!=null)
		{		
			Drawable drawable = Drawable.createFromPath(APP_CONS.get_framedImage().getAbsolutePath().trim());
			relativeLayout.setBackground(drawable);
		}
	}*/

	protected void goToMenuBackScreen() {
		
		storeUserEntryIntoDabase();
		fragmentChangeListener.navigateToCaptureScreen();

	}

	private void storeUserEntryIntoDabase() {

		Log.d(LOG_TAG, "Navigating To Effect Screen");

		SavedDBInfo savedDB = (MainActivity) getActivity();
		savedDB.enterUserIntoDataBase();
	}

	protected void printImage() {

		if (isNetworkAvailable() == false) {

			new AlertDialogManager()
					.showAlertDialog(
							getActivity(),
							"Network Error",
							"Network connection not available, Please try later",
							false);

		} else {

			MainActivity.setIncrementPrintNumber();
			Intent printIntent = new Intent(getActivity(),
					PrintDialogActivity.class);
			printIntent.setDataAndType(
					Uri.fromFile(APP_CONS.get_framedImage()), "image/jpeg");

			printIntent.putExtra("title", "Piquor Selfie Print");
			startActivity(printIntent);
		}
	}

	protected void goToHomeScreen() {
		storeUserEntryIntoDabase();
		goToHomeMenuOptions();

	}
	
	protected void goToHomeMenuOptions() {

		APP_CONS.recycleProcessedBitmap();
		fragmentChangeListener.navigateToHomeScreen();

	}


	protected void startFacebookSession() {
		fbManager.startNewSession();
	}

	protected void goToTwitterScreen() {

		if (checkInternetConnectivity()) {
			new TokenGet().execute();
		}

	}

	protected void goToEmailScreen() {

		Log.d(LOG_TAG, "Opening Fragment Manger dialog");
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		FragmentEmailScreen fragmentEmailScreen = new FragmentEmailScreen();
		fragmentEmailScreen.show(fragmentManager, "EmailDialog");

	}

	public boolean isNetworkAvailable() {

		ConnectivityManager cm = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null
		// otherwise check if we are connected
		if (networkInfo != null && networkInfo.isConnected()) {
			Log.e("Network Testing", "***Available***");
			return true;
		}
		Log.e("Network Testing", "***Not Available***");
		return false;
	}

	private boolean checkInternetConnectivity() {
		InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(
				getActivity());
		if (!internetConnectionDetector.isConnectingToInternet()) {
			new AlertDialogManager().showAlertDialog(getActivity(),
					"Intenet Connection Failed",
					"Please Ensure Internet Connectivity", false);
			return false;
		}
		return true;
	}

	private class TokenGet extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected String doInBackground(String... args) {

			try {
				requestToken = twitter
						.getOAuthRequestToken(TWITTER_CALLBACK_URL);
				oauth_url = requestToken.getAuthorizationURL();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return oauth_url;
		}

		@Override
		protected void onPostExecute(String oauth_url) {

			if (oauth_url != null) {

				oauth_url += "&force_login=true";
				Log.e("URL", oauth_url);
				auth_dialog = new Dialog(getActivity());
				auth_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				auth_dialog.setContentView(R.layout.auth_dialog);
				web = (WebView) auth_dialog.findViewById(R.id.webv);
				web.getSettings().setJavaScriptEnabled(true);

				web.loadUrl(oauth_url);
				web.setWebViewClient(new WebViewClient() {
					boolean authComplete = false;

					@Override
					public void onPageStarted(WebView view, String url,
							Bitmap favicon) {
						super.onPageStarted(view, url, favicon);
					}

					@Override
					public void onPageFinished(WebView view, String url) {

						super.onPageFinished(view, url);
						if (url.contains("oauth_verifier")
								&& authComplete == false) {
							authComplete = true;
							Log.e("Url", url);
							Uri uri = Uri.parse(url);
							oauth_verifier = uri
									.getQueryParameter("oauth_verifier");
							auth_dialog.dismiss();
							new AccessTokenGet().execute();

						} else if (url.contains("denied")) {

							auth_dialog.dismiss();
							Toast.makeText(getActivity(),
									"Sorry !, Permission Denied",
									Toast.LENGTH_SHORT).show();
						}
					}
				});

				auth_dialog.show();
				auth_dialog.setCancelable(true);

			} else {
				Toast.makeText(getActivity(),
						"Sorry !, Network Error or Invalid Credentials",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private class AccessTokenGet extends AsyncTask<String, String, Boolean> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = new ProgressDialog(getActivity());
			progress.setMessage("Fetching Data ...");
			progress.setCancelable(false);
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setIndeterminate(true);
			progress.show();
		}

		@Override
		protected Boolean doInBackground(String... args) {
			try {
				accessToken = twitter.getOAuthAccessToken(requestToken,
						oauth_verifier);
				SharedPreferences.Editor edit = pref.edit();
				edit.putString(TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_FLAG,
						accessToken.getToken());
				edit.putString(
						TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_SECRET_FLAG,
						accessToken.getTokenSecret());
				User user = twitter.showUser(accessToken.getUserId());

				long twitterId = user.getId();
				String twitterName = user.getName();
				int twitterFriends = user.getFriendsCount();
				int twitterFollowers = user.getFollowersCount();
				if (twitterId != 0) {
					MainActivity.setTwitterId(twitterId);
				}
				if (twitterName != null) {
					if (!twitterName.equals("")) {
						MainActivity.setTwitterName(twitterName);
					}
				}
				if (twitterFollowers != 0) {
					MainActivity.setTwitterFollowers(twitterFollowers);
				}
				if (twitterFriends != 0) {
					MainActivity.setTwitterFriends(twitterFriends);
				}
				if (twitterId != 0) {
					MainActivity
							.setDeliveryMode(DeliveryModeFlags.TWITTER_DELIVERYMODE_FLAG);
				}

				Log.d(LOG_TAG, "Twitter Info To be Inserted: " + "TwitterId:"
						+ twitterId + ":TwitterName:" + twitterName
						+ ":TwitterFollowers:" + twitterFollowers
						+ ":TwitterFriends:" + twitterFriends);

				profile_url = user.getOriginalProfileImageURL();
				edit.putString(TwitterConstants.TWITTER_PREF_USER_NAME_FLAG,
						user.getName());
				edit.putString(TwitterConstants.TWITTER_PREF_IMAGE_URL_FLAG,
						user.getOriginalProfileImageURL());
				edit.commit();
				postImageToTwitter();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean response) {
			if (response) {
				progress.hide();
				Log.d(LOG_TAG, "Final Execution");
				disconnectFromTwitter();
				new AlertDialogManager().showAlertDialog(getActivity(),
						"Twitter Status", "Image Uploaded To Twitter", true);
				MainActivity
						.setDeliveryMode(DeliveryModeFlags.TWITTER_DELIVERYMODE_FLAG);
				// goToBackTwitter();
				// .makeText(getActivity(), "Image Uploaded To Twitter",
				// Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void postImageToTwitter() {
		try {
			StatusUpdate status = new StatusUpdate(
					APP_CREDENTIALS.TWITTER_HASTAG + " "
							+ APP_CREDENTIALS.TWITTER_DESC);
			status.setMedia(APP_CONS.get_framedImage());
			twitter.updateStatus(status);
		} catch (TwitterException e) {
			Log.d("TAG", "Pic Upload error" + e.getErrorMessage());

		}

	}

	private void disconnectFromTwitter() {

		SharedPreferences.Editor editor = pref.edit();
		editor.remove(TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_FLAG);
		editor.remove(TwitterConstants.TWITTER_PREF_ACCESS_TOKEN_SECRET_FLAG);
		editor.remove(TwitterConstants.TWITTER_PREF_IMAGE_URL_FLAG);
		editor.remove(TwitterConstants.TWITTER_PREF_USER_NAME_FLAG);
		editor.commit();
	}
	
	private class BitmapWorkerTask extends AsyncTask<Void, Void, Bitmap>{
		
		private final WeakReference<ImageView> imageViewReference;
		private int data = 0;
		
		public BitmapWorkerTask(ImageView imageView)
		{
			imageViewReference = new WeakReference<ImageView>(imageView);
		}
		
		@Override
		protected Bitmap doInBackground(Void... params) {
			
		
			return decodeSampledBitmapFromResource(getResources(), linearLayout.getWidth(), linearLayout.getHeight());
		}
		
		 @Override
		    protected void onPostExecute(Bitmap bitmap) {
		        if (imageViewReference != null && bitmap != null) {
		            final ImageView imageView = imageViewReference.get();
		            if (imageView != null) {
		                imageView.setImageBitmap(bitmap);
		            }
		        }
		    }
		
	}
	
	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res,
	        int reqWidth, int reqHeight) {
		
	
		String pathName = APP_CONS.get_framedImage().getAbsolutePath();

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(pathName, options);
	    // Calculate inSampleSize
	   
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	//    return BitmapFactory.decodeResource(res, resId, options);
	    return BitmapFactory.decodeFile(pathName, options);
	    
	}

}
