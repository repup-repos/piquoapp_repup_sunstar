package com.piquor.utility;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.util.Log;

import com.piquor.dto.ReviewPageTempValuesDTO;

public class ReviewPageConstantsCreator {
	
	private ReviewPageTempValuesDTO reviewPageTempValuesDTO;
	private String tripAdvisorUrl;
	private String LOG_TAG = "ReviewPageConstantsCreator";
	
	public ReviewPageConstantsCreator(String tripUrl)
	{
		reviewPageTempValuesDTO = new ReviewPageTempValuesDTO();
		tripAdvisorUrl = tripUrl;
	}
	
	
	public ReviewPageTempValuesDTO getReviewPageData(String tripUrl)
	{
		try
		{
			Log.d(LOG_TAG, tripAdvisorUrl);
			Document document = Jsoup.connect(tripAdvisorUrl).get();
			reviewPageTempValuesDTO.setPlaceDetail(document.getElementById("detail").val().trim());
			reviewPageTempValuesDTO.setPlaceGeo(document.getElementById("geo").val().trim());
			reviewPageTempValuesDTO.setPlaceReviewName(document.select("[ name=ReviewName]").get(0).val().trim());
			reviewPageTempValuesDTO.setPlaceReviewLocation(document.select("[name=ReviewLocation]").get(0).val().trim());
			reviewPageTempValuesDTO.setPlaceerrorCheck(document.select("[name=errorcheck]").get(0).val().trim());
			reviewPageTempValuesDTO.setPlaceFollowUp(document.select("[name=followup]").get(0).val().trim());
			reviewPageTempValuesDTO.setPlaceLsoId(document.getElementById("LsoId").val().trim());
			reviewPageTempValuesDTO.setPlaceAltsessid(document.getElementById("altsessid").val().trim());
			reviewPageTempValuesDTO.setPlacePval(document.getElementById("pval").val().trim());
			reviewPageTempValuesDTO.setPlacePC(document.getElementById("pc").val().trim());
			reviewPageTempValuesDTO.setPlaceToken(document.getElementById("token").val().trim());
			Log.d(LOG_TAG,"Fetched Value: "+reviewPageTempValuesDTO.toString());
		}catch(Exception ex)
		{
			Log.d(LOG_TAG, "Exception ex"+ex.getMessage());
			return null;
		}
		return reviewPageTempValuesDTO;
		
	}
}
