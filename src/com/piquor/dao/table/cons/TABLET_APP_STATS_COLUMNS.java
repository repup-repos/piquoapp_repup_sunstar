package com.piquor.dao.table.cons;

public interface TABLET_APP_STATS_COLUMNS {
	
	public final String TABLE_NAME_TABLET_APP_STATS = "tablet_app_status";
	public final String COLUMN_NAME_ID = "id";
	public final String COLUMN_NAME_DEVICE_ID = "device_id";
	public final String COLUMN_NAME_LAST_APP_OPENED_TIME = "last_app_opened_time";
	public final String COLUMN_NAME_CAMPAIGNID = "campaignid";
	public final String COLUMN_NAME_TIMESTAMP = "timestamp";
	public final String COLUMN_NAME_STATS_ENTRY_UPLOADED = "stats_uploaded";
	public final String COLUMN_NAME_STATS_ENTRY_UPLOADEDSTATUS = "stats_uploadstatus";
	
	
}
