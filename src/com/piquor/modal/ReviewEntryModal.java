package com.piquor.modal;

import android.util.Log;
import android.widget.RadioGroup;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.piquortabapp.R;

public class ReviewEntryModal {

	private ReviewEntryDTO reviewEntryDTO;
	private final String FLAG_VISIT_REASON_COUPLES = "Couples";
	private final String FLAG_VISIT_REASON_FAMILTY = "Family";
	private final String FLAG_VISIT_REASON_FRIENDS = "Friends";
	private final String FLAG_VISIT_REASON_SOLO = "Solo";
	private final String FLAG_VISIT_REASON_BUSINESS = "Business";
	
	private String LOG_TAG = "ReviewEntryModal";
	
	public ReviewEntryDTO getReviewEntryDTO() {
		return reviewEntryDTO;
	}

	public void setReviewEntryDTO(ReviewEntryDTO reviewEntryDTO) {
		this.reviewEntryDTO = reviewEntryDTO;
	}

	public ReviewEntryModal()
	{
		reviewEntryDTO = new ReviewEntryDTO();
		
		
	}
	
	public String setReviewEntryTextData(String reviewTitle,String emailid)
	{
		
				if(validateText(reviewTitle, "Review Title").equals(""))
				{
					getReviewEntryDTO().setReviewentry_title(reviewTitle);
					if(validateText(emailid, "email").equals(""))
					{
						getReviewEntryDTO().setReviewentry_emailid(emailid);
						return "";
					}else
					{
						return validateText(emailid, "EmailId");
					}
				}else
				{
					return validateText(reviewTitle, "Review Text");
				}
			
		
		
	}
	
	/*public String setReviewEntryTextData(String name,String roomno,String reviewTitle,String emailid)
	{
		if(validateText(name, "name").trim().equals(""))
		{
			getReviewEntryDTO().setReviewentry_name(name);
			if(validateText(roomno, "RoomNo").equals(""))
			{
				getReviewEntryDTO().setReviewentry_roomno(roomno);
				if(validateText(reviewTitle, "Review Title").equals(""))
				{
					getReviewEntryDTO().setReviewentry_title(reviewTitle);
					if(validateText(emailid, "email").equals(""))
					{
						getReviewEntryDTO().setReviewentry_emailid(emailid);
						return "";
					}else
					{
						return validateText(emailid, "EmailId");
					}
				}else
				{
					return validateText(reviewTitle, "Review Text");
				}
			}else
			{
				return validateText(roomno, "RoomNo");
			}
		}else
		{
			return validateText(name, "name");
		}
		
	}*/

	
	public String setRatings(int ratingCheckIn,int ratingRooms,int ratingcleanliNess,
			int ratingAmneties,int ratingfoodAndBeveraes,int ratingwifiAndInternet,int ratingcheckOut,int ratingconceirge)
	{
		
		Log.d(LOG_TAG, "Setting all rating values");
		int overall = 0;
		
		if(validateRating(ratingCheckIn, "CheckIn Rating").equals(""))
		{
			getReviewEntryDTO().setReviewentry_ratingCheckIn(String.valueOf(ratingCheckIn));
			if(validateRating(ratingRooms, "Rooms").equals(""))
			{
				getReviewEntryDTO().setReviewentry_ratingRooms(String.valueOf(ratingRooms));
				if(validateRating(ratingcleanliNess, "Cleanliness").equals(""))
				{
					getReviewEntryDTO().setReviewentry_ratingClieanliness(String.valueOf(ratingcleanliNess));
					if(validateRating(ratingAmneties, "Amneties").equals(""))
					{
						getReviewEntryDTO().setReviewentry_ratingAmneties(String.valueOf(ratingAmneties));
						if(validateRating(ratingfoodAndBeveraes, "Food And Beverages").equals(""))
						{
							getReviewEntryDTO().setReviewentry_ratingFoodandBeverages(String.valueOf(ratingfoodAndBeveraes));
							if(validateRating(ratingconceirge, "Conceirge").equals(""))
							{
								getReviewEntryDTO().setReviewentry_ratingconceirge(String.valueOf(ratingconceirge));
								if(validateRating(ratingcheckOut, "CheckOut").equals(""))
								{
									getReviewEntryDTO().setReviewentry_ratingCheckout(String.valueOf(ratingcheckOut));
									if(validateRating(ratingwifiAndInternet, "Wifi And Internet").equals(""))
									{
										overall =(int) ((double)(ratingCheckIn+ratingRooms+ratingcleanliNess+ratingAmneties+ratingfoodAndBeveraes+ratingwifiAndInternet+ratingcheckOut+ratingconceirge)/8);
										
										getReviewEntryDTO().setReviewentry_ratingWifiAndInternet(String.valueOf(ratingwifiAndInternet));
										getReviewEntryDTO().setReviewentry_overallrating(String.valueOf(overall));
										Log.d(LOG_TAG, "Overall Rating:"+overall);
									}else
									{
										return validateRating(ratingwifiAndInternet, "Wifi And Internet");	
									}
								}else
								{
									return validateRating(ratingcheckOut, "CheckOut");	
								}
							}else
							{
								return validateRating(ratingconceirge, "Conceirge");	
							}
						}else
						{
							return validateRating(ratingcleanliNess, "Food And Beverages");	
						}
					}else
					{
						return validateRating(ratingcleanliNess, "Amneties");	
					}
				}else
				{
					return validateRating(ratingcleanliNess, "Cleanliness");	
				}
			}else
			{
				return validateRating(ratingRooms, "Rooms");
			}
		}else
		{
			return validateRating(ratingCheckIn, "CheckIn");
		}
		
		return "";
		
	}
	
	private String validateRating(int rating,String specifier)
	{
		if(rating==0)
		{
			return "Please select "+specifier+" rating";
		}
		return "";
	}
	
	private String validateText(String text,String specifier)
	{
		if(specifier.equals("email"))
		{
			return getValidationEmail(text);
		}else
		{
			if(text.trim().equals(""))
			{
				return "Enter "+specifier;
			}
		}
		return "";
		
	}
	
	private String getValidationEmail(String email)
	{
		if(email.equals(""))
		{
			return "Please Enter EmailId";
		}else if(android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()==false)
		{	return "Invalid EmailId";
		}
	
		return "";
	}

	public String setSortOfVisit(RadioGroup rgSortOfVisit) {
		
		String sortOfVisit = "";
		
		if(rgSortOfVisit.getCheckedRadioButtonId()!=-1){
			
			
			switch(rgSortOfVisit.getCheckedRadioButtonId())
			   {
			   		case R.id.btnCouple:
			   		{
			   			sortOfVisit = FLAG_VISIT_REASON_COUPLES;
			   			break;
			   		}
			   		case R.id.btnBusiness:
			   		{
			   			sortOfVisit = FLAG_VISIT_REASON_BUSINESS;
			   			break;
			   		}
			   		case R.id.btnFamily:
			   		{
			   			sortOfVisit = FLAG_VISIT_REASON_FAMILTY;
			   			break;
			   		}
			   		case R.id.btnFriends:
			   		{
			   			sortOfVisit = FLAG_VISIT_REASON_FRIENDS;
			   			break;
			   		}
			   		case R.id.btnSolo:
			   		{
			   			sortOfVisit = FLAG_VISIT_REASON_SOLO;
			   			break;
			   		}
				   };
			    
				   
			}
		if(sortOfVisit.equals(""))
		{
			return "Please select sort of visit";
		}else
		{
			reviewEntryDTO.setReviewentry_sortofvisit(sortOfVisit);
		}
		
		return "";
	}
	
}
