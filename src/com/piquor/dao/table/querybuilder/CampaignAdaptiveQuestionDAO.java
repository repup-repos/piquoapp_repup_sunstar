package com.piquor.dao.table.querybuilder;

import com.piquor.dao.table.cons.CAMPAIGN_ADAPTIVE_QUESTIONS;
import com.piquor.dto.AdaptiveQuestion;

public class CampaignAdaptiveQuestionDAO implements CAMPAIGN_ADAPTIVE_QUESTIONS {

	
	
	public static String getCampaignAdaptiveQuestionsTableCreateQuery()
	{
		String CREATE_CAMPAIGN_TABLE = "CREATE TABLE ["+TABLE_NAME_CAMPAIGN_ADAPTIVE_QUESTIONS+"] "
				+ "(["+COLUMN_NAME_CAMPAIGN_ID+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_QUESTION_ID+"] varchar(200) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_QUESTION+"] varchar(8000) DEFAULT 'NULL' NULL,"
				+ "["+COLUMN_NAME_ID+"] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT"
				+ ")";
		
		return CREATE_CAMPAIGN_TABLE;
	}
	
	public static String getCampaignAdaptiveQuestionsTableDropQuery()
	{
		String query = "DROP TABLE IF EXISTS " + TABLE_NAME_CAMPAIGN_ADAPTIVE_QUESTIONS;
		return query;
	}
	
	public static String getCampaignAdaptiveQuestionCRUDQuery(AdaptiveQuestion adaptiveQuestionDTO)
	{
		String CAMPAING_ADAPTIVE_QUESTION_CURD_QUERY = "";
		if(adaptiveQuestionDTO.getState().equals(STATE_ADD))
		{
			String CAMPAIGN_ADAPTIVE_QUESTION_INSERT_QUERY = "INSERT INTO " + TABLE_NAME_CAMPAIGN_ADAPTIVE_QUESTIONS + "(";
			
			CAMPAIGN_ADAPTIVE_QUESTION_INSERT_QUERY += COLUMN_NAME_CAMPAIGN_ID + ","+ COLUMN_NAME_QUESTION_ID+","+COLUMN_NAME_QUESTION+")";
		
			CAMPAIGN_ADAPTIVE_QUESTION_INSERT_QUERY += " VALUES ('"+adaptiveQuestionDTO.getCampignId().trim()+"','"+adaptiveQuestionDTO.getQuestionId().trim()+"','"+adaptiveQuestionDTO.getQuestion().trim()+"');";
			
			CAMPAING_ADAPTIVE_QUESTION_CURD_QUERY = CAMPAIGN_ADAPTIVE_QUESTION_INSERT_QUERY;
		}else if(adaptiveQuestionDTO.getState().equals(STATE_DELETE))
		{
			String CAMPAIGN_ADAPTIVE_QUESTION_DELETE_QUERY = "delete from "+TABLE_NAME_CAMPAIGN_ADAPTIVE_QUESTIONS
					+" where "+COLUMN_NAME_QUESTION_ID+"='"+adaptiveQuestionDTO.getQuestionId()+"'";
			
			CAMPAING_ADAPTIVE_QUESTION_CURD_QUERY = CAMPAIGN_ADAPTIVE_QUESTION_DELETE_QUERY;
		}else if(adaptiveQuestionDTO.getState().equals(STATE_MODIFY))
		{
			String CAMPAIGN_ADAPTIVE_QUESTION_UPDATE_QUERY = "UPDATE " + TABLE_NAME_CAMPAIGN_ADAPTIVE_QUESTIONS 
					+ " SET ";
			CAMPAIGN_ADAPTIVE_QUESTION_UPDATE_QUERY += COLUMN_NAME_QUESTION + "='"+adaptiveQuestionDTO.getQuestion()+"'";
			
			CAMPAIGN_ADAPTIVE_QUESTION_UPDATE_QUERY += " where "+COLUMN_NAME_QUESTION_ID+"='"+adaptiveQuestionDTO.getQuestionId()+"'";
			
			CAMPAING_ADAPTIVE_QUESTION_CURD_QUERY = CAMPAIGN_ADAPTIVE_QUESTION_UPDATE_QUERY;
		}
			
		return CAMPAING_ADAPTIVE_QUESTION_CURD_QUERY;
	}

	public static String getAdaptiveQuestionQuery() {
		
		String queryPrefix="select "+COLUMN_NAME_QUESTION_ID+","+COLUMN_NAME_QUESTION+","+COLUMN_NAME_CAMPAIGN_ID+" FROM "+TABLE_NAME_CAMPAIGN_ADAPTIVE_QUESTIONS+" ";
		
		return queryPrefix;
	}
	

	
}
