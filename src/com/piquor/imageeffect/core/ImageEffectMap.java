package com.piquor.imageeffect.core;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.piquor.constants.EFFECTS_TAGS;
import com.piquor.piquortabapp.R;


public class ImageEffectMap implements EFFECTS_TAGS{

	
private  static Map<String, Integer> IMAGE_EFFECTS_MAP = new TreeMap<String, Integer>();
	
	private static void addEffect(String key, Integer value) {
		
		IMAGE_EFFECTS_MAP.put(key, value);
	}
	
	static {
		
		addEffect("Normal", Integer.valueOf(NORMAL_ID));
		addEffect("Lomo",Integer.valueOf(LOMO_ID));
		addEffect("Gray",Integer.valueOf(GRAY_ID));
		//addEffect("NEON",Integer.valueOf(NEON_ID));
		addEffect("OLD", Integer.valueOf(OLD_ID));
		addEffect("Sketch", Integer.valueOf(SKETCH_ID));
		addEffect("Tint", Integer.valueOf(TINT_ID));
		addEffect("Blue Tone", Integer.valueOf(BLUE_TONE));
		addEffect("Red Tone", Integer.valueOf(RED_TONE));
		addEffect("Gauthum", Integer.valueOf(GAUTHUM_ID));
		
	}
	
	public static <K extends Comparable,V extends Comparable> Map<K,V> sortByValues(Map<K,V> map){
        List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());
      
        Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

            @Override
            public int compare(Entry<K, V> o1, Entry<K, V> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
      
        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K,V> sortedMap = new LinkedHashMap<K,V>();
      
        for(Map.Entry<K,V> entry: entries){
            sortedMap.put(entry.getKey(), entry.getValue());
        }
      
        return sortedMap;
    }

	
	
	
	public static Map<String, Integer> getImageEffectMap()
	{
		IMAGE_EFFECTS_MAP = sortByValues(IMAGE_EFFECTS_MAP);
		return IMAGE_EFFECTS_MAP;
	}
	
	

	public static int[] EFFECTS_ARRAY = {R.drawable.effect1_button_states,R.drawable.effect2_button_states,R.drawable.effect3_button_states,R.drawable.effect4_button_states
		,R.drawable.effect5_button_states,R.drawable.effect6_button_states,R.drawable.effect7_button_states,R.drawable.effect8_button_states,R.drawable.effect9_button_states
	};
}
