package com.piquor.dto;

public class AppConfigurationProvider {
	
	/*
	 * This class loads all Application configuration from database
	 * provides common messenger between 
	 * DAO'S(AppConfigDAO,PhotoAppConfigDAO,ReviewConfigDAO,VideoConfigDAO,PhotoFrameConfigDAO,
	 * ZomatoConfigDAO,TripAdvisorConfigDAO,VideoFrameConfigDAO,) and other 
	 * application components required information between respective DAO'S
	 * 
	 */
	
	private static AppConfigurationDTO appConfigurationDTO = null;
//	private static ReviewConfigDTO reviewConfigurationDTO = null;
	
	/*
	 * Loads application configuration details from AppConfigurationDAO
	 * Called in main thread class MainActivity once executable
	 */
	public static void loadAppConfiguration()
	{
		//Left Stub  Just Instantiating appConfigurationDTO 
			if(appConfigurationDTO==null)
			{
				appConfigurationDTO = new AppConfigurationDTO();
			}
		//Left Stub Just Instantiating fragmentConfigurationDTO
		/*	if(reviewConfigurationDTO == null)
			{
				reviewConfigurationDTO = new ReviewConfigDTO();
			}*/
		//
	}
	
	/*
	 * GETTER method to return application configuration
	 */
	public static AppConfigurationDTO getAppConfiguration()
	{
		return appConfigurationDTO;
	}
	
	/*
	 * GETTER method to return review configuration
	 * 
	 */
	/*public static ReviewConfigDTO getReviewConfiguration()
	{
		return reviewConfigurationDTO;
	}
	*/
	
	
	
}
