package com.piquor.utility;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.graphics.Bitmap;
import android.util.Log;

import com.piquor.constants.APP_CONS;
import com.piquor.imagecomposer.core.FrameBuilderBitmapCons;

public class SaveFinalImageUtility {
	
	private String _picID;
	private String LOG_TAG = "SaveFinalImageUtility";
	
	private void createImageFileName() {
		
		Log.d(LOG_TAG, "Creating Final Frame File To UPload Image");
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
		_picID = "INDL020" + timeStamp;
		
		
	}

	public boolean saveImage() {
		
		Log.d(LOG_TAG, "Saving Image");
		createImageFileName();
		try {
			APP_CONS.set_framedImage(File.createTempFile(_picID, ".jpeg",
					APP_CONS.get_dir()));
			
		} catch (Exception ex) {
		}
		Log.d(LOG_TAG,"Image Path:"+APP_CONS.get_framedImage().getAbsolutePath());
		if (APP_CONS.get_framedImage() != null) {
			FileOutputStream out = null;
			Log.d(LOG_TAG,"Image Final Frame Size:"+FrameBuilderBitmapCons.getFinalFrameImage().getWidth()+"x"+FrameBuilderBitmapCons.getFinalFrameImage().getHeight());
			try {
				
				out = new FileOutputStream(APP_CONS.get_framedImage());
				FrameBuilderBitmapCons.getFinalFrameImage().compress(Bitmap.CompressFormat.JPEG, 100, out);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {
				try {
					if (out != null) {
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
		}

		return false;
	}

}


