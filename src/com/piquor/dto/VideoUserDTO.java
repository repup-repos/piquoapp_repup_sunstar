package com.piquor.dto;

public class VideoUserDTO {
	
	private String uservideoId;
	private String videoId;
	private String videoCaptureDate;
	
	public String getUservideoId() {
		return uservideoId;
	}
	public void setUservideoId(String uservideoId) {
		this.uservideoId = uservideoId;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public String getVideoCaptureDate() {
		return videoCaptureDate;
	}
	public void setVideoCaptureDate(String videoCaptureDate) {
		this.videoCaptureDate = videoCaptureDate;
	}
	
}
