package com.piquor.errorhandler;

import java.util.ArrayList;
import java.util.Iterator;

import android.util.Log;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.services.UrlExecutorService;

public class ReviewDataErrorHandler {
	
	public String query;
	private ReviewErrorUpdater reviewErrorUpdater;
	private UrlExecutorService urlExecutorService;
	private String LOG_TAG = "ReviewDataErrorHandler";
	
	public ReviewDataErrorHandler()
	{
		reviewErrorUpdater = new ReviewErrorUpdater();
		urlExecutorService = new UrlExecutorService();
		
	}
	
	public boolean handleUrlEntryError(ArrayList<ReviewEntryDTO> reviewEntryDTOs)
	{

		ReviewEntryDTO dto = null;
		Iterator<ReviewEntryDTO> iterator = reviewEntryDTOs.iterator();
		while (iterator.hasNext()) {
			
			dto = (ReviewEntryDTO) iterator.next();
			logInfo("Starting Proccessing Of Record: EmailId:"+dto.getReviewentry_emailid()+"@handleUrlEntryError:DataErrorHandler");
			urlExecutorService.processReviewRemoteEntry(dto);
			logInfo("Process Completed Of Record: EmailId:"+dto.getReviewentry_emailid()+"@handleUrlEntryError:DataErrorHandler");
		}
		return true;
	}
	
	public boolean handleRemainUrlEntry(ArrayList<ReviewEntryDTO> reviewEntryDTOs)
	{
		ReviewEntryDTO dto = null;
		Iterator<ReviewEntryDTO> iterator = reviewEntryDTOs.iterator();
		while (iterator.hasNext()) {
			
			dto = (ReviewEntryDTO) iterator.next();	
			logInfo("Starting Proccessing Of Record: EmailId:"+dto.getReviewentry_emailid()+"@handleRemainingUrlEntry:ReviewDataErrorHandler");
			urlExecutorService.processReviewRemoteEntry(dto);
			logInfo("Process Completed Of Record: EmailId:"+dto.getReviewentry_emailid()+"@handleRemianingUrlEntry:ReviewDataErrorHandler");
		}
		return true;
		
	}
	
	public boolean handleRemainingUserEntryData(ArrayList<ReviewEntryDTO> reviewEntryDTOs)
	{
		Iterator<ReviewEntryDTO> iterator = reviewEntryDTOs.iterator();

		while (iterator.hasNext()) {

			ReviewEntryDTO dto = iterator.next();
			logInfo("Starting Proccessing Of Record : EmailId:" + dto.getReviewentry_emailid()
					+ "@handleRemainingUserEntryData:ReviewDataErrorHandler");

			logInfo("Photo Successfully Uploaded Starting Processing For Review Url Entry EmailId:"
					+ dto.getReviewentry_emailid()
					+ "@handleRemainingUserEntryData:ReviewDataErrorHandler");
			urlExecutorService.processReviewRemoteEntry(dto);
			logInfo("Proccessing Completed Of Record: EmailId:" + dto.getReviewentry_emailid()
					+ "@handleRemainingUserEntryData:ReviewDataErrorHandler");

		}
		return true;	
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
	
}
