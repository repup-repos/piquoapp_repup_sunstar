package com.piquor.fragments;


import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.modal.AdaptiveQuestionModal;
import com.piquor.piquortabapp.R;

public class FragmentReviewAdaptiveQuestion extends Fragment implements OnClickListener{
	
//	private EditText editTextQuestionTxt1;
//	private EditText editTextQuestionTxt2;
	private ImageButton btnSubmit;
	private ImageButton btnBack;
	private ImageButton btnSayMore;
	private ReviewEntryDTO reviewEntryDTO;
	private FragementChangeListener fragmentChangeListener;
	private FragmentManager fragmentManager;
	private ScrollView scrollView;
	private AdaptiveQuestionModal adaptiveQuestionModal;
	private LinearLayout linearLayout;
	private String LOG_TAG = "FragmentReviewAdaptiveQuestion";
	private FragmentActivity fragmentActivity;
	
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener)getActivity();
		fragmentManager = getActivity()
				.getSupportFragmentManager();
		fragmentActivity = getActivity();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_adaptivequestion, container,false);
		
	/*	editTextQuestionTxt1 = (EditText) view.findViewById(R.id.editTextQuestion1);
		editTextQuestionTxt2 = (EditText) view.findViewById(R.id.editTextQuestion2);
*/		btnBack = (ImageButton) view.findViewById(R.id.imBackAdaptiveQst);
		btnSubmit = (ImageButton) view.findViewById(R.id.imSubmitAdaptiveQst);
		btnSayMore = (ImageButton) view.findViewById(R.id.imButtonScrollMore);
		btnBack.setOnClickListener(this);
		btnSubmit.setOnClickListener(this);
		btnSayMore.setOnClickListener(this);
		scrollView = (ScrollView) view.findViewById(R.id.scrollViewAdaptiveQuestion);
		adaptiveQuestionModal = new AdaptiveQuestionModal(getActivity());
		linearLayout = (LinearLayout) view.findViewById(R.id.linearLayutAdaptiveQuestions);
		Log.d(LOG_TAG, "On create view called");
		isQuestionAddedToLinearLayout = adaptiveQuestionModal.addAdaptiveQuestionToLinearLayout(linearLayout);
	/*	if(!adaptiveQuestionModal.addAdaptiveQuestionToLinearLayout(linearLayout))
		{
			proceeedToNextScreen();	
		}*/
		setUpUIForEditText(view);
		return view;
	}
	
	
	public void setUpUIForEditText(View view)
	{
		 if(!(view instanceof EditText)) {

		        view.setOnTouchListener(new OnTouchListener() {

		            public boolean onTouch(View v, MotionEvent event) {
		                hideSoftKeyboard();
		                return false;
		            }

		        });
		    }

		    //If a layout container, iterate over children and seed recursion.
		    if (view instanceof ViewGroup) {

		        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

		            View innerView = ((ViewGroup) view).getChildAt(i);

		            setUpUIForEditText(innerView);
		        }
		    }
	}
	
	
	public  void hideSoftKeyboard() {
	    InputMethodManager inputMethodManager = (InputMethodManager)  fragmentActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(fragmentActivity.getCurrentFocus().getWindowToken(), 0);
	}
	
	@Override
		public void onResume() {
			super.onResume();
			
			Log.d(LOG_TAG, "Resume method is called:backbutton fragment press addtolayout:"+isQuestionAddedToLinearLayout);
			Log.d(LOG_TAG, "ReviewEntry:"+reviewEntryDTO.toString());
			if((!isQuestionAddedToLinearLayout||!reviewEntryDTO.getReviewentry_desc().trim().equals(""))&&fragmentChangeListener.getTemporaryOnDescDialogText().equals(""))
			{
				proceeedToNextScreen();	
			}
			//Log.d(LOG_TAG, "Resume method is called:backbutton fragment press");
		}
	
	@Override
	public void onClick(View v) {
		
		if(v.getId()==R.id.imBackAdaptiveQst)
		{
			goToBackAdaptiveScreen();
		}else if(v.getId()==R.id.imSubmitAdaptiveQst)
		{
			submitAdaptiveScreen();
		}else if(v.getId()==R.id.imButtonScrollMore)
		{
			scrollScrollViewToMoreLimit();
		}
	}
	
	int currentY = 0;
	private boolean isQuestionAddedToLinearLayout = false;
	
	private void scrollScrollViewToMoreLimit() {
		
		int x = 0;
		currentY = currentY + scrollView.getHeight();
		scrollView.scrollTo(x,currentY);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
		    ValueAnimator realSmoothScrollAnimation = 
		        ValueAnimator.ofInt(scrollView.getScrollY(), currentY);
		    realSmoothScrollAnimation.setDuration(500);
		    realSmoothScrollAnimation.addUpdateListener(new AnimatorUpdateListener()
		    {
		        @Override
		        public void onAnimationUpdate(ValueAnimator animation)
		        {
		            int scrollTo = (Integer) animation.getAnimatedValue();
		            scrollView.scrollTo(0, scrollTo);
		        }
		    });

		    realSmoothScrollAnimation.start();
		}
		else
		{
			scrollView.smoothScrollTo(0, currentY);
		}
		
	}

	public void setReviewUserDTO(ReviewEntryDTO reviewEntryDTO)
	{
		
		this.reviewEntryDTO = reviewEntryDTO;
	}
	
	private void submitAdaptiveScreen() {
		
		/*String desc = "";
		String qust1 = editTextQuestionTxt1.getText().toString().trim();
		desc = qust1;
		String qust2 = editTextQuestionTxt2.getText().toString().trim();
		desc += qust2;*/
		
		String desc = adaptiveQuestionModal.getReviewDesc();
		
		reviewEntryDTO.setReviewentry_desc(desc);
		
		if(desc.length()==200)
		{
			proceeedToNextScreen();
		}else
		{
			proceeedToNextScreen();	
		}
		
	}

	private void proceeedToNextScreen() {
		
		
		if(reviewEntryDTO==null)
		{
		   Log.d(LOG_TAG,"Review Entry DTO is null");
		}else if(fragmentManager==null)
		{
//			System.out.println("Fragment Manager null");
			Log.d(LOG_TAG, "Fragment Manager null");
			fragmentManager = getActivity()
					.getSupportFragmentManager();
			if(fragmentManager==null)
			{
				Log.d(LOG_TAG, "Fragment Manager Can't be null");
			}
			FragmentReviewDescDialog fragmentReviewAdaptiveScreen = new FragmentReviewDescDialog();
			fragmentReviewAdaptiveScreen.setReviewEntryDTO(reviewEntryDTO);
			
			fragmentReviewAdaptiveScreen.show(fragmentManager, "ReviewDescDialog");
		}else
		{
			FragmentReviewDescDialog fragmentReviewAdaptiveScreen = new FragmentReviewDescDialog();
			fragmentReviewAdaptiveScreen.setReviewEntryDTO(reviewEntryDTO);
			
			fragmentReviewAdaptiveScreen.show(fragmentManager, "ReviewDescDialog");

		}
		
		
		//fragmentChangeListener.navigateToReviewPublishScreen();
	}

	private void goToBackAdaptiveScreen() {
		
		fragmentChangeListener.navigateToBackScreen();
		
	}
	
	
	
	
}
