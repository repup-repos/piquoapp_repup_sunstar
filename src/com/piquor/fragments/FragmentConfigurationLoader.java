package com.piquor.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;

import com.piquor.constants.APP_CREDENTIALS;
import com.piquor.dto.MachineCoreDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;
import com.piquor.services.deliverymode.execute.HttpUrlExecute;
import com.piquor.services.deliverymode.execute.HttpUrlResponse;
import com.piquor.utility.AlertDialogManager;
import com.piquor.utility.InternetConnectionDetector;
import com.piquor.utility.PiquorTimestamp;
import com.services.configurationmangager.ConfigManagerUpdateChecker;
import com.services.configurationmangager.ConfigurationManagerController;

public class FragmentConfigurationLoader extends Fragment implements FRAGMENT_SCREEN_NAMES{
	
	private TableLayout tbMachineIdEntry;
	private LinearLayout lbUpdateConfig;
	private Button btnSaveMachineData;
	private EditText editTextCampaignId;
	private ProgressBar progressBarConfigManager;
	private String LOG_TAG = "FragmentConfigurationLoader";
	private FragementChangeListener fragmentChangeListener;
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener) getActivity();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragmentloadconfigscreen, container, false);
		tbMachineIdEntry = (TableLayout) view.findViewById(R.id.loadconfig_machineentry);
		lbUpdateConfig = (LinearLayout) view.findViewById(R.id.loadconfig_configentry);
		
		btnSaveMachineData = (Button) view.findViewById(R.id.btnSave_fragmentloadconfigscreen);
		btnSaveMachineData.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				saveMachineData();
			}
		});
		editTextCampaignId = (EditText) view.findViewById(R.id.fragmentconfigsscreen_txtCampaignId);
		
		
		if(ConfigManagerUpdateChecker.isApps1stLoading())
		{
			tbMachineIdEntry.setVisibility(View.VISIBLE);
			lbUpdateConfig.setVisibility(View.GONE);
		}else
		{
			tbMachineIdEntry.setVisibility(View.GONE);
			lbUpdateConfig.setVisibility(View.VISIBLE);
			startConfigurationUpdater();
		}
		
		progressBarConfigManager = (ProgressBar) view.findViewById(R.id.progressBarConfigUpdater);
		
		
		
		return view;
	}
	
	protected void saveMachineData() {
		
		String machineId = editTextCampaignId.getText().toString().trim();
		if(!machineId.equals(""))
		{
			MachineCoreDTO machineCoreDTO = new MachineCoreDTO();
			machineCoreDTO.setConfigurationmanager_servicehandlertimepattern(APP_CREDENTIALS.MACHINE_CORE_SERVICEHANDLER_RESTART_TIMEPATTERN);
			machineCoreDTO.setConfigurationmanager_statusreportertimepattern(APP_CREDENTIALS.MACHINE_CORE_STATUSREPORTER_RESTART_TIMEPATTERN);
			machineCoreDTO.setMachinecore_configurationmanagerrestarttimepattern(APP_CREDENTIALS.MACHINE_CORE_CONFIGURATIONMANGER_RESTART_TIMEPATTERN);
			machineCoreDTO.setDefaultcampaignid(APP_CREDENTIALS.MACHINE_CORE_DEFAULT_CAMPAIGNID);
			machineCoreDTO.setMachineid(machineId);
			machineCoreDTO.setRecordid(APP_CREDENTIALS.MACHINE_CORE_RECORDID);
			machineCoreDTO.setUpdatestatus(String.valueOf(0));
			java.util.Date today = new java.util.Date();
		    String timeStamp = new java.sql.Timestamp(today.getTime()).toString().trim();
			machineCoreDTO.setTimestamp(timeStamp);
			MainActivity.saveMachineCoreData(machineCoreDTO);
			startConfigurationUpdater();
		}else
		{
			new AlertDialogManager().showAlertDialog(getActivity(), "No Entry Detected", "Please enter machine id", true);
		}
		
	}

	@Override
	public void onStart() {
		super.onStart();
	
	
	}
	
	
	public void startConfigurationUpdater()
	{
		tbMachineIdEntry.setVisibility(View.GONE);
		lbUpdateConfig.setVisibility(View.VISIBLE);
		
		
		InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(getActivity());
		if(!internetConnectionDetector.isConnectingToInternet())
		{
			new AlertDialogManager().showAlertDialog(getActivity(), "Intenet Connection Failed", "Please Ensure Internet Connectivity", false);
			progressBarConfigManager.setVisibility(View.GONE);
		}else
		{
			ConfigurationUpdater configUpdater = new ConfigurationUpdater();
			configUpdater.execute();
		}
	}
	
	
	
	private class ConfigurationUpdater extends AsyncTask<String, String, String> {
	       
		 @Override
		    protected void onPreExecute() {

		 }

		@Override
       protected String doInBackground(String... args) 
		{
					
            String status = ConfigurationManagerController.isConfigurationNeedsToBeUpdated();  
            
			return status;
       }	
        
		@Override
       protected void onPostExecute(String status) {
			
			if(!status.equals(ConfigurationManagerController.STATUS_CONFIGMANAGER_APP_IS_UPTODATE))
			{
				
				readAllEssentialDataFromWebAPI();
				
			}
		}
   }



	public void readAllEssentialDataFromWebAPI() {
		
		String buildedQueryUrl="http://www.piquor.com/app/";
		logInfo("ExecutingUrl Url:"+buildedQueryUrl);
		
		
		HttpUrlResponse response=HttpUrlExecute.executeQuery(buildedQueryUrl);
		//step 1 
		increasetProgressBar(20);
		logInfo("UrlResponse Recived  Executed URL:"+buildedQueryUrl+":Response Recieved:"+response.getQueryUrlLog()+" Response from API"+response.getQueryUrlResponse()+"@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		
		if(isResponseValid(response.getQueryUrlResponse()))
		{
			boolean value = completeUserEntryOperation();
			logInfo("Log info value:"+value);
			
			//setup 2
			hitCompletedCurrentUpdateOnWeb();
			
			//steup 3
			boolean resi = readAndPopulateAllInformationOnDatabase();
			if(resi)
			{
				//setup4
				boolean res = downloadEssentialDataInInternalStorage();
				if(res)
				{
					moveToNextScreen();
					
				}else
				{
					setUrlExectionErrorOnMachineCore();
				}
			}else
			{
				setUrlExectionErrorOnMachineCore();
			}
			
		}
		else
		{
			logInfo("Url Execution is completely wrong waiting for getting results :"+PiquorTimestamp.getTimeStamp());
			setUrlExectionErrorOnMachineCore();
			
		}
		
	}
	
	private void moveToNextScreen() {
		
		fragmentChangeListener.navigateToHomeScreen();
	}

	private void increasetProgressBar(int i) {
		
		if(progressBarConfigManager.getProgress() < 100)
		{
			int oldProgress = progressBarConfigManager.getProgress();
			progressBarConfigManager.setProgress(oldProgress+i);
		}
		
	}

	private boolean downloadEssentialDataInInternalStorage() {
		
		return true;
	}

	private boolean readAndPopulateAllInformationOnDatabase() {
		
		return true;
	}

	private void hitCompletedCurrentUpdateOnWeb() {
		
		String buildedQueryUrl="http://www.piquor.com/app/";
		logInfo("ExecutingUrl Url:"+buildedQueryUrl);
		HttpUrlResponse response=HttpUrlExecute.executeQuery(buildedQueryUrl);
		logInfo("UrlResponse Recived  Executed URL:"+buildedQueryUrl+":Response Recieved:"+response.getQueryUrlLog()+" Response from API"+response.getQueryUrlResponse()+"@executeQuery:UrlBuilderUtility"+PiquorTimestamp.getTimeStamp());
		
	}

	private void setUrlExectionErrorOnMachineCore() {
		
		progressBarConfigManager.setVisibility(View.GONE);
		MainActivity.setUpdateErrorOnMachineCore();
		
	}

	private boolean completeUserEntryOperation() {
		
		return MainActivity.setUpdateCompleteOnMachineCore();
	}

	private boolean isResponseValid(String queryUrlResponse) {
		
		
		return true;
	}

	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
}
