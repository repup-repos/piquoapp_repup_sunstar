package com.piquor.datautility;

import java.util.ArrayList;

import android.util.Log;

import com.piquor.dao.table.querybuilder.ReviewEntryDAO;
import com.piquor.datastaticfields.ErrorFlags;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.errorhandler.ReviewDataErrorHandler;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.PiquorTimestamp;
import com.services.launcher.ServicesLauncher;
public class ReviewUrlEntryErrorData implements ErrorFlags {

	ReviewDataErrorHandler errorHandler;
	private String LOG_TAG = "ReviewUrlEntryErrorData";
	
	public ReviewUrlEntryErrorData()
	{
		errorHandler=new ReviewDataErrorHandler();
	}
	
	boolean pendingData=false;
	
	public boolean isDataPending()
	{
		
		int emailErrorEntries = MainActivity.getDBEntryCount(ReviewEntryDAO.getDbCountForUrlEntryQuery());
		ServicesLauncher.showMessage("Pending photos:"+emailErrorEntries);
		if(emailErrorEntries>0)
		{
			
			pendingData=true;
			return true;
			
		}else
		{
			pendingData=false;
			return false;
		}
		
	}
	
	public boolean processPendingUserData()
	{
		logInfo("Fetching User Url Error Entry Data from database @proccessPendingUserData:UserUrlEntryErrorData time:"+PiquorTimestamp.getTimeStamp());
		
		ArrayList<ReviewEntryDTO> uploadEntries = MainActivity
				.getUnsendedReviewMailList(URL_ENTRY_ERROR_FLAG);
		
		logInfo("Pending User Url Entry Error Data Fetched from database Started Processing totoal entries:"+uploadEntries.size()+" @processPendingUserData:UserUrlEntryErrorData" );
		
		errorHandler.handleUrlEntryError(uploadEntries);
		
		return false;
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
