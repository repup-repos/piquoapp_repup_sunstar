package com.services.deliverymode.urlbuilder;

import android.util.Log;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.utility.PiquorTimestamp;
import com.services.deliverymode.staticfields.DeliveryModeUrls;
import com.services.deliverymode.utility.ReviewUrlBuilderUtility;

public class ReviewUrlBuilder extends ReviewUrlBuilderUtility implements DeliveryModeUrls {

	private String LOG_TAG = "ReviewUrlBuilder";
	private String reviewEntryUrl;
	
	public ReviewUrlBuilder(ReviewEntryDTO reviewEntryDTO)
	{
		super(reviewEntryDTO);
	}
	
	@Override
	public boolean executeQueryString() {
		
		
		reviewEntryUrl = REVIEW_URL_PREFIX+buildReviewQuery();
		logInfo("Executing Review Entry URL:"+reviewEntryUrl+":For Email:"+reviewParamsDTO.getReviewentry_emailid()+"Time:"+PiquorTimestamp.getTimeStamp());
		
		return executeQuery(reviewEntryUrl);
	}
	
	private String buildReviewQuery() {
		
		String queryParameter = "campaignId=" + reviewParamsDTO.getReviewentry_campaignId()+ "&reviewDescription=" + reviewParamsDTO.getReviewentry_desc()
				+ "&reviewTitle=" + reviewParamsDTO.getReviewentry_title() + "&overallRating="
				+ reviewParamsDTO.getReviewentry_overallrating() + "&emailId=" + reviewParamsDTO.getReviewentry_emailid()
				+ "&platformId=" + reviewParamsDTO.getReviewentry_platformid() + "&status="
				+ reviewParamsDTO.getReviewentry_status() 
				+ "&reviewDate="+ reviewParamsDTO.getReviewentry_date() + "&timestamp="+ reviewParamsDTO.getReviewentry_timestamp()
				+ "&reviewSortOfVisit="+ reviewParamsDTO.getReviewentry_sortofvisit() + "&reviewWereYouHereFor="+ reviewParamsDTO.getReviewentry_wereyouherefor()
				+ "&reviewOptionalEntries="+ reviewParamsDTO.getReviewentry_optionaldesc();
		return queryParameter;
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
