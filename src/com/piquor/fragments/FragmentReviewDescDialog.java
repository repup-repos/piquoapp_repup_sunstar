package com.piquor.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.piquor.dto.ReviewEntryDTO;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;

public class FragmentReviewDescDialog extends DialogFragment {

	private ReviewEntryDTO reviewEntryDTO;
	private String templateHeading = "Still 100 characters remaining in your review desc to able to post on Tripadvisor";
	private String templateHeadingCompleted = "Your review is ready to post on TripAdvisor. Thanks for taking efforts!";
	private int minimumCharacterLimit = 200;
	private TextView txtCharacterCountShow;
	private FragementChangeListener fragmentChangeListener;
	private String DESCRIPTION_ERROR = "Enter Description";
	private EditText reviewDesc;
	String textString = "";
	private String LOG_TAG = "FragmentReviewDescDialog";
	private String pauseString = "";
	private String OPERATIONAL_FLAG_CANCEL = "CANCEL";
	private String OPERATIONAL_FLAG_POST = "POST";
	private String OPERATIONAL_FLAG_CLOSE = "CLOSE";
	private String OPERATIONAL_FLAG_SELECTED = "";

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle arg0) {
		super.onActivityCreated(arg0);

		fragmentChangeListener = (MainActivity) getActivity();

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
				getActivity());
		LayoutInflater inflator = getActivity().getLayoutInflater();
		View view = inflator.inflate(R.layout.fragmentreview_descdialog, null);
		alertBuilder.setView(view);
		setCancelable(false);
		alertBuilder.setCancelable(false);

		txtView = (TextView) view.findViewById(R.id.textViewReviewDescDialog);
		if (reviewEntryDTO.getReviewentry_desc().length() == 200) {
			txtView.setText(templateHeading);
		} else {
			charLeft = 200 - reviewEntryDTO.getReviewentry_desc().length();
			txtView.setText("Still " + charLeft
					+ " characters left in your review to post on Tripadvisor");
		}

		txtCharacterCountShow = (TextView) view
				.findViewById(R.id.textViewCharacterCount);
		txtCharacterCountShow.setText("Minimum Character left " + charLeft);

		reviewDesc = (EditText) view.findViewById(R.id.editTextReviewDesc);
		reviewDesc.setText(reviewEntryDTO.getReviewentry_desc());
		reviewDesc.setSelection(reviewEntryDTO.getReviewentry_desc().length());
		reviewDesc.addTextChangedListener(mTextEditorWatcher);

		reviewDesc.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				if (reviewDesc.getText().toString().length() > 0) {
					if (reviewDesc.getText().toString().length() < 200) {
						reviewDesc.setError(DESCRIPTION_ERROR);
					}
				}
			}
		});
		ImageButton neutralButton = (ImageButton) view
				.findViewById(R.id.imButtonCancelReview);
		neutralButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				OPERATIONAL_FLAG_SELECTED = OPERATIONAL_FLAG_POST;
				cancelReviewProcess();
			}
		});
		ImageButton postiveButton = (ImageButton) view
				.findViewById(R.id.imButtonPostOnTripAdvisor);
		postiveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				OPERATIONAL_FLAG_SELECTED = OPERATIONAL_FLAG_POST;
				goToTripAdvisorPublishScreen();
			}
		});
		ImageButton closeButton = (ImageButton) view
				.findViewById(R.id.imButtonCloseReview);
		closeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				OPERATIONAL_FLAG_SELECTED = OPERATIONAL_FLAG_CLOSE;
				closeReviewDialog();
			}
		});

		Dialog dialog = alertBuilder.create();
		return dialog;
	}

	protected void closeReviewDialog() {

		// reviewDesc.setText("");
		fragmentChangeListener.setTemporaryOnDescDialogText("");
		dismiss();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		if (OPERATIONAL_FLAG_SELECTED == "") {
			pauseString = reviewDesc.getText().toString().trim();
			Log.d(LOG_TAG, "Call onpause on fragment: " + pauseString);
			if (fragmentChangeListener != null) {
				fragmentChangeListener
						.setTemporaryOnDescDialogText(pauseString);
			}
		}
	}

	private final TextWatcher mTextEditorWatcher = new TextWatcher() {
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

			int leftCharacters = minimumCharacterLimit - s.toString().replaceAll("( )+", " ").length();
			//Log.d(LOG_TAG, "Input Character : ="+s.toString().replaceAll("( )+", " ")+":");
			// Log.d(LOG_TAG,
			// "Current text count size: "+leftCharacters+" character strength: "+s.length()+" minimum character length: "+minimumCharacterLimit);
			
			
			if(leftCharacters==0)
			{
				int tempCharactersCount = minimumCharacterLimit - s.toString().replaceAll("( )+", " ").trim().length();
				Log.d(LOG_TAG, "Temp character count: "+tempCharactersCount);
				if(tempCharactersCount==0)
				{
					leftCharacters = 0;
				}else
				{
					leftCharacters = tempCharactersCount;
				}
			}
			
			if (leftCharacters < 0) {
				
				
					leftCharacters = 0;
					minimumCharacterLimit = 200;
					txtView.setText(templateHeadingCompleted);
				
			}
			txtCharacterCountShow.setText("Minmum Character Left:"
					+ leftCharacters);
		}

		public void afterTextChanged(Editable s) {

			for (int i = s.length(); i > 0; i--) {

				if (s.subSequence(i - 1, i).toString().equals("\n"))
					s.replace(i - 1, i, "");

			}

			String myTextString = s.toString();
			// reviewDesc.setText(myTextString);
		}
	};
	private TextView txtView;
	private int charLeft = 200;

	@Override
	public void onResume() {

		super.onResume();

		if (fragmentChangeListener != null) {
			pauseString = fragmentChangeListener.getTemporaryOnDescDialogText();
		}
		Log.d(LOG_TAG, "Called at sleep on: review description set on pause:"
				+ pauseString);
		if (reviewEntryDTO.getReviewentry_desc().length() >= 200) {
			txtView.setText(templateHeadingCompleted);
			minimumCharacterLimit = 200;
			txtCharacterCountShow.setText("Minimum Character left " + 0);

		} else {
			int charLeft = 200 - reviewEntryDTO.getReviewentry_desc().length();
			txtView.setText("Still " + charLeft
					+ " characters left in your review to post on Tripadvisor");
		}

		if (pauseString != "") {
			reviewDesc.setText(pauseString);
		}
	}

	protected void goToTripAdvisorPublishScreen() {

		String reviewDescription = reviewDesc.getText().toString().trim();
		Log.d(LOG_TAG, "Go to publish screeen");
		if (reviewDescription.length() >= 200) {
			reviewEntryDTO.setReviewentry_desc(reviewDescription);
			Log.d(LOG_TAG, "Navigating to post screen");
			dismiss();
			// reviewDesc.setText("");
			fragmentChangeListener.setTemporaryOnDescDialogText("");
			fragmentChangeListener
					.navigateTotripAdvisorPostViewScreen(reviewEntryDTO);
		} else {
			reviewDesc.setError(DESCRIPTION_ERROR);
		}
	}

	public void setReviewEntryDTO(ReviewEntryDTO reviewEntryDTO) {

		this.reviewEntryDTO = reviewEntryDTO;
	}

	protected void cancelReviewProcess() {

		dismiss();
//		reviewDesc.setText("");
		fragmentChangeListener.setTemporaryOnDescDialogText("");
		fragmentChangeListener.navigateToReviewMediumSelection();
	}

}
