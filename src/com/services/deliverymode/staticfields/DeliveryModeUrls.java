package com.services.deliverymode.staticfields;

public interface DeliveryModeUrls {

	public final String WEB_URL_PREFIX="http://54.204.22.53:8080/";
	public final String EMAIL_URL_PRETFIX=WEB_URL_PREFIX+"PCRM/mailServiceController_new.pcrm?";
	public final String SMS_URL_PREIFIX=WEB_URL_PREFIX+"PCRM/smsServiceController.pcrm?";
	public final String MMS_URL_PREFIX=WEB_URL_PREFIX+"PCRM/mmsServiceController.pcrm?";
	public final String UNIVERSAL_URL_PREFIX=WEB_URL_PREFIX+"PCRM/universalDeliveryModeController.pcrm?";
	public final String TWITTER_URL_PREFIX=WEB_URL_PREFIX+"PCRM/twitterServiceController.pcrm?";
	public final String FACEBOOK_URL_PREFIX=WEB_URL_PREFIX+"PCRM/facebookServiceController.pcrm?";
	public final String WHATSAPP_URL_PREFIX=WEB_URL_PREFIX+"PCRM/whatsAppServiceController.pcrm?";
	public final String SELFI_URL_PREFIX=WEB_URL_PREFIX+"PCRM/selfieMailServiceController.pcrm?";
	public final String REVIEW_WEB_URL_PREFIX = "http://52.4.240.156:8080/";
	public final String REVIEW_URL_PREFIX = REVIEW_WEB_URL_PREFIX+"/RepUpEngine/saveRankUpReview.repup?";
	
}
