package com.piquor.app.updater.dbupdates;

public interface AdaptiveQuestionJsonCons {
	
	public final String JSON_ADAPTIVE_CAMPAIGN_ID = "campignId";
	public final String JSON_ADAPTIVE_QUESTION = "question";
	public final String JSON_ADAPTIVE_QUESTION_ID = "questionId";
	public final String JSON_ADAPTIVE_QUESTION_STATE = "state";
	
	
}
