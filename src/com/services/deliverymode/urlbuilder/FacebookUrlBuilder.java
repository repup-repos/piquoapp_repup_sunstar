package com.services.deliverymode.urlbuilder;

import com.piquor.dto.UserTableDTO;
import com.services.deliverymode.staticfields.DeliveryModeUrls;
import com.services.deliverymode.utility.UrlBuilderUtility;

public class FacebookUrlBuilder extends UrlBuilderUtility implements DeliveryModeUrls {

	public FacebookUrlBuilder(UserTableDTO userDTO) {
		super(userDTO);
	}
	
	String facebookEntryUrl;
	private String LOG_TAG = "FacebookUrlBuilder";

	@Override
	public boolean executeQueryString(String machineId) {

		/*if(SystemConfigDTO.getFacebookMode().trim().equals("photo"))
		{
			
			System.out.println("Detected Facebook Entry:");
			//System.out.println(facebookEntryUrl);
			LoggingTracker.GetRootLogger.info("Executing Facebook Entry URL:"+facebookEntryUrl+":For PicId:"+userDTO.getPicId()+":@executeQueryString:FacebookUrlBuilder"+PiquorTimestamp.getTimeStamp());
			//return true;
			return completeUserEntryOperation();
		}else
		{
			facebookEntryUrl=FACEBOOK_URL_PREFIX+buildFacebookQuery(machineId);		
			LoggingTracker.GetRootLogger.info("Executing Facebook Entry URL:"+facebookEntryUrl+":For PicId:"+userDTO.getPicId()+":Time:"+PiquorTimestamp.getTimeStamp());
			return executeQuery(facebookEntryUrl);	
		}*/
		facebookEntryUrl=FACEBOOK_URL_PREFIX+buildFacebookQuery(machineId);		
		logInfo("Executing Facebook Entry URL:"+facebookEntryUrl+":For PicId:"+userDTO.getPicId());
		return executeQuery(facebookEntryUrl);	
		
	}
	
	public String buildFacebookQuery(String machineId)
	{
		/*String queryParameter ="toc=" + userParamsDTO.getPhotoCaptureDate()
				+ "&campaignId=" + userParamsDTO.getUserCampaignId()
				+ "&picId=" + userParamsDTO.getPhotoPicId() + "&machineId="
				+ machineId+"&flag=1";*/
		
		String queryParameter = "fbUserId="+userParamsDTO.getUserFbUserId()+
				"&fbName="+userParamsDTO.getUserFbName()+
				"&fbPicId="+userParamsDTO.getPhotoPicId()+
				"&fbAId=na&friends=0&picId="+userParamsDTO.getPhotoPicId()+
				"&campaignId="+userParamsDTO.getUserCampaignId()+
				"&machineId="+machineId+
				"&captureDate="+userParamsDTO.getPhotoCaptureDate()+
				"&emailId="+userParamsDTO.getUserEmail()+
				"&gender="+userParamsDTO.getUserGender()+
				"&fbLink=na&phone="+userParamsDTO.getUserPhone()+"&policyAgreement=0";
		
		return queryParameter;
	}
}
