package com.piquor.datautility;

import java.util.ArrayList;

import android.util.Log;

import com.piquor.datastaticfields.ErrorFlags;
import com.piquor.dto.UserTableDTO;
import com.piquor.errorhandler.DataErrorHandler;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.PiquorTimestamp;
import com.services.launcher.ServicesLauncher;
public class UserUrlEntryErrorData implements ErrorFlags {

	DataErrorHandler errorHandler;
	private String LOG_TAG = "UserUrlEntryErrorData";
	public UserUrlEntryErrorData()
	{
		errorHandler=new DataErrorHandler();
	}
	
	boolean pendingData=false;
	
	public boolean isDataPending()
	{
		String dbquery = "SELECT count(*) FROM user WHERE status='2'";
		
		int emailErrorEntries = MainActivity.getDBEntryCount(dbquery);
		ServicesLauncher.showMessage("Pending photos:"+emailErrorEntries);
		if(emailErrorEntries>0)
		{
			
			pendingData=true;
			return true;
			
		}else
		{
			pendingData=false;
			return false;
		}
		
	}
	
	public boolean processPendingUserData(String machineId)
	{
		logInfo("Fetching User Url Error Entry Data from database @proccessPendingUserData:UserUrlEntryErrorData time:"+PiquorTimestamp.getTimeStamp());
		
		ArrayList<UserTableDTO> uploadEntries = MainActivity
				.getUnsendedMailList(URL_ENTRY_ERROR_FLAG);
		
		logInfo("Pending User Url Entry Error Data Fetched from database Started Processing totoal entries:"+uploadEntries.size()+" @processPendingUserData:UserUrlEntryErrorData" );
		
		errorHandler.handleUrlEntryError(uploadEntries,machineId);
		
		return false;
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
