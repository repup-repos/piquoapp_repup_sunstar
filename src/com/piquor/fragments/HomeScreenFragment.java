package com.piquor.fragments;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.R;

public class HomeScreenFragment extends Fragment implements FRAGMENT_SCREEN_NAMES {
	
	private Button btnCaptureSelfie;
	private Button btnWriteReviews;
	private String LOG_TAG = "HomeScreenFragment";
	private FragementChangeListener fragmentChangeListener;
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
	
	}
	
	private void setMobileDataEnabled(Context context, boolean enabled)
			throws Exception {

		Log.d(LOG_TAG, "SDK Version: " + Build.VERSION.SDK_INT);
		if (Build.VERSION.SDK_INT > 19) {
			
			setMobileDataEnabledFromLollipop(getActivity(), enabled);
			Log.v(LOG_TAG, "Lollipop enabling data on mobile");
		} else {
			
			Log.v(LOG_TAG, "Kitkat enabling data on mobile");
			final ConnectivityManager conman = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class
					.forName(conman.getClass().getName());
			final Field iConnectivityManagerField = conmanClass
					.getDeclaredField("mService");
			iConnectivityManagerField.setAccessible(true);
			final Object iConnectivityManager = iConnectivityManagerField
					.get(conman);
			final Class iConnectivityManagerClass = Class
					.forName(iConnectivityManager.getClass().getName());
			final Method setMobileDataEnabledMethod = iConnectivityManagerClass
					.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);

			setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		fragmentChangeListener = (FragementChangeListener) getActivity();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)
	{
		
		View view = inflator.inflate(R.layout.fragmenthomescreen, parent,false);
		
		/*btnCaptureSelfie = (Button) view.findViewById(R.id.btnCaptureSelfie);
		btnCaptureSelfie.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				try {
			//		setMobileDataEnabled(getActivity(), true);
				} catch (Exception e) {
					
					Log.d(LOG_TAG, "Exception x:"+e.getMessage());
					
				}
				openCaptureFragments();
			}
		});*/
		
		btnWriteReviews = (Button) view.findViewById(R.id.btnWriteReviews);
		btnWriteReviews.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				try {
					setMobileDataEnabled(getActivity(), true);
				} catch (Exception e) {
					
					Log.d(LOG_TAG, "Exception x:"+e.getStackTrace().toString());
					
				}
				openReviewFragments();
			}
		});
		return view;
	}
	
	protected void openReviewFragments() {
		
		fragmentChangeListener.navigateToReviewMediumSelection();
	}

	private final static String COMMAND_L_ON = "svc data enable\n ";
	private final static String COMMAND_L_OFF = "svc data disable\n ";
	private final static String COMMAND_SU = "su";
	
	private void setMobileDataEnabledFromLollipop(Context context,boolean enable)
	{
		 String command;
		    if(enable)
		        command = COMMAND_L_ON;
		    else
		        command = COMMAND_L_OFF;        

		    try{
		        Process su = Runtime.getRuntime().exec(COMMAND_SU);
		        DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

		        outputStream.writeBytes(command);
		        outputStream.flush();

		        outputStream.writeBytes("exit\n");
		        outputStream.flush();
		        try {
		            su.waitFor();
		        } catch (InterruptedException e) {
		            e.printStackTrace();
		        }

		        outputStream.close();
		    }catch(IOException e){
		        e.printStackTrace();
		    }
	}

	protected void openCaptureFragments() {
		
		Log.d(LOG_TAG,"Opening Capture Fragments");
		fragmentChangeListener.navigateToCaptureScreen();
		
	}
}
