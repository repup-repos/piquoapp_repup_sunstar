package com.piquor.dto;

public class CampaignInfoDTO {
	
	private String campaignId;
	private String machineId;
	private String mailerResponseTime;
	
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getMailerResponseTime() {
		return mailerResponseTime;
	}
	public void setMailerResponseTime(String mailerResponseTime) {
		this.mailerResponseTime = mailerResponseTime;
	}
	@Override
	public String toString() {
		return "CampaignInfoDTO [campaignId=" + campaignId + ", machineId="
				+ machineId + ", mailerResponseTime=" + mailerResponseTime
				+ "]";
	}
	
}
