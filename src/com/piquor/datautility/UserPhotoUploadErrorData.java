package com.piquor.datautility;

import java.util.ArrayList;

import android.util.Log;

import com.piquor.datastaticfields.ErrorFlags;
import com.piquor.dto.UserTableDTO;
import com.piquor.errorhandler.DataErrorHandler;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.PiquorTimestamp;
import com.services.launcher.ServicesLauncher;
public class UserPhotoUploadErrorData implements ErrorFlags {
	
	DataErrorHandler errorHandler;
	private String LOG_TAG = "UserPhotoUploadErrorData";
	
	public UserPhotoUploadErrorData()
	{
		errorHandler=new DataErrorHandler();
	}
	
	boolean pendingData;
	
	public boolean isDataPending()
	{
		int pictureUploadFailedEntries = MainActivity
				.getDBEntryCount("SELECT count(*) FROM user WHERE status='1'");
		
		
		ServicesLauncher.showMessage("Pending photos:"+pictureUploadFailedEntries);
		if(pictureUploadFailedEntries>0)
		{
			
			pendingData=true;
			return true;
			
		}else
		{
			pendingData=false;
			return false;
		}
	}
	
	public boolean processPendingUserData(String machineId)
	{
		if(pendingData)
		{
			logInfo("Fetching Pending Photo Error Entry Data from database @proccessPendingUserData:UserPhotoUploadErrorData time:"+PiquorTimestamp.getTimeStamp());
			ArrayList<UserTableDTO> uploadEntries = MainActivity
				.getUnsendedMailList(PICTURE_UPLOAD_EFFOR_FLAG);
			logInfo(" Pending Photo Error Entry  Data Fectched  from database Started Processing totoal entries:"+uploadEntries.size()+" @processPendingUserData:UserPHotoUploadErrorData" );
			errorHandler.handlePictureUploadError(uploadEntries,machineId);
			return true;
		}else
		{
			return false;
		}
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
