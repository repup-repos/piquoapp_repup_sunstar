package com.piquor.datautility;

import java.util.ArrayList;

import android.util.Log;

import com.piquor.dao.table.querybuilder.ReviewEntryDAO;
import com.piquor.datastaticfields.ErrorFlags;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.errorhandler.ReviewDataErrorHandler;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.PiquorTimestamp;
import com.services.launcher.ServicesLauncher;

public class ReviewRemainUrlEntryData implements ErrorFlags {
	
	ReviewDataErrorHandler errorHandler;
	private String LOG_TAG = "ReviewRemainUrlEntryData";
	
	public ReviewRemainUrlEntryData()
	{
		errorHandler=new ReviewDataErrorHandler();
	}
	
	boolean pendingData=false;
	
	public boolean isDataPending()
	{
		int pictureUploadFailedEntries = MainActivity
				.getDBEntryCount(ReviewEntryDAO.getDbCountForRemainingMailSentQuery());
		ServicesLauncher.showMessage("Pending photos:"+pictureUploadFailedEntries);
		if(pictureUploadFailedEntries>0)
		{
			
			pendingData=true;
			return true;
			
		}else
		{
			pendingData=false;
			return false;
		}
	}
	public boolean processPendingUserData()
	{
		if(pendingData)
		{
			logInfo("Fetching User Remaining Entry Data from database @proccessPendingUserData:UserRemaingingEntryData time:"+PiquorTimestamp.getTimeStamp());
			
			
			ArrayList<ReviewEntryDTO> mailAndUploadEntries = MainActivity
					.getUnsendedReviewMailList(USER_REMAIING_DATA_FLAG);
			
			logInfo("Pending User Remaining Entry Data Fetched from database Started Processing totoal entries:"+mailAndUploadEntries.size()+" @processPendingUserData:UserRemaingingEntryData" );
			
			errorHandler.handleRemainingUserEntryData(mailAndUploadEntries);
			
			
		
			return true;
		}else
		{
			return	false;
		}
		
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
}
