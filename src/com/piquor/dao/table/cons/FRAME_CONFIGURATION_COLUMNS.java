package com.piquor.dao.table.cons;

public interface FRAME_CONFIGURATION_COLUMNS {
	
	public final String TABLE_NAME_FRAME_CONFIGURATION = "campaign_frame_configuration";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_CAMPAIGN_ID = "campaignid";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_THUMBNAIL_URL = "thumbnail_url"; 
	public final String COLUMN_NAME_FRAME_CONFIGURATION_BACKGROUND_FRAME_URL = "backgroundFrame_url";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_FOREGROUND_FRAME_URL = "foregroundFrame_url";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_PHOTO_X = "PhotoX";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_PHOTO_Y = "PhotoY";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_SCALE_X = "ScaleX";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_SCALE_Y= "ScaleY";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_ROTATE = "Rotate";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_IMAGEONTOPX = "ImageOnTopX";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_IMAGEONTOPY= "ImageOnTopY";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_FRAME_ID = "FrameId";
	public final String COLUMN_NAME_FRAME_CONFIGURATION_UPDATE_DATE = "UpdateDate";

}
