package com.piquor.app.stats.utility;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.UUID;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.piquor.dao.table.querybuilder.TabletAppStatsDAO;
import com.piquor.dao.table.querybuilder.TabletRegistrationDAO;
import com.piquor.dto.TabletAppRegistrationDTO;
import com.piquor.dto.TabletAppStatusDTO;
import com.piquor.piquortabapp.MainActivity;

public class AppStatsBuilder {

	private String LOG_TAG = "AppStatsBuilder";
	
	private FragmentActivity fragmentAcitivity;
	
	public AppStatsBuilder(FragmentActivity fragmentActivity)
	{
		this.fragmentAcitivity = fragmentActivity;
	}
	
	
	public void initialApplicationStatus()
	{
		
		createFirstTimeAppStatus();
		createAppLauchStats();
	}
	
	private void createAppLauchStats()
	{
		TabletAppStatusDTO tabletAppStatusDTO = new TabletAppStatusDTO();
		
		String device_id = MainActivity.getDeivceIdFromRegistrtionTable();
		tabletAppStatusDTO.setDevice_id(device_id);
		tabletAppStatusDTO.setCampaignid(MainActivity.getCamapignInformation().getCampaignId());
		tabletAppStatusDTO.setLast_app_opened_time(getTimeStamp());
		tabletAppStatusDTO.setTimestamp(getTimeStamp());
		tabletAppStatusDTO.setStatsentry_upload_status("0");
		tabletAppStatusDTO.setStatsentry_uploaded("0");
		
		Log.d(LOG_TAG, "Ceating Applicaton LaunchStats: "+tabletAppStatusDTO.toString());
		
		if(MainActivity.saveApplicatonStatusData(tabletAppStatusDTO))
		{
			Log.d(LOG_TAG, "Application Launch data saved:");
		}else
		{
			Log.d(LOG_TAG, "Application launch status not saved");
		}
		
	}
	
	private void createFirstTimeAppStatus()
	{
		if(MainActivity.getDBEntryCount(TabletRegistrationDAO.getDbCountForHavingEntryQuery())>0)
		{
			//no action
		}else
		{
			TabletAppRegistrationDTO tabletAppRegistrationDTO = new TabletAppRegistrationDTO(); 
			tabletAppRegistrationDTO.setDevice_id(getDeviceId());
			tabletAppRegistrationDTO.setDevice_board(Build.BOARD);
			tabletAppRegistrationDTO.setDevice_brand_name(Build.BRAND);
			tabletAppRegistrationDTO.setDevice_display(Build.DISPLAY);
			tabletAppRegistrationDTO.setDevice_manufeturer(Build.MANUFACTURER);
			tabletAppRegistrationDTO.setDevice_modal(Build.MODEL);
			tabletAppRegistrationDTO.setDevice_productname(Build.PRODUCT);
			tabletAppRegistrationDTO.setDevice_serialno(Build.SERIAL);
			tabletAppRegistrationDTO.setDevice_version(Build.VERSION.RELEASE);
			tabletAppRegistrationDTO.setIs_campaign_asigned("false");
			tabletAppRegistrationDTO.setCampaignid("");
			tabletAppRegistrationDTO.setDefault_campaign(MainActivity.getCamapignInformation().getCampaignId());
			tabletAppRegistrationDTO.setRegistration_entry_upload_status("0");
			tabletAppRegistrationDTO.setRegistration_entry_uploaded("0");
			
			Log.d(LOG_TAG, "Tablet app registration dto: "+tabletAppRegistrationDTO.toString());
			
			if(MainActivity.saveTabletRegistrationData(tabletAppRegistrationDTO))
					{
						Log.d(LOG_TAG, "Registration data succesfully stored");
					}else
					{
						Log.d(LOG_TAG, "Registration data storage unsucessfull");
					}
			
			
		}
	}
	
	private String getDeviceId()
	{
		
		final TelephonyManager tm = (TelephonyManager) fragmentAcitivity.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

	    final String tmDevice, tmSerial, androidId;
	    tmDevice = "" + tm.getDeviceId();
	    tmSerial = "" + tm.getSimSerialNumber();
	    androidId = "" + android.provider.Settings.Secure.getString(fragmentAcitivity.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

	    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
	    String deviceId = deviceUuid.toString();
	    
	    return deviceId;

	}
	
	private String getTimeStamp() {
		Calendar calendar = Calendar.getInstance();
		Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		return currentTimestamp.toString();
	}

	
}
