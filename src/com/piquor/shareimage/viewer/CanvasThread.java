package com.piquor.shareimage.viewer;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class CanvasThread extends Thread {

	private SurfaceHolder _surfaceHolder;
	private PanelSurface _panel;
	private boolean _run = false;
	
	public CanvasThread(SurfaceHolder surfaceHolder, PanelSurface panel)
	{
		_surfaceHolder = surfaceHolder;
		_panel = panel;
	}
	
	public void setRunning(boolean run)
	{
		_run = run;
	}
	
	@Override
	public void run() {
		
		Canvas c = null;
		while(_run)
		{
			try
			{
			c = null;
			c = _surfaceHolder.lockCanvas(null);
			synchronized (_surfaceHolder) {
				
				if(c!=null)
				{
					_panel.draw(c);
				}
			}
			}finally{
				
				if(c!=null)
				{
					_surfaceHolder.unlockCanvasAndPost(c);
				}
			}
		}
	}
	
}
