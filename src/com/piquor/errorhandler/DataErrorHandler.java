package com.piquor.errorhandler;

import java.util.ArrayList;
import java.util.Iterator;

import android.util.Log;

import com.piquor.dto.UserTableDTO;
import com.piquor.imageupload.S3Upload;
import com.piquor.services.UrlExecutorService;

public class DataErrorHandler {
	
	public String Query;
	
	private ErrorUpdater errorUpdater;
	private UrlExecutorService urlExecutorService;
	private S3Upload s3upload;
	private String LOG_TAG = "DATAERRORHANDLER";
	
	public DataErrorHandler()
	{
		
		errorUpdater=new ErrorUpdater();
		s3upload=new S3Upload();
		urlExecutorService=new UrlExecutorService();
		
	}
	
	public boolean handlePictureUploadError(ArrayList<UserTableDTO> userDTO,String machineId)
	{	
		
		Iterator<UserTableDTO> iterator = userDTO.iterator();
		
		while (iterator.hasNext()) {
			
			UserTableDTO dto = iterator.next();
			logInfo("Starting Proccessing Of Record: PicId:"+dto.getPicId()+"@handlePictureUploadError:DataErrorHandler");
			
			boolean uploadResponse=s3upload.uploadImageToS3(dto.getPicId().trim());
			
			if(uploadResponse)
			{
				logInfo("Photo Successfully Uploaded Starting Processing For Email Url Entry PicId:"+dto.getPicId()+"@handlePictureUploadError:DataErrorHandler");
				urlExecutorService.processRemoteEntry(dto, machineId);
								
			}
			logInfo("Proccessing Completed Of Record: PicId:"+dto.getPicId()+"@handlePictureUploadError:DataErrorHandler");
			
		}
		return true;
		
	}
	
	public boolean handleUrlEntryError(ArrayList<UserTableDTO> userDTO,String machineId)
	{

		UserTableDTO dto = null;
		Iterator<UserTableDTO> iterator = userDTO.iterator();
		while (iterator.hasNext()) {
			
			dto = (UserTableDTO) iterator.next();
			logInfo("Starting Proccessing Of Record: PicId:"+dto.getPicId()+"@handleUrlEntryError:DataErrorHandler");
			urlExecutorService.processRemoteEntry(dto, machineId);
			logInfo("Process Completed Of Record: PicId:"+dto.getPicId()+"@handleUrlEntryError:DataErrorHandler");
		}
		return true;
	}
	
	public boolean handleRemainUrlEntry(ArrayList<UserTableDTO> userDTO,String machineId)
	{
		UserTableDTO dto = null;
		Iterator<UserTableDTO> iterator = userDTO.iterator();
		while (iterator.hasNext()) {
			
			dto = (UserTableDTO) iterator.next();	
			logInfo("Starting Proccessing Of Record: PicId:"+dto.getPicId()+"@handleRemainingUrlEntry:DataErrorHandler");
			urlExecutorService.processRemoteEntry(dto, machineId);
			logInfo("Process Completed Of Record: PicId:"+dto.getPicId()+"@handleRemianingUrlEntry:DataErrorHandler");
		}
		return true;
		
	}
	
	public boolean handleRemainingUserEntryData(ArrayList<UserTableDTO> userDTO,String machineId)
	{
		Iterator<UserTableDTO> iterator = userDTO.iterator();
		
		while (iterator.hasNext()) {
			
			UserTableDTO dto = iterator.next();
			logInfo("Starting Proccessing Of Record : PicId:"+dto.getPicId()+"@handleRemainingUserEntryData:DataErrorHandler");
			
			boolean uploadResponse=s3upload.uploadImageToS3(dto.getPicId().trim());
			
			if(uploadResponse)
			{
				logInfo("Photo Successfully Uploaded Starting Processing For Email Url Entry PicId:"+dto.getPicId()+"@handleRemainingUserEntryData:DataErrorHandler");
				urlExecutorService.processRemoteEntry(dto, machineId);
				logInfo("Proccessing Completed Of Record: PicId:"+dto.getPicId()+"@handleRemainingUserEntryData:DataErrorHandler");
			}
			
		}
		return true;	
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
}
