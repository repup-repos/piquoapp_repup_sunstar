package com.piquor.dto;

public class DataInfoDTO {
	
	private long totalEntries;
	private long totalEmailEntries;
	private long totalFacebookEntries;
	private long totalPrintEntires;
	private long totalTwitterEntries;
	private long totalUploadedEntries;
	
	public DataInfoDTO()
	{
		totalEmailEntries = 0;
		totalFacebookEntries = 0;
		totalPrintEntires = 0;
		totalTwitterEntries = 0;
		totalEntries = 0;
		setTotalUploadedEntries(0);
	}
	
	public long getTotalEntries() {
		return totalEntries;
	}
	public void setTotalEntries(long totalEntries) {
		this.totalEntries = totalEntries;
	}
	public long getTotalEmailEntries() {
		return totalEmailEntries;
	}
	public void setTotalEmailEntries(long totalEmailEntries) {
		this.totalEmailEntries = totalEmailEntries;
	}
	public long getTotalFacebookEntries() {
		return totalFacebookEntries;
	}
	public void setTotalFacebookEntries(long totalFacebookEntries) {
		this.totalFacebookEntries = totalFacebookEntries;
	}
	public long getTotalPrintEntires() {
		return totalPrintEntires;
	}
	public void setTotalPrintEntires(long totalPrintEntires) {
		this.totalPrintEntires = totalPrintEntires;
	}
	public long getTotalTwitterEntries() {
		return totalTwitterEntries;
	}
	public void setTotalTwitterEntries(long totalTwitterEntries) {
		this.totalTwitterEntries = totalTwitterEntries;
	}
	

	public long getTotalUploadedEntries() {
		return totalUploadedEntries;
	}

	public void setTotalUploadedEntries(long totalUploadedEntries) {
		this.totalUploadedEntries = totalUploadedEntries;
	}

	@Override
	public String toString() {
		return "DataInfoDTO [totalEntries=" + totalEntries
				+ ", totalEmailEntries=" + totalEmailEntries
				+ ", totalFacebookEntries=" + totalFacebookEntries
				+ ", totalPrintEntires=" + totalPrintEntires
				+ ", totalTwitterEntries=" + totalTwitterEntries
				+ ", totalUploadedEntries=" + totalUploadedEntries + "]";
	}
	
}
