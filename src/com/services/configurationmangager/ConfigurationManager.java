package com.services.configurationmangager;

import android.content.SharedPreferences;

import com.piquor.app.cons.PREFERED_CONSTANTS;
import com.piquor.piquortabapp.MainActivity;

/*
 * This class will load all Configurations, Event Data
 * and Frames for the event automatically from a server
 * The client shall enter his EventKey provided to him
 * and everything will be automated from that point
 */

public class ConfigurationManager implements PREFERED_CONSTANTS {
	private static MainActivity parentActivity;
	private static SharedPreferences sharedPreferences; 
	
	
	public static void setMainActivity(MainActivity mainActivity) {
		parentActivity = mainActivity;
	}
	
	public static void loadConfigManager() {
		// Check if First App Launch
		
		
		
		if (isFirstAppLaunch() == true) {
			//Create new DB
		}
		else {
			//Read information from DB
		}	
	}
	
	private static boolean isFirstAppLaunch() {
		sharedPreferences = parentActivity.getPreferences(0);
		return sharedPreferences.getBoolean(CONFIGURATION_IS_FIRST_APP_LAUNCH, false);
	}
}
