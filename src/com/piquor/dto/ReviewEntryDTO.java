package com.piquor.dto;

public class ReviewEntryDTO {
	

	private  String reviewentry_id = "id";
	private  String reviewentry_type = "na";
	private  String reviewentry_name = "na";
	private  String reviewentry_roomno = "na";
	private  String reviewentry_date = "na";
	private  String reviewentry_overallrating = "na";
	private  String reviewentry_wereyouherefor = "na";
	private  String reviewentry_sortofvisit = "na";
	private  String reviewentry_title = "na";
	private  String reviewentry_desc = "";
	private  String reviewentry_optionaldesc = "na";
	private  String reviewentry_emailid = "na";
	private  String reviewentry_platformid = "na";
	private  String reviewentry_status = "0";
	private  String reviewentry_timestamp = "na";
	private  String reviewentry_campaignId = "na";
	private  String reviewentry_reviewUploaded = "0";
	private  String reviewentry_reviewuploadStatus = "1";
	private  String reviewentry_ratingCheckIn = "na";
	private  String reviewentry_ratingRooms = "na";
	private  String reviewentry_ratingClieanliness = "na";
	private  String reviewentry_ratingAmneties = "na";
	private  String reviewentry_ratingFoodandBeverages = "na";
	private  String reviewentry_ratingWifiAndInternet = "na";
	private  String reviewentry_ratingCheckout = "na";
	private  String reviewentry_ratingconceirge = "na";
	
	
	public String getReviewentry_ratingCheckIn() {
		return reviewentry_ratingCheckIn;
	}
	public void setReviewentry_ratingCheckIn(String reviewentry_ratingCheckIn) {
		this.reviewentry_ratingCheckIn = reviewentry_ratingCheckIn;
	}
	public String getReviewentry_ratingRooms() {
		return reviewentry_ratingRooms;
	}
	public void setReviewentry_ratingRooms(String reviewentry_ratingRooms) {
		this.reviewentry_ratingRooms = reviewentry_ratingRooms;
	}
	
	public String getReviewentry_ratingAmneties() {
		return reviewentry_ratingAmneties;
	}
	public void setReviewentry_ratingAmneties(String reviewentry_ratingAmneties) {
		this.reviewentry_ratingAmneties = reviewentry_ratingAmneties;
	}
	public String getReviewentry_ratingFoodandBeverages() {
		return reviewentry_ratingFoodandBeverages;
	}
	public void setReviewentry_ratingFoodandBeverages(
			String reviewentry_ratingFoodandBeverages) {
		this.reviewentry_ratingFoodandBeverages = reviewentry_ratingFoodandBeverages;
	}
	public String getReviewentry_ratingWifiAndInternet() {
		return reviewentry_ratingWifiAndInternet;
	}
	public void setReviewentry_ratingWifiAndInternet(
			String reviewentry_ratingWifiAndInternet) {
		this.reviewentry_ratingWifiAndInternet = reviewentry_ratingWifiAndInternet;
	}
	public String getReviewentry_ratingCheckout() {
		return reviewentry_ratingCheckout;
	}
	public void setReviewentry_ratingCheckout(String reviewentry_ratingCheckout) {
		this.reviewentry_ratingCheckout = reviewentry_ratingCheckout;
	}
	public String getReviewentry_ratingconceirge() {
		return reviewentry_ratingconceirge;
	}
	public void setReviewentry_ratingconceirge(String reviewentry_ratingconceirge) {
		this.reviewentry_ratingconceirge = reviewentry_ratingconceirge;
	}
	
	
	
	public String getReviewentry_id() {
		return reviewentry_id;
	}
	public void setReviewentry_id(String reviewentry_id) {
		this.reviewentry_id = reviewentry_id;
	}

	public String getReviewentry_type() {
		return reviewentry_type;
	}
	public void setReviewentry_type(String reviewentry_type) {
		this.reviewentry_type = reviewentry_type;
	}
	public String getReviewentry_date() {
		return reviewentry_date;
	}
	public void setReviewentry_date(String reviewentry_date) {
		this.reviewentry_date = reviewentry_date;
	}
	public String getReviewentry_overallrating() {
		return reviewentry_overallrating;
	}
	public void setReviewentry_overallrating(String reviewentry_overallrating) {
		this.reviewentry_overallrating = reviewentry_overallrating;
	}
	public String getReviewentry_wereyouherefor() {
		return reviewentry_wereyouherefor;
	}
	public void setReviewentry_wereyouherefor(String reviewentry_wereyouherefor) {
		this.reviewentry_wereyouherefor = reviewentry_wereyouherefor;
	}
	public String getReviewentry_sortofvisit() {
		return reviewentry_sortofvisit;
	}
	public void setReviewentry_sortofvisit(String reviewentry_sortofvisit) {
		this.reviewentry_sortofvisit = reviewentry_sortofvisit;
	}
	public String getReviewentry_title() {
		return reviewentry_title;
	}
	public void setReviewentry_title(String reviewentry_title) {
		this.reviewentry_title = reviewentry_title;
	}
	public String getReviewentry_desc() {
		return reviewentry_desc;
	}
	public void setReviewentry_desc(String reviewentry_desc) {
		this.reviewentry_desc = reviewentry_desc;
	}
	public String getReviewentry_optionaldesc() {
		return reviewentry_optionaldesc;
	}
	public void setReviewentry_optionaldesc(String reviewentry_optionaldesc) {
		this.reviewentry_optionaldesc = reviewentry_optionaldesc;
	}
	public String getReviewentry_emailid() {
		return reviewentry_emailid;
	}
	public void setReviewentry_emailid(String reviewentry_emailid) {
		this.reviewentry_emailid = reviewentry_emailid;
	}
	public String getReviewentry_platformid() {
		return reviewentry_platformid;
	}
	public void setReviewentry_platformid(String reviewentry_platformid) {
		this.reviewentry_platformid = reviewentry_platformid;
	}
	public String getReviewentry_status() {
		return reviewentry_status;
	}
	public void setReviewentry_status(String reviewentry_status) {
		this.reviewentry_status = reviewentry_status;
	}
	public String getReviewentry_timestamp() {
		return reviewentry_timestamp;
	}
	public void setReviewentry_timestamp(String reviewentry_timestamp) {
		this.reviewentry_timestamp = reviewentry_timestamp;
	}
	public String getReviewentry_campaignId() {
		return reviewentry_campaignId;
	}
	public void setReviewentry_campaignId(String reviewentry_campaignId) {
		this.reviewentry_campaignId = reviewentry_campaignId;
	}

	public String getReviewentry_reviewUploaded() {
		return reviewentry_reviewUploaded;
	}
	public void setReviewentry_reviewUploaded(String reviewentry_reviewUploaded) {
		this.reviewentry_reviewUploaded = reviewentry_reviewUploaded;
	}
	public String getReviewentry_reviewuploadStatus() {
		return reviewentry_reviewuploadStatus;
	}
	public void setReviewentry_reviewuploadStatus(
			String reviewentry_reviewuploadStatus) {
		this.reviewentry_reviewuploadStatus = reviewentry_reviewuploadStatus;
	}
	@Override
	public String toString() {
		return "ReviewEntryDTO [reviewentry_id=" + reviewentry_id
				+ ", reviewentry_type=" + reviewentry_type
				+ ", reviewentry_date=" + reviewentry_date
				+ ", reviewentry_overallrating=" + reviewentry_overallrating
				+ ", reviewentry_wereyouherefor=" + reviewentry_wereyouherefor
				+ ", reviewentry_sortofvisit=" + reviewentry_sortofvisit
				+ ", reviewentry_title=" + reviewentry_title
				+ ", reviewentry_desc=" + reviewentry_desc
				+ ", reviewentry_optionaldesc=" + reviewentry_optionaldesc
				+ ", reviewentry_emailid=" + reviewentry_emailid
				+ ", reviewentry_platformid=" + reviewentry_platformid
				+ ", reviewentry_status=" + reviewentry_status
				+ ", reviewentry_timestamp=" + reviewentry_timestamp
				+ ", reviewentry_campaignId=" + reviewentry_campaignId
				+ ", reviewentry_reviewUploaded=" + reviewentry_reviewUploaded
				+ ", reviewentry_reviewuploadStatus="
				+ reviewentry_reviewuploadStatus + "]";
	}
	public String getReviewentry_name() {
		return reviewentry_name;
	}
	public void setReviewentry_name(String reviewentry_name) {
		this.reviewentry_name = reviewentry_name;
	}
	public String getReviewentry_roomno() {
		return reviewentry_roomno;
	}
	public void setReviewentry_roomno(String reviewentry_roomno) {
		this.reviewentry_roomno = reviewentry_roomno;
	}
	public String getReviewentry_ratingClieanliness() {
		return reviewentry_ratingClieanliness;
	}
	public void setReviewentry_ratingClieanliness(
			String reviewentry_ratingClieanliness) {
		this.reviewentry_ratingClieanliness = reviewentry_ratingClieanliness;
	}
	
	
}
