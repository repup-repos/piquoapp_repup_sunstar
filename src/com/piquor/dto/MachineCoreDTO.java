package com.piquor.dto;

public class MachineCoreDTO {

	private String recordid;
	private String machineid;
	private String defaultcampaignid;
	private String configurationmanager_statusreportertimepattern;
	private String configurationmanager_servicehandlertimepattern;
	private String machinecore_configurationmanagerrestarttimepattern;
	private String timestamp;
	private String updatestatus;
	
	
	public String getRecordid() {
		return recordid;
	}
	public void setRecordid(String recordid) {
		this.recordid = recordid;
	}
	public String getMachineid() {
		return machineid;
	}
	public void setMachineid(String machineid) {
		this.machineid = machineid;
	}
	public String getDefaultcampaignid() {
		return defaultcampaignid;
	}
	public void setDefaultcampaignid(String defaultcampaignid) {
		this.defaultcampaignid = defaultcampaignid;
	}

	public String getConfigurationmanager_statusreportertimepattern() {
		return configurationmanager_statusreportertimepattern;
	}
	public void setConfigurationmanager_statusreportertimepattern(
			String configurationmanager_statusreportertimepattern) {
		this.configurationmanager_statusreportertimepattern = configurationmanager_statusreportertimepattern;
	}
	public String getConfigurationmanager_servicehandlertimepattern() {
		return configurationmanager_servicehandlertimepattern;
	}
	public void setConfigurationmanager_servicehandlertimepattern(
			String configurationmanager_servicehandlertimepattern) {
		this.configurationmanager_servicehandlertimepattern = configurationmanager_servicehandlertimepattern;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getUpdatestatus() {
		return updatestatus;
	}
	public void setUpdatestatus(String updatestatus) {
		this.updatestatus = updatestatus;
	}
	
	public String getMachinecore_configurationmanagerrestarttimepattern() {
		return machinecore_configurationmanagerrestarttimepattern;
	}
	public void setMachinecore_configurationmanagerrestarttimepattern(
			String machinecore_configurationmanagerrestarttimepattern) {
		this.machinecore_configurationmanagerrestarttimepattern = machinecore_configurationmanagerrestarttimepattern;
	}
	@Override
	public String toString() {
		return "MachineCoreDTO [recordid=" + recordid + ", machineid="
				+ machineid + ", defaultcampaignid=" + defaultcampaignid
				+ ", configurationmanager_statusreportertimepattern="
				+ configurationmanager_statusreportertimepattern
				+ ", configurationmanager_servicehandlertimepattern="
				+ configurationmanager_servicehandlertimepattern
				+ ", machinecore_configurationmanagerrestarttimepattern="
				+ machinecore_configurationmanagerrestarttimepattern
				+ ", timestamp=" + timestamp + ", updatestatus=" + updatestatus
				+ "]";
	}

}
