package com.piquor.constants;

public interface EFFECTS_TAGS {

	public final int LOMO_ID = 3;
	public final int GRAY_ID = 2;
	public final int NEON_ID = 7;
	public final int OLD_ID = 5;
	public final int SKETCH_ID = 4;
	public final int NORMAL_ID = 1;
	public final int GAUTHUM_ID = 6;
	public final int TINT_ID = 8;
	public final int BLUE_TONE = 9;
	public final int RED_TONE = 10;
}
