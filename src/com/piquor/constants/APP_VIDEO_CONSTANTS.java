package com.piquor.constants;

import java.io.File;

public class APP_VIDEO_CONSTANTS {
	
	public static File get_file() {
		return _file;
	}
	public static void set_file(File _file) {
		APP_VIDEO_CONSTANTS._file = _file;
	}
	public static File get_dir() {
		return _dir;
	}
	public static void set_dir(File _dir) {
		APP_VIDEO_CONSTANTS._dir = _dir;
	}
	
	public static String getVideoId() {
		return vId;
	}
	public static void setVideoId(String vId) {
		APP_VIDEO_CONSTANTS.vId = vId;
	}

	private static File _file 			  = null;
	private static File _dir              = null;
	private static String vId           = null;
	
	public static void recycleAllHoldParameters() {
	
		_file = null;
		_dir = null;
		vId = "";
	}
}
