package com.piquor.serviceshandler;

import android.util.Log;

import com.piquor.app.updater.urlbuilder.AdaptiveQuestionUpdateUrlBuilder;
import com.piquor.dto.CampaignInfoDTO;
import com.piquor.piquortabapp.MainActivity;
import com.services.launcher.ServicesLauncher;

public class AppReportServiceHandler {

	private String LOG_TAG = "AppUpdateServiceHandler";
	
	
	public AppReportServiceHandler()
	{
		
	}
	
	public void executeServices()
	{
		if(isInternetConnected())
		{
			logInfo("Fetching Adaptive Question Data");
			CampaignInfoDTO campaignInfoDTO = MainActivity.getCamapignInformation();
			//campaignInfoDTO.setCampaignId("8a6a32a94c975c9b014c9813d0990003");
			boolean response = new AdaptiveQuestionUpdateUrlBuilder(campaignInfoDTO).executeQueryString();
			logInfo("Response Recieved for getting question data:"+response);
		}
	}
	
	
	private boolean isInternetConnected() {
		
		logInfo("Checking Internet Connection isInternetConnected@PiquorSericeHandler");
		return ServicesLauncher.checkInternetConnectivity();
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
