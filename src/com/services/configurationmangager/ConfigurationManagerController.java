package com.services.configurationmangager;

import com.piquor.piquortabapp.MainActivity;

public class ConfigurationManagerController {

	public static String STATUS_CONFIGMANAGER_APP_INTIAL_LOADING = "APP_1ST_LAUNCH";
	public static String STATUS_CONFIGMANAGER_APP_LAST_UPDATE_ERROR = "APP_LAST_UPDATE_ERROR";
	public static String STATUS_CONFIGMANAGER_APP_UPGRADE_PENDING = "APPLICATION_UPGRADE_PENDING";
	public static String STATUS_CONFIGMANAGER_APP_IS_UPTODATE = "APPLICATION_CONFIGURATION_COMPLETE";	
	
	public static String isConfigurationNeedsToBeUpdated()
	{
		if(isAppFirstTimeLoading())
		{
			
			return STATUS_CONFIGMANAGER_APP_INTIAL_LOADING;
		}else if(isAppConfigUpdateErrorSet())
		{
			return STATUS_CONFIGMANAGER_APP_LAST_UPDATE_ERROR;
		}else if(isAppConfigUpdatePending())
		{
			return STATUS_CONFIGMANAGER_APP_UPGRADE_PENDING;
		}else
		{
			return STATUS_CONFIGMANAGER_APP_IS_UPTODATE;
		}
	}

	private static boolean isAppConfigUpdatePending() {
		
		
		return ConfigManagerUpdateChecker.isUpdatesPending();
	}

	private static boolean isAppConfigUpdateErrorSet() {
		
		
		return MainActivity.getCurrentMachineCoreUpdateStatus();
	}

	private static boolean isAppFirstTimeLoading() {

		return ConfigManagerUpdateChecker.isApps1stLoading();
	}
}
