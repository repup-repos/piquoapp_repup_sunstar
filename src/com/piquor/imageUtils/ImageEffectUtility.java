package com.piquor.imageUtils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;

public class ImageEffectUtility {

	private static String LOG_TAG = "ImageEffectUtility";
    
	public static Bitmap scaleImageWithAspectRatio(Bitmap inputBitmap,int width,int height)
    {
    	//Creating Background Bitmap with required height and width
    	Bitmap background = Bitmap.createBitmap((int)width, (int)height, Config.ARGB_8888);
    	
    	float originalWidth = inputBitmap.getWidth(), originalHeight = inputBitmap.getHeight();
    	Canvas canvas = new Canvas(background);
    	
    	float scale = height/originalHeight;
    	float yTranslation = 0.0f, xTranslation = (width - originalWidth * scale)/2.0f;
    	
//    	float scale = width/originalWidth;
//    	float xTranslation = 0.0f, yTranslation = (height - originalHeight * scale)/2.0f;
    	
    	Matrix transformation = new Matrix();
    	transformation.postTranslate(xTranslation, yTranslation);
    	transformation.preScale(scale, scale);
    	Paint paint = new Paint();
    	paint.setFilterBitmap(true);
    	
    	//Creating inputBitmap 
    	canvas.drawBitmap(inputBitmap, transformation, paint);
    	
    	return background;
    }
  
    public static Bitmap createResizedBitmap(Bitmap capturedBitmp,float dWidth,float dHeight)
    {
    	  int width = capturedBitmp.getWidth();
		  int height = capturedBitmp.getHeight();
		  float original_ratio = ((float)width / height);
		  float designer_ratio = ((float)dWidth / dHeight);
				  
		  if(original_ratio > designer_ratio)
		  {
				      dHeight = dWidth / original_ratio;
		  } else{
				      dWidth = dHeight * original_ratio;
		  }
		  Log.d(LOG_TAG,"New Height And Width:"+dHeight+"x"+dWidth+": Original Size :"+height+": width :"+width);
		    // createa matrix for the manipulation
		  Matrix matrix = new Matrix();
		    // resize the bit map
		  matrix.postScale(dWidth, dHeight);
		    // rotate the Bitmap
		  matrix.postRotate(0);
		  // recreate the new Bitmap
		  Bitmap resizedBitmap = Bitmap.createBitmap(capturedBitmp, 0, 0,
		                      (int)dWidth, (int) dHeight, matrix, true);
		  // make a Drawable from Bitmap to allow to set the BitMap
		  // to the ImageView, ImageButton or what ever
		  return resizedBitmap;
    }
}
