package com.piquor.constants;

public class APP_CREDENTIALS {

	
	public final static String TWITTER_HASTAG = "#IndiaTravelPundits";
	public final static String TWITTER_DESC = " ";
	public final static String MACHINE_CORE_CONFIGURATIONMANGER_RESTART_TIMEPATTERN = "1 0/1 * * * ?";
	public final static String MACHINE_CORE_STATUSREPORTER_RESTART_TIMEPATTERN = "1 0/15 * * * ?";
	public final static String MACHINE_CORE_SERVICEHANDLER_RESTART_TIMEPATTERN = "1 0/15 * * * ?";
	public final static String MACHINE_CORE_DEFAULT_CAMPAIGNID = "8a6182c84407e5d201440adeb2b90003";
	public final static String MACHINE_CORE_RECORDID = "1";
	
}
