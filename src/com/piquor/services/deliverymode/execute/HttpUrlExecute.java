package com.piquor.services.deliverymode.execute;

import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import com.piquor.utility.PiquorTimestamp;

public class HttpUrlExecute {

	public static HttpUrlResponse executeQuery(String queryUrl)
	{
		HttpUrlResponse urlResponse=new HttpUrlResponse();
		
		try
		{
			HttpClient httpclient = new DefaultHttpClient();
		
			httpclient=HttpClientWrapper.wrapClient(httpclient);
		
			URLEncoder.encode(queryUrl, "UTF-8");
			
			//System.out.println("Query URL:"+queryUrl);
			
			HttpGet getRequest = new HttpGet(queryUrl);
	
			getRequest.setHeader("Accept", "application/json");
	
			getRequest.setHeader("Content-Type", "application/json");
	
			HttpResponse response = null;
	
		//	System.out.println("Query Is:"+queryUrl);
			
			response = httpclient.execute(getRequest);

			String queryResponse = EntityUtils.toString(response.getEntity());
			
			String mailerLog = response.getStatusLine().toString()+":Response:"+queryResponse;
			
			
			System.out.println("Mailer Log::"+mailerLog);
			
			urlResponse.setQueryUrlLog(mailerLog);
			
			urlResponse.setQueryUrlResponse(queryResponse);
			
			return urlResponse;
			
		}catch(Exception e)
		{
			String mailerLog="Exception Occured:"+e.getMessage()+": TimeStamp:"+PiquorTimestamp
					.getTimeStamp().toString();
			
			urlResponse.setQueryUrlLog(mailerLog);
			
			urlResponse.setQueryUrlResponse("false");
			
			return urlResponse;
		}
			
	}

}
