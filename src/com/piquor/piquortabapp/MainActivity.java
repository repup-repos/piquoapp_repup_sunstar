package com.piquor.piquortabapp;

import java.util.ArrayList;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.piquor.app.cons.PREFERED_CONSTANTS;
import com.piquor.app.stats.utility.AppStatsBuilder;
import com.piquor.com.dao.model.SavedDBInfo;
import com.piquor.constants.APP_CONS;
import com.piquor.dao.DBHandler;
import com.piquor.dto.AdaptiveQuestion;
import com.piquor.dto.CampaignInfoDTO;
import com.piquor.dto.DataInfoDTO;
import com.piquor.dto.MachineCoreDTO;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.dto.ReviewsConfigDTO;
import com.piquor.dto.TabletAppRegistrationDTO;
import com.piquor.dto.TabletAppStatusDTO;
import com.piquor.dto.UserTableDTO;
import com.piquor.dto.VideoUserDTO;
import com.piquor.fragments.FragmentCaptureScreen;
import com.piquor.fragments.FragmentConfigurationLoader;
import com.piquor.fragments.FragmentMenuScreen;
import com.piquor.fragments.FragmentReviewAdaptiveQuestion;
import com.piquor.fragments.FragmentTripAdvisorHotelSupportiveQuestion1;
import com.piquor.fragments.FragmentTripAdvisorMediumSelection;
import com.piquor.fragments.FragmentTripAdvisorPlaceRating;
import com.piquor.fragments.FragmentTripAdvisorPlaceSelection;
import com.piquor.fragments.FragmentTripAdvisorRestaurantOptionalRating;
import com.piquor.fragments.FragmentTripAdvisorReviewEntry;
import com.piquor.fragments.FragmentTripAdvisorReviewPostView;
import com.piquor.fragments.FragmentTripAdvisorReviewSubmission;
import com.piquor.fragments.HomeScreenFragment;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.fragments.listener.FragmentTextSetView;
import com.piquor.imagecomposer.core.FrameBuilderBitmapCons;
import com.piquor.utility.AlertDialogManager;
import com.piquor.utility.InternetConnectionDetector;
import com.services.deliverymode.staticfields.DeliveryModeFlags;
import com.services.launcher.ServicesLauncher;

public class MainActivity extends FragmentActivity implements
		FragmentTextSetView, FragementChangeListener, SavedDBInfo,
		PREFERED_CONSTANTS, FRAGMENT_SCREEN_NAMES {

	// CREATING USER DTO TO STORE USER DATA
	private static UserTableDTO userDTO;
	// SHARED PREFERENCES INSTANCES
	private SharedPreferences sharedPreferences;
	// SETTING MAINACTIVITY
	private static String LOG_TAG = "MainActivity";
	private static CampaignInfoDTO campaignInfoDTO;
	private static DBHandler dbHandler;
	// SERVEICE LAUCHER INSTANCES //TO DO IS STATUSREPORT MAILER //CONFIGURATION
	// CHECKER
	private static ServicesLauncher servicesLauncher;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Setting android orientation to landscape
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		sharedPreferences = getPreferences(0);

		// Setting preferences to be used by other fragments and services
		SharedPreferences.Editor edit = sharedPreferences.edit();
		edit.putString(TWITTER_PREF_CONSUMER_KEY_FLAG, TWITTER_CONSUMER_KEY);
		edit.putString(TWITTER_PREF_CONSUMER_SECRET_FLAG,
				TWITTER_CONSUMER_SECRET);
		edit.commit();

		dbHandler = new DBHandler(this, "piquorappdb", null, 2);

		/*
		 * if(ConfigurationManagerController.isConfigurationNeedsToBeUpdated().
		 * equals
		 * (ConfigurationManagerController.STATUS_CONFIGMANAGER_APP_IS_UPTODATE
		 * )) {
		 */
		updateCurrentEntries();
		navigateToHomeScreen();

	}

	private void navigateToConfigManger() {

		android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment containerFragment = fragmentManager
				.findFragmentById(R.id.container);
		if (containerFragment == null) {
			containerFragment = new FragmentConfigurationLoader();
			fragmentManager.beginTransaction()
					.replace(R.id.container, containerFragment).commit();
		}

	}

	public void updateCurrentEntries() {

		populateCampaignInfromation();
		AppStatsBuilder appStatusBuilder = new AppStatsBuilder(this);
		appStatusBuilder.initialApplicationStatus();
		servicesLauncher = new ServicesLauncher();
		servicesLauncher.setMainActivity(this);
		servicesLauncher.startServices();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {

		super.onPostCreate(savedInstanceState);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		return super.onCreateOptionsMenu(menu);
	
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
	}

	Fragment containerFrament;

	private void releaseAllBitmapData() {

		logInfo("Release All Bitmap Data");
		APP_CONS.releaseAllImageData();
		// FrameBuilderBitmapCons.recycleAllImages();
		FrameBuilderBitmapCons.recycleTempImages();

	}

	/**
	 * A placeholder fragment containing a simple view.
	 */

	/*
	 * DATA HOLDER AND UPDATER FROM FRAGMENTS
	 */
	public static void createUserDTO() {

		logInfo("Creating UserDTO");
		userDTO = new UserTableDTO();
	}

	public static UserTableDTO getUserDTO() {
		return userDTO;
	}

	// Statis Data Entry Fields
	public static void setDeliveryMode(String deliveryMode) {
		String preDeliveryMode = userDTO.getDelieveryMode();
		logInfo("Revieded DeliveryMode:" + deliveryMode);
		if ((deliveryMode.trim().equals(""))
				|| (!preDeliveryMode.contains(deliveryMode))) {
			if (preDeliveryMode.trim().equals("")) {
				preDeliveryMode = deliveryMode;
			} else {
				preDeliveryMode += "#" + deliveryMode.trim();
			}
			userDTO.setDelieveryMode(preDeliveryMode);
			logInfo("DeliveryMode Entered:" + preDeliveryMode);
		} else {
			logInfo("DeliveryMode:" + deliveryMode + " already Registered:"
					+ preDeliveryMode);
		}

	}

	public static void setEmail(String email) {
		logInfo("Recived Emailid:" + LOG_TAG);
		userDTO.setEmail(email);
	}

	public static void setPhoneNo(String phoneNo) {
		logInfo("Revieved PhoneNo:" + phoneNo);
		userDTO.setPhone(phoneNo);
	}

	public static void setName(String name) {
		logInfo("Recived Name:" + name);
		userDTO.setName(name);
	}

	public static void setPicId(String picId) {
		logInfo("Recived PicId:" + picId);
		userDTO.setPicId(picId);
	}

	public static void setDOB(String dob) {
		logInfo("Recived DOB:" + dob);
		userDTO.setDateofbirth(dob);
	}

	public static void setDate(String date) {
		logInfo(":Recived Date:" + date);
		userDTO.setDate(date);
	}

	public static void logInfo(String message) {
		Log.d(LOG_TAG, message);
	}

	public static void populateCampaignInfromation() {
		campaignInfoDTO = dbHandler.getCampaignInformation();
		logInfo("Campaign Information Added" + campaignInfoDTO.toString());
	}

	public static void updateCampaignInformation(CampaignInfoDTO dto) {
		dbHandler.updateCampaignInformation(dto);
	}

	public static CampaignInfoDTO getCamapignInformation() {
		return campaignInfoDTO;
	}

	private void showLastUserDTOEntry() {

		logInfo("Printing Current Data Entry");

		UserTableDTO userDTO = dbHandler.getLastInsertedUser();
		if (userDTO != null) {
			logInfo(userDTO.toString());
		} else {
			logInfo("No Last Entry Detected");
		}

		// DBHandler dbHandler = new DBHandler(this, null, null, 1);
		// dbHandler.getLastInsertedUser();

	}

	@Override
	public void enterUserIntoDataBase() {

		logInfo("Saving User Info In Database");
		logInfo("Current UserDTO :" + userDTO.toString());
		if (userDTO.getDelieveryMode().trim().equals("")) {
			userDTO.setDelieveryMode(DeliveryModeFlags.EMAIL_DELIVERYMODE_FLAG);
		}
		dbHandler.addUser(userDTO);
		logInfo("Current User Database Entry Completed");
	}

	public static void addVideoUserEntery(VideoUserDTO videoUserDTO) {
		dbHandler.addVideoUserEntriesInDatabase(videoUserDTO);
	}

	public static void setIncrementPrintNumber() {

		String Noprints = userDTO.getNOOfPrints();
		int noprints = Integer.parseInt(Noprints) + 1;
		logInfo("Storing Number Of Prints:" + noprints);
		userDTO.setNOOfPrints(String.valueOf(noprints));

	}

	public static void setCamapignId(String campaignId) {

		logInfo("Setting CmpaignId:" + campaignId);
		userDTO.setCampaignId(campaignId);
	}

	public static void setFbUserName(String fbUserName) {

		logInfo("Setting Facebook UserName:" + fbUserName);
		userDTO.setFbUserName(fbUserName);
	}

	public static void setFbUserId(String fbUserId) {

		logInfo("Setting Facebook UserId:" + fbUserId);
		userDTO.setFbUserId(fbUserId);
	}

	public static void setFbName(String name) {

		logInfo("Setting FbName:" + name);
		userDTO.setFbName(name);
	}

	public static void setTwitterId(long twitterId) {

		logInfo("Setting TwitterId");
		userDTO.setTwitterId(String.valueOf(twitterId));
	}

	public static void setTwitterName(String twitterName) {

		logInfo("Setting TwitterName");
		userDTO.setTwitterName(twitterName);
	}

	public static void setTwitterFollowers(int twitterFollowers) {

		logInfo("Setting Twittter Followsers");
		userDTO.setTwitterFollowers(String.valueOf(twitterFollowers));
	}

	public static void setTwitterFriends(int twitterFriends) {

		logInfo("Setting TwitterFriends");
		userDTO.setTwitterFriendsCount(String.valueOf(twitterFriends));
	}

	public static DataInfoDTO getAllDataEntries() {
		return dbHandler.getDataEntryInfo();
	}

	public static void deleteAllEntriesInDatabase() {
		dbHandler.deleteAllUserEntries();
	}

	public static boolean executeUpdateQuery(String query) {

		return dbHandler.executeUpdateQuery(query);
	}

	public static int getDBEntryCount(String query) {
		return dbHandler.getDBEntryCount(query);
	}

	public static ArrayList<UserTableDTO> getUnsendedMailList(
			String urlPendingDataFlag) {
		return dbHandler.getUnsendedMailList(urlPendingDataFlag);
	}

	public static ArrayList<ReviewEntryDTO> getUnsendedReviewMailList(
			String urlPendingDataFlag) {
		return dbHandler.getUnsendedReviewMailList(urlPendingDataFlag);
	}

	@Override
	public void onStop() {
		super.onStop();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// .stopSevices();
		servicesLauncher.stopSevices();
	}

	public static ArrayList<ReviewsConfigDTO> getCurrentTripAdvisorInfo() {

		ArrayList<ReviewsConfigDTO> configDTO = dbHandler.getTripAdvisorInfo();
		Log.d(LOG_TAG, ":Configuration DTO:" + configDTO.toString());
		return configDTO;
	}

	public static void updateTripAdvisorRestaurentDetails(
			ArrayList<ReviewsConfigDTO> updatedList) {

		Log.d(LOG_TAG, ":Configuration DTO:" + updatedList.toString());
		dbHandler.updateTripAdvisorDetials(updatedList);
	}

	public static void addPlaceInformation(ReviewsConfigDTO configDTO) {

		Log.d(LOG_TAG,
				"Adding Place Information on Database:" + configDTO.toString());
		dbHandler.addPlaceInfo(configDTO);
	}

	public static void deletePlaceNames(String selectedPlaceName) {

		Log.d(LOG_TAG, "Deleting Place:" + selectedPlaceName);
		dbHandler.deletePlace(selectedPlaceName);
	}

	public static void addReviewEntriesInDatabase(ReviewEntryDTO reviewEntryDTO) {
		Log.d(LOG_TAG, "Adding Restaurant Review Entries in database:"
				+ reviewEntryDTO.toString());
		dbHandler.addRestaurantReviewEntries(reviewEntryDTO);
	}

	public static int getTotalRowEntriesMachineCore() {

		return dbHandler.getTotalEntryInMachineCoreTable();
	}

	public static void saveMachineCoreData(MachineCoreDTO machineCoreDTO) {
		logInfo("Saving Machine Core data in database"
				+ machineCoreDTO.toString());
		dbHandler.saveMachineCoreTableData(machineCoreDTO);
	}

	public static void setUpdateErrorOnMachineCore() {

		dbHandler.setUpdateErrorStatusOnMachineCore();

	}

	public static boolean setUpdateCompleteOnMachineCore() {

		return dbHandler.setUpdateCompleteOnMachineCore();
	}

	public static boolean getCurrentMachineCoreUpdateStatus() {

		return dbHandler.getMachineCoreUpdateStatus();
	}

	@Override
	public void navigateToReviewPublishScreen() {

		if (checkInternetConnectivity()) {

			String tag = "publishreview";
			FragmentManager fragmentManager = getSupportFragmentManager();

			FragmentTripAdvisorReviewSubmission reviewSubmission = new FragmentTripAdvisorReviewSubmission();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.replace(R.id.reviewContainer, reviewSubmission);
			fragmentTransaction.addToBackStack("navigateToReviewPublishScreen");
			fragmentTransaction.commit();
		}
	}

	@Override
	public void navigateToRestaurantReviewOptionalScreen() {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTripAdvisorRestaurantOptionalRating reviewSubmission = new FragmentTripAdvisorRestaurantOptionalRating();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.reviewContainer, reviewSubmission);
		fragmentTransaction.addToBackStack("navigateToReviewPublishScreen");
		fragmentTransaction.commit();
	}

	@Override
	public void navigateToHotelReviewOptionalScreen() {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTripAdvisorHotelSupportiveQuestion1 reviewSubmission = new FragmentTripAdvisorHotelSupportiveQuestion1();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.reviewContainer, reviewSubmission);
		fragmentTransaction.addToBackStack("navigateToReviewPublishScreen");
		fragmentTransaction.commit();
	}

	private boolean checkInternetConnectivity() {
		InternetConnectionDetector internetConnectionDetector = new InternetConnectionDetector(
				this);
		if (!internetConnectionDetector.isConnectingToInternet()) {
			new AlertDialogManager().showAlertDialog(this,
					"Intenet Connection Failed",
					"Please Ensure Internet Connectivity", false);
			return false;
		}
		return true;
	}

	@Override
	public void navigateToBackScreen() {

		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.popBackStack();
	}

	@Override
	public void navigateToCaptureScreen() {

		Log.d(LOG_TAG, "Relase all bitmap captured data");
		releaseAllBitmapData();
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentCaptureScreen captureScreen = new FragmentCaptureScreen();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.viewHolder, captureScreen);
		fragmentTransaction.addToBackStack("navigateToCaptureScreen");
		fragmentTransaction.commit();

	}

	@Override
	public void navigateToReviewMediumSelection() {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTripAdvisorMediumSelection reviewMediumSelection = new FragmentTripAdvisorMediumSelection();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.viewHolder, reviewMediumSelection);
		fragmentTransaction.addToBackStack("navigateToReviewMediumSelection");
		fragmentTransaction.commit();
	}

	@Override
	public void navigateToReviewPlaceSelection() {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTripAdvisorPlaceSelection reviewPlaceSelection = new FragmentTripAdvisorPlaceSelection();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.viewHolder, reviewPlaceSelection);
		fragmentTransaction.addToBackStack("navigateToReviewPlaceSelection");
		fragmentTransaction.commit();

	}

	@Override
	public void navigateToHomeScreen() {

		android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment containerFragment = fragmentManager
				.findFragmentById(R.id.container);
		if (containerFragment == null) {
			containerFragment = new HomeScreenFragment();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			fragmentTransaction.replace(R.id.viewHolder, containerFragment);
			fragmentTransaction.addToBackStack("navigateToHomeScreen");
			fragmentTransaction.commit();
		}
	}

	@Override
	public void navigateToImageSharingScreen() {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentMenuScreen fragmentMenuScreen = new FragmentMenuScreen();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.viewHolder, fragmentMenuScreen);
		fragmentTransaction.addToBackStack("navigateToImageSharingScreen");
		fragmentTransaction.commit();
	}

	@Override
	public void updateTextView() {

		FragmentManager fragmentManager = getSupportFragmentManager();
		String fragmentTag = "Place_Entries";
		FragmentTripAdvisorPlaceRating fragment = (FragmentTripAdvisorPlaceRating) fragmentManager
				.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			fragment.updateEditTextView();
		}
	}

	public static boolean saveAdaptiveQuesitonData(
			ArrayList<AdaptiveQuestion> adaptiveQuestions) {

		return dbHandler.saveAdaptiveQuestions(adaptiveQuestions);

	}

	public static ArrayList<AdaptiveQuestion> getAdaptiveQuestionList() {

		return dbHandler.getAllAdaptiveQuestinList();
	}

	@Override
	public void navigateToReiewEntryScreen() {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTripAdvisorReviewEntry fragmentMenuScreen = new FragmentTripAdvisorReviewEntry();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.viewHolder, fragmentMenuScreen);
		fragmentTransaction.addToBackStack("navigateToReviewEntryScreen");
		fragmentTransaction.commit();
	}

	@Override
	public void navigateToAdaptiveQuesitonScreen(ReviewEntryDTO reviewEntryDTO) {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentReviewAdaptiveQuestion fragmentMenuScreen = new FragmentReviewAdaptiveQuestion();
		fragmentMenuScreen.setReviewUserDTO(reviewEntryDTO);
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.viewHolder, fragmentMenuScreen);
		fragmentTransaction.addToBackStack("navigateToAdaptiveQuestionScreen");
		fragmentTransaction.commit();

	}

	@Override
	public void navigateTotripAdvisorPostViewScreen(ReviewEntryDTO reviewEntryDTO) {

		FragmentManager fragmentManager = getSupportFragmentManager();

		FragmentTripAdvisorReviewPostView fragmentReviewPost = new FragmentTripAdvisorReviewPostView();
		fragmentReviewPost.setReviewUserDTO(reviewEntryDTO);
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		System.out.println("Replacing Fragment Review Post");
		fragmentTransaction.replace(R.id.viewHolder, fragmentReviewPost);
		fragmentTransaction.addToBackStack("navigateToReviewPost");
		fragmentTransaction.commit();
	}

	public static boolean saveTabletRegistrationData(
			TabletAppRegistrationDTO tabletAppRegistrationDTO) {

		return dbHandler.saveTabletRegistrationData(tabletAppRegistrationDTO);
	}

	public static String getDeivceIdFromRegistrtionTable() {

		return dbHandler.getDeviceIdFromRegistrationTable();
	}

	public static boolean saveApplicatonStatusData(
			TabletAppStatusDTO tabletAppStatusDTO) {

		return dbHandler.saveTabletAppStatus(tabletAppStatusDTO);
	}

	private String tmpReviewDesc = "";

	@Override
	public void setTemporaryOnDescDialogText(String reviewDesc) {

		tmpReviewDesc = reviewDesc;
	}

	@Override
	public String getTemporaryOnDescDialogText() {

		return tmpReviewDesc;
	}
	
	@Override
	public float getBatteryLevel() {
		
		Intent batteryIntent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	    int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
	    int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

	    // Error checking that probably isn't needed but I added just in case.
	    if(level == -1 || scale == -1) {
	        return 50.0f;
	    }

	    return ((float)level / (float)scale) * 100.0f; 
	}

}
