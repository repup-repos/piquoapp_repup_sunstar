package com.piquor.dao.table.cons;

public interface MACHINE_CONFIGURATION_COLUMNS {

	public final String TABLE_NAME_MACHINE_CONFIGURATION = "machine_configuration";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_RECORDID = "RecordId"; 
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_CAPTUREVIDEO = "CaptureVideo"; 
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_CAPTUREIMAGE = "CaptureImage";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_CAPTUREREVIEW = "CaptureReview";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_PREVIEW_CAMERA_HEIGHT = "PreiviewCameraHeight";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_PREVIEW_CAMERA_WIDTH = "PreivewCameraWidth"; 
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_PREVIEW_CAMERA_X = "PreviewCameraX";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_PREVIEW_CAMERA_Y = "PreviewCameraY";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_CAMERA_RES_X = "CameraResX";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_CAMERA_RES_Y= "CameraResY";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_SCREENSAVERTIME = "ScreenSaverTime"; 
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_MACHINEID = "MachineId";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_ASKFORNAME = "AskForName"; 
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_ASKFORPHONE = "AskForPhone";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_PHONEPREFIX = "PhonePrefix";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_RECORDEDVIDEOTIMING = "RecordedVideoTiming";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_TAKEPHOTOCOUNTERTIMING = "TakePhotoCounterTiming";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_SHOWPRINTBUTTONONMENUSCREEN = "ShowPrintButtonOnMenuScreen";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_SHOWEMAILBUTTONONMENUSCREEN = "ShowEmailButtonOnMenuScreen";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_SHOWTWITTERSHAREBUTTONONMENUSCREEN = "ShowTwitterShareButtonOnMenuScreen";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_SHOWFACEBOOKSHAREBUTTONONMENUSCREEN = "ShowFacebookShareButtonOnMenuScreen";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_SHOWFANPAGEBUTTONONMENUSCREEN = "ShowFanPageButtonOnMenuScreen";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_TWITTERPOSTTEXT = "TwitterPostText";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_FACEBOOKPOSTTEXT = "FacebookPostText";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_APPLICATION_BACKGROUND_URL = "ApplicatonBackgroundUrl";
	public final String COLUMN_NAME_MACHINE_CONFIGURATION_UPDATEDATE = "UpdateDate";

}	
