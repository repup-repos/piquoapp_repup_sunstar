package com.piquor.dto;

public class AdaptiveQuestion {

	private String questionId;
	private String question;
	private String campignId;
	private String state;
	
	public AdaptiveQuestion() {
		super();
	}

	public AdaptiveQuestion(String questionId, String question, String campignId,String state) {
		super();
		this.questionId = questionId;
		this.question = question;
		this.campignId = campignId;
		this.setState(state);
	}

	
	
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getCampignId() {
		return campignId;
	}
	public void setCampignId(String campignId) {
		this.campignId = campignId;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}



	@Override
	public String toString() {
		return "AdaptiveQuestion [questionId=" + questionId + ", question="
				+ question + ", campignId=" + campignId + ", state=" + state
				+ "]";
	}
	
}
