package com.piquor.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.ReviewsConfigDTO;
import com.piquor.dto.TripAdisorEssentialsDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;

public class FragmentTripAdvisorPlaceSelection extends Fragment implements FRAGMENT_SCREEN_NAMES,  ACTIVITY_FLAGS, OnItemClickListener{

	private String LOG_TAG           = "FragmetnTripAdvisorPlaceSelection";
	private ArrayList<ReviewsConfigDTO> tripAdvisorList;
	private String[] TRIPADVISOR_NAMES;
	private ListView restaurantList;
	private CustomListAdapter customListAdapter;
	private FragementChangeListener fragmentChangeListener;
	
	@Override 
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener) getActivity();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)
	{
		
		View view = inflator.inflate(R.layout.fragmenttripadvisorplaceselection, parent,false);
		restaurantList = (ListView) view.findViewById(R.id.fragment_view_list);
		tripAdvisorList = MainActivity.getCurrentTripAdvisorInfo();
		TRIPADVISOR_NAMES = new String[tripAdvisorList.size()];
		
		for(int counter = 0; counter<tripAdvisorList.size(); counter++ )
		{
			ReviewsConfigDTO configDTO = tripAdvisorList.get(counter);
			TRIPADVISOR_NAMES[counter] = configDTO.getTripAdvisorRestaurentName();
		}
		
		ImageButton btnBack = (ImageButton) view.findViewById((R.id.btnfragmentreviewplaceselection_back));
		btnBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				navigateToBackScreen();
			}
		});
		
		customListAdapter = new CustomListAdapter(getActivity());
		restaurantList.setAdapter(customListAdapter);
		restaurantList.setOnItemClickListener(this);
		restaurantList.performItemClick(restaurantList.getChildAt(0), 0, customListAdapter.getItemId(0));
		
		
		FragmentManager fragmentManager = getActivity()
				.getSupportFragmentManager();
		FragmentReviewAdaptiveQuestionDialog fragmentReviewAdaptiveScreen = new FragmentReviewAdaptiveQuestionDialog();
		fragmentReviewAdaptiveScreen.show(fragmentManager, "EmailREviewDialog");
		
		
		return view;
	}
	
	
	protected void navigateToBackScreen() {
		
		fragmentChangeListener.navigateToBackScreen();
	}


	
	class CustomListAdapter extends BaseAdapter{
		
		private Context mContext;
		private int[] images = {
						R.drawable.button_restaurant_1,
						R.drawable.button_restaurant_2,
						R.drawable.button_restaurant_3,
						R.drawable.button_restaurant_4};
		
		public CustomListAdapter(Context mContext)
		{
			this.mContext = mContext;
		}
		
		@Override
		public int getCount() {
			return TRIPADVISOR_NAMES.length;
		}

		@Override
		public Object getItem(int position) {

			return TRIPADVISOR_NAMES[position];
		}

		@Override
		public long getItemId(int position) {

			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			View row = null;
			if(convertView==null)
			{
				LayoutInflater inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflator.inflate(R.layout.listview_restaurantselection, parent,false);
				
				
			}else
			{
				row = convertView;
			}
			
			TextView txtView = (TextView) row.findViewById(R.id.txtRestaurantName_ListView);
			ImageView imgView = (ImageView) row.findViewById(R.id.imgView_RestaurantIcon_ListView);
			
			txtView.setText(TRIPADVISOR_NAMES[position]);
			imgView.setImageResource(images[position]);
			
			return row;
		}
		
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		setFragmentContainer(position);
		
	}


	private void setFragmentContainer(int position) {
	
		String placeName = tripAdvisorList.get(position).getTripAdvisorRestaurentName().trim();
		String placeUrl = tripAdvisorList.get(position).getTripAdvisorRestaurantURL().trim();
		String placeType = tripAdvisorList.get(position).getTripAdvisorRestaurantType().trim();
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURENT_NAME(placeName);
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_RESTAURENT_URL(placeUrl);
		TripAdisorEssentialsDTO.setTRIP_ADVISOR_PLACE_TYPE(placeType);
		
		moveToEssentialEntries();
		
	}


	private void moveToEssentialEntries() {
		
		String fragmentTag = "Place_Entries";
		FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
		Fragment fragment = fragmentManager.findFragmentByTag(fragmentTag);
		if(fragment==null)
		{
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			FragmentTripAdvisorPlaceRating placeRating = new FragmentTripAdvisorPlaceRating();
			fragmentTransaction.add(R.id.reviewContainer, placeRating,fragmentTag);
			fragmentTransaction.commit();
		}else
		{
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			FragmentTripAdvisorPlaceRating placeRating = new FragmentTripAdvisorPlaceRating();
			fragmentTransaction.replace(R.id.reviewContainer, placeRating,fragmentTag);
			fragmentTransaction.commit();
		}
		
	}
	
	
}
