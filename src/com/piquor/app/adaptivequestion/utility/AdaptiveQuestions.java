package com.piquor.app.adaptivequestion.utility;

import java.util.ArrayList;
import java.util.HashMap;

import com.piquor.dto.AdaptiveQuestion;
import com.piquor.piquortabapp.MainActivity;

public class AdaptiveQuestions implements AdaptiveQuestionCons  {
	
	  /*  public String[] QUESTION_KEYS;
	    pu*/
	
	    public String[] QUESTION_KEYS = null ;
	    public String[] QUESTION_VALUE =  null;
	
	// Minimum characters required by 
		public static final int REQ_CHARACTERS = 200;
		
		// List of all questions yet to be asked to the current user
		HashMap<String, String> questionsToBeAsked;
		
		// List of all possible questions related to the current hotel / restaurant
		HashMap<String, String> allQuestionsWhichCanBeAsked = new HashMap<String, String>();
		
		// Concatenation of all answers given by the user
		String reviewOfVisitor;
		
		public AdaptiveQuestions() {
			
			populateQuestionData();
			if(QUESTION_KEYS!=null)
			{
				getListOfAllPossibleQuestions();
				resetSettings();
			}
			
		}
		
		/*
		 *  Populating all question data from database
		 */
		private void populateQuestionData() {
			
			ArrayList<AdaptiveQuestion> adaptiveQuestions = MainActivity.getAdaptiveQuestionList();
			if(adaptiveQuestions!=null)
			{
				QUESTION_KEYS = new String[adaptiveQuestions.size()];
				QUESTION_VALUE = new String[adaptiveQuestions.size()];
				for(int counter=0;counter<adaptiveQuestions.size();counter++)
				{
					QUESTION_KEYS[counter] = adaptiveQuestions.get(counter).getQuestionId().trim();
					QUESTION_VALUE[counter] = adaptiveQuestions.get(counter).getQuestion().trim();
				}
			}
		}

		// Empty review of previous user
		// Reset list of questions to be asked
		public void resetSettings() {
			reviewOfVisitor = "";
			resetQuestions();
		}
		
		// Reset list of questions to be asked
		public void resetQuestions() {
			if (questionsToBeAsked != null) {
				questionsToBeAsked.clear();
				questionsToBeAsked = null;
			}
			
			questionsToBeAsked = new HashMap<String, String>(allQuestionsWhichCanBeAsked);
		}
		
		// Adding the current answer to the overall feedback
		public void addAnswer(String ans) {
			reviewOfVisitor = reviewOfVisitor + " " + ans;
		}
		
		public String getNextQuestion(String key) {
			
			// Choose a 'Key' based on some settings
			
			String question = questionsToBeAsked.get(key);
			if(question!=null)
			{
				questionsToBeAsked.remove(key);
			}else
			{
				question = "";
			}
			return question;
		}
		
		// Get the percentage of feedback completed by the user
		public float percentageReviewCompleted() {
			float completed = reviewOfVisitor.length() * 100 / REQ_CHARACTERS;
			if (completed > 100) {
				return 100;
			}
			return completed; 
		}
		
		// From DB get all questions to be asked / can be asked regarding the current hotel / restaurant
		public void getListOfAllPossibleQuestions() {
			
			//	Get these questions from a DB
			
			for(int counter=0;counter<QUESTION_KEYS.length;counter++)
			{
				allQuestionsWhichCanBeAsked.put(QUESTION_KEYS[counter], QUESTION_VALUE[counter]);
			}
		}
		
		public String getReviewOfVisitor()
		{	
			return reviewOfVisitor;
		}

		public String[] getQuestionKeys() {
			
			return QUESTION_KEYS;
		}

		public boolean isQuestionsEmpty() {
			
			return allQuestionsWhichCanBeAsked.isEmpty();
		}
}
