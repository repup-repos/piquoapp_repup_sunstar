package com.piquor.app.updater.dbupdates;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.piquor.dto.AdaptiveQuestion;
import com.piquor.piquortabapp.MainActivity;

import android.util.Log;

public class AdaptiveQuestionUpdater implements AdaptiveQuestionJsonCons {

	private String LOG_TAG = "AdaptiveQuestionUpdater";
	private ArrayList<AdaptiveQuestion> adaptiveQuestions;
	
	public AdaptiveQuestionUpdater()
	{
		adaptiveQuestions = new ArrayList<AdaptiveQuestion>();
	}
	
	
	public boolean createAdaptiveQuestionList(String responseJsonArray)
	{
		try{
		
		JSONArray jsonArray = new JSONArray(responseJsonArray);
		System.out.println(jsonArray.length());
		if(jsonArray.length()>0)
		{
			for(int counter=0;counter<jsonArray.length();counter++)
			{
				
				JSONObject jsonObject =  jsonArray.getJSONObject(counter);
				String questionId = jsonObject.getString(JSON_ADAPTIVE_QUESTION_ID).trim();
				String question = jsonObject.getString(JSON_ADAPTIVE_QUESTION).trim();
				String campaignId = jsonObject.getString(JSON_ADAPTIVE_CAMPAIGN_ID).trim();
				String state = jsonObject.getString(JSON_ADAPTIVE_QUESTION_STATE).trim();
				AdaptiveQuestion adaptiveQuestion = new AdaptiveQuestion(questionId,question,campaignId,state);
				adaptiveQuestions.add(adaptiveQuestion);
				System.out.println("Adaptive Quesiton Recieved:"+adaptiveQuestion.toString());
				
				
			}
			return true;
		}else
		{
			System.out.println("No question entries found");
			return true;
		}
		}catch(Exception ex)
		{
			logInfo("Exception Occurred: "+ex.getMessage());
			return false;
		}
		
	}
	
	public void saveToDatabase()
	{
		if(adaptiveQuestions.size()>0)
		{
			boolean resposne = MainActivity.saveAdaptiveQuesitonData(adaptiveQuestions);
			if(resposne)
			{
				logInfo("Question adapter data succesfully stored");
			}else
			{
				logInfo("Error in saving configuration data");
			}
		}else
		{
			logInfo("No Question to add to database");
		}
	}

	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}
