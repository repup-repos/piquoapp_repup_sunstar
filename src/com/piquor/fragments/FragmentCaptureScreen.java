package com.piquor.fragments;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.constants.APP_CONS;
import com.piquor.fileUtils.ImageFileUtility;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.frameconfig.core.FrameConfigLoader;
import com.piquor.frameconfig.dto.FrameConfig;
import com.piquor.imagecomposer.core.FrameBuilder;
import com.piquor.imagecomposer.core.FrameBuilderBitmapCons;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;
import com.piquor.utility.SaveFinalImageUtility;

public class FragmentCaptureScreen extends Fragment implements FRAGMENT_SCREEN_NAMES,
		ACTIVITY_FLAGS {

	private String LOG_TAG = "FragmentCaptureScreen";
	private int REQUEST_IMAGE_CAPTURE=  1;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	private FragementChangeListener fragmentChangeListener;
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
		
		Log.d(LOG_TAG, "On Create being called");
		Log.d(LOG_TAG,"On start is being called");
		Log.d(LOG_TAG, "Checking for Camera Availabilty");
		isCreatedCalled = true;
		if(isCameraAvailable())
		{
			Log.d(LOG_TAG, "Camera Is Available Creating Image Directory");
			createImageDirectory();
			Log.d(LOG_TAG,"Image Directory Creation Successfull");
			if(APP_CONS.getBitmapCaptured()==null)
			{
				Log.d(LOG_TAG, "Directory Created Starting Capture Intent");
				startCaptureIntent();
			}else
			{
				Log.d(LOG_TAG,"Release All Bitmap Data");
				APP_CONS.releaseAllImageData();
				//FrameBuilderBitmapCons.recycleAllImages();
				FrameBuilderBitmapCons.recycleTempImages();
				startCaptureIntent();
				
			}
		}else
		{
			Log.d(LOG_TAG, "Camera Is Not Connected");
			Toast.makeText(getActivity(), "Camera Is Not Connected", Toast.LENGTH_LONG).show();

		}
	
		
	}
	
	
	boolean isCreatedCalled = false;
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
	}
	
	
	
	//Opening Camera For Capturing Photograph
		private void startCaptureIntent() {
			
			 Log.d(LOG_TAG,"Starting Camera Capture Intent");
			 Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			 
			 if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
				 
				   Log.d(LOG_TAG, "Creating File to Whold Captured Image");
				 	try {
			             APP_CONS.set_file(File.createTempFile(ImageFileUtility.getImageFileName(), ".jpg",APP_CONS.get_dir()));
			        } catch (Exception ex) {
			        }
			        // Continue only if the File was successfully created
			        if (APP_CONS.get_file() != null) {
			            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
			                    Uri.fromFile(APP_CONS.get_file()));
			            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
			        }
			    }
			
		}
		
		
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			   
			if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode== Activity.RESULT_OK) {
		      
		    	Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			    
		    	Uri contentUri = Uri.fromFile(APP_CONS.get_file());
			    
		    	mediaScanIntent.setData(contentUri);
			    
		    	getActivity().sendBroadcast(mediaScanIntent);
			    
		    	APP_CONS.setBitmapCaptured(ImageFileUtility.loadImageBitmap(APP_CONS.get_file().getAbsolutePath()));
		    	
		    	Log.d("CaptureFragment","Image Saved Moving To Capture Fragment");
			    
		    	Log.d(LOG_TAG,"Creating User DTO");
		    	MainActivity.createUserDTO();
		    	
		    	new ImageFrameComposer().execute();
		    	
		    }else if(resultCode==Activity.RESULT_CANCELED)
		    {
		    
		    	startCaptureIntent();
		    }else
		    {
		    	startCaptureIntent();
		    }
		}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		fragmentChangeListener = (FragementChangeListener)getActivity();
	}


	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)
	{
		
		View view = inflator.inflate(R.layout.fragmentcapturescreen, parent,false);
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
	}
	
	private void navigetToImagePreviewScreen() {
		
		Log.d(LOG_TAG, "Navigating To Effect Screen");
		/*
		 * FragementChangeListener fragmentChangeListener = (FragementChangeListener) getActivity();
			fragmentChangeListener.replaceFragment(SET_PIC_ID, FRAGMENT_CAPTURE_SCREEN, FRAGMENT_EFFECT_SCREEN);
		 */	
		isCreatedCalled = false;
		fragmentChangeListener.navigateToImageSharingScreen();
	}

	private void createImageDirectory() {
		
		APP_CONS.set_dir(ImageFileUtility.getAppDirectory());
	}

	private boolean isCameraAvailable() {
		
		PackageManager pm = getActivity().getApplicationContext().getPackageManager();
		if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA))
		{
			return true;
		}else if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT))
		{
			return true;
		}else if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	
		class ImageFrameComposer extends AsyncTask<String, String, String>
		{
			
			private ArrayList<FrameConfig> framesConfiguration;
			ProgressDialog progressDialog;
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				framesConfiguration = FrameConfigLoader.getFramesConfiguration();
				progressDialog = ProgressDialog.show(getActivity(), "", "", true);
				progressDialog.setIndeterminate(true);
				progressDialog.setCancelable(false);
				progressDialog.show();
			}
			
			

			@Override
			protected String doInBackground(String... params) {
			
				int totalFrames = framesConfiguration.size();
				for(int thumbnailCounter = 0;thumbnailCounter<totalFrames;thumbnailCounter++ )
				{
					FrameConfig frameConfig = framesConfiguration.get(thumbnailCounter);
					
					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inScaled = false;
					if(thumbnailCounter == 0)
					{
						if(FrameBuilderBitmapCons.getBgImage()==null)
						{
							FrameBuilderBitmapCons.setBgImage(BitmapFactory.decodeResource(getResources(), frameConfig.getBgFrameID(),options));
							Log.d(LOG_TAG,"Dim:widthxheight"+FrameBuilderBitmapCons.getBgImage().getWidth()+"x"+FrameBuilderBitmapCons.getBgImage().getHeight());
						}
						if(FrameBuilderBitmapCons.getFgImage()==null)
						{
							FrameBuilderBitmapCons.setFgImage(BitmapFactory.decodeResource(getResources(), frameConfig.getFgFrameID(),options));
							Log.d(LOG_TAG,"Dim:widthxheight"+FrameBuilderBitmapCons.getFgImage().getWidth()+"x"+FrameBuilderBitmapCons.getFgImage().getHeight());
						}
						
						if(FrameBuilderBitmapCons.getFinalFrameImage()==null)
						{
							FrameBuilderBitmapCons.setFinalFrameImage(new FrameBuilder().placeFrameOverImage(frameConfig));
						}
					}
				}
				
				saveImageToSDCard();
				
				return null;
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				progressDialog.dismiss();
				navigetToImagePreviewScreen();
				
			}
			
			private void saveImageToSDCard() {
				
				SaveFinalImageUtility save = new SaveFinalImageUtility();
				
				boolean value = save.saveImage();
				
				String picId = APP_CONS.get_framedImage().getName();
				StringTokenizer tokenizer = new StringTokenizer(picId, ".");
				String pId = "";
				while (tokenizer.hasMoreElements()) {

					pId = (String) tokenizer.nextElement().toString()
							.trim();
					Log.d(LOG_TAG, "PicID: Recieved :" + pId);
					break;
				}
				MainActivity.setPicId(pId);
				MainActivity.setCamapignId(MainActivity.getCamapignInformation().getCampaignId().trim());
				
				//Set current pic date
				java.util.Date now = new java.util.Date();
				//converting java.util.Date to java.sql.Date in Java
		        java.sql.Date sqlDate = new java.sql.Date(now.getTime());
		        Log.d(LOG_TAG,"Converted value of java.sql.Date : " + sqlDate.toString());
				MainActivity.setDate(sqlDate.toString());
		        
				Log.d(LOG_TAG, "Saving image response "+value);
				Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				Uri contentUri = Uri.fromFile(APP_CONS.get_framedImage());
				mediaScanIntent.setData(contentUri);
				getActivity().getApplicationContext().sendBroadcast(mediaScanIntent);
			}

		}
}
