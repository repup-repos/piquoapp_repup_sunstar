package com.piquor.dao.table.cons;

public interface USER_TABLE_COLUMNS {
	
	public final String TABLE_NAME_USER = "user";
	public final String COLUMN_ID = "id";
	public final String COLUMN_SOCIAL_MEDIA_PERMISSION = "socialMediaPermission";
	public final String COLUMN_DOB_COLUMN = "dob";
	public final String COLUMN_CAMPAIGN_ID =   "campaignid";
	public final String COLUMN_ISPRINTED =  "IsPrinted";	
	public final String COLUMN_STATUS = "status";
	public final String COLUMN_FEEDBACK =  "feedback";
	public final String COLUMN_FRAME_SELECTED = "frame_selected";
	public final String COLUMN_DATE ="date";
	public final String COLUMN_GENDER = "gender";
	public final String COLUMN_PICID = "picID";
	public final String COLUMN_NAME = "name";
	public final String COLUMN_EMAIL = "email";
	public final String COLUMN_MAILSENT = "mailsent";
	public final String COLUMN_FILEUPLOAD = "fileUpload";
	public final String COLUMN_PHONE  = "phone";
	public final String COLUMN_DELIEVERYMODE = "DelieveryMode";
	public final String COLUMN_FBUSERID = "FbUserId";
	public final String COLUMN_FBUSERNAME = "FbUserName";
	public final String COLUMN_FBNAME = "FbName";
	public final String COLUMN_TWITTER_ID = "TwitterId";
	public final String COLUMn_TWITTERNAME = "TwitterName";
	public final String COLUMN_TWITTER_FOLLOWERS = "TwitterFollowers";
	public final String COLUMN_TWITTER_FRIENDS = "TwitterFriends";
			
	
}
