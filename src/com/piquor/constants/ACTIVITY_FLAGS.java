package com.piquor.constants;

public interface ACTIVITY_FLAGS {
	
	public final String CREATE_USER_DTO = "CREATE_DTO";
	public final String SET_PIC_ID		= "SET_PICID";
	public final String CREATE_TRIP_ADVISOR_DTO = "CREATE_TRIPADVISOR_DTO";
	public final String PLACE_RESTAURANT_NAME = "restaurant";
	public final String PLACE_HOTEL_NAME = "hotel";
	public final String PLACE_REVIEW_TYPE_ZOMATO = "ZOMATO";
	public final String PLACE_REVIEW_TYPE_TRIPADVISOR = "TRIPADVISOR";
}
