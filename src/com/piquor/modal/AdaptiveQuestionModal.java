package com.piquor.modal;

import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.piquor.app.adaptivequestion.utility.AdaptiveQuestions;
import com.piquor.piquortabapp.R;

public class AdaptiveQuestionModal {
	
	private AdaptiveQuestions adaptiveQuestion;
	private String[] QUESTION_KEYS;
	private FragmentActivity fragmentActivity;
	private EditText[] editTexts;
	private TextView[] txtViews;
	private String LOG_TAG = "AdaptiveQuestinModal";
	
	public AdaptiveQuestionModal(FragmentActivity fragmentActivity)
	{
		this.fragmentActivity = fragmentActivity;
	}
	
	public boolean addAdaptiveQuestionToLinearLayout(LinearLayout linearLayout)
	{

		adaptiveQuestion = new AdaptiveQuestions();
		QUESTION_KEYS = adaptiveQuestion.getQuestionKeys(); 
		if(!adaptiveQuestion.isQuestionsEmpty())
		{
			int length = QUESTION_KEYS.length;
			editTexts = new EditText[length];
			txtViews = new TextView[length];
			
			Log.d(LOG_TAG, "....Total Length is.... "+length);
			
			for(int counter=0;counter<length;counter++)
			{
				String questionText = adaptiveQuestion
					.getNextQuestion(QUESTION_KEYS[counter]);
			    txtViews[counter] = createTextViewField(questionText);
			    editTexts[counter] = createEditTextField();
			}
			for(int counter=0;counter<length;counter++)
			{
				linearLayout.addView(txtViews[counter]);
				linearLayout.addView(editTexts[counter]);
			}
			
			return true;
		}else
		{
			return false;
		}
	}
	
	
	/*<TextView
    android:id="@+id/textView3"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:layout_marginBottom="20dp"
    android:layout_marginLeft="20dp"
    android:layout_marginRight="20dp"
    android:layout_marginTop="10dp"
    android:maxLines="3"
    android:singleLine="false"
    android:text="@string/txtAdaptvieQestion1"
    android:textColor="@color/fragmentadaptiveQst_headingcolor"
    android:textSize="@dimen/fragmentadaptivequestion_textsize" />*/
	
	private TextView createTextViewField(String text)
	{
		TextView txtView = new TextView(fragmentActivity);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(20, 10, 20, 20);
		txtView.setLayoutParams(layoutParams);
		Log.d(LOG_TAG, "..... Adding Text...."+text);
		txtView.setText(text);
		txtView.setMaxLines(3);
		txtView.setSingleLine(false);
		txtView.setTextColor(fragmentActivity.getResources().getColor(R.color.fragmentadaptiveQst_headingcolor));
		float textSize = fragmentActivity.getResources().getDimension(R.dimen.fragmentadaptivequestion_textsize);
		txtView.setTextSize(textSize);
		
		return txtView;
	}
	
	
/*	<EditText
    android:id="@+id/editTextQuestion1"
    android:layout_width="match_parent"
    android:layout_height="@dimen/fragmentadaptivequestion_edittext_size"
    android:layout_marginBottom="20dp"
    android:layout_marginLeft="90dp"
    android:layout_marginRight="20dp"
    android:layout_marginTop="5dp"
    android:background="@drawable/edittext_layout_shap_qst"
    android:ems="10"
    android:gravity="top"
    android:inputType="textMultiLine"
    android:padding="@dimen/fragmentadaptivequestion_edittext_padding" >
</EditText>*/
	
	
	private EditText createEditTextField()
	{
		EditText editText = new EditText(fragmentActivity);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, fragmentActivity.getResources().getDimensionPixelSize(R.dimen.fragmentadaptivequestion_edittext_size));
		layoutParams.setMargins(90,5,20,20);
		editText.setLayoutParams(layoutParams);
		editText.setBackground(fragmentActivity.getResources().getDrawable(R.drawable.edittext_layout_shap_qst));
		editText.setEms(10);
		editText.setGravity(Gravity.TOP);
		int padding = fragmentActivity.getResources().getDimensionPixelSize(R.dimen.fragmentadaptivequestion_edittext_padding);
		editText.setPadding(padding, padding, padding, padding);
		editText.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		editText.addTextChangedListener(mTextEditorWatcher);
		return editText;
		
	}

	public String getReviewDesc() {
	
		String desc = "";
		
		for(int counter=0;counter<QUESTION_KEYS.length;counter++)
		{
			desc += editTexts[counter].getText().toString()+". ";
		}
		
		return desc.trim();
	}
	
	private final TextWatcher mTextEditorWatcher = new TextWatcher() {
	     
        public void afterTextChanged(Editable s) {
        	
        	for(int i = s.length(); i > 0; i--){

                if(s.subSequence(i-1, i).toString().equals("\n"))
                     s.replace(i-1, i, "");

            }

        	String myTextString = s.toString();
        //	reviewDesc.setText(myTextString);
        }

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
	};
}
