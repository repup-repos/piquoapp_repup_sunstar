package com.piquor.fragments;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.ReviewEntryDTO;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.piquortabapp.R;

public class FragmentReviewPostErrorDialog extends DialogFragment implements ACTIVITY_FLAGS {

	
	private FragementChangeListener fragmentChangeListener;
	private String LOG_TAG = "FragmentReviewPostErrorDialog";
	private String error = "";
	private TextView txtErrorDesc ;
	private ReviewEntryDTO reviewEntryDTO;
	
	
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle arg0) {
		super.onActivityCreated(arg0);

		fragmentChangeListener = (MainActivity) getActivity();

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
				getActivity());
		LayoutInflater inflator = getActivity().getLayoutInflater();
		View view = inflator.inflate(R.layout.fragmentreviewpost_errordialog, null);
		alertBuilder.setView(view);
		setCancelable(false);
		alertBuilder.setCancelable(false);

		txtErrorDesc = (TextView) view.findViewById(R.id.txtReviewErrorDialog);	
		txtErrorDesc.setText(error);
		ImageButton postiveButton = (ImageButton) view
				.findViewById(R.id.btnRetryReviewPostErrorDialog);
		postiveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				retryTripAdvisorPosting();
			}
		});
		ImageButton closeButton = (ImageButton) view
				.findViewById(R.id.btnHomeReviewPostErrorDialog);
		closeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				goToHomeScreen();
			}
		});

		Dialog dialog = alertBuilder.create();
		return dialog;
	}

	protected void retryTripAdvisorPosting() {
		
		dismiss();
		
		fragmentChangeListener.navigateToBackScreen();
	}

	protected void goToHomeScreen() {
		

		dismiss();
		
		saveEntryOnDatabase();
		try {
			setMobileDataEnabled(getActivity(), false);
			Log.d(LOG_TAG, "Set mobile data enabled is false");
		} catch (Exception e) {
			Log.d(LOG_TAG,
					"Exception at mobile data enabled: ", e);
		}
		fragmentChangeListener.navigateToReviewMediumSelection();
		
	}
	
	private void setMobileDataEnabled(Context context, boolean enabled)
			throws Exception {

		
		Log.d(LOG_TAG, "SDK Version: " + Build.VERSION.SDK_INT);
		if (Build.VERSION.SDK_INT > 19) {
			
			setMobileDataEnabledFromLollipop(getActivity(),enabled);
			
		} else {

			final ConnectivityManager conman = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class conmanClass = Class
					.forName(conman.getClass().getName());
			final Field iConnectivityManagerField = conmanClass
					.getDeclaredField("mService");
			iConnectivityManagerField.setAccessible(true);
			final Object iConnectivityManager = iConnectivityManagerField
					.get(conman);
			final Class iConnectivityManagerClass = Class
					.forName(iConnectivityManager.getClass().getName());
			final Method setMobileDataEnabledMethod = iConnectivityManagerClass
					.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);

			setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
		}
	}
	private final static String COMMAND_L_ON = "svc data enable\n ";
	private final static String COMMAND_L_OFF = "svc data disable\n ";
	private final static String COMMAND_SU = "su";
	
	private void setMobileDataEnabledFromLollipop(Context context,boolean enable)
	{
		 String command;
		    if(enable)
		        command = COMMAND_L_ON;
		    else
		        command = COMMAND_L_OFF;        

		    try{
		        Process su = Runtime.getRuntime().exec(COMMAND_SU);
		        DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

		        outputStream.writeBytes(command);
		        outputStream.flush();

		        outputStream.writeBytes("exit\n");
		        outputStream.flush();
		        try {
		            su.waitFor();
		        } catch (InterruptedException e) {
		            e.printStackTrace();
		        }

		        outputStream.close();
		    }catch(IOException e){
		        e.printStackTrace();
		    }
	}

	protected void closeReviewDialog() {

		dismiss();

	}

	@Override
	public void onPause() {
		super.onPause();

	}

	
	@Override
	public void onResume() {
		super.onResume();	
	}

	
	public void setErrorDescription(String reviewErrorDesc,ReviewEntryDTO reviewEntryDTO) {
		
		this.error = reviewErrorDesc;
		
		this.reviewEntryDTO = reviewEntryDTO;
		Log.d(LOG_TAG, "Setting Error: "+error);
		
	}
	private int status = 0;

	public void saveEntryOnDatabase() {
		
	
		
		Calendar cal = Calendar.getInstance();
		String DATE_FORMAT_NOW = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String date = sdf.format(cal.getTime());
		
		
		String optionalParameter = "Hotel Optional: "
				+ " CheckIn Rating:"
				+ reviewEntryDTO.getReviewentry_ratingCheckIn()
				+ " Rooms Rating:"
				+ reviewEntryDTO.getReviewentry_ratingRooms()
				+ " Cleanliness:"
				+ reviewEntryDTO.getReviewentry_ratingClieanliness()
				+ " Amneties:"
				+ reviewEntryDTO.getReviewentry_ratingAmneties()
				+ " Food and Beverages:"
				+ reviewEntryDTO.getReviewentry_ratingFoodandBeverages()
				+ " Wifi & Internet:"
				+ reviewEntryDTO.getReviewentry_ratingWifiAndInternet()
				+ " Checkout:"
				+ reviewEntryDTO.getReviewentry_ratingCheckout()
				+ " Checkout:"
				+ reviewEntryDTO.getReviewentry_ratingconceirge();
		reviewEntryDTO.setReviewentry_campaignId(MainActivity
				.getCamapignInformation().getCampaignId().trim());
		reviewEntryDTO.setReviewentry_optionaldesc(optionalParameter);
		reviewEntryDTO.setReviewentry_date(date);
		reviewEntryDTO.setReviewentry_type(PLACE_REVIEW_TYPE_TRIPADVISOR);
		reviewEntryDTO.setReviewentry_status(String.valueOf(status));
		reviewEntryDTO.setReviewentry_timestamp(getTimeStamp());
		//reviewEntryDTO.setReviewentry_emailid(TripAdisorEssentialsDTO.getRevientry_emailid());
		reviewEntryDTO.setReviewentry_platformid("1");
		reviewEntryDTO.setReviewentry_reviewUploaded("0");
		reviewEntryDTO.setReviewentry_reviewuploadStatus("0");
		
		MainActivity.addReviewEntriesInDatabase(reviewEntryDTO);

	}
	
	private String getTimeStamp() {
		Calendar calendar = Calendar.getInstance();
		Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		return currentTimestamp.toString();
	}

}
