package com.piquor.serviceshandler;

import android.util.Log;

import com.piquor.datautility.UserPendingUrlEntryData;
import com.piquor.datautility.UserPhotoUploadErrorData;
import com.piquor.datautility.UserRemainUrlEntryData;
import com.piquor.datautility.UserUrlEntryErrorData;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.PiquorTimestamp;
import com.services.launcher.ServicesLauncher;

public class PiquorServicesHandler {
	
	UserPendingUrlEntryData userPendingUrlEntryData;
	UserPhotoUploadErrorData userPhotoUploadErrorData;
	UserRemainUrlEntryData userReminaingEntryData;
	UserUrlEntryErrorData userUrlEntryErrorData;
	private String LOG_TAG = "PiquorServicesHandler";
	
	public PiquorServicesHandler()
	{
		userPendingUrlEntryData=new UserPendingUrlEntryData();
		userPhotoUploadErrorData=new UserPhotoUploadErrorData();
		userReminaingEntryData=new UserRemainUrlEntryData();
		userUrlEntryErrorData=new UserUrlEntryErrorData();
	}
	
	public void executeServices()
	{
		
		if(isInternetConnected())
		{
			logInfo("Internet Connected Starting Processing @PiquorServiceaHandler.executeServices() Time:"+PiquorTimestamp.getTimeStamp());
			String machineId=MainActivity.getCamapignInformation().getMachineId();
			if(userPendingUrlEntryData.isDataPending())
			{
				logInfo("Pending Email Entries Found @PiquorServiceHandler.executeServices() Time:"+PiquorTimestamp.getTimeStamp());
				userPendingUrlEntryData.processPendingUserData(machineId);
				logInfo("Pending Email Entries Processed @PiquorServiceHandler.executeServices Time:"+PiquorTimestamp.getTimeStamp());
			}else if(userPhotoUploadErrorData.isDataPending())
			{
				logInfo("Photo Upload Error Entries Found @PiquorServiceHanlder.executeServices Time:"+PiquorTimestamp.getTimeStamp());
				userPhotoUploadErrorData.processPendingUserData(machineId);
				logInfo("Photo Upload Error Entries Processed @PiquorServiceHandler.executeServices Time:"+PiquorTimestamp.getTimeStamp());
			}else if(userUrlEntryErrorData.isDataPending())
			{
				logInfo("Url Error Entries Found @PiquorServiceHandler.executeServices Time:"+PiquorTimestamp.getTimeStamp());
				userUrlEntryErrorData.processPendingUserData(machineId);
				logInfo("Url Error Entries Processed @PiquorServiceHandler.executeServices Time:"+PiquorTimestamp.getTimeStamp());
			}else if(userReminaingEntryData.isDataPending())
			{
				logInfo("Remaing User Entries Found @PiquorServiceHandler.executeServices");
				userReminaingEntryData.processPendingUserData(machineId);
				logInfo("Remaing User Entries Processed @PiquorServiceHandler.executeServices");
			}else
			{
				logInfo(" No Photogrpahs Pending @PiquorServiceHandler.execteservices()");
			}
			
		}
		else
		{
			logInfo("Internet Disconnected Executing Scripts:@execteServieces:PiquorServiceHandler");
		 
		}
	}

	

	private boolean isInternetConnected() {
		
		logInfo("Checking Internet Connection isInternetConnected@PiquorSericeHandler");
		return ServicesLauncher.checkInternetConnectivity();
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
}

