package com.piquor.datautility;

import java.util.ArrayList;

import android.util.Log;

import com.piquor.datastaticfields.ErrorFlags;
import com.piquor.dto.UserTableDTO;
import com.piquor.errorhandler.DataErrorHandler;
import com.piquor.piquortabapp.MainActivity;
import com.piquor.utility.PiquorTimestamp;
import com.services.launcher.ServicesLauncher;

public class UserRemainUrlEntryData implements ErrorFlags {
	
	DataErrorHandler errorHandler;
	private String LOG_TAG = "UserRemainUrlEntryData";
	
	public UserRemainUrlEntryData()
	{
		errorHandler=new DataErrorHandler();
	}
	
	boolean pendingData=false;
	
	public boolean isDataPending()
	{
		String dbquery =  "select count(*) from user where mailsent=0 and fileupload=0 and status='0'";
		int pictureUploadFailedEntries = MainActivity
				.getDBEntryCount(dbquery);
		ServicesLauncher.showMessage("Pending photos:"+pictureUploadFailedEntries);
		if(pictureUploadFailedEntries>0)
		{
			
			pendingData=true;
			return true;
			
		}else
		{
			pendingData=false;
			return false;
		}
	}
	public boolean processPendingUserData(String machineId)
	{
		if(pendingData)
		{
			logInfo("Fetching User Remaining Entry Data from database @proccessPendingUserData:UserRemaingingEntryData time:"+PiquorTimestamp.getTimeStamp());
			
			
			ArrayList<UserTableDTO> mailAndUploadEntries = MainActivity
					.getUnsendedMailList(USER_REMAIING_DATA_FLAG);
			
			logInfo("Pending User Remaining Entry Data Fetched from database Started Processing totoal entries:"+mailAndUploadEntries.size()+" @processPendingUserData:UserRemaingingEntryData" );
			
			errorHandler.handleRemainingUserEntryData(mailAndUploadEntries,machineId);
			
			
		
			return true;
		}else
		{
			return	false;
		}
		
	}
	
	public void logInfo(String info)
	{
		Log.d(LOG_TAG, info);
	}
	
}
