package com.piquor.fragments;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.piquor.constants.ACTIVITY_FLAGS;
import com.piquor.dto.TripAdisorEssentialsDTO;
import com.piquor.fragments.cons.FRAGMENT_SCREEN_NAMES;
import com.piquor.fragments.listener.FragementChangeListener;
import com.piquor.piquortabapp.R;
import com.piquor.utility.AlertDialogManager;

public class FragmentTripAdvisorPlaceRating extends Fragment implements FRAGMENT_SCREEN_NAMES,ACTIVITY_FLAGS,OnClickListener {

	private RatingBar overallRatingBar;
	
	
	private String LOG_TAG = "FragmentTripADvisorRestaurantRating";
	private LinearLayout llReasonVisit;
	private String[] visitReasons;
	private String selected_sort_of_visit = "";
	private String where_you_here_for = "";
	private final String FLAG_VISIT_REASON_COUPLES = "Couples";
	private final String FLAG_VISIT_REASON_FAMILTY = "Family";
	private final String FLAG_VISIT_REASON_FRIENDS = "Friends";
	private final String FLAG_VISIT_REASON_SOLO = "Solo";
	private final String FLAG_VISIT_REASON_BUSINESS = "Business";
	private TextView txtCharacterCountShow;
	private int minimumCharacterLimit = 100;
	private String TITLE_ERROR = "Enter Title";
	private String DESCRIPTION_ERROR = "";
	private EditText txtReviewDesc;
	private EditText txtReviewTitle;
	private RadioButton[] rbtnFoodArray;
	
	
	@Override
	public void onCreate(Bundle savedInstance)
	{
		super.onCreate(savedInstance);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		fragmentChangeListener = (FragementChangeListener) getActivity();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflator,ViewGroup parent,Bundle savedInstance)	{
	
		View view = inflator.inflate(R.layout.fragmentstaresturentrating, parent,false);
		
		previousClick = null;
		overallRatingBar = (RatingBar) view.findViewById(R.id.ratingBarOverall);
		LayerDrawable stars = (LayerDrawable) overallRatingBar.getProgressDrawable();
		stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.textColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.starOriginalColor), android.graphics.PorterDuff.Mode.SRC_ATOP);
		
		llReasonVisit = (LinearLayout) view.findViewById(R.id.llReasonVisit);
		RelativeLayout llwereyouherefor = (RelativeLayout) view.findViewById(R.id.llwereyouherefor);
		visitReasons = new String[] {"Couples","Family","Friends","Business","Solo"};
	    txtWereYouHereFor = (TextView) view.findViewById(R.id.txtWereYouHereFor);
		
		boolean isFirst = false;
		
		for(String visitR:visitReasons)
		{
			Button btnVisitReason = new Button(getActivity());
			if(!isFirst)
			{
				btnCouple = btnVisitReason;
				isFirst = true;
			}
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
	                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(20, 2, 0, 0);
			
			btnVisitReason.setLayoutParams(params);
			btnVisitReason.setTag(visitR);
			
			if(visitR.equals(FLAG_VISIT_REASON_COUPLES))
			{
				btnVisitReason.setBackgroundResource(R.drawable.couple_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_FAMILTY))
			{
				btnVisitReason.setBackgroundResource(R.drawable.famil_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_FRIENDS))
			{
				btnVisitReason.setBackgroundResource(R.drawable.friends_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_SOLO))
			{
				btnVisitReason.setBackgroundResource(R.drawable.solo_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_BUSINESS))
			{
				btnVisitReason.setBackgroundResource(R.drawable.business_ns);
			}
			btnVisitReason.setOnClickListener(this);
			llReasonVisit.addView(btnVisitReason);
		}
		
		final List<String> foodSelectionArray = new ArrayList<String>();
		foodSelectionArray.add("Breakfast");
		foodSelectionArray.add("Brunch");
		foodSelectionArray.add("Lunch");
		foodSelectionArray.add("Dinner");
		foodSelectionArray.add("Coffee or tea");
		foodSelectionArray.add("Drinks");
		foodSelectionArray.add("Late night food");
		
		rbtnFoodArray = new RadioButton[foodSelectionArray.size()];
		rgFoodArray = new RadioGroup(getActivity());
		rgFoodArray.setOrientation(RadioGroup.HORIZONTAL);
		int counter = 0;
		
		for(String foodSelection:foodSelectionArray)
		{
		
			RadioButton btnFoodSelection = new RadioButton(getActivity());
			btnFoodSelection.setTag(foodSelection);
			btnFoodSelection.setGravity(Gravity.CENTER);
			btnFoodSelection.setText(foodSelection);
			btnFoodSelection.setButtonDrawable(new StateListDrawable());
			btnFoodSelection.setWidth(90);
			btnFoodSelection.setHeight(50);
			btnFoodSelection.setBackgroundResource(R.drawable.radio_button_style_breakfast);
			rgFoodArray.addView(btnFoodSelection);
			rbtnFoodArray[counter] = btnFoodSelection;
			counter++;
		}
		
		llwereyouherefor.addView(rgFoodArray);
		
		
		
		float ratingValue = 1.0f;
		overallRatingBar.setStepSize(ratingValue);
		ImageButton btnPostOnTripAdvisor = (ImageButton) view.findViewById(R.id.btnfragmentrestaurantrating_posttotripadvisor);
		btnPostOnTripAdvisor.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				replaceContainerToPostReview();
			}
		});
		
		ImageButton btnReviewOptionOnTripAdvisor = (ImageButton) view.findViewById(R.id.btnfragmentrestaurantrating_saymoreentries);
		btnReviewOptionOnTripAdvisor.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				replaceContainerToOptionEntries();
			}
		});
		
		if(TripAdisorEssentialsDTO.getTRIP_ADVISOR_PLACE_TYPE().equals(PLACE_HOTEL_NAME))
		{
			llwereyouherefor.setVisibility(View.GONE);
			txtWereYouHereFor.setVisibility(View.GONE);
		}
		
		txtCharacterCountShow = (TextView) view.findViewById(R.id.txtCharacterShow);
		txtReviewDesc = (EditText) view.findViewById(R.id.txtTripAdvisorDesc);
		
		if(TripAdisorEssentialsDTO.getTRIP_ADVISOR_PLACE_TYPE().equals(PLACE_RESTAURANT_NAME))
		{
			minimumCharacterLimit = 100;
		}else if(TripAdisorEssentialsDTO.getTRIP_ADVISOR_PLACE_TYPE().equals(PLACE_HOTEL_NAME))
		{
			minimumCharacterLimit = 200;
		}
		txtCharacterCountShow.setText("Minimum Character left "+minimumCharacterLimit);
		txtReviewDesc.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if(txtReviewDesc.getText().toString().length() > 0){
                    if(txtReviewDesc.getText().toString().length() < 101)
                    {
                    	txtReviewDesc.setError(DESCRIPTION_ERROR);
                    }
                }
			}
		});
		
		txtReviewDesc.addTextChangedListener(mTextEditorWatcher);
		//txtReviewDesc.setKeyListener(null);
		txtReviewTitle = (EditText) view.findViewById(R.id.txtTripAdvisorTitle);
		txtReviewTitle.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
			
				
				if(txtReviewTitle.getText().toString().trim().length() == 0)
				{
					txtReviewTitle.setError(TITLE_ERROR);
				}
			}
		});
		
		txtReviewDesc.setText(TripAdisorEssentialsDTO.getTRIP_ADVISOR_DESC());
		rbtnFoodArray[0].setChecked(true);
		btnCouple.performClick();
	    overallRatingBar.setRating(1.0f);
	    txtReviewTitle.requestFocus();
		return view;
	}
	

	protected void replaceContainerToOptionEntries() {
		
		boolean response = fillRequiredDataToForms();
		
		if(response)
		{
			if(TripAdisorEssentialsDTO.getTRIP_ADVISOR_PLACE_TYPE().equals(PLACE_RESTAURANT_NAME))
			{
				fragmentChangeListener.navigateToRestaurantReviewOptionalScreen();;
				
			}else if(TripAdisorEssentialsDTO.getTRIP_ADVISOR_PLACE_TYPE().equals(PLACE_HOTEL_NAME))
			{
				fragmentChangeListener.navigateToHotelReviewOptionalScreen();;
			}
		}
	}


	private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           //This sets a textview to the current length
        	
           int leftCharacters = minimumCharacterLimit-s.length();
           
           if(leftCharacters<0)
           {
        	   leftCharacters = 0;
           }
           txtCharacterCountShow.setText("Minmum Character Left:"+leftCharacters);
        }

        public void afterTextChanged(Editable s) {
        }
	};
	
	@Override
    public void onStart() {
        super.onStart();
        
	}
	
	private void showMessage(String message)
	{
		new AlertDialogManager().showAlertDialog(getActivity(), "Info", message, false);

	}
	
	protected void replaceContainerToPostReview() {
		
		
		boolean response = fillRequiredDataToForms();
		if(response)
		{
			FragmentManager fragmentManager = getActivity()
					.getSupportFragmentManager();
			FragmentReviewEmailScreen fragmentEmailScreen = new FragmentReviewEmailScreen();
			fragmentEmailScreen.show(fragmentManager, "EmailREviewDialog");
		}
		Log.d(LOG_TAG, "Opening Fragment Manger dialog");
		
		
		
	}
	
	
	public boolean fillRequiredDataToForms()
	{
		int id = rgFoodArray.getCheckedRadioButtonId();
		if(rgFoodArray.getCheckedRadioButtonId()==-1)
		{
		}else
		{
			for(int counter = 0;counter < rbtnFoodArray.length; counter++)
			if(id == rbtnFoodArray[counter].getId())
			{
				where_you_here_for = rbtnFoodArray[counter].getTag().toString().trim();
				break;
			}
		}
		
		if(overallRatingBar.getRating() == 0)
		{
		    showMessage("Please Select Rating");
		    return false;
		}else if(where_you_here_for.equals("")&&TripAdisorEssentialsDTO.getTRIP_ADVISOR_PLACE_TYPE().equals(PLACE_RESTAURANT_NAME))
		{
			showMessage("Please select were you here for");
			return false;
		}else if(selected_sort_of_visit.equals(""))
		{
			showMessage("Please select which sor of visit it was");
			return false;
		}else
		{
			TripAdisorEssentialsDTO.setTRIP_ADVISOR_OVERALL_RATING(overallRatingBar.getRating());
			TripAdisorEssentialsDTO.setTRIP_SHOR_OF_VISIT(selected_sort_of_visit);
			TripAdisorEssentialsDTO.setTRIP_WERE_YOU_HERE_FOR(where_you_here_for);
			Log.d(LOG_TAG, "Navigating To Trip Advisor text entry");
				Log.d(LOG_TAG, "Selected Place Type:"+TripAdisorEssentialsDTO.getTRIP_ADVISOR_PLACE_TYPE());
			
			String title = txtReviewTitle.getText().toString().trim();
			String description = txtReviewDesc.getText().toString().trim();
			
			if(title.length()==0)
			{
				     new AlertDialogManager().showAlertDialog(getActivity(), "Text Input Error", "Please enter title", false);
				     txtReviewTitle.setError(TITLE_ERROR);
				     return false;
			}else if(description.length()<minimumCharacterLimit)
			{
			     	new AlertDialogManager().showAlertDialog(getActivity(), "Text Input Error", "Please enter minimum "+minimumCharacterLimit+" characters here", false);
			     	txtReviewDesc.setError(DESCRIPTION_ERROR);
			     	return false;
			}else
			{
				TripAdisorEssentialsDTO.setTRIP_ADVISOR_TITILE(title);
				TripAdisorEssentialsDTO.setTRIP_ADVISOR_DESC(description);
			}
			
			return true;
		}
		
	}


	private Button previousClick = null;
	private RadioGroup rgFoodArray;
	private TextView     txtWereYouHereFor;
	private FragementChangeListener fragmentChangeListener;


	private Button btnCouple;
	
	@Override
	public void onClick(View v) {
		
		Button clicked = (Button)v;
		if(previousClick != null)
		{
			String visitR = previousClick.getTag().toString().trim();
			if(visitR.equals(FLAG_VISIT_REASON_COUPLES))
			{
				previousClick.setBackgroundResource(R.drawable.couple_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_FAMILTY))
			{
				previousClick.setBackgroundResource(R.drawable.famil_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_FRIENDS))
			{
				previousClick.setBackgroundResource(R.drawable.friends_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_SOLO))
			{
				previousClick.setBackgroundResource(R.drawable.solo_ns);
			}else if(visitR.equals(FLAG_VISIT_REASON_BUSINESS))
			{
				previousClick.setBackgroundResource(R.drawable.business_ns);
			}
		}
		
		selected_sort_of_visit = clicked.getTag().toString().trim();
		if(selected_sort_of_visit.equals(FLAG_VISIT_REASON_COUPLES))
		{
			clicked.setBackgroundResource(R.drawable.couple_selected);
		}else if(selected_sort_of_visit.equals(FLAG_VISIT_REASON_FAMILTY))
		{
			clicked.setBackgroundResource(R.drawable.family_selected);
		}else if(selected_sort_of_visit.equals(FLAG_VISIT_REASON_FRIENDS))
		{
			clicked.setBackgroundResource(R.drawable.friends_selected);
		}else if(selected_sort_of_visit.equals(FLAG_VISIT_REASON_SOLO))
		{
			clicked.setBackgroundResource(R.drawable.solo_selected);
		}else if(selected_sort_of_visit.equals(FLAG_VISIT_REASON_BUSINESS))
		{
			clicked.setBackgroundResource(R.drawable.business_selected);
		}
		previousClick = clicked;
	}
	
	public void updateEditTextView()
	{
		txtReviewDesc.setText(TripAdisorEssentialsDTO.getTRIP_ADVISOR_DESC().trim());
	}
	
}
