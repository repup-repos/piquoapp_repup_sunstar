package com.piquor.dto;


public class UserTableDTO {
	private int id;
	private String status;
	private String feedback;
	private String frameSelected;
	private String date;
	private String gender;
	private String picId;
	private String name;
	private String email;
	private String mailSent;
	private String fileUpload;
	private String phone;
	private String DelieveryMode;
	private String campaignId;
	private String videoprocessed;
	private String dateofbirth;
	private String photoPermissionSocialMedia;
	private String NOOfPrints;
	private String FbUserName;
	private String fbUserId;
	private String fbName;
	private String twitterId;
	private String twitterName;
	private String twitterFollowers;
	private String twitterFriendsCount;

	
	public UserTableDTO() {
		status = "0";
		feedback = "cancelled";
		frameSelected = "NA";
		date = "NA";
		gender = "NA";
		picId = "NA";
		name = "NA";
		email = "cancel@xyz.com";
		mailSent = "0";
		fileUpload = "0";
		phone = "NA";
		DelieveryMode = "";
		campaignId = "";
		videoprocessed = "0";
		NOOfPrints = "0";
		fbUserId = "NA";
		FbUserName = "NA";
		fbName = "NA";
		twitterId="NA";
		twitterFriendsCount="0";
		twitterFollowers="0";
		twitterName = "NA";
		setPhotoPermissionSocialMedia("2");
		setDateofbirth("0000-00-00");
		setId(0);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getFrameSelected() {
		return frameSelected;
	}

	public void setFrameSelected(String frameSelected) {
		this.frameSelected = frameSelected;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setPicId(String picId) {
		this.picId = picId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMailSent() {
		return mailSent;
	}

	public void setMailSent(String mailSent) {
		this.mailSent = mailSent;
	}

	public String getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(String fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPicId() {
		return picId;
	}

	public String getDelieveryMode() {
		return DelieveryMode;
	}

	public void setDelieveryMode(String delieveryMode) {
		DelieveryMode = delieveryMode;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getVideoprocessed() {
		return videoprocessed;
	}

	public void setVideoprocessed(String videoprocessed) {
		this.videoprocessed = videoprocessed;
	}

	public String getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getPhotoPermissionSocialMedia() {
		return photoPermissionSocialMedia;
	}

	public void setPhotoPermissionSocialMedia(String photoPermissionSocialMedia) {
		this.photoPermissionSocialMedia = photoPermissionSocialMedia;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNOOfPrints() {
		return NOOfPrints;
	}

	public void setNOOfPrints(String nOOfPrints) {
		NOOfPrints = nOOfPrints;
	}
	

	public String getFbUserName() {
		return FbUserName;
	}

	public void setFbUserName(String fbUserName) {
		FbUserName = fbUserName;
	}

	public String getFbUserId() {
		return fbUserId;
	}

	public void setFbUserId(String fbUserId) {
		this.fbUserId = fbUserId;
	}

	public String getFbName() {
		return fbName;
	}

	public void setFbName(String fbName) {
		this.fbName = fbName;
	}
	
	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}

	public String getTwitterName() {
		return twitterName;
	}

	public void setTwitterName(String twitterName) {
		this.twitterName = twitterName;
	}

	public String getTwitterFollowers() {
		return twitterFollowers;
	}

	public void setTwitterFollowers(String twitterFollowers) {
		this.twitterFollowers = twitterFollowers;
	}

	public String getTwitterFriendsCount() {
		return twitterFriendsCount;
	}

	public void setTwitterFriendsCount(String twitterFriendsCount) {
		this.twitterFriendsCount = twitterFriendsCount;
	}


	public String toString()
	{
		return "Id:"+id+":MailSent:"+mailSent+":fileUpload:"+fileUpload+":Email:"+email+":Phone:"
				+phone+":Status:"+status+":SocialMediaPermission"+photoPermissionSocialMedia+
				":Name:"+name+":NumberOfPrints:"+NOOfPrints+":DOB:"+dateofbirth+":Date:"+date
				+":frame_selected:"+frameSelected+":PicId:"+":DileveryMode:"+DelieveryMode
				+":CampaignId:"+campaignId+":Gender:"+gender+"feedbacK:"+feedback+
				":FbName:"+fbName+":FbUserId:"+fbUserId+":FbUserName:"+FbUserName+
				":TwitterId:"+twitterId+":TwitterName:"+twitterName+":TwitterFollower:"+twitterFollowers+
				":TwitterFriends:"+twitterFriendsCount;

	}
}
