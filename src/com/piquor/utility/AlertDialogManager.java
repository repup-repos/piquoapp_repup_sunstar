package com.piquor.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.piquor.piquortabapp.R;

public class AlertDialogManager
{
	public void showAlertDialog(Context context, String title, String message,Boolean status)
	{
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context,AlertDialog.THEME_HOLO_DARK);
		// set title
		alertDialogBuilder.setTitle(title);

		// set dialog message
		alertDialogBuilder.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.cancel();
					}
				});
		alertDialogBuilder.setIcon((status) ? R.drawable.success : R.drawable.fail);
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

		/*AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		if(status != null)
		{
			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		}
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {}
		});
		alertDialog.show();*/
	}
	
}